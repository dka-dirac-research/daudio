# DeviceType

## API

* scanDevices - return true if device list has changed.
* devices - return current device list.
* createDevice - create a device. Throws exception if not found in device list.
* create - create device type
* registerDevicesChangedCallback - register a callback for when devices have changed.

When a device change callback is received, the following shall take place:
1. Call updateDevices
2. Call devices and update the main Device manager list.