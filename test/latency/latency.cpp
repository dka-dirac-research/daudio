
#include <daudio/info.h>
#include <daudio/listing.h>
#include <daudio/session.h>

#include <daudio/waveform.h>

#include <atomic>
#include <cmath>
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <numeric>
#include <regex>
#include <thread>
using namespace std;
using namespace chrono_literals;
using namespace daudio;

#include <log/log.hpp>

#include <CLI/CLI.hpp>

#include <assert.h>

namespace
{
    struct Logger : dirac::log::Logger {
        void log(const dirac::log::LogEvent& logEvent) override
        {
            using namespace dirac::log;
            if (logEvent.m_level >= LogLevel::LogLevel_kERROR)
                cerr << logEvent.toString() << endl;
            else
                cout << logEvent.toString() << endl;
        }
    };

    struct LatencyTest {
        vector<float> stimuli_;
        vector<float> measurement_;
        atomic<size_t> pos_play_{0};
        atomic<size_t> pos_rec_{0};
        atomic<bool> rec_done_{false};

        LatencyTest()
        {
            const unsigned period_length = 37;
            const unsigned periods       = 10;
            const float factor           = 2.0f * 3.141592654f / period_length;
            vector<float> one_period;
            one_period.reserve(period_length);
            stimuli_.reserve(period_length * periods);

            for (unsigned i = 0; i < period_length; ++i) {
                float v = 0.5f * sinf(i * factor);
                one_period.push_back(v);
            }
            for (unsigned i = 0; i < periods; ++i) {
                copy(one_period.begin(), one_period.end(), back_inserter(stimuli_));
            }

            measurement_.resize(8192);
        }

        bool isRecDone() const { return !!rec_done_; }

        unsigned getLatencyInSamples()
        {
            if (!isRecDone())
                throw runtime_error("record not done yet!");

#if 1
            vector<float> correlation;
            for (unsigned i = 0; i < measurement_.size() - stimuli_.size(); ++i) {
                float sum = 0.f;
                for (unsigned j = 0; j < stimuli_.size(); ++j)
                    sum += stimuli_[j] * measurement_[i + j];
                correlation.push_back(abs(sum));
            }

            auto it = max_element(correlation.begin(), correlation.end());

            pos_rec_  = 0;
            pos_play_ = 0;
            rec_done_ = false;

            return unsigned(it - correlation.begin());
#else
            pos_rec_  = 0;
            pos_play_ = 0;
            fill(measurement_.begin(), measurement_.end(), 0.f);
            rec_done_ = false;
            return 0;
#endif
        }

        void recPlayCallback(const daudio::InputBufferView& inView,
                             daudio::OutputBufferView& outView)
        {
            if (rec_done_)
                return;

            // Record input (first channel only)
            if (pos_rec_ < measurement_.size()) {
                auto frames = inView.bufferSize();
                auto offset = size_t(0);
                while (frames > 0) {
                    auto frames_now = min(frames, measurement_.size() - pos_rec_);
                    if (!frames_now)
                        break;

                    copy_n(inView.channel[0].begin() + offset,
                           frames_now,
                           measurement_.data() + pos_rec_);
                    pos_rec_ += frames_now;
                    offset += frames_now;
                    frames -= frames_now;
                }
            } else
                rec_done_ = true;

            // Play on all channels
            if (pos_play_ < stimuli_.size()) {
                auto frames = outView.bufferSize();
                auto offset = size_t(0);
                while (frames > 0) {
                    auto frames_now = min(frames, stimuli_.size() - pos_play_);
                    if (!frames_now)
                        break;

                    for (auto& channel : outView.channel)
                        copy_n(stimuli_.data() + pos_play_, frames_now, channel.begin() + offset);

                    pos_play_ += frames_now;
                    offset += frames_now;
                    frames -= frames_now;
                }
                if (frames > 0) {
                    for (auto& channel : outView.channel)
                        fill_n(channel.begin() + offset, frames, 0.f);
                }
                outView.is_silent = false;
            }
        }
    };

    template <typename Iterator>
    double mean(Iterator first, Iterator last)
    {
        auto n = distance(first, last);
        if (n == 0)
            return 0.f;
        return accumulate(first, last, 0.0) / n;
    }

    template <typename Iterator>
    double variance(Iterator first, Iterator last)
    {
        return accumulate(first, last, 0.0, [m = mean(first, last)](auto a, auto b) {
            return a + (b - m) * (b - m);
        });
    }

    template <typename Iterator>
    double stddev(Iterator first, Iterator last)
    {
        return sqrt(variance(first, last));
    }
}  // namespace

void setSignalHandler();

[[noreturn]] void die(const string& message, int code)
{
    cerr << message << endl;
    std::exit(code);
}

static bool run_loop_{true};

int main(int argc, const char* argv[])
{
    Logger the_logger;

    try {
        CLI::App app{"latency tester"};

        float buffer_time_ms = 20.0f;
        int buffer_frames    = 0;
        std::string dev_name;
        std::string log_level = "INFO";

        auto bt =
            app.add_option("-b, --buffer-time", buffer_time_ms, "Buffer time in milliseconds");
        auto bf = app.add_option("-f, --buffer-frames", buffer_frames, "Buffer size in frames")
                      ->excludes(bt);
        app.add_option("-d, --device", dev_name, "Device name")->required();
        app.add_set("-l,--log-level",
                    log_level,
                    {"TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL"},
                    "Log level. Default is INFO.");
        CLI11_PARSE(app, argc, argv);

        dirac::log::setLogLevelFromString(log_level);
        LOG_INFO("Start");

        setSignalHandler();

        condition_variable cv_update;
        mutex cv_mutex;
        bool device_list_changed{false};

        auto cb_token = registerDevicesChangedCallback([&] {
            lock_guard<mutex> lock{cv_mutex};
            device_list_changed = true;
            cv_update.notify_one();
        });

        const auto all = allDevices();

        atomic<int> retry_counter{0};

        while (run_loop_) {
            device_list_changed = false;

            const auto devices = synchronousDevices();

            regex r(dev_name, regex::icase | regex::extended);
            const auto dev = find_if(devices.begin(), devices.end(), [&](const auto& d) {
                return regex_search(d.name, r);
            });

            if (dev == devices.end()) {
                retry_counter = 0;
                cerr << "Device regex '" << dev_name << "' not found..." << endl;

                while (run_loop_) {
                    auto lock = unique_lock<mutex>{cv_mutex};
                    if (cv_update.wait_for(lock, 100ms, [&] { return !!device_list_changed; }))
                        break;
                }
            } else if (++retry_counter > 5) {
                retry_counter = 0;
                cerr << "Number of retries exhausted..." << endl;
                while (run_loop_) {
                    auto lock = unique_lock<mutex>{cv_mutex};
                    if (cv_update.wait_for(lock, 100ms, [&] { return !!device_list_changed; }))
                        break;
                }
            } else {
                try {
                    auto sampleRateToUse = defaultSamplerate(*dev);

                    // Select default buffer size if buf time option not set
                    auto bufSize = defaultBuffersize(*dev);
                    if (!bt->empty()) {
                        auto bufs = supportedBufferSizes(*dev);
                        bufSize   = bufs[0];
                        for (auto size : bufs) {
                            if (size > int(sampleRateToUse * buffer_time_ms / 1000.0f))
                                break;
                            bufSize = size;
                        }
                    } else if (!bf->empty()) {
                        bufSize = buffer_frames;
                    }

                    auto no_of_inputs  = (*dev).inputchannels;
                    auto no_of_outputs = (*dev).outputchannels;
                    if (no_of_inputs == 0 && no_of_outputs == 0) {
                        retry_counter = 5;
                        throw runtime_error("no input nor output selected!");
                    }

                    auto session = open(*dev,
                                        sampleRateToUse,
                                        bufSize,
                                        daudio::ActiveChannels(no_of_inputs),
                                        daudio::ActiveChannels(no_of_outputs));

                    cout << "Output: '" << (*dev).name << "', @" << currentSamplerate(*session)
                         << "/" << currentBuffersize(*session) << endl;

                    LatencyTest latencyTest;

                    // Let audio settle for 2 seconds before starting measurement
                    this_thread::sleep_for(chrono::seconds(2));

                    auto token = registerCallback(*session,
                                                  [&](const daudio::InputBufferView& inView,
                                                      daudio::OutputBufferView& outView) {
                                                      latencyTest.recPlayCallback(inView, outView);
                                                  });

                    //
                    vector<unsigned> latencies;
                    while (run_loop_) {
                        // Will throw an exception if terminated
                        daudio::waitForTermination(*session, 50);

                        if (latencyTest.isRecDone()) {
                            auto frames = latencyTest.getLatencyInSamples();
                            latencies.push_back(frames);
                            if (latencies.size() >= 10) {
                                break;
                            }
                        }
                    }

                    auto m  = mean(latencies.begin(), latencies.end());
                    auto s  = stddev(latencies.begin(), latencies.end());
                    auto ms = 1000 * m / currentSamplerate(*session);
                    cout << "Latency: mean = " << ms << " ms (" << m << "), stddev = " << s
                         << " frames" << endl;
                    if (s > 1.0) {
                        cerr << "Std dev too large (> 1 frame)" << endl;
                        return 1;
                    }
                    return 0;
                } catch (exception& e) {
                    cerr << e.what() << endl;
                    this_thread::sleep_for(500ms);
                }
            }
        }
        cout << endl << "Test loop terminated" << endl;
    } catch (exception& e) {
        cerr << e.what() << endl;
        return 1;
    }

    return 0;
}

#ifdef _WIN32

#include <objbase.h>
#include <windows.h>

BOOL WINAPI HandlerRoutine(_In_ DWORD dwCtrlType)
{
    switch (dwCtrlType) {
    case CTRL_C_EVENT:
        run_loop_ = false;
        return TRUE;
    default:
        // Pass signal on to the next handler
        return FALSE;
    }
}

void setSignalHandler() { SetConsoleCtrlHandler(HandlerRoutine, TRUE); }

#else
#include <signal.h>

void intHandler(int)
{
    //
    run_loop_ = false;
}

void setSignalHandler()
{
    signal(SIGINT, intHandler);
    signal(SIGSTOP, intHandler);
}

#endif
