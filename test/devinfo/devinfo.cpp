#include <iostream>
#include <regex>

#include <daudio/info.h>
#include <daudio/listing.h>
#include <daudio/session.h>

using namespace std;

void printInfo(const daudio::AudioDevice& dev)
{
    cout << "Devicename: " << dev.name << "\n"
         << "Devicetype: " << dev.typeName() << "\n"
         << "Inputs: " << dev.inputchannels << "\n"
         << "Outputs: " << dev.outputchannels << endl;

    cout << "Input channel names:" << endl;

    for (auto name : daudio::inputChannelNames(dev))
        cout << "\t" << name << endl;

    cout << "Output channel names:" << endl;
    for (auto name : daudio::outputChannelNames(dev))
        cout << "\t" << name << endl;

    cout << "Supported samplerates: ";
    for (auto fs : daudio::supportedSamplerates(dev))
        cout << fs << "Hz ";

    cout << endl << endl;
}

int main(int argc, char* argv[])
{
    if (argc != 2) {
        cerr << "Usage: " << argv[0] << " <devicename regex>" << endl;
        return 1;
    }

    regex nameregex(argv[1], regex::icase | regex::extended);

    for (auto device : daudio::allDevices())
        if (regex_search(device.name, nameregex))
            printInfo(device);

    return 0;
}
