
#include <daudio/info.h>
#include <daudio/listing.h>
#include <daudio/session.h>
#include <daudio/waveform.h>

#include <atomic>
#include <cmath>
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <regex>
#include <thread>
using namespace std;
using namespace chrono_literals;
using namespace daudio;

#include <assert.h>

#include <CLI/CLI.hpp>
#include <log/log.hpp>

namespace
{
    struct Logger : dirac::log::Logger {
        void log(const dirac::log::LogEvent& logEvent) override
        {
            using namespace dirac::log;
            if (logEvent.m_level >= LogLevel::LogLevel_kERROR)
                cerr << logEvent.toString() << endl;
            else
                cout << logEvent.toString() << endl;
        }
    };

    namespace little_endian_io
    {
        template <typename Word>
        std::ostream& write_word(std::ostream& outs, Word value, unsigned size = sizeof(Word))
        {
            for (; size; --size, value >>= 8)
                outs.put(static_cast<char>(value & 0xFF));
            return outs;
        }
    }  // namespace little_endian_io
    using namespace little_endian_io;
    struct wav_writer {
        ofstream f_;
        size_t data_chunk_pos_{0};

        vector<daudio::Waveforms> queue_;
        atomic<unsigned> write_ptr_{0};
        atomic<unsigned> read_ptr_{0};
        static const unsigned mask_ = (1 << 6) - 1;

        thread write_thread_;
        atomic<bool> stop_thread_{false};

        wav_writer(const string& name, int fs, int channels, int buffer_size)
            : f_(name, ios::binary)
        {
            if (!f_.is_open())
                throw runtime_error("failed to open '" + name + "' for writing");

            // Write the file headers
            f_ << "RIFF----WAVEfmt ";              // (chunk size to be filled in later)
            write_word(f_, 16, 4);                 // no extension data
            write_word(f_, 1, 2);                  // PCM - integer samples
            write_word(f_, channels, 2);           // number of channels
            write_word(f_, fs, 4);                 // samples per second (Hz)
            write_word(f_, fs * channels * 2, 4);  // (Sample Rate * BitsPerSample * Channels) / 8
            write_word(f_, channels * 2, 2);  // data block size (size of two integer samples, one
                                              // for each channel, in bytes)
            write_word(f_, 16, 2);            // number of bits per sample (use a multiple of 8)

            // Write the data chunk header
            data_chunk_pos_ = f_.tellp();
            f_ << "data----";  // (chunk size to be filled in later)

            queue_.resize(mask_ + 1);
            for (auto& buf : queue_)
                buf.resize(channels, buffer_size);

            thread t([this] {
                while (!stop_thread_) {
                    handleQueue();
                    this_thread::sleep_for(chrono::milliseconds(20));
                }
            });
            write_thread_.swap(t);
        }
        ~wav_writer()
        {
            stop_thread_ = true;
            write_thread_.join();
            handleQueue();

            // (We'll need the final file size to fix the chunk sizes above)
            size_t file_length = f_.tellp();

            // Fix the data chunk header to contain the data size
            f_.seekp(data_chunk_pos_ + 4);
            write_word(f_, file_length - data_chunk_pos_ + 8);

            // Fix the file header to contain the proper RIFF chunk size, which is (file size - 8)
            // bytes
            f_.seekp(0 + 4);
            write_word(f_, file_length - 8, 4);
        }

        void handleQueue()
        {
            auto convertToInt = [](float value) -> short {
                constexpr double max_amplitude = (double)0x7fff;
                double r                       = value * max_amplitude;
                return (short)(r < -max_amplitude ? -max_amplitude
                                                  : (r > max_amplitude ? max_amplitude : r));
            };
            while (read_ptr_ != write_ptr_) {
                const auto& buf = queue_[read_ptr_++ & mask_];
                // Write interleaved
                for (unsigned i = 0; i < buf.waveformSize(); ++i) {
                    for (const auto& channel : buf.waveforms) {
                        write_word(f_, convertToInt(channel[i]), 2);
                    }
                }
            }
        }

        void push(const daudio::InputBufferView& in_view)
        {
            auto& buf = queue_[write_ptr_++ & mask_];
            assert((write_ptr_ - read_ptr_) < mask_);
            for (unsigned i = 0; i < in_view.channel.size(); ++i)
                copy_n(in_view.channel[i].begin(), in_view.bufferSize(), buf.waveforms[i].data());
        }
    };
}  // namespace

void setSignalHandler();

[[noreturn]] void die(const string& message, int code)
{
    cerr << message << endl;
    std::exit(code);
}

daudio::AudioCallback makePeakLevelCallback(atomic<float>* current_peak_level)
{
    auto cb = [=](const daudio::InputBufferView& inView, daudio::OutputBufferView&) mutable {
        float maxLevel = *current_peak_level;
        for (unsigned i = 0; i < inView.channel.size(); ++i) {
            auto& channel = inView.channel[i];
            auto mm       = minmax_element(channel.begin(), channel.end());
            maxLevel      = max({maxLevel, fabsf(*mm.first), *mm.second});
        }
        *current_peak_level = maxLevel;
    };
    return cb;
}

daudio::AudioCallback makeClippingDetectorCallback(atomic<bool>* is_clipping,
                                                   unsigned update_interval_frames)
{
    vector<unsigned> histogram(20);
    size_t histogram_cntr = 0;
    auto cb = [=](const daudio::InputBufferView& inView, daudio::OutputBufferView&) mutable {
        for (auto& channel : inView.channel) {
            for (unsigned i = 0; i < inView.bufferSize(); ++i) {
                auto h_idx =
                    std::min(size_t(abs(channel[i]) * histogram.size()), histogram.size() - 1);
                histogram[h_idx]++;
            }
        }
        histogram_cntr += inView.bufferSize();
        if (histogram_cntr >= update_interval_frames) {
            histogram_cntr = 0;
            *is_clipping   = histogram.back() > histogram.front();
            fill(histogram.begin(), histogram.end(), 0);
        }
    };
    return cb;
}

daudio::AudioCallback makeRecordCallback(const string& name, const daudio::AudioSession& s)
{
    auto wav = make_shared<wav_writer>(
        name, (int)currentSamplerate(s), (int)currentInputChannels(s), (int)currentBuffersize(s));
    auto cb = [wav](const daudio::InputBufferView& in_view, daudio::OutputBufferView&) {
        wav->push(in_view);
    };
    return cb;
}

daudio::AudioCallback makeRenderCallback(unsigned outChannels,
                                         double base_frequency,
                                         double sample_rate)
{
    vector<double> accumulator(outChannels, 0.0);
    vector<double> incr(outChannels);

    for (unsigned i = 0; i < outChannels; ++i) {
        auto freq = base_frequency * pow(2.0, double(i) / outChannels);
        incr[i]   = 2 * 3.141592654 * freq / sample_rate;
    }

    auto cb = [=](const daudio::InputBufferView& inView,
                  daudio::OutputBufferView& outView) mutable {
        for (unsigned i = 0; i < outChannels; ++i) {
            generate(outView.channel[i].begin(), outView.channel[i].end(), [&] {
                return float(0.5 * sin(accumulator[i] += incr[i]));
            });
        }
        for (unsigned i = 0; i < inView.channel.size(); ++i) {
            for (unsigned j = 0; j < inView.bufferSize(); ++j) {
                outView.channel[i][j] += inView.channel[i][j];
            }
        }
    };

    return cb;
}

daudio::AudioCallback makeCopyCallback()
{
    auto cb = [=](const daudio::InputBufferView& inView,
                  daudio::OutputBufferView& outView) mutable {
        auto channels = min(inView.channel.size(), outView.channel.size());
        auto frames   = inView.bufferSize();
        for (unsigned i = 0; i < channels; ++i)
            copy_n(inView.channel[i].begin(), frames, outView.channel[i].begin());
        outView.is_silent = inView.is_silent;
    };

    return cb;
}

daudio::AudioCallback makeMeasurementCallback(
    vector<chrono::high_resolution_clock::time_point>* time_points,
    atomic<unsigned>* pos)
{
    auto cb = [=](const daudio::InputBufferView&, daudio::OutputBufferView&) {
        if (*pos < time_points->size()) {
            (*time_points)[*pos] = chrono::high_resolution_clock::now();
            (*pos)++;
        }
    };
    return cb;
}

void printInfo(const daudio::AudioDevice& dev)
{
    cout << (dev.is_default ? "* " : "  ");
    cout << dev.typeName() << ": '" << dev.name << "'" << endl;
    cout << "   channels : " << dev.inputchannels << "/" << dev.outputchannels << endl;
    cout << "   rates    : [";
    auto def_rate = daudio::defaultSamplerate(dev);
    auto rates    = daudio::supportedSamplerates(dev);
    for (unsigned i = 0; i < rates.size(); ++i) {
        if (i != 0)
            cout << ',';
        if (rates[i] == def_rate && rates.size() > 1)
            cout << '*';
        cout << rates[i];
    }
    cout << "]" << endl;
    auto def_buf_size = daudio::defaultBuffersize(dev);
    auto sizes        = daudio::supportedBufferSizes(dev);
    cout << "   bufsizes : [";
    for (unsigned i = 0; i < sizes.size(); ++i) {
        if (i != 0)
            cout << ',';
        if (sizes[i] == def_buf_size && sizes.size() > 1)
            cout << '*';
        cout << sizes[i];
    }
    cout << "]" << endl;
}

static void listDevices()
{
    cout << "Device list (* = default) -------------------------------" << endl;
    for (const auto& dev : daudio::allDevices())
        printInfo(dev);
}

static bool run_loop_{true};

int main(int argc, const char* argv[])
{
    Logger the_logger;

    try {
        CLI::App app{"Sines generator"};

        float buffer_time_ms = 20.0f;
        int buffer_frames    = 0;
        std::string rec_file_name;
        std::string dev_name;
        std::string log_level = "INFO";

        app.add_option("-d, --device", dev_name, "Device name");
        auto devsFlag       = app.add_flag("--devs", "List the available devices");
        auto opt_in         = app.add_flag("-i,--input", "Use inputs");
        auto opt_out        = app.add_flag("-o,--output", "Use outputs");
        auto opt_vu         = app.add_flag("--vu-meter", "Enables ASCII vu-meter");
        auto opt_measure    = app.add_flag("-m,--measure", "Measures callback timing");
        auto opt_gen_output = app.add_flag("-g,--generate", "Forces generator output");
        auto opt_bt =
            app.add_option("-b, --buffer-time", buffer_time_ms, "Buffer time in milliseconds")
                ->type_name("milliseconds");
        auto opt_bf = app.add_option("-f, --buffer-frames", buffer_frames, "Buffer size in frames")
                          ->excludes(opt_bt)
                          ->type_name("frames");
        app.add_set("-l,--log-level",
                    log_level,
                    {"TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL"},
                    "Log level. Default is INFO.");
        app.add_option("-r,--record", rec_file_name, "Records input (if any) to file")
            ->type_name("filename");
        CLI11_PARSE(app, argc, argv);

        dirac::log::setLogLevelFromString(log_level);
        LOG_INFO("Start");

        if (*devsFlag) {
            listDevices();
            throw 0;
        }

        setSignalHandler();

        condition_variable cv_update;
        mutex cv_mutex;
        bool device_list_changed{false};

        auto cb_token = registerDevicesChangedCallback([&] {
            //listDevices();
            lock_guard<mutex> lock{cv_mutex};
            device_list_changed = true;
            cv_update.notify_one();
        });

        const auto all = allDevices();

        atomic<int> retry_counter{0};

        while (run_loop_) {
            device_list_changed = false;

            const auto devices = allDevices();

            regex r(dev_name, regex::icase | regex::extended);
            const auto dev = find_if(devices.begin(), devices.end(), [&](const auto& d) {
                bool isok = false;
                if (*opt_in && *opt_out) {
                    isok = d.inputchannels > 0 && d.outputchannels > 0;
                } else if (*opt_in) {
                    isok = d.inputchannels > 0;
                } else if (*opt_out) {
                    isok = d.outputchannels > 0;
                }
                if (isok) {
                    if (dev_name.empty()) {
                        if (d.is_default) {
                            return true;
                        }
                    } else if (regex_search(d.name, r)) {
                        return true;
                    }
                }
                return false;
            });

            if (dev == devices.end()) {
                retry_counter = 0;
                cout << "Device regex '" << dev_name << "' not found..." << endl;

                while (run_loop_) {
                    auto lock = unique_lock<mutex>{cv_mutex};
                    if (cv_update.wait_for(lock, 100ms, [&] { return !!device_list_changed; }))
                        break;
                }
            } else if (++retry_counter > 5) {
                retry_counter = 0;
                cout << "Number of retries exhausted..." << endl;
                while (run_loop_) {
                    auto lock = unique_lock<mutex>{cv_mutex};
                    if (cv_update.wait_for(lock, 100ms, [&] { return !!device_list_changed; }))
                        break;
                }
            } else {
                try {
                    // Check key/value pair
                    cout << "Key/value map for device " << (*dev).name << ":" << endl;
                    auto keyValueMap = getKeyValuePairs(*dev);
                    for (auto entry : keyValueMap) {
                        cout << entry.first << " = " << entry.second << endl;
                    }

                    auto sampleRateToUse = defaultSamplerate(*dev);

                    // Select default buffer size if buf time option not set
                    auto bufSize = defaultBuffersize(*dev);
                    if (*opt_bt) {
                        auto bufs = supportedBufferSizes(*dev);
                        bufSize   = bufs[0];
                        for (auto size : bufs) {
                            if (size > int(sampleRateToUse * buffer_time_ms / 1000.0f))
                                break;
                            bufSize = size;
                        }
                    }
                    if (*opt_bf) {
                        bufSize = buffer_frames;
                    }

                    auto no_of_inputs  = (*opt_in) ? (*dev).inputchannels : 0;
                    auto no_of_outputs = (*opt_out) ? (*dev).outputchannels : 0;
                    if (no_of_inputs == 0 && no_of_outputs == 0) {
                        retry_counter = 5;
                        throw runtime_error("no input nor output selected!");
                    }

                    cout << "Opening device '" << dev->name << "' @" << sampleRateToUse << "/"
                         << bufSize << "..." << endl;

                    auto session = open(*dev,
                                        sampleRateToUse,
                                        bufSize,
                                        daudio::ActiveChannels(no_of_inputs),
                                        daudio::ActiveChannels(no_of_outputs));
                    cout << "Streaming on device '" << dev->name << "' @"
                         << currentSamplerate(*session) << "/" << currentBuffersize(*session)
                         << ", " << (*dev).inputchannels << "/" << (*dev).outputchannels << endl;

                    atomic<float> peak_level{0.f};
                    atomic<bool> is_clipping{false};
                    bool was_clipping{false};

                    unique_ptr<daudio::CallbackToken> cb_capture_token_peak;
                    unique_ptr<daudio::CallbackToken> cb_capture_token_clip;
                    unique_ptr<daudio::CallbackToken> cb_rec_token;
                    unique_ptr<daudio::CallbackToken> cb_render_token;
                    if (currentInputChannels(*session) > 0) {
                        cb_capture_token_peak =
                            registerCallback(*session, makePeakLevelCallback(&peak_level));
                        cb_capture_token_clip = registerCallback(
                            *session, makeClippingDetectorCallback(&is_clipping, 1024));

                        if (!rec_file_name.empty())
                            cb_rec_token = registerCallback(
                                *session, makeRecordCallback(rec_file_name, *session));
                    }

                    if (currentOutputChannels(*session) > 0) {
                        auto cb_render_copy = makeCopyCallback();
                        auto cb_render_sine =
                            makeRenderCallback(unsigned(currentOutputChannels(*session)),
                                               440.0,
                                               currentSamplerate(*session));

                        const bool full_duplex = currentInputChannels(*session) > 0 &&
                                                 currentOutputChannels(*session) > 0;

                        cb_render_token = registerCallback(
                            *session,
                            full_duplex && !(*opt_gen_output) ? cb_render_copy : cb_render_sine);
                    }

                    vector<chrono::high_resolution_clock::time_point> time_points(
                        unsigned(currentSamplerate(*session) / currentBuffersize(*session)));
                    atomic<unsigned> time_rec_pos{0};
                    unique_ptr<daudio::CallbackToken> cb_measure_token;
                    if (*opt_measure) {
                        cb_measure_token = registerCallback(
                            *session, makeMeasurementCallback(&time_points, &time_rec_pos));
                    }

                    const int vu_width = currentInputChannels(*session) > 0 && (*opt_vu) ? 80 : 0;
                    const float vu_db_range  = 40.0f;
                    const int vu_minus_3db   = int(vu_width * (vu_db_range - 3.f) / vu_db_range);
                    int peak                 = 0;
                    unsigned min_diff_global = numeric_limits<unsigned>::max();
                    unsigned max_diff_global = 0;

                    while (run_loop_) {
                        // Will throw an exception if terminated
                        daudio::waitForTermination(*session, 50);

                        if (time_rec_pos >= time_points.size()) {
                            unsigned min_diff_local = numeric_limits<unsigned>::max();
                            unsigned max_diff_local = 0;
                            uint64_t mean_diff      = 0;
                            for (unsigned i = 1; i < time_points.size(); ++i) {
                                auto diff = unsigned(chrono::duration_cast<chrono::microseconds>(
                                                         time_points[i] - time_points[i - 1])
                                                         .count());
                                mean_diff += diff;
                                min_diff_local = min(diff, min_diff_local);
                                max_diff_local = max(diff, max_diff_local);
                            }
                            mean_diff = (mean_diff + (time_points.size() - 1) / 2) /
                                        (time_points.size() - 1);
                            min_diff_global = min(min_diff_local, min_diff_global);
                            max_diff_global = max(max_diff_local, max_diff_global);
                            double std_dev  = 0.0;
                            for (unsigned i = 1; i < time_points.size(); ++i) {
                                auto diff = chrono::duration_cast<chrono::microseconds>(
                                                time_points[i] - time_points[i - 1])
                                                .count();
                                auto v = (diff - mean_diff);
                                std_dev += v * v;
                            }
                            std_dev = sqrt(std_dev / (time_points.size() - 1));
                            if (vu_width > 0) {
                                cout << string(vu_width + 2, ' ') << '\r';
                            }
                            cout << "min, mean, max, stddev: " << min_diff_local << ", "
                                 << mean_diff << ", " << max_diff_local << ", " << std_dev << endl;
                            time_rec_pos = 0;
                        }

                        if (vu_width > 0) {
                            const auto peak_db = 20.0f * log10(peak_level);
                            int level          = max(0, int((peak_db + 40.0f) / 40.0f * vu_width));
                            if (level > peak)
                                peak = level;
                            for (int i = 0; i < vu_width; ++i) {
                                cout << (i == peak
                                             ? '|'
                                             : (i <= level ? '|' : (i < vu_minus_3db ? '_' : '.')));
                            }
                            cout << (is_clipping ? '+' : ' ') << '\r';
                            peak_level = 0;
                            if (peak > 0)
                                --peak;
                        } else {
                            if (is_clipping) {
                                if (!was_clipping) {
                                    was_clipping = true;
                                    cout << "Clipping detected!" << endl;
                                }
                            } else
                                was_clipping = false;
                        }
                        cout.flush();
                    }
                    cout << endl;

                } catch (exception& e) {
                    cerr << e.what() << endl;
                    this_thread::sleep_for(500ms);
                }
            }
        }
        cout << endl << "Play loop terminated" << endl;
    } catch (int exitCode) {
        return exitCode;
    } catch (exception& e) {
        cerr << e.what() << endl;
        return 1;
    }

    return 0;
}

#ifdef _WIN32

#include <objbase.h>
#include <windows.h>

BOOL WINAPI HandlerRoutine(_In_ DWORD dwCtrlType)
{
    switch (dwCtrlType) {
    case CTRL_C_EVENT:
        run_loop_ = false;
        return TRUE;
    default:
        // Pass signal on to the next handler
        return FALSE;
    }
}

void setSignalHandler() { SetConsoleCtrlHandler(HandlerRoutine, TRUE); }

#else
#include <signal.h>

void intHandler(int)
{
    //
    run_loop_ = false;
}

void setSignalHandler()
{
    signal(SIGINT, intHandler);
    signal(SIGSTOP, intHandler);
}

#endif
