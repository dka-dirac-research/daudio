
#include <daudio/listing.h>
#include <daudio/volume.h>

#include <atomic>
#include <cmath>
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <map>
#include <mutex>
#include <numeric>
#include <regex>
#include <thread>
using namespace std;
using namespace chrono_literals;
using namespace daudio;

#include <log/log.hpp>

#include <CLI/CLI.hpp>

#include <assert.h>

#ifdef WIN32
#include <conio.h>
#endif

namespace
{
    struct Logger : dirac::log::Logger {
        void log(const dirac::log::LogEvent& logEvent) override
        {
            using namespace dirac::log;
            if (logEvent.m_level >= LogLevel::LogLevel_kERROR)
                cerr << logEvent.toString() << endl;
            else
                cout << logEvent.toString() << endl;
        }
    };
}  // namespace

void printInfo(const daudio::AudioDevice& dev)
{
    cout << (dev.is_default ? "* " : "  ");
    cout << dev.typeName() << ": '" << dev.name << "'" << endl;
    cout << "   channels : " << dev.inputchannels << "/" << dev.outputchannels << endl;
}

void setSignalHandler();

[[noreturn]] void die(const string& message, int code)
{
    cerr << message << endl;
    std::exit(code);
}

static bool run_loop_{true};

int main(int argc, const char* argv[])
{
    Logger the_logger;

    try {
        CLI::App app{"Volume monitor"};
        std::string dev_name;
        std::string log_level = "INFO";

        app.add_option("-d, --device", dev_name, "Device name")->required();
        app.add_set("-l,--log-level",
                    log_level,
                    {"TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL"},
                    "Log level. Default is INFO.");
        CLI11_PARSE(app, argc, argv);

        dirac::log::setLogLevelFromString(log_level);
        LOG_INFO("Start");

        setSignalHandler();

        const auto devices = allDevices();

        regex r(dev_name, regex::icase | regex::extended);
        const auto dev = find_if(
            devices.begin(), devices.end(), [&](const auto& d) { return regex_search(d.name, r); });

        if (dev == devices.end()) {
            cerr << "Device not found, list is: " << endl;
            for (const auto& dev : devices)
                printInfo(dev);
            exit(1);
        }

        condition_variable cv_changed;
        mutex mtx_changed;
        bool changed{true};
        float inputGain    = 0.f;
        float outputVolume = 0.f;
        bool inputMute{false};
        bool outputMute{false};

        unique_ptr<daudio::CallbackToken> cb_input;
        if (daudio::hasInputGainControl(*dev)) {
            inputGain = daudio::getInputGain(*dev);
            inputMute = daudio::getInputMuted(*dev);
            cb_input  = daudio::registerInputGainChangeCallback(
                *dev, [&](float value, bool muted, bool /*internal_changed*/) {
                    lock_guard<mutex> lock(mtx_changed);
                    inputGain = value;
                    inputMute = muted;
                    changed   = true;
                    cv_changed.notify_all();
                });
        }

        unique_ptr<daudio::CallbackToken> cb_output;
        if (daudio::hasOutputVolumeControl(*dev)) {
            outputVolume = daudio::getOutputVolume(*dev);
            outputMute   = daudio::getOutputMuted(*dev);
            cb_output    = daudio::registerOutputVolumeChangeCallback(
                *dev, [&](float value, bool muted, bool /*internal_changed*/) {
                    lock_guard<mutex> lock(mtx_changed);
                    outputVolume = value;
                    outputMute   = muted;
                    changed      = true;
                    cv_changed.notify_all();
                });
        }

        cout << "Monitoring " << (*dev).name << ":" << endl;

#ifdef WIN32
        cout << "Use arrow keys and I/O for volume and mute control" << endl;
#endif

        const int kWidth = 40;

        cout << "Input" << string(kWidth, ' ') << "Output" << endl;

        cv_changed.notify_one();

        auto fn_change_gain = [&](float v) {
            float gain = daudio::getInputGain(*dev);
            gain       = max(0.f, min(1.0f, gain + v));
            daudio::setInputGain(*dev, gain);
        };
        auto fn_change_volume = [&](float v) {
            float vol = daudio::getOutputVolume(*dev);
            vol       = max(0.f, min(1.0f, vol + v));
            daudio::setOutputVolume(*dev, vol);
        };

        const float incr                   = 0.02f;
        map<int, function<void()>> key_map = {
            {72, bind(fn_change_gain, incr)},     // arrow up
            {80, bind(fn_change_gain, -incr)},    // arrow down
            {77, bind(fn_change_volume, incr)},   // arrow right
            {75, bind(fn_change_volume, -incr)},  // arrow left
        };

        bool first_update = true;
        while (run_loop_) {
            unique_lock<mutex> lock(mtx_changed);
            changed = false;
            if (first_update || cv_changed.wait_for(lock, 50ms, [&] { return changed; })) {
                int in_level = int(inputGain * kWidth + 0.5f);
                for (int i = 0; i < kWidth; ++i)
                    cout << (i >= in_level ? '.' : '|');
                cout << (inputMute ? 'M' : ' ') << "    ";
                int out_level = int(outputVolume * kWidth + 0.5f);
                for (int i = 0; i < kWidth; ++i)
                    cout << (i >= out_level ? '.' : '|');
                cout << (outputMute ? 'M' : ' ') << '\r';
                first_update = false;
            }
#ifdef WIN32
            if (_kbhit()) {
                const auto c = _getch();
                switch (c) {
                case 0xe0: {
                    auto it = key_map.find(_getch());
                    if (it != key_map.end())
                        (it->second)();
                } break;

                case 'I':
                case 'i':
                    daudio::setInputMuted(*dev, !daudio::getInputMuted(*dev));
                    break;

                case 'O':
                case 'o':
                    daudio::setOutputMuted(*dev, !daudio::getOutputMuted(*dev));
                    break;
                }
            }
#endif
        }

        cout << endl << "Monitor loop terminated" << endl;
    } catch (exception& e) {
        cerr << e.what() << endl;
        return 1;
    }

    return 0;
}

#ifdef _WIN32

#include <objbase.h>
#include <windows.h>

BOOL WINAPI HandlerRoutine(_In_ DWORD dwCtrlType)
{
    switch (dwCtrlType) {
    case CTRL_C_EVENT:
        run_loop_ = false;
        return TRUE;
    default:
        // Pass signal on to the next handler
        return FALSE;
    }
}

void setSignalHandler() { SetConsoleCtrlHandler(HandlerRoutine, TRUE); }

#else
#include <signal.h>

void intHandler(int)
{
    //
    run_loop_ = false;
}

void setSignalHandler()
{
    signal(SIGINT, intHandler);
    signal(SIGSTOP, intHandler);
}

#endif
