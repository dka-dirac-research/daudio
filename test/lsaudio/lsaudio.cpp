#include <daudio/listing.h>
#include <iostream>

#include <log/log.hpp>

using namespace std;

void printInfo(const daudio::AudioDevice& dev)
{
    cout << "DeviceName: " << dev.name << "\n"
         << "Devicetype: " << dev.typeName() << "\n"
         << "Inputs: " << dev.inputchannels << "\n"
         << "Outputs: " << dev.outputchannels << "\n"
         << endl;
}

namespace
{
    struct Logger : dirac::log::Logger {
        Logger() { dirac::log::setLogLevel(dirac::log::LogLevel::LogLevel_kERROR); }
        void log(const dirac::log::LogEvent& logEvent) override
        {
            cerr << logEvent.toString() << endl;
        }
    };
}  // namespace

int main()
{
    static Logger logger;

    for (const auto& dev : daudio::allDevices())
        printInfo(dev);

    return 0;
}
