package com.example.androidwhitebox;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements
        AdapterView.OnItemSelectedListener  {

    String devName;
    String sampleRate;
    NativeLib test;
    TextView counterTextField;
    Button playRecButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        test = new NativeLib();

        String[] devsNames = test.getDeviceNames();

        Spinner spin = (Spinner) findViewById(R.id.spinner);
        spin.setOnItemSelectedListener(this);

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,devsNames);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);

        counterTextField = (TextView) findViewById(R.id.textViewConter);

        CountDownTimer counter = new CountDownTimer(21000, 1000) {
            public void onTick(long millisUntilFinished) {
                if(millisUntilFinished > 10e3){
                    counterTextField.setText("RECORDING! TIME LEFT: " + (millisUntilFinished / 1000 - 10) + " S");
                    counterTextField.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                } else {
                    counterTextField.setText("PLAYBACK! TIME LEFT: " + (millisUntilFinished / 1000) + " S");
                    counterTextField.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
                }
            }

            public void onFinish() {
                counterTextField.setVisibility(View.INVISIBLE);
                playRecButton.setVisibility(View.VISIBLE);
            }
        };

        playRecButton = findViewById(R.id.button_id);
        playRecButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                test.playDev(devName, Integer.parseInt(sampleRate));
                counterTextField.setVisibility(View.VISIBLE);
                playRecButton.setVisibility(View.INVISIBLE);
                counter.start();
            }
        });
    }

    //Performing action onItemSelected and onNothing selected
    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        switch(arg0.getId()){
            case R.id.spinner:

                devName = arg0.getSelectedItem().toString();
                String[] sampleRates = test.getSampleRatesForDev(devName);

                Spinner spin = (Spinner) findViewById(R.id.spinner2);
                spin.setOnItemSelectedListener(this);

                ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,sampleRates);
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spin.setAdapter(aa);

                break;
            case R.id.spinner2:
                sampleRate = arg0.getSelectedItem().toString();
                break;
        }
        TextView devView = (TextView)findViewById(R.id.device_info);
        devView.setText("Device name: " + devName + "\n" + "Sample rate: " + sampleRate);
    }
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
}