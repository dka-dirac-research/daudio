package com.example.androidwhitebox;

public class NativeLib {

    static {
        System.loadLibrary("native-lib");
    }

    private long nativeObjectPointer;

    private native long nativeNew();

    public NativeLib() {
        nativeObjectPointer = nativeNew();
    }
    public native String[] getDeviceNames();
    public native String[] getSampleRatesForDev(String dev);
    public native void playDev(String dev, int sampleRate);
    public native void deleteThis();
}