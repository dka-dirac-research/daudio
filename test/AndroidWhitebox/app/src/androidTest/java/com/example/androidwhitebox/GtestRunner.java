package com.example.androidwhitebox;

import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;

public class GtestRunner extends Runner {
    private static boolean sOnceFlag = false;

    private Class mTargetClass;
    private Description mDescription;

    public GtestRunner(Class testClass) {
        synchronized (GtestRunner.class) {
            if (sOnceFlag) {
                throw new IllegalStateException("Error multiple GtestRunners defined");
            }
            sOnceFlag = true;
        }

        mTargetClass = testClass;
        TargetLibrary library = (TargetLibrary) testClass.getAnnotation(TargetLibrary.class);
        if (library == null) {
            throw new IllegalStateException("Missing required @TargetLibrary annotation");
        }
        System.loadLibrary(library.value());
        mDescription = Description.createSuiteDescription(testClass);
        nInitialize(mDescription);
    }

    @Override
    public Description getDescription() {
        return mDescription;
    }

    @Override
    public void run(RunNotifier notifier) {
        nRun(notifier);
    }

    private static native void nInitialize(Description description);
    private static native void nRun(RunNotifier notifier);
}
