#include <jni.h>
#include <chrono>
#include <string>
#include <thread>

#include <daudio/info.h>
#include <daudio/listing.h>
#include <daudio/session.h>

using std::string;

class NativeLib
{
private:
public:
    NativeLib(){};
};

extern "C" JNIEXPORT jlong JNICALL
Java_com_example_androidwhitebox_NativeLib_nativeNew(JNIEnv* env, jobject self)
{
    NativeLib* p = new NativeLib();
    return reinterpret_cast<jlong>(p);
}

static NativeLib* getObject(JNIEnv* env, jobject self)
{
    jclass cls = env->GetObjectClass(self);
    if (!cls)
        env->FatalError("GetObjectClass failed");

    jfieldID nativeObjectPointerID = env->GetFieldID(cls, "nativeObjectPointer", "J");
    if (!nativeObjectPointerID)
        env->FatalError("GetFieldID failed");

    jlong nativeObjectPointer = env->GetLongField(self, nativeObjectPointerID);
    return reinterpret_cast<NativeLib*>(nativeObjectPointer);
}

extern "C" JNIEXPORT void JNICALL
Java_com_example_androidwhitebox_NativeLib_deleteThis(JNIEnv* env, jobject self)
{
    NativeLib* _self = getObject(env, self);
    delete _self;
}

extern "C" JNIEXPORT jobjectArray JNICALL
Java_com_example_androidwhitebox_NativeLib_getDeviceNames(JNIEnv* env, jobject self)
{
    NativeLib* _self = getObject(env, self);
    auto devs        = daudio::allDevices();

    std::vector<std::string> devices;

    for (const auto& dev : devs) {
        if (dev.inputchannels > 0)
            devices.push_back(dev.name);
    }

    jobjectArray ret = (jobjectArray)env->NewObjectArray(
        devices.size(), env->FindClass("java/lang/String"), env->NewStringUTF(""));

    for (int i = 0; i < devices.size(); i++) {
        env->SetObjectArrayElement(ret, i, env->NewStringUTF(devices[i].c_str()));
    }

    return ret;
}

std::string jstring2string(JNIEnv* env, jstring jStr)
{
    if (!jStr)
        return "";

    const jclass stringClass = env->GetObjectClass(jStr);
    const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    const jbyteArray stringJbytes =
        (jbyteArray)env->CallObjectMethod(jStr, getBytes, env->NewStringUTF("UTF-8"));

    size_t length = (size_t)env->GetArrayLength(stringJbytes);
    jbyte* pBytes = env->GetByteArrayElements(stringJbytes, NULL);

    std::string ret = std::string((char*)pBytes, length);
    env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

    env->DeleteLocalRef(stringJbytes);
    env->DeleteLocalRef(stringClass);
    return ret;
}

extern "C" JNIEXPORT jobjectArray JNICALL
Java_com_example_androidwhitebox_NativeLib_getSampleRatesForDev(JNIEnv* env,
                                                                jobject self,
                                                                jstring name)
{
    const auto stdName = jstring2string(env, name);
    auto devs          = daudio::allDevices();

    auto it =
        std::find_if(devs.begin(), devs.end(), [&](auto& entry) { return entry.name == stdName; });

    const auto fss = daudio::supportedSamplerates(*it);
    std::vector<std::string> sampleRates(fss.size());

    auto i = 0u;
    for (const auto& fs : fss) {
        sampleRates[i] = std::to_string(static_cast<int>(fs));
        i++;
    }

    jobjectArray ret = (jobjectArray)env->NewObjectArray(
        fss.size(), env->FindClass("java/lang/String"), env->NewStringUTF(""));

    for (i = 0; i < fss.size(); i++) {
        env->SetObjectArrayElement(ret, i, env->NewStringUTF(sampleRates[i].c_str()));
    }

    return ret;
}

extern "C" JNIEXPORT void JNICALL
Java_com_example_androidwhitebox_NativeLib_playDev(JNIEnv* env,
                                                   jobject self,
                                                   jstring name,
                                                   jint sampleRate)
{
    const auto stdName = jstring2string(env, name);
    const auto fs      = static_cast<int>(sampleRate);
    const auto fsf     = static_cast<float>(fs);

    auto f = [](std::string name, float fsf) {
        auto all = daudio::allDevices();

        auto it =
            std::find_if(all.begin(), all.end(), [&](auto& entry) { return entry.name == name; });

        auto dev     = *it;
        int time_rec = 10;
        int time_ms  = time_rec * 1000;
        auto i_rec   = 0u;
        auto i_play  = 0u;

        const auto isFsLargerThen48k = fsf > 48000.f;
        const auto subSampInc = isFsLargerThen48k ? static_cast<unsigned>(fsf / 48000.f) : 1u;

        std::vector<float> rec_buffer(time_rec * fsf * 2);

        auto rec_cb = [&](const daudio::InputBufferView& inputView,
                          daudio::OutputBufferView& outView) {
            for (const auto& ch : inputView.channel)
                for (const auto& sample : ch) {
                    rec_buffer[i_rec] = sample;
                    i_rec++;
                }
        };

        auto play_cb = [&](const daudio::InputBufferView&, daudio::OutputBufferView& outView) {
            for (int ch_i = 0; ch_i < dev.inputchannels; ++ch_i) {
                auto& ch = outView.channel[ch_i];
                for (auto& sample : ch) {
                    sample = rec_buffer[i_play];
                    i_play = i_play + subSampInc;
                }
            }
        };

        {
            auto session       = daudio::open(dev, fsf);
            auto token0        = daudio::registerCallback(*session, rec_cb);
            using clock        = std::chrono::high_resolution_clock;
            const auto t_start = clock::now();

            while (true) {
                daudio::waitForTermination(*session, 10);
                if (time_ms > 0) {
                    auto time_so_far_ms = std::chrono::duration_cast<std::chrono::milliseconds>(
                                              clock::now() - t_start)
                                              .count();
                    if (time_so_far_ms >= time_ms) {
                        break;
                    }
                }
            }
        }

        {
            auto session       = daudio::open(all[0], fsf > 48000 ? 48000 : fsf);
            auto token0        = daudio::registerCallback(*session, play_cb);
            using clock        = std::chrono::high_resolution_clock;
            const auto t_start = clock::now();

            while (true) {
                daudio::waitForTermination(*session, 10);
                if (time_ms > 0) {
                    auto time_so_far_ms = std::chrono::duration_cast<std::chrono::milliseconds>(
                                              clock::now() - t_start)
                                              .count();
                    if (time_so_far_ms >= time_ms) {
                        break;
                    }
                }
            }
        }
    };

    std::thread t(f, stdName, fsf);
    t.detach();
}
