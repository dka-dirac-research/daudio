
#include <daudio/info.h>
#include <daudio/listing.h>
#include <daudio/session.h>

#include <daudio/waveform.h>

#include <atomic>
#include <cmath>
#include <condition_variable>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <regex>
#include <thread>
using namespace std;
using namespace chrono_literals;
using namespace daudio;

#include <log/log.hpp>

#include <CLI/CLI.hpp>

#include <assert.h>

namespace
{
    struct Logger : dirac::log::Logger {
        void log(const dirac::log::LogEvent& logEvent) override
        {
            using namespace dirac::log;

            if (logEvent.m_level >= LogLevel::LogLevel_kERROR)
                cerr << logEvent.toString() << endl;
            else
                cout << logEvent.toString() << endl;
        }
    };

    namespace little_endian_io
    {
        template <typename Word>
        std::ostream& write_word(std::ostream& outs, Word value, unsigned size = sizeof(Word))
        {
            for (; size; --size, value >>= 8)
                outs.put(static_cast<char>(value & 0xFF));
            return outs;
        }
    }  // namespace little_endian_io
    using namespace little_endian_io;
    struct wav_writer {
        std::ofstream f_;
        size_t data_chunk_pos_{0};

        std::vector<daudio::Waveforms> queue_;
        std::atomic<unsigned> write_ptr_{0};
        std::atomic<unsigned> read_ptr_{0};
        unsigned mask_{0};

        std::thread write_thread_;
        std::atomic<bool> stop_thread_{false};

        wav_writer(const std::string& name,
                   int fs,
                   int channels,
                   int buffer_size,
                   bool no_thread = false)
            : f_(name, std::ios::binary), mask_(no_thread ? 3 : (1 << 6) - 1)
        {
            if (!f_.is_open())
                throw std::runtime_error("failed to open '" + name + "' for writing");

            // Write the file headers
            f_ << "RIFF----WAVEfmt ";              // (chunk size to be filled in later)
            write_word(f_, 16, 4);                 // no extension data
            write_word(f_, 1, 2);                  // PCM - integer samples
            write_word(f_, channels, 2);           // number of channels
            write_word(f_, fs, 4);                 // samples per second (Hz)
            write_word(f_, fs * channels * 2, 4);  // (Sample Rate * BitsPerSample * Channels) / 8
            write_word(f_, channels * 2, 2);  // data block size (size of two integer samples, one
                                              // for each channel, in bytes)
            write_word(f_, 16, 2);            // number of bits per sample (use a multiple of 8)

            // Write the data chunk header
            data_chunk_pos_ = f_.tellp();
            f_ << "data----";  // (chunk size to be filled in later)

            queue_.resize(mask_ + 1);
            for (auto& buf : queue_)
                buf.resize(channels, buffer_size);

            if (!no_thread) {
                std::thread t([this] {
                    while (!stop_thread_) {
                        handleQueue();
                        std::this_thread::sleep_for(std::chrono::milliseconds(20));
                    }
                });
                write_thread_.swap(t);
            }
        }
        ~wav_writer()
        {
            if (write_thread_.joinable()) {
                stop_thread_ = true;
                write_thread_.join();
            }
            handleQueue();

            // (We'll need the final file size to fix the chunk sizes above)
            size_t file_length = f_.tellp();

            // Fix the data chunk header to contain the data size
            f_.seekp(data_chunk_pos_ + 4);
            write_word(f_, file_length - data_chunk_pos_ + 8);

            // Fix the file header to contain the proper RIFF chunk size, which is (file size - 8)
            // bytes
            f_.seekp(0 + 4);
            write_word(f_, file_length - 8, 4);
        }

        void handleQueue()
        {
            auto convertToInt = [](float value) -> short {
                constexpr double max_amplitude = (double)0x7fff;
                double r                       = value * max_amplitude;
                return (short)(r < -max_amplitude ? -max_amplitude
                                                  : (r > max_amplitude ? max_amplitude : r));
            };
            while (read_ptr_ != write_ptr_) {
                const auto& buf = queue_[read_ptr_++ & mask_];
                // Write interleaved
                for (unsigned i = 0; i < buf.waveformSize(); ++i) {
                    for (const auto& channel : buf.waveforms) {
                        write_word(f_, convertToInt(channel[i]), 2);
                    }
                }
            }
        }

        void push(const daudio::InputBufferView& in_view)
        {
            auto& buf = queue_[write_ptr_++ & mask_];
            assert((write_ptr_ - read_ptr_) <= mask_);
            const auto n_channels = (std::min)(in_view.channel.size(), buf.waveforms.size());
            for (unsigned i = 0; i < n_channels; ++i)
                std::copy_n(
                    in_view.channel[i].begin(), in_view.bufferSize(), buf.waveforms[i].data());
        }
    };

}  // namespace

void setSignalHandler();

[[noreturn]] void die(const string& message, int code)
{
    cerr << message << endl;
    std::exit(code);
}

void printInfo(const daudio::AudioDevice& dev)
{
    cout << (dev.is_default ? "* " : "  ");
    cout << dev.typeName() << ": '" << dev.name << "'" << endl;
    cout << "   channels : " << dev.inputchannels << "/" << dev.outputchannels << endl;
    cout << "   rates    :";
    auto def_rate = daudio::defaultSamplerate(dev);
    auto rates    = daudio::supportedSamplerates(dev);
    for (auto rate : rates)
        cout << (rate == def_rate ? "*" : " ") << rate << " ";
    cout << endl;
    auto def_buf_size = daudio::defaultBuffersize(dev);
    auto sizes        = daudio::supportedBufferSizes(dev);
    cout << "   bufsizes : [" << sizes.front() << ".. *" << def_buf_size << " .." << sizes.back()
         << "]" << endl;
}

static void listDevices()
{
    cout << "Device list (* = default) -------------------------------" << endl;
    for (const auto& dev : daudio::allDevices())
        printInfo(dev);
}

static bool run_loop_{true};

int main(int argc, const char* argv[])
{
    Logger the_logger;

    try {
        CLI::App app{"record test application"};

        int buffer_frames = 0;
        int time_ms       = 0;
        std::string dev_name;
        std::string log_level = "INFO";
        std::string out_path;

        auto devsFlag = app.add_flag("--devs", "List the available devices");
        app.add_option("-f, --buffer-frames", buffer_frames, "Buffer size in frames")
            ->type_name("frames");
        app.add_option("-t,--time", time_ms, "Time after which recording stops (optional)");
        app.add_option("-d,--device", dev_name, "Device name")->required();
        app.add_set("-l,--log-level",
                    log_level,
                    {"TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL"},
                    "Log level. Default is INFO.");
        app.add_option("out_path", out_path, "Name of output file");
        CLI11_PARSE(app, argc, argv);

        dirac::log::setLogLevelFromString(log_level);
        LOG_INFO("Start");

        if (*devsFlag) {
            listDevices();
            exit(0);
        }

        setSignalHandler();

        if (out_path.empty())
            throw runtime_error("Output file path not given");

        const auto devices = inputDevices();
        regex r(dev_name, regex::icase | regex::extended);

        const auto dev = find_if(
            devices.begin(), devices.end(), [&](const auto& d) { return regex_search(d.name, r); });

        if (dev == devices.end())
            throw runtime_error(dev_name + " not found");

        auto fs       = daudio::defaultSamplerate(*dev);
        auto buf_size = daudio::defaultBuffersize(*dev);
        if (buffer_frames > 0)
            buf_size = buffer_frames;

        auto session = daudio::open(*dev, fs, buf_size);

        if (daudio::currentSamplerate(*session) != fs)
            throw runtime_error("Failed to open session with sample rate = " + to_string(fs));
        if (daudio::currentBuffersize(*session) != buf_size)
            throw runtime_error("Failed to open session with buf size = " + to_string(buf_size));

        wav_writer writer(out_path, int(fs), 1, buf_size);

        auto writer_cb = [&](const daudio::InputBufferView& inputs, daudio::OutputBufferView&) {
            writer.push(inputs);
        };

        auto token = daudio::registerCallback(*session, writer_cb);

        using clock        = chrono::high_resolution_clock;
        const auto t_start = clock::now();
        while (run_loop_) {
            daudio::waitForTermination(*session, 10);
            if (time_ms > 0) {
                auto time_so_far_ms =
                    chrono::duration_cast<chrono::milliseconds>(clock::now() - t_start).count();
                if (time_so_far_ms >= time_ms) {
                    cout << "Capture time has been reached" << endl;
                    break;
                }
            }
        }
    } catch (exception& e) {
        cerr << e.what() << endl;
        return 1;
    }

    return 0;
}

#ifdef _WIN32

#include <objbase.h>
#include <windows.h>

BOOL WINAPI HandlerRoutine(_In_ DWORD dwCtrlType)
{
    switch (dwCtrlType) {
    case CTRL_C_EVENT:
        run_loop_ = false;
        return TRUE;
    default:
        // Pass signal on to the next handler
        return FALSE;
    }
}

void setSignalHandler() { SetConsoleCtrlHandler(HandlerRoutine, TRUE); }

#else
#include <signal.h>

void intHandler(int)
{
    //
    run_loop_ = false;
}

void setSignalHandler()
{
    signal(SIGINT, intHandler);
    signal(SIGSTOP, intHandler);
}

#endif
