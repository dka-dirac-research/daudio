#include <dminternal/utils.h>
#include <gtest/gtest.h>
using namespace std::string_literals;

TEST(utils, UtilsTrimString)
{
    const std::string str = "abcd";

    ASSERT_EQ(utils::trim(" abcd "), str) << "trim beginning and end failed";
    ASSERT_EQ(utils::trim(" ab cd "), "ab cd") << "trim beginning and end failed";
    ASSERT_EQ(utils::trim(" abcd"), str) << "trim beginning failed";
    ASSERT_EQ(utils::trim("abcd "), str) << "trim end failed";
    ASSERT_EQ(utils::trim("\t\n\v\f\r abcd \t\n\v\f\r"), str)
        << "trim beginning and end should trim all whitespace chars";
    ASSERT_EQ(utils::trim(" "), "");
    ASSERT_EQ(utils::trim(""), "");
}
