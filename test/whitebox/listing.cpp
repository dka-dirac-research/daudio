#include <gtest/gtest.h>
#include <algorithm>
#include <stdexcept>
#include <thread>

#include <daudio/info.h>
#include <daudio/listing.h>

using namespace std;

namespace
{
    static void initDevices()
    {
        int cntr = 10;
        while (--cntr) {
            auto devs = daudio::allDevices();
            if (!devs.empty())
                break;
            this_thread::sleep_for(chrono::milliseconds(500));
        }
        if (cntr == 0)
            throw runtime_error("no devices!?");
    }
}  // namespace

TEST(listing, allDevices)
{
    initDevices();

    const auto all = daudio::allDevices();

    // Allow some time for devices to appear

    EXPECT_GT(all.size(), 0u) << "the device manager should find some devices";

    for (const auto& dev : all) {
        ASSERT_GT(dev.name.size(), 0u) << "no device names should be empty";
        ASSERT_TRUE(dev.inputchannels || dev.outputchannels)
            << "every device should have either input or output channels";
    }
}

TEST(listing, inputDevices)
{
    const auto all    = daudio::allDevices();
    const auto inputs = daudio::inputDevices();

    ASSERT_GE(all.size(), inputs.size()) << "the input list is never longer than the complete list";

    for (const auto& dev : inputs) {
        ASSERT_GT(dev.name.size(), 0u) << "no device names should be empty";
        ASSERT_TRUE(dev.inputchannels) << "input devices should have input channels";
        ASSERT_TRUE(any_of(all.begin(), all.end(), [&](const auto& d) { return d == dev; }))
            << "all input devices should be present in the complete device list";
    }
}

TEST(listing, outputDevices)
{
    const auto all     = daudio::allDevices();
    const auto outputs = daudio::outputDevices();

    ASSERT_GE(all.size(), outputs.size())
        << "the output list is never longer than the complete list";

    for (const auto& dev : outputs) {
        ASSERT_GT(dev.name.size(), 0u) << "no device names should be empty";
        ASSERT_TRUE(dev.outputchannels) << "output devices should have output channels";
        ASSERT_TRUE(any_of(all.begin(), all.end(), [&](const auto& d) { return d == dev; }))
            << "all output devices should be present in the complete device list";
    }
}
