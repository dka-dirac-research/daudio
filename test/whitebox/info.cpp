#include <gtest/gtest.h>
#include <algorithm>
#include <stdexcept>
#include <thread>
using namespace std;

#include <daudio/info.h>
#include <daudio/listing.h>
#include <daudio/session.h>

namespace
{
    static void initDevices()
    {
        int cntr = 10;
        while (--cntr) {
            auto devs = daudio::allDevices();
            if (!devs.empty())
                break;
            this_thread::sleep_for(chrono::milliseconds(500));
        }
        if (cntr == 0)
            throw runtime_error("no devices!?");
    }
}  // namespace

TEST(info, supportedSamplerates)
{
    initDevices();

    const auto all = daudio::allDevices();
    EXPECT_GT(all.size(), 0u) << "test is meaningless without devices";

    for (const auto& dev : all) {
        const auto fss = daudio::supportedSamplerates(dev);
        ASSERT_GT(fss.size(), 0u);
        ASSERT_TRUE(all_of(fss.begin(), fss.end(), [](auto fs) { return fs > 0.f; }))
            << "samplerates are positive";
        ASSERT_TRUE(all_of(fss.begin(), fss.end(), [](auto fs) { return fs < 10e6; }))
            << "implausible samplerate, even bats have limits";
    }
}

TEST(info, inputChannelNames)
{
    const auto all = daudio::allDevices();
    EXPECT_GT(all.size(), 0u) << "test is meaningless without devices";

    for (const auto& dev : all) {
        const auto names = daudio::inputChannelNames(dev);
        ASSERT_EQ(unsigned(dev.inputchannels), names.size()) << "all channels should have names";
        ASSERT_TRUE(all_of(names.begin(), names.end(), [](const auto& n) { return !!n.size(); }))
            << "no names should be empty";
    }
}

TEST(info, outputChannelNames)
{
    const auto all = daudio::allDevices();
    EXPECT_GT(all.size(), 0u) << "test is meaningless without devices";

    for (const auto& dev : all) {
        const auto names = daudio::outputChannelNames(dev);
        ASSERT_EQ(unsigned(dev.outputchannels), names.size()) << "all channels should have names";
        ASSERT_TRUE(all_of(names.begin(), names.end(), [](const auto& n) { return !!n.size(); }))
            << "no names should be empty";
    }
}

TEST(info, deviceForSession)
{
    const auto all = daudio::allDevices();
    EXPECT_GT(all.size(), 0u) << "test is meaningless without devices";

    auto devicesTested = 0;
    for (const auto& dev : all) {
        const auto fs = daudio::supportedSamplerates(dev)[0];
        try {
            auto session = daudio::open(dev, fs);
            ASSERT_EQ(dev, daudio::deviceForSession(*session))
                << "the opened session should use the right device";
            devicesTested++;
        } catch (std::exception&) {
        }
    }

    EXPECT_GT(devicesTested, 0) << "all devices busy";
}

TEST(info, currentSampleRate)
{
    const auto all = daudio::allDevices();
    EXPECT_GT(all.size(), 0u) << "test is meaningless without devices";

    auto devicesTested = 0;
    for (const auto& dev : all) {
        const auto fs = daudio::supportedSamplerates(dev)[0];
        try {
            auto session = daudio::open(dev, fs);
            ASSERT_FLOAT_EQ(fs, daudio::currentSamplerate(*session))
                << "the opened session should use the right samplerate";
            devicesTested++;
        } catch (std::exception&) {
        }
    }

    EXPECT_GT(devicesTested, 0) << "all devices busy";
}

TEST(info, currentBuffersize)
{
    const auto all = daudio::allDevices();
    EXPECT_GT(all.size(), 0u) << "test is meaningless without devices";

    auto devicesTested = 0;
    for (const auto& dev : all) {
        const auto fs = daudio::supportedSamplerates(dev)[0];
        try {
            auto session          = daudio::open(dev, fs);
            const auto bufferSize = daudio::currentBuffersize(*session);

            ASSERT_GT(bufferSize, 0u) << "the buffer size should be larger than zero";

            EXPECT_LT(bufferSize, 1000000u) << "suspiciously large buffersize";
            devicesTested++;
        } catch (std::exception&) {
        }
    }

    EXPECT_GT(devicesTested, 0) << "all devices busy";
}
