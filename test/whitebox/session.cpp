#include <chrono>
#include <condition_variable>
#include <mutex>
#include <stdexcept>
#include <thread>
#include <utility>

#include <gtest/gtest.h>

#include <daudio/info.h>
#include <daudio/listing.h>
#include <daudio/session.h>

using namespace std;
using namespace chrono_literals;

namespace
{
    daudio::AudioCallback makeZeroingCallback(mutex& lock, condition_variable& var, bool& done)
    {
        return [&](const daudio::InputBufferView&, daudio::OutputBufferView& outView) {
            for (auto& ch : outView.channel)
                for (auto& sample : ch)
                    sample = 0.f;

            {
                lock_guard<mutex> guard{lock};
                done = true;
            }
            var.notify_one();
        };
    }

    daudio::AudioCallback makeLowNoiseCallback()
    {
        return [](const daudio::InputBufferView&, daudio::OutputBufferView& outView) {
            for (auto& ch : outView.channel)
                for (auto& sample : ch)
                    *(int32_t*)(&sample) = 0xCAFE;
        };
    }

    daudio::AudioCallback makeAssertCallback()
    {
        return [](const daudio::InputBufferView&, daudio::OutputBufferView& outView) {
            for (auto& ch : outView.channel)
                for (auto& sample : ch)
                    ASSERT_EQ(*(int32_t*)(&sample), 0xCAFE)
                        << "the previous callback did not modify the buffer";
        };
    }

    daudio::AudioCallback makeCountingCallback(mutex& lock,
                                               condition_variable& var,
                                               volatile unsigned& counter)
    {
        return [&](const daudio::InputBufferView&, daudio::OutputBufferView&) {
            {
                lock_guard<mutex> guard{lock};
                ++counter;
            }

            var.notify_one();
        };
    }

    static void initDevices()
    {
        int cntr = 10;
        while (--cntr) {
            auto devs = daudio::allDevices();
            if (!devs.empty())
                break;
            this_thread::sleep_for(chrono::milliseconds(500));
        }
        if (cntr == 0)
            throw runtime_error("no devices!?");
    }
}  // namespace

TEST(session, open_and_close)
{
    initDevices();

    auto all = daudio::allDevices();
    EXPECT_GT(all.size(), 0u) << "test is meaningless without devices";

    for (const auto& dev : all) {
        ASSERT_FALSE(daudio::isOpen(dev)) << "device should be closed when the test starts";

        const auto samplerates = daudio::supportedSamplerates(dev);
        ASSERT_GT(samplerates.size(), 0u) << "device reports no supported samplerates";

        auto session1 = daudio::open(dev, samplerates[0]);

        ASSERT_FALSE(session1 == nullptr) << "open returned nullptr";
        ASSERT_TRUE(daudio::isOpen(dev)) << "device reported close after being opened";

        ASSERT_THROW(daudio::open(dev, samplerates[0] * 2), std::invalid_argument)
            << "opening a device for a second time with different samplerate should not be allowed";

        auto session2 = daudio::open(dev, samplerates[0]);
        ASSERT_FALSE(session2 == nullptr) << "open returned nullptr during second call";
        ASSERT_TRUE(daudio::isOpen(dev))
            << "device reported close after being opened a second time";

        session2 = nullptr;
        ASSERT_TRUE(daudio::isOpen(dev))
            << "device should remain open while there are active sessions";

        session1 = nullptr;
        ASSERT_FALSE(daudio::isOpen(dev))
            << "device should close automatically when the last session disappears";
    }
}

TEST(session, open_and_reopen)
{
    auto all = daudio::allDevices();
    EXPECT_GT(all.size(), 0u) << "test is meaningless without devices";

    for (const auto& dev : all) {
        ASSERT_FALSE(daudio::isOpen(dev)) << "device should be closed when the test starts";

        const auto fs = daudio::supportedSamplerates(dev)[0];
        auto session  = daudio::open(dev, fs);

        ASSERT_FALSE(session == nullptr) << "open returned nullptr";
        ASSERT_TRUE(daudio::isOpen(dev)) << "device reported closed after being opened";

        vector<unique_ptr<daudio::AudioSession>> otherSessions;
        for (int i = 0; i < 100; ++i) {
            otherSessions.push_back(daudio::open(dev));
        }

        ASSERT_TRUE(none_of(otherSessions.begin(), otherSessions.end(), [](auto& s) {
            return s == nullptr;
        })) << "no calls to open should return null pointers";

        session = nullptr;
        ASSERT_TRUE(daudio::isOpen(dev))
            << "device remains open after the original session is gone";

        otherSessions.clear();
        ASSERT_FALSE(daudio::isOpen(dev))
            << "device should close automatically when the last session disappears";
    }
}

TEST(session, registerCallback_writing)
{
    auto all = daudio::allDevices();
    EXPECT_GT(all.size(), 0u) << "test is meaningless without devices";

    for (const auto& dev : all) {
        ASSERT_FALSE(daudio::isOpen(dev)) << "device should be closed when the test starts";

        const auto fs = daudio::supportedSamplerates(dev)[0];
        auto session  = daudio::open(dev, fs);

        mutex lock{};
        condition_variable cv{};
        bool done{false};

        auto token0 = daudio::registerCallback(*session, makeLowNoiseCallback());
        auto token1 = daudio::registerCallback(*session, makeAssertCallback());
        auto token2 = daudio::registerCallback(*session, makeZeroingCallback(lock, cv, done));

        {
            unique_lock<mutex> guard{lock};
            cv.wait(guard, [&] { return done; });
        }
    }
}

TEST(session, registerCallback_removal)
{
    auto all = daudio::allDevices();
    EXPECT_GT(all.size(), 0u) << "test is meaningless without devices";

    for (const auto& dev : all) {
        const auto fs = daudio::supportedSamplerates(dev)[0];
        auto session  = daudio::open(dev, fs);

        mutex lock{};
        condition_variable cv{};
        volatile unsigned counter{0};

        {
            auto token =
                daudio::registerCallback(*session, makeCountingCallback(lock, cv, counter));
            {
                unique_lock<mutex> guard{lock};
                cv.wait(guard, [&] { return !!counter; });
            }
        }

        // If the manager works correctly it has removed the callback here,
        // since the token has gone out of scope

        unsigned finalCounterVal;
        {
            lock_guard<mutex> guard{lock};
            finalCounterVal = counter;
        }

        this_thread::sleep_for(200ms);

        {
            // If the counter value continues to change, then the callback
            // has continued to run.

            lock_guard<mutex> guard{lock};
            ASSERT_EQ(counter, finalCounterVal)
                << "callbacks should be removed when the token goes out of scope";
        }
    }
}

TEST(session, release_with_loopback)
{
    for (const auto& dev : daudio::allDevices()) {
        if (dev.type != daudio::AudioDeviceType::loopback) {
            continue;
        }

        auto session = daudio::open(dev);

        ASSERT_NO_THROW(daudio::deviceForSession(*session));
        ASSERT_NO_THROW(daudio::release());
        ASSERT_THROW(daudio::deviceForSession(*session), std::invalid_argument)
            << "release should clear sessions";

        ASSERT_THROW(daudio::registerCallback(*session, makeLowNoiseCallback()),
                     std::invalid_argument)
            << "release should clear sessions";
    }
}

TEST(session, release_with_device)
{
    initDevices();
    for (const auto& dev : daudio::allDevices()) {
        if (dev.type == daudio::AudioDeviceType::loopback) {
            continue;
        }

        const auto fs = daudio::supportedSamplerates(dev)[0];
        auto session  = daudio::open(dev, fs);
        ASSERT_NO_THROW(daudio::deviceForSession(*session));
        ASSERT_NO_THROW(daudio::release());

        ASSERT_THROW(daudio::deviceForSession(*session), std::invalid_argument)
            << "release should clear sessions";

        ASSERT_THROW(daudio::registerCallback(*session, makeLowNoiseCallback()),
                     std::invalid_argument)
            << "release should clear sessions";

        // after release, all devices are destroyed so we can't continue iterating
        break;
    }
}
