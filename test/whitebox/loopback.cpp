#include <atomic>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <stdexcept>
#include <thread>
#include <utility>

#include <gtest/gtest.h>

#include <daudio/info.h>
#include <daudio/listing.h>
#include <daudio/loopback.h>
#include <daudio/session.h>

using namespace std;
using namespace chrono_literals;

static daudio::AudioCallback makeLoopbackChecker(mutex& lock,
                                                 condition_variable& cv,
                                                 int& callsMade,
                                                 int maxCalls)
{
    return [&, maxCalls](const daudio::InputBufferView& in, daudio::OutputBufferView& out) {
        unique_lock<mutex> guard{lock};
        for (auto& o : out.channel) {
            auto i = 0;
            for (auto& s : o)
                s = float(i++ + callsMade);
        }

        out.is_silent = false;

        if (out.channel.size() && callsMade > 0)
            for (auto& i : in.channel) {
                auto j = 0;
                for (auto& s : i)
                    ASSERT_FLOAT_EQ(float(j++ + callsMade - 1), s / float(out.channel.size()));
            }

        if (++callsMade < maxCalls)
            return;

        guard.unlock();
        cv.notify_one();
    };
}

static daudio::AudioCallback makeLoopbackTimer(mutex& lock,
                                               condition_variable& cv,
                                               int& callsMade,
                                               int maxCalls,
                                               chrono::high_resolution_clock::time_point& startTime,
                                               chrono::high_resolution_clock::time_point& stopTime)
{
    return [&, maxCalls](const daudio::InputBufferView&, daudio::OutputBufferView&) {
        unique_lock<mutex> guard{lock};

        if (++callsMade > maxCalls)
            return;

        if (callsMade == 1)
            startTime = chrono::high_resolution_clock::now();

        if (callsMade == maxCalls) {
            stopTime = chrono::high_resolution_clock::now();
            guard.unlock();
            cv.notify_one();
        }
    };
}

static daudio::AudioCallback makeZeroingCallback(mutex& lock, condition_variable& var, bool& done)
{
    return [&](const daudio::InputBufferView&, daudio::OutputBufferView& outView) {
        for (auto& ch : outView.channel)
            for (auto& sample : ch)
                sample = 0.f;
        outView.is_silent = false;
        {
            lock_guard<mutex> guard{lock};
            done = true;
        }
        var.notify_one();
    };
}

static daudio::AudioCallback makeLowNoiseCallback()
{
    return [=](const daudio::InputBufferView&, daudio::OutputBufferView& out) {
        for (auto& ch : out.channel)
            for (auto& sample : ch)
                *(int32_t*)(&sample) = 0xCAFE;
        out.is_silent = false;
    };
}

static daudio::AudioCallback makeAssertCallback()
{
    return [=](const daudio::InputBufferView&, daudio::OutputBufferView& out) {
        for (auto& ch : out.channel)
            for (auto& sample : ch)
                ASSERT_EQ(*(int32_t*)(&sample), 0xCAFE)
                    << "the previous callback did not modify the buffer";
    };
}

static daudio::AudioCallback makeMixer(atomic<int>* callsMade)
{
    return [=](const daudio::InputBufferView& in, daudio::OutputBufferView& out) {
        (*callsMade)++;
        for (auto i = 0u; i < in.bufferSize(); ++i) {
            auto mixed = 0.f;
            for (auto& ch : in.channel)
                mixed += *(ch.begin() + i);

            for (auto& ch : out.channel)
                *(ch.begin() + i) = mixed;
        }
        out.is_silent = false;
    };
}

static void initDevices()
{
    daudio::setLoopBackEnabled(true);
    int cntr = 10;
    while (--cntr) {
        auto devs = daudio::allDevices();
        for (auto& dev : devs) {
            if (dev.type != daudio::AudioDeviceType::loopback)
                return;
        }

        this_thread::sleep_for(chrono::milliseconds(500));
    }
    if (cntr == 0)
        throw runtime_error("no devices!?");
}

TEST(loopback, open)
{
    initDevices();
    auto devs = daudio::allDevices();
    EXPECT_FALSE(devs.empty());
    bool has_loopback = false;
    for (auto& dev : devs) {
        if (dev.type != daudio::AudioDeviceType::loopback) {
            continue;
        }

        has_loopback                             = true;
        unique_ptr<daudio::AudioSession> session = nullptr;
        ASSERT_NO_THROW(session = daudio::open(dev));
        ASSERT_TRUE(session);
    }
    EXPECT_TRUE(has_loopback);
}

TEST(loopback, registerCallback)
{
    initDevices();
    auto devs = daudio::allDevices();
    for (auto& dev : devs) {
        if (dev.type != daudio::AudioDeviceType::loopback) {
            continue;
        }

        auto session = daudio::open(dev);
        ASSERT_TRUE(session);
        mutex callsLock;
        condition_variable cv{};
        auto callsMade = 0;
        auto cb        = makeLoopbackChecker(callsLock, cv, callsMade, 10);
        auto cbToken   = daudio::registerCallback(*session, cb);
        unique_lock<mutex> guard{callsLock};
        cv.wait(guard, [&]() { return callsMade >= 10; });
    }
}

// Reimplement once transfer functions are back in play
TEST(loopback, withTransferFunc)
{
    initDevices();
    auto devs = daudio::allDevices();
    for (auto& dev : devs) {
        if (dev.type != daudio::AudioDeviceType::loopback) {
            continue;
        }

        atomic<int> mixerCalled;
        mixerCalled  = 0;
        auto session = daudio::open(dev);
        daudio::setTransferFunc(dev, makeMixer(&mixerCalled));

        ASSERT_TRUE(session);
        mutex callsLock;
        condition_variable cv{};
        auto callsMade = 0;
        auto cb        = makeLoopbackChecker(callsLock, cv, callsMade, 10);
        auto cbToken   = daudio::registerCallback(*session, cb);
        unique_lock<mutex> guard{callsLock};
        cv.wait(guard, [&]() { return callsMade >= 10; });
        guard.unlock();

        daudio::setTransferFunc(dev, nullptr);

        ASSERT_GE(callsMade, mixerCalled);
        ASSERT_GT(mixerCalled, 0);
    }
}

TEST(loopback, callbackTiming)
{
    initDevices();
    auto devs = daudio::allDevices();
    for (auto& dev : devs) {
        if (dev.type != daudio::AudioDeviceType::loopback) {
            continue;
        }

        auto session = daudio::open(dev);
        ASSERT_TRUE(session);

        mutex callsLock;
        condition_variable cv{};
        auto callsMade      = 0;
        const auto maxCalls = 20;

        chrono::high_resolution_clock::time_point start =
            chrono::high_resolution_clock::time_point::min();
        chrono::high_resolution_clock::time_point stop =
            chrono::high_resolution_clock::time_point::max();
        auto cb      = makeLoopbackTimer(callsLock, cv, callsMade, maxCalls, start, stop);
        auto cbToken = daudio::registerCallback(*session, cb);
        unique_lock<mutex> guard{callsLock};
        cv.wait(guard, [&]() { return callsMade >= maxCalls; });

        const auto samplerate = daudio::currentSamplerate(*session);
        const auto buffersize = daudio::currentBuffersize(*session);

        const auto samplePeriodMs = 1000.0 / samplerate;
        const auto expectedTimeMs =
            chrono::milliseconds(long(buffersize * maxCalls * samplePeriodMs));
        const auto actualTimeMs = chrono::duration_cast<chrono::milliseconds>(stop - start);

        EXPECT_LT(expectedTimeMs - actualTimeMs, 500ms);
        EXPECT_LT(actualTimeMs - expectedTimeMs, 500ms);
    }
}

TEST(loopback, registerMultipleCallbacks)
{
    initDevices();
    auto devs = daudio::allDevices();
    for (auto& dev : devs) {
        if (dev.type != daudio::AudioDeviceType::loopback) {
            continue;
        }

        auto session = daudio::open(dev);

        mutex lock{};
        condition_variable cv{};
        bool done{false};

        auto token0 = daudio::registerCallback(*session, makeLowNoiseCallback());
        auto token1 = daudio::registerCallback(*session, makeAssertCallback());
        auto token2 = daudio::registerCallback(*session, makeZeroingCallback(lock, cv, done));

        {
            unique_lock<mutex> guard{lock};
            cv.wait(guard, [&] { return done; });
        }
    }
}

TEST(loopback, deviceForSession)
{
    initDevices();
    auto devs = daudio::allDevices();
    for (auto& dev : devs) {
        if (dev.type != daudio::AudioDeviceType::loopback) {
            continue;
        }

        auto session = daudio::open(dev);

        ASSERT_TRUE(session);
        ASSERT_EQ(dev, daudio::deviceForSession(*session));
    }
}
