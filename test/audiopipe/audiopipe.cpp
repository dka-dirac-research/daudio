#include <algorithm>
#include <chrono>
#include <iostream>
#include <regex>
#include <stdexcept>
#include <thread>

#include <daudio/audio_buffer.h>
#include <daudio/callbacks.h>

#include <daudio/info.h>
#include <daudio/listing.h>
#include <daudio/session.h>

using namespace std;
using namespace chrono_literals;
using namespace daudio;

[[noreturn]] void die(const string& message, int code)
{
    cerr << message << endl;
    std::exit(code);
}

daudio::AudioCallback makeCallback(unsigned inChannels, unsigned outChannels)
{
    auto cb = [=](const daudio::InputBufferView& inView, daudio::OutputBufferView& outView) {
        static vector<float> inframe(inChannels);
        static vector<float> outframe(outChannels);

        auto frameCount = inView.channel[0].end() - inView.channel[0].begin();
        for (unsigned i = 0; i < frameCount; ++i) {
            cin.read((char*)outframe.data(), outframe.size() * sizeof(float));

            for (unsigned j = 0; j < inChannels; ++j)
                inframe[j] = *(inView.channel[j].begin() + i);

            for (unsigned j = 0; j < outChannels; ++j)
                *(outView.channel[j].begin() + i) = outframe[j];

            cout.write((char*)inframe.data(), inframe.size() * sizeof(float));
        }
    };

    return cb;
}

int main(int argc, char* argv[])
{
    if (argc != 2)
        die(string("Usage: ") + argv[0] + " <devicename regex>", 1);

    const auto all = allDevices();

    regex r(argv[1], regex::icase | regex::extended);
    const auto dev =
        find_if(all.begin(), all.end(), [&](const auto& d) { return regex_search(d.name, r); });

    if (dev == all.end())
        die("No such device", 1);

    auto fss     = supportedSamplerates(*dev);
    auto session = open(*dev, *max_element(fss.begin(), fss.end()));
    auto cb      = makeCallback(dev->inputchannels, dev->outputchannels);
    auto cbToken = registerCallback(*session, cb);

    for (;;)
        this_thread::sleep_for(10ms);

    die("Good luck getting here", 0);
}
