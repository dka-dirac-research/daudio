include(FetchContent)
FetchContent_Declare(
    asiosdk
    URL https://www.steinberg.net/asiosdk
)
FetchContent_GetProperties(asiosdk)
if(NOT asiosdk_POPULATED)
    set(FETCHCONTENT_QUIET OFF CACHE BOOL "" FORCE)
    FetchContent_Populate(asiosdk)
    mark_as_advanced(
        FETCHCONTENT_BASE_DIR
        FETCHCONTENT_FULLY_DISCONNECTED
        FETCHCONTENT_QUIET
        FETCHCONTENT_SOURCE_DIR_ASIOSDK
        FETCHCONTENT_UPDATES_DISCONNECTED
        FETCHCONTENT_UPDATES_DISCONNECTED_ASIOSDK
    )

    set(SOURCES
        ${asiosdk_SOURCE_DIR}/common/asio.cpp
        ${asiosdk_SOURCE_DIR}/common/asio.h
        ${asiosdk_SOURCE_DIR}/common/asiosys.h
        ${asiosdk_SOURCE_DIR}/common/combase.cpp
        ${asiosdk_SOURCE_DIR}/common/combase.h
        ${asiosdk_SOURCE_DIR}/common/debugmessage.cpp
        ${asiosdk_SOURCE_DIR}/common/dllentry.cpp
        ${asiosdk_SOURCE_DIR}/common/iasiodrv.h
        ${asiosdk_SOURCE_DIR}/common/register.cpp
        ${asiosdk_SOURCE_DIR}/common/wxdebug.h
        ${asiosdk_SOURCE_DIR}/host/ASIOConvertSamples.cpp
        ${asiosdk_SOURCE_DIR}/host/ASIOConvertSamples.h
        ${asiosdk_SOURCE_DIR}/host/asiodrivers.cpp
        ${asiosdk_SOURCE_DIR}/host/asiodrivers.h
        ${asiosdk_SOURCE_DIR}/host/ginclude.h
        ${asiosdk_SOURCE_DIR}/host/pc/asiolist.cpp
        ${asiosdk_SOURCE_DIR}/host/pc/asiolist.h
    )

    add_library(asiosdk STATIC 
        ${SOURCES}
    )

    target_include_directories(
        asiosdk
        PUBLIC
        ${asiosdk_SOURCE_DIR}/common ${asiosdk_SOURCE_DIR}/host
        PRIVATE
        ${asiosdk_SOURCE_DIR}/host/pc
    )
    target_compile_definitions(asiosdk PRIVATE _CRT_SECURE_NO_WARNINGS)
    target_compile_options(asiosdk PRIVATE /wd4267 /wd4996)

    set_target_properties(
        asiosdk
        PROPERTIES
        FOLDER "Libraries/External"
    )
endif()



