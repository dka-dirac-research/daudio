# Tries to find ALSA package, and if cross compiling, downloads and builds it. 
# Needs environment set up with CC/CXX/CPP etc. variables.
find_package(ALSA)
if (NOT ALSA_FOUND)
    if (NOT CMAKE_CROSSCOMPILING)
        message(FATAL_ERROR "ALSA not found, is required for native builds.")
    else()
        include(ExternalProject)
        set(ALSA_INSTALL_DIR ${CMAKE_BINARY_DIR}/_deps/alsa-lib/install)
        separate_arguments(CONFIG_ARGS UNIX_COMMAND $ENV{CONFIGURE_FLAGS})
        set(CONFIG_ARGS "<SOURCE_DIR>/configure;--disable-python;${CONFIG_ARGS}")
        set(ALSA_INCLUDE_DIRS ${ALSA_INSTALL_DIR}/usr/include CACHE PATH "")
        set(ALSA_LIBRARIES ${ALSA_INSTALL_DIR}/usr/lib/libasound.so CACHE FILEPATH "")
        ExternalProject_Add(
            alsa-lib
            PREFIX ${CMAKE_BINARY_DIR}/_deps/alsa-lib
            URL ftp://ftp.alsa-project.org/pub/lib/alsa-lib-1.1.5.tar.bz2
            INSTALL_DIR ${ALSA_INSTALL_DIR}
            CONFIGURE_COMMAND ${CONFIG_ARGS}
            BUILD_COMMAND make
            INSTALL_COMMAND make install DESTDIR=<INSTALL_DIR>
            BUILD_BYPRODUCTS ${ALSA_LIBRARIES}
            BUILD_IN_SOURCE 1)
    endif()
endif()
