#include "dminternal/devicemanager.h"
#include "dminternal/device.h"
#include "dminternal/internal_loopback.h"

#include <dminternal/callbacktoken.h>
#include <dminternal/utils.h>

#include <future>
#include <iterator>
#include <stdexcept>

using namespace std;

dminternal::DeviceManager::DeviceManager() : pending_updates_{0}
{
    // This tries to coalesce multiple updates
    change_thread_ = make_unique<dminternal::TimerThread>(
        [this] {
            if (pending_updates_.exchange(0) > 0) {
                lock_guard<mutex> guard{callback_lock_};
                for (auto& cb : change_callbacks_)
                    (cb.second)();
            }
        },
        1000);
}
dminternal::DeviceManager::~DeviceManager()
{
    change_thread_ = nullptr;
    uninitialize();
}

void dminternal::DeviceManager::uninitialize()
{
    for (auto& it : device_types_) {
        for (auto& dev : it.second->devices()) {
            auto device = it.second->getDevice(dev.id);
            if (device != nullptr && device->isOpened())
                device->close();
        }
    }

    device_types_.clear();
    initialized_ = false;
}

void dminternal::DeviceManager::init()
{
    if (initialized_)
        return;

    const bool registerCallback = true;

    function<void()> fnCallback{nullptr};
    if (registerCallback)
        fnCallback = [this] { ++pending_updates_; };

#define REGISTER_DEVICE_TYPE(Type)                                                               \
    if (auto type = dminternal::DeviceType::create<daudio::AudioDeviceType::Type>(fnCallback)) { \
        device_types_.insert(make_pair(daudio::AudioDeviceType::Type, move(type)));              \
    }

    if (loopback_enabled_ &&
        (list_all_types_ || type_to_list_ == daudio::AudioDeviceType::loopback))
        REGISTER_DEVICE_TYPE(loopback);

#ifdef WIN32
#if DEVICEMANAGER_BUILD_ASIO
    if (list_all_types_ || type_to_list_ == daudio::AudioDeviceType::asio)
        REGISTER_DEVICE_TYPE(asio);
#endif
    if (list_all_types_ || type_to_list_ == daudio::AudioDeviceType::wasapi)
        REGISTER_DEVICE_TYPE(wasapi);
#endif
#ifdef __linux__
#ifdef __ANDROID__
    if (list_all_types_ || type_to_list_ == daudio::AudioDeviceType::oboe)
        REGISTER_DEVICE_TYPE(oboe);
    if (list_all_types_ || type_to_list_ == daudio::AudioDeviceType::opensles)
        REGISTER_DEVICE_TYPE(opensles);
#else
    if (list_all_types_ || type_to_list_ == daudio::AudioDeviceType::alsa)
        REGISTER_DEVICE_TYPE(alsa);
#endif
#endif
#ifdef __APPLE__
    if (list_all_types_ || type_to_list_ == daudio::AudioDeviceType::coreaudio)
        REGISTER_DEVICE_TYPE(coreaudio);
#endif

    initialized_ = true;
}

vector<daudio::AudioDevice> dminternal::DeviceManager::allDevices()
{
    daudio::AudioDevices devices;
    for (auto& entry : device_types_) {
        auto devs = entry.second->devices();
        copy(devs.begin(), devs.end(), back_inserter(devices));
    }
    return devices;
}

shared_ptr<dminternal::Device> dminternal::DeviceManager::find(device_id_t id)
{
    for (auto& dt : device_types_) {
        auto dev = dt.second->getDevice(id);
        if (dev != nullptr)
            return dev;
    }
    throw invalid_argument("device id not found");
}

daudio::AudioDevice dminternal::DeviceManager::registerLoopbackDevice(int inputs,
                                                                      int outputs,
                                                                      float fs,
                                                                      int buffer_size)
{
    auto dt = device_types_.find(daudio::AudioDeviceType::loopback);
    if (dt == device_types_.end())
        throw invalid_argument("no loopback device type registered");
    return dminternal::createNewLoopbackDevice(*dt->second, inputs, outputs, fs, buffer_size);
}

unique_ptr<daudio::CallbackToken> dminternal::DeviceManager::registerDevicesChangedCallback(
    function<void()> callback)
{
    lock_guard<mutex> guard{callback_lock_};
    auto cb_id = dminternal::add_callback(change_callbacks_, callback);
    return make_unique<dminternal::CallbackToken>([cb_id, weak_this = weak_from_this()] {
        if (auto p = weak_this.lock()) {
            lock_guard<mutex> guard{p->callback_lock_};
            p->change_callbacks_.erase(cb_id);
        }
    });
}

void dminternal::DeviceManager::setTypeToList(daudio::AudioDeviceType type_to_list)
{
    if (list_all_types_ || type_to_list != type_to_list_) {
        type_to_list_   = type_to_list;
        list_all_types_ = false;
        uninitialize();
        init();
    }
}

void dminternal::DeviceManager::setListAllTypes()
{
    type_to_list_ = daudio::AudioDeviceType::unknown;
    if (!list_all_types_) {
        list_all_types_ = true;
        uninitialize();
        init();
    }
}

void dminternal::DeviceManager::setLoopBackEnabled(bool should_be_enabled)
{
    if (loopback_enabled_ != should_be_enabled) {
        loopback_enabled_ = should_be_enabled;
        uninitialize();
        init();
    }
}
