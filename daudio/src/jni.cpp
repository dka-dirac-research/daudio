#include "daudio/jni.h"
#include "dminternal/internal_jni.h"

namespace daudio
{
    void register_jni_vm(JavaVM* vm) { dminternal::register_jni_vm(vm); }
}  // namespace daudio
