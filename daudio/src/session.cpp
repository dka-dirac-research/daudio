#include "daudio/session.h"
#include "dminternal/device.h"
#include "dminternal/dmholder.h"
#include "dminternal/internal_session.h"

using namespace std;

unique_ptr<daudio::AudioSession> daudio::open(const daudio::AudioDevice& device, float fs)
{
    auto d = dminternal::internalDevice(device.id);
    return d.first->addSession(fs);
}

std::unique_ptr<daudio::AudioSession> daudio::open(const daudio::AudioDevice& device,
                                                   float fs,
                                                   int bufferSize)
{
    auto d = dminternal::internalDevice(device.id);
    return d.first->addSession(fs, bufferSize);
}

std::unique_ptr<daudio::AudioSession> daudio::open(const daudio::AudioDevice& device,
                                                   float fs,
                                                   int bufferSize,
                                                   daudio::ActiveChannels inputMask,
                                                   daudio::ActiveChannels outputMask)
{
    auto d = dminternal::internalDevice(device.id);
    return d.first->addSession(fs, bufferSize, inputMask, outputMask);
}

unique_ptr<daudio::AudioSession> daudio::open(const daudio::AudioDevice& device)
{
    auto d = dminternal::internalDevice(device.id);
    return d.first->addSession();
}

std::unique_ptr<daudio::CallbackToken> daudio::registerCallback(daudio::AudioSession& session,
                                                                daudio::AudioCallback callback)
{
    auto d = dminternal::internalDevice(session);
    return d.first->addCallback(callback);
}

std::unique_ptr<daudio::CallbackToken> daudio::registerCallback(
    daudio::AudioSession& session,
    daudio::AudioCallbackRO readonly_callback)
{
    auto d = dminternal::internalDevice(session);
    return d.first->addReadOnlyCallback(readonly_callback);
}

void daudio::waitForTermination(daudio::AudioSession& session, const int timeout_ms /*= -1*/)
{
    auto& s = dynamic_cast<dminternal::AudioSession&>(session);
    s.waitForTermination(timeout_ms);
}

pair<daudio::ActiveChannels, daudio::ActiveChannels> daudio::allChannels(
    const daudio::AudioDevice& device)
{
    daudio::ActiveChannels inmask, outmask;
    inmask.channels       = std::vector<bool>(device.inputchannels, true);
    outmask.channels      = std::vector<bool>(device.outputchannels, true);
    outmask.markedChannel = inmask.markedChannel = 0;
    return std::make_pair(std::move(inmask), std::move(outmask));
}
