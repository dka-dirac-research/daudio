#include <daudio/volume.h>

#include <dminternal/device.h>
#include <dminternal/dmholder.h>

bool daudio::hasInputGainControl(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->hasInputGainControl();
}

float daudio::getInputGain(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->getInputGain();
}

void daudio::setInputGain(const daudio::AudioDevice& device, float value)
{
    return dminternal::internalDevice(device.id).first->setInputGain(value);
}

bool daudio::getInputMuted(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->getInputMuted();
}

void daudio::setInputMuted(const daudio::AudioDevice& device, bool mute)
{
    return dminternal::internalDevice(device.id).first->setInputMuted(mute);
}

std::unique_ptr<daudio::CallbackToken> daudio::registerInputGainChangeCallback(
    const daudio::AudioDevice& device,
    notification_callback_type callback)
{
    return dminternal::internalDevice(device.id).first->addInputGainChangeCallback(callback);
}

bool daudio::hasOutputVolumeControl(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->hasOutputVolumeControl();
}

float daudio::getOutputVolume(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->getOutputVolume();
}

void daudio::setOutputVolume(const daudio::AudioDevice& device, float value)
{
    return dminternal::internalDevice(device.id).first->setOutputVolume(value);
}

bool daudio::getOutputMuted(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->getOutputMuted();
}

void daudio::setOutputMuted(const daudio::AudioDevice& device, bool mute)
{
    return dminternal::internalDevice(device.id).first->setOutputMuted(mute);
}

std::unique_ptr<daudio::CallbackToken> daudio::registerOutputVolumeChangeCallback(
    const daudio::AudioDevice& device,
    notification_callback_type callback)
{
    return dminternal::internalDevice(device.id).first->addOutputVolumeChangeCallback(callback);
}
