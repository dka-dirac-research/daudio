#include <algorithm>
#include <cassert>
#include <stdexcept>

#include "dminternal/device.h"
#include "dminternal/devicemanager.h"
#include "dminternal/dmholder.h"
#include "dminternal/internal_session.h"

using namespace std;
using namespace dminternal;

namespace
{
    shared_timed_mutex dm_lock;

    struct DmHolder {
        DmHolder() : dm(make_shared<DeviceManager>()) { dm->init(); }
        ~DmHolder() { dm->uninitialize(); }
        shared_ptr<DeviceManager> dm;
    };

    pair<shared_ptr<DeviceManager>, shared_lock<shared_timed_mutex>> init()
    {
        thread_local PlatformInitializer pi;
        shared_lock<shared_timed_mutex> lock{dm_lock};
        static DmHolder holder;
        return make_pair(holder.dm, move(lock));
    }
}  // namespace

vector<daudio::AudioDevice> dminternal::allDevices()
{
    auto dm = init();
    return dm.first->allDevices();
}

void dminternal::release()
{
    auto dm = init();
    dm.first->uninitialize();
}

pair<std::shared_ptr<dminternal::Device>, shared_lock<shared_timed_mutex>>
dminternal::internalDevice(device_id_t device_id)
{
    auto dm  = init();
    auto dev = dm.first->find(device_id);
    return make_pair(dev, move(dm.second));
}

pair<std::shared_ptr<dminternal::Device>, shared_lock<shared_timed_mutex>>
dminternal::internalDevice(const daudio::AudioSession& session)
{
    auto s = dynamic_cast<const dminternal::AudioSession*>(&session);
    if (s == nullptr)
        throw invalid_argument("invalid session object");

    auto dm = init();

    auto dev = dm.first->find(s->getDeviceId());

    if (!dev->hasOpenSessions()) {
        throw invalid_argument("session closed");
    }

    return make_pair(dev, move(dm.second));
}

daudio::AudioDevice dminternal::registerLoopbackDevice(int inputs,
                                                       int outputs,
                                                       float fs,
                                                       int buffer_size)
{
    auto dm = init();
    return dm.first->registerLoopbackDevice(inputs, outputs, fs, buffer_size);
}

std::unique_ptr<daudio::CallbackToken> dminternal::registerDevicesChangedCallback(
    std::function<void()> callback)
{
    auto dm = init();
    return dm.first->registerDevicesChangedCallback(callback);
}

void dminternal::setTypeToList(daudio::AudioDeviceType type_to_list)
{
    auto dm = init();
    return dm.first->setTypeToList(type_to_list);
}

void dminternal::setListAllTypes()
{
    auto dm = init();
    return dm.first->setListAllTypes();
}

void dminternal::setLoopBackEnabled(bool should_be_enabled)
{
    auto dm = init();
    return dm.first->setLoopBackEnabled(should_be_enabled);
}
