#include "daudio/audio_device.h"

namespace
{
    device_id_t get_device_id(const std::string& name)
    {
        std::hash<std::string> h;
        return h(name);
    }
}  // namespace

daudio::AudioDeviceType daudio::AudioDevice::typeFromName(std::string name)
{
    if (name == "ALSA")
        return AudioDeviceType::alsa;
    if (name == "ASIO")
        return AudioDeviceType::asio;
    if (name == "CoreAudio")
        return AudioDeviceType::coreaudio;
    if (name == "OpenSLES")
        return AudioDeviceType::opensles;
    if (name == "WASAPI")
        return AudioDeviceType::wasapi;
    if (name == "Loopback")
        return AudioDeviceType::loopback;
    if (name == "Oboe")
        return AudioDeviceType::oboe;

    return AudioDeviceType::unknown;
}

std::string daudio::AudioDevice::nameFromType(AudioDeviceType type)
{
    switch (type) {
    case AudioDeviceType::alsa:
        return "ALSA";
    case AudioDeviceType::asio:
        return "ASIO";
    case AudioDeviceType::coreaudio:
        return "CoreAudio";
    case AudioDeviceType::opensles:
        return "OpenSLES";
    case AudioDeviceType::wasapi:
        return "WASAPI";
    case AudioDeviceType::loopback:
        return "Loopback";
    case AudioDeviceType::oboe:
        return "Oboe";
    case AudioDeviceType::unknown:
    default:
        return "Unknown";
    }
}

daudio::AudioDevice const daudio::AudioDevice::none{};

daudio::AudioDevice::AudioDevice(const std::string& name,
                                 AudioDeviceType type,
                                 int inputchannels,
                                 int outputchannels,
                                 bool is_default)
    : name{name}
    , type{type}
    , inputchannels{inputchannels}
    , outputchannels{outputchannels}
    , is_default{is_default}
    , id{get_device_id(name + nameFromType(type) + std::to_string(inputchannels) + "/" +
                       std::to_string(outputchannels))}
{
}

bool daudio::AudioDevice::operator==(const AudioDevice& other) const { return (id == other.id); }

bool daudio::AudioDevice::operator!=(const AudioDevice& other) const { return (id != other.id); }

std::string daudio::AudioDevice::typeName() const { return nameFromType(type); }
