#include <daudio/loopback.h>
#include <dminternal/dmholder.h>
#include <dminternal/internal_loopback.h>

using namespace std;

daudio::AudioDevice daudio::registerLoopback(int inputCount,
                                             int outputCount,
                                             float fs,
                                             int bufferSize)
{
    return dminternal::registerLoopbackDevice(inputCount, outputCount, fs, bufferSize);
}

void daudio::setTransferFunc(const daudio::AudioDevice& device, daudio::AudioCallback h)
{
    auto dev = dminternal::internalDevice(device.id);
    dev.first->setTransferFunc(h);
}
