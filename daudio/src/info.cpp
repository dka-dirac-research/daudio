#include "daudio/info.h"

#include <iterator>

#include "dminternal/device.h"
#include "dminternal/dmholder.h"

float daudio::currentSamplerate(const daudio::AudioSession& session)
{
    return dminternal::internalDevice(session).first->currentSampleRate();
}

int daudio::currentBuffersize(const daudio::AudioSession& session)
{
    return dminternal::internalDevice(session).first->currentBuffersize();
}

daudio::ActiveChannels daudio::currentInputChannels(const daudio::AudioSession& session)
{
    return dminternal::internalDevice(session).first->activeInputChannels();
}

daudio::ActiveChannels daudio::currentOutputChannels(const daudio::AudioSession& session)
{
    return dminternal::internalDevice(session).first->activeOutputChannels();
}

std::vector<float> daudio::supportedSamplerates(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->supportedSamplerates();
}

float daudio::defaultSamplerate(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->defaultSampleRate();
}

std::vector<int> daudio::supportedBufferSizes(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->supportedBuffersizes();
}

int daudio::defaultBuffersize(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->defaultBuffersize();
}

daudio::AudioDevice daudio::deviceForSession(const daudio::AudioSession& session)
{
    return dminternal::internalDevice(session).first->externalDevice();
}

std::vector<std::string> daudio::inputChannelNames(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->inputChannelNames();
}

std::vector<std::string> daudio::outputChannelNames(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->outputChannelNames();
}

bool daudio::isOpen(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->hasOpenSessions();
}

std::map<std::string, std::string> daudio::getKeyValuePairs(const daudio::AudioDevice& device)
{
    return dminternal::internalDevice(device.id).first->getKeyValueMap();
}
