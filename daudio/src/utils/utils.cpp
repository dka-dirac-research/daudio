#include <dminternal/utils.h>
#include <string>

std::string utils::ltrim(const std::string& str, const std::string& chars)
{
    std::string retval(str);
    retval.erase(0, retval.find_first_not_of(chars));
    return retval;
}

std::string utils::rtrim(const std::string& str, const std::string& chars)
{
    std::string retval(str);
    retval.erase(retval.find_last_not_of(chars) + 1);
    return retval;
}

std::string utils::trim(const std::string& str, const std::string& chars)
{
    return ltrim(rtrim(str, chars), chars);
}
