#include "dminternal/internal_session.h"

#include <dminternal/dmholder.h>

using namespace std;

dminternal::AudioSession::AudioSession(std::shared_ptr<dminternal::Device> device)
    : device_(device), token_(device->addSessionTerminatedCallback([&](exception_ptr e) {
        lock_guard<mutex> guard{m_};
        error_ = move(e);
        cv_.notify_one();
    }))
{
}

dminternal::AudioSession::~AudioSession()
{
    try {
        if (auto device = device_.lock())
            device->removeSession();
    } catch (...) {
    }
}

void dminternal::AudioSession::waitForTermination(int millisecs /* = -1 */)
{
    unique_lock<mutex> lock{m_};
    if (cv_.wait_for(lock, chrono::milliseconds(millisecs), [&] { return !!error_; }))
        rethrow_exception(error_);
}

device_id_t dminternal::AudioSession::getDeviceId() const
{
    if (auto device = device_.lock())
        return device->id();
    return 0;
}
