#include "dminternal/platform.h"

#ifdef _WIN32
#include <windows.h>
// Needs COINIT_APARTMENTTHREADED for ASIO to work
dminternal::PlatformInitializer::PlatformInitializer()
{
    ps_init = (uint64_t)CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
}
dminternal::PlatformInitializer::~PlatformInitializer()
{
    if (SUCCEEDED((HRESULT)ps_init))
        CoUninitialize();
}
#else
dminternal::PlatformInitializer::PlatformInitializer() {}
dminternal::PlatformInitializer::~PlatformInitializer() {}
#endif
