
#include "dminternal/threadutil.h"

#include "log/log.hpp"

#ifdef _WIN32
#include <windows.h>

#include <avrt.h>

void dminternal::AudioThread::setSchedulerHighPrio()
{
    // Maximize prio for thread
    DWORD taskIndex = 0;
    HANDLE h        = AvSetMmThreadCharacteristicsW(L"Pro Audio", &taskIndex);
    if (h != 0) {
        AvSetMmThreadPriority(h, AVRT_PRIORITY_CRITICAL);
    } else
        LOG_INFO("Failed getting handle from AvSetMmThreadCharacteristicsW");
}

#else
#ifdef __linux__

void dminternal::AudioThread::setSchedulerHighPrio()
{
    struct sched_param sched_param;

    if (sched_getparam(0, &sched_param) < 0) {
        LOG_WARN("Scheduler getparam failed");
        return;
    }

    sched_param.sched_priority = sched_get_priority_max(SCHED_RR);
    if (!sched_setscheduler(0, SCHED_RR, &sched_param)) {
        LOG_DEBUG("Scheduler set to Round Robin with priority " << sched_param.sched_priority);
        return;
    }

    LOG_INFO("!!!Scheduler set to Round Robin with priority " << sched_param.sched_priority
                                                              << " FAILED!");
}

#else
#endif
#endif