#include "dminternal/active.h"

using namespace std;

namespace
{
    void active_worker(bool* done,
                       mutex* mq_lock,
                       condition_variable* cv,
                       dminternal::MessageQueue* mq)
    {
        while (!*done) {
            unique_lock<mutex> lock{*mq_lock};

            if (mq->empty()) {
                cv->wait(lock);
                continue;
            }

            auto m = mq->front();
            mq->pop();
            lock.unlock();

            m();
        }
    }
}  // namespace

dminternal::Active::Active() : thd(active_worker, &done, &mq_lock, &mq_cv, &mq) {}

dminternal::Active::~Active()
{
    send([this] { done = true; });
    thd.join();
}

void dminternal::Active::send(Message m)
{
    {
        lock_guard<mutex> guard{mq_lock};
        mq.push(m);
    }
    mq_cv.notify_one();
}

void dminternal::Active::wait()
{
    bool wait_done = false;
    mutex m;
    condition_variable cv;
    send([&] {
        lock_guard<mutex> lock{m};
        wait_done = true;
        cv.notify_one();
    });
    unique_lock<mutex> lock{m};
    cv.wait(lock, [&] { return wait_done; });
}
