#include <dminternal/callbacktoken.h>

dminternal::CallbackToken::CallbackToken(function_type fn) : fn_(fn) {}

dminternal::CallbackToken::~CallbackToken() { fn_(); }
