#include <algorithm>
#include <stdexcept>

#include "dminternal/device.h"
#include "dminternal/internal_loopback.h"
#include "dminternal/internal_session.h"

using namespace std;
using namespace dminternal;

dminternal::Loopback::Loopback(int index,
                               int num_inputs,
                               int num_outputs,
                               float fs,
                               int buffer_size)
    : Device{daudio::AudioDevice("Loopback-" + to_string(index),
                                 daudio::AudioDeviceType::loopback,
                                 num_inputs,
                                 num_outputs,
                                 false)}
    , buffer_size{buffer_size}
    , fs{fs}
    , period{static_cast<long long>(buffer_size * 1000000000.0 / fs)}
    , in(num_inputs, buffer_size, fs)
    , out(num_outputs, buffer_size, fs)
{
    if (num_inputs < 0 || num_outputs < 0) {
        throw invalid_argument("number of inputs/outputs must be non-negative");
    }

    if (buffer_size < 64) {
        throw invalid_argument("buffer size must be at least 64 samples");
    }

    if (fs < 20.f) {
        throw invalid_argument("samplerate must be larger than 20Hz");
    }
}

dminternal::Loopback::~Loopback()
{
    // cf
    close();
}

void dminternal::Loopback::process()
{
    vector<float> mixed(buffer_size);

#ifdef NDEBUG
    auto next = chrono::high_resolution_clock::now();
#endif

    while (!stop_work_thread) {
#ifdef NDEBUG
        next += period;
#else
        // A bit of drift is better than inexplicable behavior when debugging
        // In debug the timing will not be as exact, but the process loop
        // will also pause when you hit a breakpoint somewhere else.
        const auto next = chrono::high_resolution_clock::now() + period;
#endif
        if (!h) {
            fill(mixed.begin(), mixed.end(), 0.f);

            for (auto& o : out.waveforms) {
                for (int i = 0; i < buffer_size; ++i)
                    mixed[i] += o[i];
                fill(o.begin(), o.end(), 0.f);
            }

            for (auto& i : in.waveforms)
                i = mixed;
        } else {
            daudio::OutputBufferView transView;
            for (auto& ch : in.waveforms)
                transView.channel.emplace_back(ch.data(), ch.data() + buffer_size);

            {
                unique_lock<mutex> guard_lock{lock, try_to_lock};
                if (guard_lock.owns_lock())
                    h(out.view(), transView);
            }

            for (auto& ch : out.waveforms)
                fill(ch.begin(), ch.end(), 0.f);
        }

        auto inView = in.view();

        daudio::OutputBufferView outView;
        for (auto& ch : out.waveforms)
            outView.channel.emplace_back(ch.data(), ch.data() + buffer_size);

        callCallbacks(inView, outView);

        this_thread::sleep_until(next);
    }
}

vector<float> dminternal::Loopback::supportedSamplerates() const { return vector<float>{fs}; }
vector<int> dminternal::Loopback::supportedBuffersizes() const { return vector<int>{buffer_size}; }
float dminternal::Loopback::currentSampleRate() const { return fs; }
int dminternal::Loopback::currentBuffersize() const { return buffer_size; }
vector<string> dminternal::Loopback::inputChannelNames() const
{
    vector<string> names;
    const auto& dev = this->externalDevice();
    for (int i = 0; i < dev.inputchannels; ++i) {
        names.emplace_back("Input " + to_string(i));
    }

    return names;
}

vector<std::string> dminternal::Loopback::outputChannelNames() const
{
    vector<string> names;
    const auto& dev = this->externalDevice();
    for (int i = 0; i < dev.outputchannels; ++i) {
        names.emplace_back("Speaker " + to_string(i));
    }

    return names;
}

void dminternal::Loopback::setTransferFunc(daudio::AudioCallback h)
{
    lock_guard<mutex> guard_lock{lock};
    this->h = move(h);
}

float dminternal::Loopback::defaultSampleRate() const { return 48000.0f; }

int dminternal::Loopback::defaultBuffersize() const { return 512; }

void dminternal::Loopback::open(float fs,
                                int bufferSize,
                                daudio::ActiveChannels inputMask,
                                daudio::ActiveChannels outputMask)
{
    (void)fs;
    (void)bufferSize;
    if (!opened) {
        stop_work_thread = false;
        std::thread tmp(&Loopback::process, this);
        work_thread.swap(tmp);
        opened            = true;
        activeInChannels  = inputMask;
        activeOutChannels = outputMask;
    }
}

void dminternal::Loopback::close()
{
    if (opened) {
        opened           = false;
        stop_work_thread = true;
        work_thread.join();
    }
}

bool dminternal::Loopback::isOpened() const { return !!opened; }

const daudio::ActiveChannels& dminternal::Loopback::activeInputChannels() const
{
    return activeInChannels;
}

const daudio::ActiveChannels& dminternal::Loopback::activeOutputChannels() const
{
    return activeOutChannels;
}

namespace
{
    struct LoopbackDeviceType : dminternal::DeviceType {
        vector<shared_ptr<Loopback>> devices_;
        atomic<int> next_id_;
        mutex m_;

        LoopbackDeviceType() : next_id_{0}
        {
            for (int io : {2, 4, 6, 8, 12}) {
                registerNewDevice(io, io, 48000.0f, 512);
            }
        }

        daudio::AudioDevice registerNewDevice(int inputs, int outputs, float fs, int buffer_size)
        {
            auto id = ++next_id_;
            lock_guard<mutex> lock{m_};
            auto dev = make_shared<Loopback>(id, inputs, outputs, fs, buffer_size);
            devices_.push_back(dev);
            return devices_.back()->externalDevice();
        }

        virtual std::vector<daudio::AudioDevice> devices() override
        {
            std::vector<daudio::AudioDevice> devs;
            lock_guard<mutex> lock{m_};
            for (auto& entry : devices_)
                devs.emplace_back(entry->externalDevice());
            return devs;
        }

        virtual std::shared_ptr<Device> getDevice(device_id_t id) override
        {
            lock_guard<mutex> lock{m_};
            for (auto& entry : devices_) {
                if (entry->externalDevice().id == id)
                    return entry;
            }
            return nullptr;
        }
    };
}  // namespace

namespace dminternal
{
    template <>
    unique_ptr<dminternal::DeviceType> DeviceType::create<daudio::AudioDeviceType::loopback>(
        function<void()>)
    {
        return make_unique<LoopbackDeviceType>();
    }

    daudio::AudioDevice createNewLoopbackDevice(DeviceType& dev_type,
                                                int inputs,
                                                int outputs,
                                                float fs,
                                                int buffer_size)
    {
        auto dt = dynamic_cast<LoopbackDeviceType*>(&dev_type);
        if (dt == nullptr)
            throw bad_cast();
        return dt->registerNewDevice(inputs, outputs, fs, buffer_size);
    }
}  // namespace dminternal
