#include "dminternal/device.h"

#include "dminternal/callbacktoken.h"
#include "dminternal/internal_session.h"
#include "dminternal/mathutil.h"
#include "dminternal/utils.h"

using namespace std;

dminternal::Device::Device(const daudio::AudioDevice& device) : device{device}, num_sessions{0} {}

bool dminternal::Device::hasInputGainControl() const { return false; }
float dminternal::Device::getInputGain() const { throw logic_error("not supported"); }
void dminternal::Device::setInputGain(float) { throw logic_error("not supported"); }
bool dminternal::Device::getInputMuted() const { throw logic_error("not supported"); }
void dminternal::Device::setInputMuted(bool) { throw logic_error("not supported"); }

bool dminternal::Device::hasOutputVolumeControl() const { return false; }
float dminternal::Device::getOutputVolume() const { throw logic_error("not supported"); }
void dminternal::Device::setOutputVolume(float) { throw logic_error("not supported"); }
bool dminternal::Device::getOutputMuted() const { throw logic_error("not supported"); }
void dminternal::Device::setOutputMuted(bool) { throw logic_error("not supported"); }

std::unique_ptr<daudio::CallbackToken> dminternal::Device::addCallback(
    daudio::AudioCallback callback)
{
    lock_guard<mutex> guard_lock{add_lock};
    auto cb_id = dminternal::add_callback(callbacks, callback);
    return make_unique<dminternal::CallbackToken>([cb_id, weak_this = weak_from_this()] {
        if (auto p = weak_this.lock()) {
            lock_guard<mutex> guard_lock{p->add_lock};
            p->callbacks.erase(cb_id);
        }
    });
}

std::unique_ptr<daudio::CallbackToken> dminternal::Device::addReadOnlyCallback(
    daudio::AudioCallbackRO callback)
{
    lock_guard<mutex> guard_lock{add_lock};
    auto cb_id = dminternal::add_callback(readonly_callbacks, callback);
    return make_unique<dminternal::CallbackToken>([cb_id, weak_this = weak_from_this()] {
        if (auto p = weak_this.lock()) {
            lock_guard<mutex> guard_lock{p->add_lock};
            p->readonly_callbacks.erase(cb_id);
        }
    });
}

std::unique_ptr<daudio::AudioSession> dminternal::Device::addSession()
{
    auto fs = hasOpenSessions() ? currentSampleRate() : defaultSampleRate();
    return addSession(fs);
}

std::unique_ptr<daudio::AudioSession> dminternal::Device::addSession(float fs)
{
    auto buf_size = hasOpenSessions() ? currentBuffersize() : defaultBuffersize();
    return addSession(fs, buf_size);
}

std::unique_ptr<daudio::AudioSession> dminternal::Device::addSession(float fs, int buffer_size)
{
    daudio::ActiveChannels in_channels;
    daudio::ActiveChannels out_channels;
    if (hasOpenSessions()) {
        in_channels  = activeInputChannels();
        out_channels = activeOutputChannels();
    } else {
        in_channels.channels.resize(inputChannelNames().size(), true);
        out_channels.channels.resize(outputChannelNames().size(), true);
    }
    return addSession(fs, buffer_size, in_channels, out_channels);
}

std::unique_ptr<daudio::AudioSession> dminternal::Device::addSession(
    float fs,
    int buffer_size,
    daudio::ActiveChannels in_channels,
    daudio::ActiveChannels out_channels)
{
    if (num_sessions == 0) {
        open(fs, buffer_size, in_channels, out_channels);
    } else {
        if (int(fs) != int(currentSampleRate()))
            throw std::invalid_argument("invalid sample rate");
        if (buffer_size != currentBuffersize())
            throw std::invalid_argument("invalid buffer size");
        if (in_channels.channels != activeInputChannels().channels)
            throw std::invalid_argument("invalid input mask");
        if (out_channels.channels != activeOutputChannels().channels)
            throw std::invalid_argument("invalid output mask");
    }
    ++num_sessions;
    return unique_ptr<daudio::AudioSession>(new AudioSession(shared_from_this()));
}

bool dminternal::Device::hasOpenSessions() { return !!num_sessions; }

const std::map<std::string, std::string>& dminternal::Device::getKeyValueMap() const
{
    return keyValueMap;
}

void dminternal::Device::removeSession()
{
    if (--num_sessions == 0) {
        close();
    }
}

std::unique_ptr<daudio::CallbackToken> dminternal::Device::addSessionTerminatedCallback(
    std::function<void(std::exception_ptr)> fn)
{
    lock_guard<mutex> guard{add_lock};
    auto cb_id = dminternal::add_callback(terminated_callbacks, fn);
    return make_unique<dminternal::CallbackToken>([cb_id, weak_this = weak_from_this()] {
        if (auto p = weak_this.lock()) {
            lock_guard<mutex> guard_lock{p->add_lock};
            p->terminated_callbacks.erase(cb_id);
        }
    });
}

std::unique_ptr<daudio::CallbackToken> dminternal::Device::addInputGainChangeCallback(
    notification_callback_type fn)
{
    lock_guard<mutex> guard{add_lock};
    auto cb_id = dminternal::add_callback(input_gain_callbacks, fn);
    return make_unique<dminternal::CallbackToken>([cb_id, weak_this = weak_from_this()] {
        if (auto p = weak_this.lock()) {
            lock_guard<mutex> guard_lock{p->add_lock};
            p->input_gain_callbacks.erase(cb_id);
        }
    });
}

std::unique_ptr<daudio::CallbackToken> dminternal::Device::addOutputVolumeChangeCallback(
    notification_callback_type fn)
{
    lock_guard<mutex> guard{add_lock};
    auto cb_id = dminternal::add_callback(output_volume_callbacks, fn);
    return make_unique<dminternal::CallbackToken>([cb_id, weak_this = weak_from_this()] {
        if (auto p = weak_this.lock()) {
            lock_guard<mutex> guard_lock{p->add_lock};
            p->output_volume_callbacks.erase(cb_id);
        }
    });
}

void dminternal::Device::setTransferFunc(daudio::AudioCallback)
{
    throw std::logic_error("transfer functions are not supported for this type of device");
}

device_id_t dminternal::Device::id() const { return device.id; }

void dminternal::Device::callCallbacks(daudio::InputBufferView& inView,
                                       daudio::OutputBufferView& outView)
{
    unique_lock<mutex> guard_lock(add_lock, try_to_lock);
    outView.is_silent = true;
    if (guard_lock.owns_lock()) {
        inView.is_silent  = inView.channel.empty();
        outView.is_silent = callbacks.empty();

        for (auto& callback : readonly_callbacks)
            callback.second(inView);

        for (auto& callback : callbacks)
            callback.second(inView, outView);
    }
    if (outView.is_silent) {
        for (auto& channel : outView.channel)
            fill(channel.begin(), channel.end(), 0.f);
    }
}

void dminternal::Device::inputGainChanged(float value, bool muted, bool internal_change)
{
    unique_lock<mutex> guard_lock(add_lock, try_to_lock);
    if (guard_lock.owns_lock()) {
        for (auto& callback : input_gain_callbacks) {
            try {
                callback.second(value, muted, internal_change);
            } catch (...) {
            }
        }
    }
}

void dminternal::Device::outputVolumeChanged(float value, bool muted, bool internal_change)
{
    unique_lock<mutex> guard_lock(add_lock, try_to_lock);
    if (guard_lock.owns_lock()) {
        for (auto& callback : output_volume_callbacks) {
            try {
                callback.second(value, muted, internal_change);
            } catch (...) {
            }
        }
    }
}

void dminternal::Device::sessionTerminated(std::exception_ptr e)
{
    lock_guard<mutex> guard{add_lock};
    for (auto& callback : terminated_callbacks)
        (callback.second)(e);
}

vector<float> dminternal::Device::possibleSampleRates()
{
    return {22050.0f,
            32000.0f,
            44100.0f,
            48000.0f,
            64000.0f,
            88200.0f,
            96000.0f,
            128000.0f,
            176400.0f,
            192000.0f};
}

vector<int> dminternal::Device::possibleBufferSizes(int min_size, int max_size)
{
    vector<int> retval;
    auto n   = dminternal::next_power_of_2(unsigned(min_size));
    max_size = min(8192, max_size);
    while (n <= unsigned(max_size)) {
        retval.push_back(n);
        n += dminternal::next_power_of_2(n / 3);
    }
    return retval;
}
