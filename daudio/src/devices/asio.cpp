#include <dminternal/device.h>

#include <future>
#include <set>
using namespace std;

#include <Shlwapi.h>
#include <windows.h>

#include <assert.h>

#include <algorithm>
#include <codecvt>
#include <future>
#include <limits>
#include <map>
#include <mutex>
#include <set>

#include <asiodrvr.h>

#include <dminternal/comutils.h>
#include <dminternal/platform.h>
#include <dminternal/sampleformat.h>
#include <dminternal/threadutil.h>
using dminternal::ComSmartPtr;

#include <log/log.hpp>

#undef min
#undef max

#define ASIOCALLBACK __cdecl

namespace
{
    // convert wstring to UTF-8 string
    static string wstring_to_utf8(const wstring& str)
    {
        wstring_convert<codecvt_utf8<wchar_t>> myconv;
        return myconv.to_bytes(str);
    }

    struct AsioDevice;

    namespace callback_handling
    {
        static AsioDevice* volatile asio_devices[32] = {0};
        constexpr auto asio_max_devices_count = sizeof(asio_devices) / sizeof(asio_devices[0]);
        mutex devMutex;

        template <typename Type, typename... Args>
        unique_ptr<Type> create(Args... args)
        {
            lock_guard<mutex> lock{devMutex};
            auto slotIt = std::find_if(asio_devices,
                                       asio_devices + asio_max_devices_count,
                                       [&](auto p) { return p == nullptr; });
            if (slotIt == asio_devices + asio_max_devices_count)
                throw runtime_error("too many opened ASIO devices");
            return move(make_unique<Type>(args..., int(distance(asio_devices, slotIt))));
        }

        void release(AsioDevice* p)
        {
            lock_guard<mutex> lock{devMutex};
            auto it = std::find_if(asio_devices,
                                   asio_devices + asio_max_devices_count,
                                   [&](auto _p) { return p == _p; });
            if (it != asio_devices + asio_max_devices_count)
                *it = nullptr;
        }
    }  // namespace callback_handling

    struct IDeviceActive {
        virtual ~IDeviceActive() {}
        virtual void setActive(wstring id) = 0;
        //         virtual void startStreaming(int period_us) = 0;
        //         virtual void pingStreaming()               = 0;
        //         virtual void stopStreaming()               = 0;
        virtual void setInactive(wstring id)       = 0;
        virtual void notifyDeviceError(wstring id) = 0;
    };

    struct AsioDevice : dminternal::Device {
        ComSmartPtr<IASIO> asio_object_;
        const wstring class_id_;
        const string device_name_;
        long in_channels_{0};
        long out_channels_{0};
        vector<float> rates;
        float current_rate_{0};
        vector<int> buffer_sizes_;
        int current_buffersize_{0};
        int preferred_buffersize_{0};

        bool post_output_{false};
        std::atomic<bool> opened{false};

        vector<ASIOBufferInfo> buffer_infos_;
        vector<ASIOClockSource> clocks;

        struct ChannelData {
            ChannelData(long type, long buffer_size, ASIOBufferInfo& info)
                : format(static_cast<dminternal::SampleFormat::Type>(type))
                , buffer(buffer_size, 0.f)
                , buffer_info(info)
            {
            }
            dminternal::SampleFormat format;
            vector<float> buffer;
            ASIOBufferInfo& buffer_info;
        };

        vector<ChannelData> in_buffers_;
        vector<ChannelData> out_buffers_;

        daudio::ActiveChannels active_input_channels_;
        daudio::ActiveChannels active_output_channels_;

        daudio::InputBufferView input_bufferview_;
        daudio::OutputBufferView output_bufferview_;

        ASIOCallbacks asio_callbacks_;
        std::shared_ptr<IDeviceActive> active_;

        std::unique_ptr<dminternal::TimerThread> callbackMonitorThread_;
        atomic<bool> callbackCalled_{false};
        mutex monitorMutex_;

        template <int deviceIndex>
        struct ASIOCallbackFunctions {
            static ASIOTime* ASIOCALLBACK bufferSwitchTimeInfoCallback(ASIOTime*, long index, long)
            {
                if (callback_handling::asio_devices[deviceIndex] != nullptr)
                    callback_handling::asio_devices[deviceIndex]->callback(index);

                return nullptr;
            }

            static void ASIOCALLBACK bufferSwitchCallback(long index, long)
            {
                if (callback_handling::asio_devices[deviceIndex] != nullptr)
                    callback_handling::asio_devices[deviceIndex]->callback(index);
            }

            static long ASIOCALLBACK asioMessagesCallback(long selector, long value, void*, double*)
            {
                return callback_handling::asio_devices[deviceIndex] != nullptr
                           ? callback_handling::asio_devices[deviceIndex]->asioMessagesCallback(
                                 selector, value)
                           : 0;
            }

            static void ASIOCALLBACK sampleRateChangedCallback(ASIOSampleRate)
            {
                if (callback_handling::asio_devices[deviceIndex] != nullptr)
                    callback_handling::asio_devices[deviceIndex]->resetRequest(
                        runtime_error("asio: sample rate changed"));
            }

            static void setCallbacks(ASIOCallbacks& callbacks) noexcept
            {
                callbacks.bufferSwitch         = &bufferSwitchCallback;
                callbacks.asioMessage          = &asioMessagesCallback;
                callbacks.bufferSwitchTimeInfo = &bufferSwitchTimeInfoCallback;
                callbacks.sampleRateDidChange  = &sampleRateChangedCallback;
            }

            static void setCallbacksForDevice(ASIOCallbacks& callbacks, AsioDevice* device) noexcept
            {
                if (callback_handling::asio_devices[deviceIndex] == device)
                    setCallbacks(callbacks);
                else
                    ASIOCallbackFunctions<deviceIndex + 1>::setCallbacksForDevice(callbacks,
                                                                                  device);
            }
        };

        template <>
        struct ASIOCallbackFunctions<callback_handling::asio_max_devices_count> {
            static void setCallbacksForDevice(ASIOCallbacks&, AsioDevice*) noexcept {}
        };

        void testDevicePresence()
        {
            if (asio_object_ && !opened) {
                daudio::ActiveChannels in;
                in.channels.resize(in_channels_, true);
                daudio::ActiveChannels out;
                out.channels.resize(out_channels_, true);

                prepareForStart(defaultSampleRate(), defaultBuffersize(), in, out);
                disposeOfBuffers();
            }
        }

        AsioDevice(std::shared_ptr<IDeviceActive> active,
                   std::wstring classId,
                   const std::wstring& name,
                   int slotIdx = -1)
            : AsioDevice(active, classId, wstring_to_utf8(name), slotIdx)
        {
        }

        AsioDevice(std::shared_ptr<IDeviceActive> active,
                   std::wstring classId,
                   const std::string& name,
                   int slotIdx = -1)
            : class_id_(classId)
            , active_(active)
            , Device{daudio::AudioDevice(name, daudio::AudioDeviceType::asio, -1, -1, false)}
        {
            if (initInterface(classId)) {
                if (active_) {
                    active_->setActive(classId);
                }

                if (slotIdx >= 0) {
                    assert(callback_handling::asio_devices[slotIdx] == nullptr);
                    callback_handling::asio_devices[slotIdx] = this;
                    ASIOCallbackFunctions<0>::setCallbacksForDevice(asio_callbacks_, this);
                }
                init();
                device.inputchannels  = in_channels_;
                device.outputchannels = out_channels_;
            }
        }

        ~AsioDevice()
        {
            close();
            releaseInterface();
            callback_handling::release(this);
            if (active_) {
                active_->setInactive(class_id_);
            }
        }

        bool initInterface(const std::wstring& classId)
        {
            CLSID cls_id;
            if (CLSIDFromString((LPOLESTR)classId.c_str(), &cls_id) == S_OK) {
                __try {
                    auto hr = CoCreateInstance(
                        cls_id, 0, CLSCTX_INPROC_SERVER, cls_id, (void**)&asio_object_);
                    if (SUCCEEDED(hr) && asio_object_ != nullptr) {
                        if (asio_object_->init(NULL)) {
                            auto result = asio_object_->getChannels(&in_channels_, &out_channels_);
                            if (result == ASE_OK) {
                                return true;
                            }
                        }
                    }
                } __except (EXCEPTION_EXECUTE_HANDLER) {
                }
            }
            releaseInterface();
            return false;
        }

        void releaseInterface()
        {
            __try {
                asio_object_ = nullptr;
            } __except (EXCEPTION_EXECUTE_HANDLER) {
            }
        }

        bool exists() const
        {
            return asio_object_ != nullptr && (in_channels_ > 0 || out_channels_ > 0);
        }

        void init()
        {
            // Retrieve supported sample rates
            for (auto rate : possibleSampleRates())
                if (asio_object_->canSampleRate(ASIOSampleRate(rate)) == ASE_OK)
                    rates.push_back(float(rate));

            ASIOSampleRate rate;
            if (asio_object_->getSampleRate(&rate) == ASE_OK)
                current_rate_ = float(rate);

            // Retrieve buffer sizes
            buffer_sizes_.clear();
            long minSize, maxSize, preferredSize, granularity;
            if (asio_object_->getBufferSize(&minSize, &maxSize, &preferredSize, &granularity) ==
                ASE_OK) {
                if (granularity >= 0) {
                    granularity = std::max(16, (int)granularity);

                    for (int i = std::max((int)(minSize + 15) & ~15, (int)granularity);
                         i < std::min(6400, (int)maxSize);
                         i += granularity)
                        buffer_sizes_.push_back(i);
                } else if (granularity < 0) {
                    for (int i = 0; i < 18; ++i) {
                        const int s = (1 << i);

                        if (s >= minSize && s <= maxSize)
                            buffer_sizes_.push_back(s);
                    }
                }
                buffer_sizes_.push_back(preferredSize);
                sort(buffer_sizes_.begin(), buffer_sizes_.end());
                buffer_sizes_.erase(unique(buffer_sizes_.begin(), buffer_sizes_.end()),
                                    buffer_sizes_.end());
                preferred_buffersize_ = preferredSize;
            }

            // Update clock sources
            long numSources = 1;
            clocks.resize(numSources);
            if (asio_object_->getClockSources(clocks.data(), &numSources) == ASE_OK) {
                if (numSources != 1) {
                    clocks.resize(numSources);
                    asio_object_->getClockSources(clocks.data(), &numSources);
                }
                if (none_of(clocks.begin(), clocks.end(), [](auto& clock) {
                        return clock.isCurrentSource;
                    })) {
                    asio_object_->setClockSource(clocks[0].index);
                }
            }
        }

        vector<ASIOBufferInfo> prepareIoBuffers(daudio::ActiveChannels inputs,
                                                daudio::ActiveChannels outputs)
        {
            vector<ASIOBufferInfo> bufferInfo;
            for (long i = 0; i < min(in_channels_, long(inputs.channels.size())); ++i) {
                if (!inputs.channels[i])
                    continue;

                ASIOBufferInfo info = {0};
                info.isInput        = true;
                info.channelNum     = i;
                bufferInfo.push_back(info);
            }

            for (long i = 0; i < min(out_channels_, long(outputs.channels.size())); ++i) {
                if (!outputs.channels[i])
                    continue;

                ASIOBufferInfo info = {0};
                info.isInput        = false;
                info.channelNum     = i;
                bufferInfo.push_back(info);
            }
            return move(bufferInfo);
        }

        virtual float defaultSampleRate() const override
        {
            float defRate = rates[0];
            for (auto rate : rates) {
                if (rate > 48000.f)
                    break;
                defRate = rate;
            }
            return defRate;
        }

        virtual int defaultBuffersize() const override { return preferred_buffersize_; }

        virtual std::vector<float> supportedSamplerates() const override { return rates; }

        virtual std::vector<int> supportedBuffersizes() const override { return buffer_sizes_; }

        vector<string> getChannelNames(bool input) const
        {
            vector<string> names;
            const int num = input ? in_channels_ : out_channels_;
            for (int i = 0; i < num; ++i) {
                ASIOChannelInfo info;
                info.channel = i;
                info.isInput = input;
                if (asio_object_->getChannelInfo(&info) == ASE_OK)
                    names.push_back(info.name);
            }
            return move(names);
        }

        virtual std::vector<std::string> inputChannelNames() const override
        {
            return move(getChannelNames(true));
        }

        virtual std::vector<std::string> outputChannelNames() const override
        {
            return move(getChannelNames(false));
        }

        void prepareForStart(float fs,
                             int bufferSize,
                             daudio::ActiveChannels inputMask,
                             daudio::ActiveChannels outputMask)
        {
            auto res = asio_object_->setSampleRate(ASIOSampleRate(fs));
            if (res != ASE_OK)
                throw runtime_error("failed to set sample rate");

            if (find(buffer_sizes_.begin(), buffer_sizes_.end(), bufferSize) == buffer_sizes_.end())
                throw runtime_error("buffer size not accepted");

            // Prepare bufferInfo vector before createBuffers
            buffer_infos_ = prepareIoBuffers(inputMask, outputMask);

            res = asio_object_->createBuffers(
                buffer_infos_.data(), long(buffer_infos_.size()), bufferSize, &asio_callbacks_);
            if (res != ASE_OK)
                throw runtime_error("ASIO create buffers failed");
        }

        void disposeOfBuffers() { asio_object_->disposeBuffers(); }

        virtual void open(float fs,
                          int bufferSize,
                          daudio::ActiveChannels inputMask,
                          daudio::ActiveChannels outputMask) override
        {
            close();

            prepareForStart(fs, bufferSize, inputMask, outputMask);

            in_buffers_.clear();
            out_buffers_.clear();
            for (auto& info : buffer_infos_) {
                ASIOChannelInfo chInfo;
                chInfo.channel = info.channelNum;
                chInfo.isInput = info.isInput;
                asio_object_->getChannelInfo(&chInfo);
                chInfo.isInput ? in_buffers_.emplace_back(chInfo.type, bufferSize, info)
                               : out_buffers_.emplace_back(chInfo.type, bufferSize, info);
            }

            // Create input and output views
            input_bufferview_.channel.clear();
            for (auto& channel : in_buffers_)
                input_bufferview_.channel.emplace_back(
                    channel.buffer.data(), channel.buffer.data() + channel.buffer.size());
            output_bufferview_.channel.clear();
            for (auto& channel : out_buffers_)
                output_bufferview_.channel.emplace_back(
                    channel.buffer.data(), channel.buffer.data() + channel.buffer.size());

            post_output_ = (asio_object_->outputReady() == ASE_OK);

            if (asio_object_->start() != ASE_OK)
                throw runtime_error("ASIO start failed");

            current_rate_           = fs;
            current_buffersize_     = bufferSize;
            active_input_channels_  = inputMask;
            active_output_channels_ = outputMask;
            opened                  = true;

            auto period_ms  = (int((std::max)(20.0f, 4 * 1000 * bufferSize / fs)) / 10) * 10;
            callbackCalled_ = false;
            lock_guard<mutex> lock(monitorMutex_);
            callbackMonitorThread_ = std::make_unique<dminternal::TimerThread>(
                [this] {
                    if (!callbackCalled_) {
                        active_->notifyDeviceError(class_id_);
                        resetRequest(runtime_error("asio: callback timeout"));
                    }
                    callbackCalled_ = false;
                },
                period_ms);
        }

        virtual void close() override
        {
            if (opened) {
                {
                    lock_guard<mutex> lock(monitorMutex_);
                    callbackMonitorThread_ = nullptr;
                }

                auto res = asio_object_->stop();
                disposeOfBuffers();

                opened              = false;
                current_rate_       = 0.f;
                current_buffersize_ = 0;
                in_buffers_.clear();
                out_buffers_.clear();
                input_bufferview_.channel.clear();
                output_bufferview_.channel.clear();
                buffer_infos_.clear();
                active_input_channels_  = daudio::ActiveChannels{};
                active_output_channels_ = daudio::ActiveChannels{};
            }
        }

        virtual bool isOpened() const override { return opened; }

        virtual float currentSampleRate() const override { return current_rate_; }

        virtual int currentBuffersize() const override { return current_buffersize_; }

        virtual const daudio::ActiveChannels& activeInputChannels() const override
        {
            return active_input_channels_;
        }

        virtual const daudio::ActiveChannels& activeOutputChannels() const override
        {
            return active_output_channels_;
        }

        //////////////////////////////////////////////////////////////////////////
        // ASIO callback functions
        void resetRequest(std::exception& e)
        {
            lock_guard<mutex> lock(monitorMutex_);
            if (callbackMonitorThread_) {
                callbackMonitorThread_->stop();
            }
            sessionTerminated(make_exception_ptr(e));
        }

        long asioMessagesCallback(long selector, long value)
        {
            switch (selector) {
            case kAsioSelectorSupported:
                if (value == kAsioResetRequest || value == kAsioEngineVersion ||
                    value == kAsioResyncRequest || value == kAsioLatenciesChanged ||
                    value == kAsioSupportsInputMonitor)
                    return 1;
                break;

            case kAsioBufferSizeChange:
                resetRequest(runtime_error("asio: buffer size changed"));
                return 1;
            case kAsioResetRequest:
                resetRequest(runtime_error("asio: reset requested"));
                return 1;
            case kAsioResyncRequest:
                resetRequest(runtime_error("asio: resync requested"));
                return 1;
            case kAsioLatenciesChanged:
                return 1;
            case kAsioEngineVersion:
                return 2;

            case kAsioSupportsTimeInfo:
            case kAsioSupportsTimeCode:
            default:
                return ASE_OK;
            }

            return ASE_OK;
        }

        void callback(long index)
        {
            // Convert input to float buffers
            if (in_buffers_.size() > 0) {
                input_bufferview_.is_silent = false;
                for (auto& channel : in_buffers_)
                    channel.format.convertToFloat(channel.buffer_info.buffers[index],
                                                  channel.buffer.data(),
                                                  current_buffersize_);
            }

            callCallbacks(input_bufferview_, output_bufferview_);

            // Convert float buffers to output
            for (auto& channel : out_buffers_)
                channel.format.convertFromFloat(
                    channel.buffer.data(), channel.buffer_info.buffers[index], current_buffersize_);

            if (post_output_ && asio_object_ != nullptr)
                asio_object_->outputReady();

            callbackCalled_ = true;
        }
    };

    struct AsioDeviceType;

    struct ActiveImpl : IDeviceActive {
        set<wstring> active_class_ids_;
        mutex m_active_;
        atomic<bool> notify_change_;

        ActiveImpl()  = default;
        ~ActiveImpl() = default;

        void setActive(wstring id) override
        {
            LOG_TRACE("id = " << wstring_to_utf8(id));
            lock_guard<mutex> guard{m_active_};
            active_class_ids_.insert(id);
        }

        void setInactive(wstring id) override
        {
            LOG_TRACE("id = " << wstring_to_utf8(id));
            lock_guard<mutex> guard{m_active_};
            active_class_ids_.erase(id);
        }

        void notifyDeviceError(wstring id) override
        {
            LOG_TRACE("id = " << wstring_to_utf8(id));
            lock_guard<mutex> guard{m_active_};
            active_class_ids_.erase(id);
            notify_change_ = true;
        }

        bool deviceIsActive(wstring id)
        {
            lock_guard<mutex> guard{m_active_};
            return active_class_ids_.find(id) != active_class_ids_.end();
        }

        bool notifyChange()
        {
            bool retval    = notify_change_;
            notify_change_ = false;
            return retval;
        }
    };

    struct AsioDeviceType : dminternal::DeviceType {
        using dev_map_type = map<wstring, daudio::AudioDevice>;
        struct DeviceMap {
            dev_map_type devs;
        };
        using device_map = map<wstring, std::shared_ptr<AsioDevice>>;
        device_map devices_;

        mutex mutable m_;

        atomic<bool> stop_thread_{false};
        thread scan_thread_;

        std::shared_ptr<ActiveImpl> activeImpl_;

        std::shared_ptr<AsioDevice> getDeviceWithId(wstring id)
        {
            lock_guard<mutex> lock(m_);
            auto it = devices_.find(id);
            if (it != devices_.end())
                return it->second;
            return std::shared_ptr<AsioDevice>();
        }

        //==============================================================================
        static bool checkClassIsOk(const wchar_t* classId)
        {
            HKEY hk = 0;
            bool ok = false;

            if (RegOpenKeyW(HKEY_CLASSES_ROOT, L"clsid", &hk) == ERROR_SUCCESS) {
                int index = 0;
                wchar_t name[512];

                while (RegEnumKeyW(hk, index++, name, 512) == ERROR_SUCCESS) {
                    if (wstring(classId) == wstring(name)) {
                        HKEY subKey, pathKey;

                        if (RegOpenKeyExW(hk, name, 0, KEY_READ, &subKey) == ERROR_SUCCESS) {
                            if (RegOpenKeyExW(subKey, L"InprocServer32", 0, KEY_READ, &pathKey) ==
                                ERROR_SUCCESS) {
                                wchar_t pathName[1024] = {0};
                                DWORD dtype            = REG_SZ;
                                DWORD dsize            = sizeof(pathName);

                                if (RegQueryValueExW(
                                        pathKey, 0, 0, &dtype, (LPBYTE)pathName, &dsize) ==
                                    ERROR_SUCCESS)
                                    // In older code, this used to check for the existence of
                                    // the file, but there are situations where our process doesn't
                                    // have access to it, but where the driver still loads ok..
                                    ok = (pathName[0] != 0);

                                RegCloseKey(pathKey);
                            }

                            RegCloseKey(subKey);
                        }

                        break;
                    }
                }

                RegCloseKey(hk);
            }

            return ok;
        }

        void addDevice(dev_map_type& devices, const wchar_t* keyName, HKEY hk)
        {
            HKEY subKey;
            if (RegOpenKeyExW(hk, keyName, 0, KEY_READ, &subKey) == ERROR_SUCCESS) {
                wchar_t buf[256] = {0};
                DWORD dtype      = REG_SZ;
                DWORD dsize      = sizeof(buf);

                if (RegQueryValueExW(subKey, L"clsid", 0, &dtype, (LPBYTE)buf, &dsize) ==
                    ERROR_SUCCESS) {
                    if (dsize > 0 && checkClassIsOk(buf)) {
                        std::wstring classId(buf);
                        dtype = REG_SZ;
                        dsize = sizeof(buf);
                        wstring deviceName;

                        if (RegQueryValueExW(
                                subKey, L"description", 0, &dtype, (LPBYTE)buf, &dsize) ==
                            ERROR_SUCCESS)
                            deviceName = buf;
                        else
                            deviceName = keyName;

                        AsioDevice dev(nullptr, classId, deviceName);
                        if (activeImpl_->deviceIsActive(classId) || dev.exists()) {
                            devices.insert(make_pair(classId, dev.externalDevice()));
                        }
                    }

                    RegCloseKey(subKey);
                }
            }
        }

        dev_map_type scanNewDevices()
        {
            dev_map_type devices;
            HKEY hk = 0;
            if (RegOpenKeyW(HKEY_LOCAL_MACHINE, L"software\\asio", &hk) == ERROR_SUCCESS) {
                wchar_t name[256];
                DWORD index = 0;
                while (RegEnumKeyW(hk, index++, name, 256) == ERROR_SUCCESS)
                    addDevice(devices, name, hk);

                RegCloseKey(hk);
            }
            return devices;
        }

        bool isSameDevices(const dev_map_type& devs)
        {
            size_t existing = 0;
            size_t new_one  = 0;
            for (auto& entry : devs) {
                if (devices_.find(entry.first) != devices_.end())
                    ++existing;
                else
                    ++new_one;
            }
            return new_one == 0 && existing == devices_.size();
        }

        AsioDeviceType(function<void()> callback) : activeImpl_(std::make_shared<ActiveImpl>())
        {
            if (callback) {
                thread t([this, callback] {
                    volatile dminternal::PlatformInitializer platform;
                    auto hr     = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
                    auto t_next = chrono::system_clock::now();
                    while (!stop_thread_) {
                        if (chrono::system_clock::now() >= t_next || activeImpl_->notifyChange()) {
                            t_next     = chrono::system_clock::now() + chrono::seconds(5);
                            auto token = scanDevices();
                            if (token && updateDevices(move(token)))
                                callback();
                        }
                        this_thread::sleep_for(chrono::milliseconds(50));
                    }
                });
                scan_thread_.swap(t);
            }
        }
        ~AsioDeviceType()
        {
            stop_thread_ = true;
            devices_.clear();
            scan_thread_.join();
        }

        std::unique_ptr<DeviceMap> scanDevices()
        {
            auto devs = scanNewDevices();
            lock_guard<mutex> lock{m_};
            if (isSameDevices(devs))
                return nullptr;

            auto token = make_unique<DeviceMap>();
            token->devs.swap(devs);
            return token;
        }

        bool updateDevices(std::unique_ptr<DeviceMap> newDevs)
        {
            bool retval = false;
            lock_guard<mutex> lock{m_};
            // Check what to remove
            set<wstring> to_remove;
            for (auto& entry : devices_) {
                auto it = newDevs->devs.find(entry.first);
                if (it == newDevs->devs.end()) {
                    to_remove.insert(entry.first);
                }
            }
            // Then remove
            for (auto& entry : to_remove) {
                devices_.erase(entry);
                retval = true;
            }
            // Check what to add
            for (auto& entry : newDevs->devs) {
                auto it = devices_.find(entry.first);
                if (it == devices_.end()) {
                    std::shared_ptr<AsioDevice> new_device(callback_handling::create<AsioDevice>(
                        activeImpl_, entry.first, entry.second.name));
                    if (new_device->exists()) {
                        devices_.insert(make_pair(entry.first, new_device));
                        retval = true;
                    }
                }
            }
            return retval;
        }

        virtual std::vector<daudio::AudioDevice> devices() override
        {
            lock_guard<mutex> lock{m_};
            std::vector<daudio::AudioDevice> devs;
            for (auto it : devices_)
                devs.emplace_back(it.second->externalDevice());
            return devs;
        }

        virtual std::shared_ptr<dminternal::Device> getDevice(device_id_t id) override
        {
            lock_guard<mutex> lock{m_};
            auto it = std::find_if(devices_.begin(), devices_.end(), [&](auto& entry) {
                return entry.second->id() == id;
            });
            if (it == devices_.end())
                return nullptr;
            return it->second;
        }
    };

}  // namespace

namespace dminternal
{
    template <>
    unique_ptr<dminternal::DeviceType> DeviceType::create<daudio::AudioDeviceType::asio>(
        function<void()> callback)
    {
        return make_unique<AsioDeviceType>(callback);
    }
}  // namespace dminternal
