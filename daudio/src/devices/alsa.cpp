#include <daudio/waveform.h>
#include <dminternal/defer.h>
#include <dminternal/device.h>
#include <dminternal/mathutil.h>
#include <dminternal/sampleformat.h>
#include <dminternal/threadutil.h>

#include <algorithm>
#include <fstream>
#include <log/log.hpp>
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <streambuf>
#include <string>
#include <type_traits>

using namespace std;
using namespace dminternal;

#include <alsa/asoundlib.h>
#include <signal.h>

namespace
{
    constexpr unsigned kNoOfAlsaPeriods = 2;

    static void alsa_lib_error_handler(const char* file,
                                       int line,
                                       const char* function,
                                       int /*err*/,
                                       const char* fmt,
                                       ...)
    {
#if DAUDIO_ENABLE_ALSA_ERROR_HANDLER
        using namespace dirac::log;
        char buffer[4096];
        va_list vl;
        va_start(vl, fmt);
        vsnprintf(buffer, sizeof(buffer), fmt, vl);
        va_end(vl);
        LOG_OSTREAM(LogLevel::LogLevel_kVERBOSE, file, line, function, buffer);
#else
        (void)file;
        (void)line;
        (void)function;
        (void)fmt;
#endif
    }

    static void find_and_replace(string& source, string const& find, string const& replace)
    {
        for (string::size_type i = 0; (i = source.find(find, i)) != string::npos;) {
            source.replace(i, find.length(), replace);
            i += replace.length();
        }
    }

    static vector<float> getDeviceSampleRates(snd_pcm_t* handle)
    {
        snd_pcm_hw_params_t* params;
        snd_pcm_hw_params_alloca(&params);

        vector<float> rates;
        for (auto rate : dminternal::Device::possibleSampleRates()) {
            if (snd_pcm_hw_params_any(handle, params) >= 0 &&
                snd_pcm_hw_params_test_rate(handle, params, (unsigned int)rate, 0) == 0) {
                rates.push_back(rate);
            }
        }
        return rates;
    }

    static bool isPeriodSizeSupported(snd_pcm_t* handle, snd_pcm_uframes_t& period_size)
    {
        snd_pcm_hw_params_t* params;
        snd_pcm_hw_params_alloca(&params);

        if (snd_pcm_hw_params_any(handle, params) >= 0 &&
            snd_pcm_hw_params_set_period_size_near(handle, params, &period_size, 0) >= 0) {
            return true;
        }
        return false;
    }

    static pair<unsigned, unsigned> getDeviceNumChannels(snd_pcm_t* handle)
    {
        snd_pcm_hw_params_t* params;
        snd_pcm_hw_params_alloca(&params);
        int rv;

        if ((rv = snd_pcm_hw_params_any(handle, params)) < 0) {
            return {0, 0};
        }

        unsigned int minChans, maxChans, chans;
        snd_pcm_hw_params_get_channels_min(params, &minChans);
        snd_pcm_hw_params_get_channels_max(params, &maxChans);
        snd_pcm_hw_params_get_channels(params, &chans);

        LOG_TRACE("getDeviceNumChannels: " << (int)minChans << "," << (int)maxChans << ","
                                           << chans);

        // some virtual devices (dmix for example) report 10000 channels , we have to clamp
        // these values
        maxChans = min(maxChans, 32u);
        minChans = min(minChans, maxChans);
        return make_pair(minChans, maxChans);
    }

    static pair<unsigned, unsigned> getDeviceBufferSizeMinMax(snd_pcm_t* handle)
    {
        snd_pcm_hw_params_t* params;
        snd_pcm_hw_params_alloca(&params);

        if (snd_pcm_hw_params_any(handle, params) < 0)
            throw runtime_error("getDeviceBufferSizeMinMax failed");

        snd_pcm_uframes_t min_size, max_size;
        snd_pcm_hw_params_get_buffer_size_min(params, &min_size);
        snd_pcm_hw_params_get_buffer_size_max(params, &max_size);

        LOG_TRACE("getDeviceBufferSizeMinMax: " << (int)min_size << "," << (int)max_size);

        return make_pair(min_size, max_size);
    }

    static bool failed(const int errorNum, int lineNum)
    {
        if (errorNum >= 0)
            return false;

        auto error = snd_strerror(errorNum);
        LOG_ERROR("ALSA error: " << error << " (" << lineNum << ")");
        return true;
    }

#define FAILED(xx) failed((xx), __LINE__)

    static dminternal::SampleFormat::Type getSampleFormat(_snd_pcm_format t)
    {
        switch (t) {
        case SND_PCM_FORMAT_FLOAT_LE:
            return dminternal::SampleFormat::Type::kFloat32LSB;
        case SND_PCM_FORMAT_FLOAT_BE:
            return dminternal::SampleFormat::Type::kFloat32MSB;
        case SND_PCM_FORMAT_S32_LE:
            return dminternal::SampleFormat::Type::kInt32LSB;
        case SND_PCM_FORMAT_S32_BE:
            return dminternal::SampleFormat::Type::kInt32LSB;
        case SND_PCM_FORMAT_S24_3LE:
            return dminternal::SampleFormat::Type::kInt24LSB;
        case SND_PCM_FORMAT_S24_3BE:
            return dminternal::SampleFormat::Type::kInt24MSB;
        case SND_PCM_FORMAT_S24_LE:
            return dminternal::SampleFormat::Type::kInt32LSB24;
        case SND_PCM_FORMAT_S16_LE:
            return dminternal::SampleFormat::Type::kInt16LSB;
        case SND_PCM_FORMAT_S16_BE:
            return dminternal::SampleFormat::Type::kInt16MSB;
        default:
            break;
        };
        throw logic_error("_snd_pcm_format " + to_string(t) + " not recognized");
    }

    template <typename InputIterator, typename ValueType>
    InputIterator closest(InputIterator first, InputIterator last, ValueType value)
    {
        return std::min_element(first, last, [&](ValueType x, ValueType y) {
            return std::abs(x - value) < std::abs(y - value);
        });
    }

    struct AlsaDeviceInfo {
        string dev_id;
        bool is_input{false};
        bool is_output{false};
        daudio::AudioDevice dev;
        bool operator==(const AlsaDeviceInfo& other) const { return dev_id == other.dev_id; }
    };

    struct AlsaDeviceBase {
        snd_pcm_t* handle_{nullptr};
        snd_async_handler_t* async_handler_{nullptr};
        function<void()> callback_;
        pair<unsigned, unsigned> minmax_channels_{0, 0};
        bool interleaved_{false};
        unsigned latency_{0};

        daudio::ActiveChannels active_channels_;
        unsigned num_channels_{0};

        vector<float> rates_;
        float current_rate_{0.f};
        float default_rate_{0.f};

        vector<int> buffer_sizes_;

        unsigned current_buffer_size_{0};

        std::unique_ptr<dminternal::SampleFormat> format_;
        unsigned bytes_per_frame_{0};

        const string device_id;
        const bool is_input;
        int card_number{-1};

        AlsaDeviceBase(const string& id, bool for_input) : device_id(id), is_input(for_input)
        {
            LOG_TRACE("ID = " << id << ", for_input = " << for_input);
            openDevice();
            defer
            {
                closeDevice();
                LOG_TRACE("Device closed.");
            };
            minmax_channels_ = getDeviceNumChannels(handle_);
            if (minmax_channels_.first == 0 && minmax_channels_.second == 0)
                throw runtime_error("no device channels");

            rates_ = getDeviceSampleRates(handle_);
            if (rates_.empty())
                throw runtime_error("no device sample rates?");

            default_rate_ = rates_[0];
            for (auto rate : rates_) {
                if (rate > 48000.f)
                    break;
                default_rate_ = rate;
            }

            auto minMaxBufferSize = getDeviceBufferSizeMinMax(handle_);
            for (snd_pcm_uframes_t size : dminternal::Device::possibleBufferSizes(
                     minMaxBufferSize.first, minMaxBufferSize.second)) {
                if (isPeriodSizeSupported(handle_, size)) {
                    buffer_sizes_.push_back(size);
                }
            }
            // Remove duplicates
            buffer_sizes_.erase(std::unique(buffer_sizes_.begin(), buffer_sizes_.end()),
                                buffer_sizes_.end());
            if (buffer_sizes_.empty())
                throw runtime_error("no device buffer sizes?");

            LOG_TRACE("Number of buffer sizes: " << buffer_sizes_.size());
            std::stringstream ss;
            ss << buffer_sizes_[0];
            for (unsigned i = 1; i < buffer_sizes_.size(); ++i) {
                ss << "," << buffer_sizes_[i];
            }
            LOG_TRACE("Buffer sizes: " << ss.str());
        }
        ~AlsaDeviceBase() { close(); }

        void openDevice()
        {
            auto err = snd_pcm_open(
                &handle_,
                device_id.c_str(),
                is_input ? SND_PCM_STREAM_CAPTURE : SND_PCM_STREAM_PLAYBACK,
                SND_PCM_NONBLOCK | SND_PCM_NO_AUTO_RESAMPLE | SND_PCM_NO_AUTO_CHANNELS);
            if (err < 0) {
                stringstream error;
                if (-err == EBUSY)
                    error << "The device \"" << device_id
                          << "\" is busy (another application is using it).";
                else if (-err == ENOENT)
                    error << "The device \"" << device_id << "\" is not available.";
                else
                    error << "Could not open " << (is_input ? "input" : "output") << " device \""
                          << device_id << "\": " << snd_strerror(err) << " (" << err << ")";
                LOG_ERROR(error.str());
                throw runtime_error(error.str());
            }

            snd_pcm_info_t* pcmInfo;
            snd_pcm_info_alloca(&pcmInfo);
            if (snd_pcm_info(handle_, pcmInfo) == 0) {
                card_number = snd_pcm_info_get_card(pcmInfo);
            }
        }

        void closeDevice()
        {
            if (handle_ != nullptr) {
                snd_pcm_close(handle_);
                handle_ = nullptr;
            }
        }

        void close() { closeDevice(); }

        void openStream(unsigned fs, int buffer_size, daudio::ActiveChannels channels)
        {
            if (channels == 0)
                return;

            if (handle_ == nullptr)
                openDevice();

            LOG_DEBUG(device_id << ", " << (int)fs << ", " << channels << ", " << buffer_size
                                << ", input = " << is_input);

            snd_pcm_hw_params_t* hwParams;
            snd_pcm_hw_params_alloca(&hwParams);

            if (snd_pcm_hw_params_any(handle_, hwParams) < 0) {
                // this is the error message that aplay returns when an error happens here,
                // it is a bit more explicit that "Invalid parameter"
                throw runtime_error(
                    "Broken configuration for this PCM: no configurations available");
            }

            // set hardware resampling 0 = none
            auto err = snd_pcm_hw_params_set_rate_resample(handle_, hwParams, 0);
            if (err < 0)
                throw runtime_error(string{"ALSA set resample failed: "} + snd_strerror(err));

            if (snd_pcm_hw_params_set_access(handle_, hwParams, SND_PCM_ACCESS_RW_NONINTERLEAVED) >=
                0)  // works better for plughw..
                interleaved_ = false;
            else if (snd_pcm_hw_params_set_access(
                         handle_, hwParams, SND_PCM_ACCESS_RW_INTERLEAVED) >= 0)
                interleaved_ = true;
            else {
                throw runtime_error("ALSA failed setting interleaved mode");
            }

#if 1
            const _snd_pcm_format formatsToTry[] = {SND_PCM_FORMAT_FLOAT_LE,
                                                    SND_PCM_FORMAT_FLOAT_BE,
                                                    SND_PCM_FORMAT_S32_LE,
                                                    SND_PCM_FORMAT_S32_BE,
                                                    SND_PCM_FORMAT_S24_3LE,
                                                    SND_PCM_FORMAT_S24_3BE,
                                                    SND_PCM_FORMAT_S24_LE,
                                                    SND_PCM_FORMAT_S16_LE,
                                                    SND_PCM_FORMAT_S16_BE};
#else
            const _snd_pcm_format formatsToTry[] = {SND_PCM_FORMAT_S16_LE, SND_PCM_FORMAT_S16_BE};
#endif  // else

            for (auto format : formatsToTry) {
                if (snd_pcm_hw_params_set_format(handle_, hwParams, format) >= 0) {
                    format_ = std::make_unique<dminternal::SampleFormat>(getSampleFormat(format));
                    break;
                }
            }

            if (!format_)
                throw runtime_error("device doesn't support a compatible PCM format");

            int dir                            = 0;
            unsigned int periods               = kNoOfAlsaPeriods;
            snd_pcm_uframes_t samplesPerPeriod = (snd_pcm_uframes_t)buffer_size;

            num_channels_ =
                max(minmax_channels_.first, min(minmax_channels_.second, unsigned(channels)));

            if (FAILED(snd_pcm_hw_params_set_rate_near(handle_, hwParams, &fs, 0)))
                throw runtime_error("ALSA: snd_pcm_hw_params_set_rate_near failed");

            if (FAILED(
                    snd_pcm_hw_params_set_channels(handle_, hwParams, (unsigned int)num_channels_)))
                throw runtime_error("ALSA: snd_pcm_hw_params_set_channels failed - channels = " +
                                    to_string(num_channels_));

            if (FAILED(snd_pcm_hw_params_set_periods_near(handle_, hwParams, &periods, &dir)))
                throw runtime_error("ALSA: snd_pcm_hw_params_set_periods_near failed");

            if (FAILED(snd_pcm_hw_params_set_period_size_near(
                    handle_, hwParams, &samplesPerPeriod, &dir)))
                throw runtime_error("ALSA: snd_pcm_hw_params_set_period_size_near failed");

            if (FAILED(snd_pcm_hw_params(handle_, hwParams)))
                throw runtime_error("ALSA: snd_pcm_hw_params failed");

            snd_pcm_uframes_t frames = 0;

            if (FAILED(snd_pcm_hw_params_get_period_size(hwParams, &frames, &dir)) ||
                FAILED(snd_pcm_hw_params_get_periods(hwParams, &periods, &dir)))
                latency_ = 0;
            else
                latency_ =
                    (int)frames *
                    ((int)periods - 1);  // (this is the method JACK uses to guess the latency..)

            LOG_DEBUG("frames: " << (int)frames << ", periods: " << (int)periods
                                 << ", samplesPerPeriod: " << (int)samplesPerPeriod
                                 << ", channels: " << num_channels_);

            snd_pcm_sw_params_t* swParams;
            snd_pcm_sw_params_alloca(&swParams);
            snd_pcm_uframes_t boundary;

            if (FAILED(snd_pcm_sw_params_current(handle_, swParams)) ||
                FAILED(snd_pcm_sw_params_get_boundary(swParams, &boundary)) ||
                FAILED(snd_pcm_sw_params_set_silence_threshold(handle_, swParams, 0)) ||
                FAILED(snd_pcm_sw_params_set_silence_size(handle_, swParams, boundary)) ||
                FAILED(snd_pcm_sw_params_set_start_threshold(handle_, swParams, frames)) ||
                FAILED(snd_pcm_sw_params_set_stop_threshold(handle_, swParams, boundary)) ||
                FAILED(snd_pcm_sw_params(handle_, swParams))) {
                throw runtime_error("ALSA set sw params error occurred");
            }

            if (dirac::log::currentLogLevel() == dirac::log::LogLevel::LogLevel_kVERBOSE) {
                snd_output_t* out;
                snd_output_stdio_attach(&out, stderr, 0);
                snd_pcm_hw_params_dump(hwParams, out);
                snd_pcm_sw_params_dump(swParams, out);
            }

            active_channels_     = channels;
            current_buffer_size_ = frames;
            bytes_per_frame_     = num_channels_ * format_->byteStride;
        }

        void prepare(std::function<void()> callback = {})
        {
            auto err = snd_pcm_prepare(handle_);
            if (err < 0)
                throw runtime_error(string{"failed to prepare device: "} + snd_strerror(err));

            if (callback) {
                callback_ = callback;
                err       = snd_async_add_pcm_handler(
                    &async_handler_,
                    handle_,
                    [](snd_async_handler_t* p) {
                        auto pThis = reinterpret_cast<AlsaDeviceBase*>(
                            snd_async_handler_get_callback_private(p));
                        if (pThis->callback_)
                            pThis->callback_();
                    },
                    this);
                if (err < 0)
                    throw runtime_error(string{"failed to add async handler: "} +
                                        snd_strerror(err));
            }
        }

        void start()
        {
            auto err = snd_pcm_start(handle_);
            if (err < 0) {
                throw runtime_error(string{"failed to start "} +
                                    (is_input ? "capture" : "playback") +
                                    " pcm device : " + snd_strerror(err));
            }
        }

        void stop()
        {
            snd_pcm_drop(handle_);
            if (async_handler_) {
                snd_async_del_handler(async_handler_);
                async_handler_ = nullptr;
            }
        }
    };

    struct AlsaInputDevice : AlsaDeviceBase {
        function<void()> callback_;
        vector<uint8_t> buffer_;
        vector<float> dummyBuffer_;

        AlsaInputDevice(string id) : AlsaDeviceBase(id, true) {}

        void open(float fs, int buffer_size, daudio::ActiveChannels channels)
        {
            openStream(fs, buffer_size, channels);
            buffer_.resize(bytes_per_frame_ * current_buffer_size_);
            dummyBuffer_.resize(current_buffer_size_);
        }

        // Read into view
        void handleBuffer(daudio::InputBufferView& view)
        {
            snd_pcm_sframes_t bufSize = view.bufferSize();
            size_t offset             = 0;
            float** const dst         = (float**)alloca(num_channels_ * sizeof(float*));
            unsigned err_counter      = 0;
            while (bufSize > 0) {
                snd_pcm_sframes_t available_frames, delay_frames;
                auto err = snd_pcm_avail_delay(handle_, &available_frames, &delay_frames);
                if (err == -ENODEV)
                    throw runtime_error("device removed");

                if (err < 0) {
                    if (FAILED(snd_pcm_recover(handle_, err, 0))) {
                        throw runtime_error("input pcm recover failed");
                    }
                    if (++err_counter > 5)
                        throw runtime_error("input error counter overflow");
                    continue;
                }

                auto frames_now = std::min(bufSize, available_frames);

                for (unsigned i = 0; i < view.channel.size(); ++i)
                    dst[i] = const_cast<float*>(view.channel[i].begin()) + offset;
                for (unsigned i = view.channel.size(); i < num_channels_; ++i)
                    dst[i] = dummyBuffer_.data();

                if (interleaved_) {
                    auto n_read = snd_pcm_readi(handle_, buffer_.data(), frames_now);
                    if (n_read != frames_now)
                        throw runtime_error("snd_pcm_readi returned " + to_string(n_read) +
                                            ", should be " + to_string(frames_now));
                    format_->convertToFloat(buffer_.data(), dst, num_channels_, int(frames_now));
                } else {
                    auto src = (void**)alloca(num_channels_ * sizeof(void*));
                    for (unsigned i = 0; i < num_channels_; ++i)
                        src[i] = buffer_.data() + i * frames_now * format_->byteStride;
                    auto n_read = snd_pcm_readn(handle_, src, frames_now);
                    if (n_read != frames_now)
                        throw runtime_error("snd_pcm_readn returned " + to_string(n_read) +
                                            ", should be " + to_string(frames_now));
                    format_->convertToFloatNonInterleaved(
                        (const void**)src, dst, num_channels_, int(frames_now));
                }
                bufSize -= frames_now;
                offset += frames_now;
            }
        }
    };

    struct AlsaOutputDevice : AlsaDeviceBase {
        AlsaOutputDevice(string id) : AlsaDeviceBase(id, false) {}
        vector<uint8_t> buffer_;
        vector<float> dummyBuffer_;

        void open(float fs, int buffer_size, daudio::ActiveChannels channels)
        {
            openStream(fs, buffer_size, channels);
            buffer_.resize(bytes_per_frame_ * current_buffer_size_);
            dummyBuffer_.resize(current_buffer_size_);
        }

        void handleBuffer(const daudio::OutputBufferView& view)
        {
            snd_pcm_sframes_t bufSize = view.bufferSize();
            size_t view_offset        = 0;
            unsigned err_counter      = 0;
            auto src                  = (const float**)alloca(num_channels_ * sizeof(float*));

            while (bufSize > 0) {
                snd_pcm_sframes_t available_frames, delay_frames;
                auto err = snd_pcm_avail_delay(handle_, &available_frames, &delay_frames);
                if (available_frames == 0 || err == -ENODEV)
                    throw runtime_error("device removed");
                if (delay_frames < 0) {
                    snd_pcm_reset(handle_);
                    // continue;
                }

                if (err < 0) {
                    if (FAILED(snd_pcm_recover(handle_, err, 0)))
                        throw runtime_error("output device pcm recover failed");

                    if (++err_counter > 5)
                        throw runtime_error("input error counter overflow");
                    continue;
                }
                if (available_frames < bufSize)
                    continue;

                auto frames_now = std::min(available_frames, bufSize);
                if (frames_now == 0)
                    break;

                for (unsigned i = 0; i < view.channel.size(); ++i)
                    src[i] = view.channel[i].begin() + view_offset;
                for (unsigned i = view.channel.size(); i < num_channels_; ++i)
                    src[i] = dummyBuffer_.data();

                if (interleaved_) {
                    format_->convertFromFloat(src, buffer_.data(), num_channels_, frames_now);
                    auto err = snd_pcm_writei(handle_, buffer_.data(), frames_now);
                    if (err < 0) {
                        err = snd_pcm_prepare(handle_);
                        if (err < 0)
                            throw runtime_error("writei prepare failed");
                        continue;
                    }
                } else {
                    auto dest = (void**)alloca(num_channels_ * sizeof(void*));
                    for (unsigned i = 0; i < num_channels_; ++i)
                        dest[i] = buffer_.data() + i * frames_now * format_->byteStride;
                    format_->convertFromFloatNonInterleaved(src, dest, num_channels_, frames_now);
                    auto err = snd_pcm_writen(handle_, dest, frames_now);
                    if (err < 0) {
                        err = snd_pcm_prepare(handle_);
                        if (err < 0)
                            throw runtime_error("writen prepare failed");
                        continue;
                    }
                }
                snd_pcm_avail_update(handle_);

                bufSize -= frames_now;
                view_offset += frames_now;
            }
        }
    };

    struct AlsaDevice : dminternal::Device {
        unique_ptr<AlsaInputDevice> capture_;
        unique_ptr<AlsaOutputDevice> render_;

        vector<float> rates_;
        float default_rate_{0.f};
        float current_rate_{0.f};
        vector<int> buffer_sizes_;
        int default_buffer_size_{0};
        int current_buffer_size_{0};

        atomic<bool> opened_{false};
        atomic<bool> linked_{false};

        unique_ptr<dminternal::AudioThread> poll_thread_;

        const AlsaDeviceInfo dev_info;
        AlsaDevice(AlsaDeviceInfo info) : dminternal::Device{info.dev}, dev_info{info} { init(); }

        template <class Type>
        static bool ok(unique_ptr<Type>& dev)
        {
            return !!dev && dev->handle_ != nullptr;
        }

        void init()
        {
            // Try open capture and render, as the input&output info from scanDevices
            // might not tell the whole truth
            stringstream error;
            try {
                if (dev_info.is_input) {
                    capture_ = make_unique<AlsaInputDevice>(dev_info.dev_id);
                    keyValueMap["alsa.capture.card"] = std::to_string(capture_->card_number);
                }
            } catch (exception& e) {
                error << e.what();
            }
            try {
                if (dev_info.is_output) {
                    render_ = make_unique<AlsaOutputDevice>(dev_info.dev_id);
                    keyValueMap["alsa.playback.card"] = std::to_string(render_->card_number);
                }
            } catch (exception& e) {
                if (!error.str().empty()) {
                    error << ", " << e.what();
                }
            }

            if (!capture_ && !render_) {
                LOG_INFO(error.str());
                throw runtime_error("init error: " + error.str());
            }

            if (capture_ && render_) {
                set_intersection(capture_->rates_.begin(),
                                 capture_->rates_.end(),
                                 render_->rates_.begin(),
                                 render_->rates_.end(),
                                 back_inserter(rates_));
                if (rates_.empty())
                    throw runtime_error("capture&render does not share any sample rates");

                if (render_->default_rate_ == capture_->default_rate_)
                    default_rate_ = render_->default_rate_;
                else if (rates_.size() == 1) {
                    default_rate_ = rates_[0];
                } else {
                    for (auto rate : rates_) {
                        if (rate > 48000.0f)
                            break;
                        default_rate_ = rate;
                    }
                }

                set_intersection(capture_->buffer_sizes_.begin(),
                                 capture_->buffer_sizes_.end(),
                                 render_->buffer_sizes_.begin(),
                                 render_->buffer_sizes_.end(),
                                 back_inserter(buffer_sizes_));
                if (buffer_sizes_.empty())
                    throw runtime_error("capture&render does not share any buffer sizes");
            } else {
                auto d = capture_ ? static_cast<AlsaDeviceBase*>(capture_.get())
                                  : static_cast<AlsaDeviceBase*>(render_.get());
                rates_        = d->rates_;
                default_rate_ = d->default_rate_;
                buffer_sizes_ = d->buffer_sizes_;
            }

            device.inputchannels  = capture_ ? capture_->minmax_channels_.second : 0;
            device.outputchannels = render_ ? render_->minmax_channels_.second : 0;

            auto it = closest(buffer_sizes_.begin(), buffer_sizes_.end(), int(default_rate_ / 100));
            default_buffer_size_ = *it;
        }

        virtual float defaultSampleRate() const override { return default_rate_; }

        virtual int defaultBuffersize() const override { return default_buffer_size_; }

        virtual std::vector<float> supportedSamplerates() const override { return rates_; }

        virtual std::vector<int> supportedBuffersizes() const override { return buffer_sizes_; }

        std::vector<std::string> getChannelNames(bool input) const
        {
            std::vector<std::string> retval;
            auto n = input ? device.inputchannels : device.outputchannels;
            if (n >= 0) {
                for (int i = 0; i < n; ++i)
                    retval.push_back("Channel #" + to_string(i + 1));
            }
            return retval;
        }

        virtual std::vector<std::string> inputChannelNames() const override
        {
            return getChannelNames(true);
        }

        virtual std::vector<std::string> outputChannelNames() const override
        {
            return getChannelNames(false);
        }

        virtual void open(float fs,
                          int bufferSize,
                          daudio::ActiveChannels inputMask,
                          daudio::ActiveChannels outputMask) override
        {
            close();

            try {
                if (capture_ && inputMask > 0) {
                    capture_->open(fs, bufferSize, inputMask);
                }
                if (render_ && outputMask > 0) {
                    render_->open(fs, bufferSize, outputMask);
                }

                linked_ = false;
                if (ok(capture_) && ok(render_)) {
                    if (capture_->current_buffer_size_ != render_->current_buffer_size_)
                        throw runtime_error("capture/render buffer size mismatch");

                    auto err = snd_pcm_link(render_->handle_, capture_->handle_);
                    if (err < 0) {
                        LOG_WARN("Failed to link capture&render devices: " << snd_strerror(err));
                    } else
                        linked_ = true;
                }

                current_rate_ = fs;
                current_buffer_size_ =
                    ok(render_) ? render_->current_buffer_size_ : capture_->current_buffer_size_;

                prepare();

                poll_thread_ = make_unique<dminternal::AudioThread>(
                    bind(&AlsaDevice::processThread, this, placeholders::_1, placeholders::_2));

                start();

                opened_ = true;
            } catch (...) {
                close();
                throw;  // Rethrow
            }
        }

        void prepare()
        {
            if (linked_) {
                render_->prepare();
            } else {
                if (ok(capture_))
                    capture_->prepare();
                if (ok(render_))
                    render_->prepare();
            }
        }

        void start()
        {
            if (linked_) {
                render_->start();
            } else {
                if (ok(capture_))
                    capture_->start();
                if (ok(render_))
                    render_->start();
            }
        }

        virtual void close() override
        {
            poll_thread_ = nullptr;

            if (opened_) {
                if (!linked_ && ok(capture_))
                    capture_->stop();
                if (ok(render_))
                    render_->stop();
                if (linked_)
                    snd_pcm_unlink(capture_->handle_);

                if (ok(capture_))
                    capture_->close();
                if (ok(render_))
                    render_->close();

                opened_ = false;
            }
        }

        virtual bool isOpened() const override { return opened_; }

        virtual float currentSampleRate() const override { return current_rate_; }

        virtual int currentBuffersize() const override { return current_buffer_size_; }

        virtual const daudio::ActiveChannels& activeInputChannels() const override
        {
            static daudio::ActiveChannels none{};
            return capture_ ? capture_->active_channels_ : none;
        }

        virtual const daudio::ActiveChannels& activeOutputChannels() const override
        {
            static daudio::ActiveChannels none{};
            return render_ ? render_->active_channels_ : none;
        }

        void processThread(atomic<bool>& thread_started, atomic<bool>& stop_thread)
        {
            const auto numInputBuffers  = ok(capture_) ? unsigned(capture_->active_channels_) : 0u;
            const auto numOutputBuffers = ok(render_) ? unsigned(render_->active_channels_) : 0;

            daudio::Waveforms inBuffers(numInputBuffers, current_buffer_size_, current_rate_);
            daudio::Waveforms outBuffers(numOutputBuffers, current_buffer_size_, current_rate_);

            daudio::InputBufferView inputBufferView;
            daudio::OutputBufferView outputBufferView;

            for (unsigned i = 0; i < numInputBuffers; ++i)
                inputBufferView.channel.emplace_back(
                    inBuffers.waveforms[i].data(),
                    inBuffers.waveforms[i].data() + current_buffer_size_);
            for (unsigned i = 0; i < numOutputBuffers; ++i)
                outputBufferView.channel.emplace_back(
                    outBuffers.waveforms[i].data(),
                    outBuffers.waveforms[i].data() + current_buffer_size_);

            const int in_poll_count =
                ok(capture_) ? snd_pcm_poll_descriptors_count(capture_->handle_) : 0;
            const int out_poll_count =
                ok(render_) ? snd_pcm_poll_descriptors_count(render_->handle_) : 0;

            const int poll_count = in_poll_count + out_poll_count;

            struct pollfd* ufds = (struct pollfd*)alloca(poll_count * sizeof(struct pollfd));

            if (in_poll_count) {
                snd_pcm_poll_descriptors(capture_->handle_, ufds, in_poll_count);
            }
            if (out_poll_count) {
                snd_pcm_poll_descriptors(render_->handle_, ufds + in_poll_count, out_poll_count);
            }

            try {
                if (poll_count == 0)
                    throw runtime_error("No poll descriptors");

                const auto sleepTimeUs = chrono::microseconds(
                    std::max(500,
                             std::min(int(1000000 * current_buffer_size_ / current_rate_) / 2 - 100,
                                      10000)));

                const int timeoutMs =
                    max(100, int(5 * 1000 * current_buffer_size_ / current_rate_));

                auto preload_render_buffers = out_poll_count ? kNoOfAlsaPeriods : 0;
                thread_started              = true;

                enum { CAPTURE = 1, PLAYBACK = 2 };
                auto get_poll_result = [&]() -> unsigned {
                    if (preload_render_buffers > 0) {
                        --preload_render_buffers;
                        return PLAYBACK;
                    }

                    unsigned retval = 0;

                    int res = poll(ufds, poll_count, timeoutMs);
                    if (res <= 0)
                        throw runtime_error("poll timed out");

                    unsigned short revents;
                    if (in_poll_count) {
                        res = snd_pcm_poll_descriptors_revents(
                            capture_->handle_, ufds, in_poll_count, &revents);
                        if (res < 0)
                            throw runtime_error("poll revents error: " + string{snd_strerror(res)});
                        if (revents & POLLERR)
                            throw runtime_error("capture poll error");
                        if (revents & POLLIN) {
                            retval |= CAPTURE;
                        }
                    }

                    if (out_poll_count) {
                        res = snd_pcm_poll_descriptors_revents(
                            render_->handle_, ufds + in_poll_count, out_poll_count, &revents);
                        if (res < 0)
                            throw runtime_error("poll revents error: " + string{snd_strerror(res)});
                        if (revents & POLLERR)
                            throw runtime_error("render poll error");
                        if (revents & POLLOUT) {
                            retval |= PLAYBACK;
                        }
                    }

                    return retval;
                };

                while (!stop_thread) {
                    auto res = get_poll_result();
                    if (res == 0) {
                        this_thread::sleep_for(sleepTimeUs);
                        continue;
                    }
                    if (res & CAPTURE) {
                        capture_->handleBuffer(inputBufferView);
                        if (!out_poll_count)
                            res |= PLAYBACK;
                    }
                    if (res & PLAYBACK) {
                        callCallbacks(inputBufferView, outputBufferView);
                        if (out_poll_count)
                            render_->handleBuffer(outputBufferView);
                    }
                }
            } catch (...) {
                sessionTerminated(current_exception());
            }
        }
    };

    struct AlsaDeviceType : dminternal::DeviceType {
        struct ScanToken {
            vector<AlsaDeviceInfo> devices;
        };
        vector<string> device_ignore_list_;

        vector<AlsaDeviceInfo> last_devices_;

        mutex m_;
        map<string, std::shared_ptr<AlsaDevice>> devices_;

        unique_ptr<dminternal::TimerThread> dev_change_thread_;

        size_t alsa_cards_hash_{0};

        static size_t getAlsaSoundcardsHash()
        {
            std::ifstream is("/proc/asound/cards");
            std::string str((std::istreambuf_iterator<char>(is)), std::istreambuf_iterator<char>());
            return std::hash<std::string>{}(str);
        }

        vector<string> getAlsaIgnoreList()
        {
            vector<string> retval;
            auto p = getenv("DAUDIO_ALSA_IGNORE_LIST");
            if (p != NULL) {
                istringstream tokens(p);
                string s;
                while (getline(tokens, s, ',')) {
                    retval.push_back(s);
                }
            }
            return retval;
        }

        /* Enumerates all ALSA output devices (as output by the command aplay -L)
           Does not try to open the devices (with "testDevice" for example),
           so that it also finds devices that are busy and not yet available.
        */
        static vector<AlsaDeviceInfo> getAlsaSoundcards(const vector<string>& ignore_list)
        {
            LOG_TRACE("Get all sound cards...");
            vector<AlsaDeviceInfo> cards;

            auto hintToString = [](const void* hints, const char* t) {
                char* const hint = snd_device_name_get_hint(hints, t);
                if (hint != nullptr) {
                    defer { ::free(hint); };
                    return string{hint};
                }
                return string{};
            };

            void** hints = nullptr;

            if (snd_device_name_hint(-1, "pcm", &hints) == 0) {
                defer { snd_device_name_free_hint(hints); };

                for (char** h = (char**)hints; *h; ++h) {
                    const string id(hintToString(*h, "NAME"));
                    string description(hintToString(*h, "DESC"));
                    const string ioid(hintToString(*h, "IOID"));

                    if (id.empty())
                        continue;

                    LOG_TRACE("ID: " << id << "; desc: " << description << "; ioid: " << ioid);

                    if (id.find("null") != string::npos)
                        continue;

                    if (!ignore_list.empty()) {
                        bool ignored = false;
                        for (auto item : ignore_list) {
                            regex r(item, regex::extended);
                            if (regex_search(id, r)) {
                                ignored = true;
                                break;
                            }
                        }
                        if (ignored) {
                            LOG_TRACE("Device in ignore list, discarding");
                            continue;
                        }
                    }

                    string name = id;
                    find_and_replace(name, "\n", "; ");
                    if (name.empty()) {
                        auto idx_eq = id.find_first_of('=');
                        if (idx_eq != string::npos) {
                            auto idx_c = id.find_first_of(',', idx_eq);
                            if (idx_c != string::npos)
                                name = id.substr(idx_eq + 1, idx_c - idx_eq + 1);
                        }
                    }
                    auto idx_c = description.find_first_of(',');
                    if (idx_c != string::npos)
                        description = description.substr(0, idx_c);

                    if (!description.empty())
                        name += " (" + description + ")";

                    AlsaDeviceInfo dev_info;
                    dev_info.dev_id    = id;
                    dev_info.dev       = daudio::AudioDevice{name,
                                                       daudio::AudioDeviceType::alsa,
                                                       -1,
                                                       -1,
                                                       id.find("default:") != string::npos};
                    dev_info.is_input  = (ioid != "Output") && (id.find("dmix:") == string::npos);
                    dev_info.is_output = (ioid != "Input") && (id.find("dsnoop:") == string::npos);
                    if (dev_info.is_input || dev_info.is_output)
                        cards.push_back(dev_info);
                }
            }
            LOG_TRACE("Got " << cards.size() << " devices");
            return cards;
        }

        AlsaDeviceType(function<void()> callback) : device_ignore_list_(getAlsaIgnoreList())
        {
            snd_lib_error_set_handler(alsa_lib_error_handler);

            if (callback) {
                auto scan_cb = [this, callback] {
                    auto cards_hash = getAlsaSoundcardsHash();
                    if (cards_hash != alsa_cards_hash_) {
                        LOG_TRACE("ALSA cards hash mismatch");
                        auto token = scanDevices();
                        if (updateDevices(move(token))) {
                            alsa_cards_hash_ = cards_hash;
                            callback();
                        }
                    }
                };
#if !defined(DAUDIO_DISABLE_ALSA_CARDS_SCAN) || (DAUDIO_DISABLE_ALSA_CARDS_SCAN == 0)
                dev_change_thread_ =
                    make_unique<dminternal::TimerThread>([scan_cb] { scan_cb(); }, 1000, 0);
#else
                scan_cb();
#endif
            }
        }
        ~AlsaDeviceType() { dev_change_thread_ = nullptr; }

        std::unique_ptr<ScanToken> scanDevices()
        {
            auto token     = make_unique<ScanToken>();
            token->devices = getAlsaSoundcards(device_ignore_list_);
            return token;
        }

        bool updateDevices(std::unique_ptr<ScanToken> token)
        {
            LOG_TRACE("enter, #devices = " << devices_.size()
                                           << ", #devices in = " << token->devices.size());
            bool retval = false;
            lock_guard<mutex> lock{m_};
            set<string> to_remove;
            for (auto& entry : devices_) {
                auto it = find_if(token->devices.begin(), token->devices.end(), [&](auto& new_dev) {
                    return entry.first == new_dev.dev_id;
                });
                if (it == token->devices.end())
                    to_remove.insert(entry.first);
                else {
                    AlsaDevice* dev = entry.second.get();
                    if (dev->externalDevice().is_default != (*it).dev.is_default) {
                        dev->setDefault((*it).dev.is_default);
                        retval = true;
                    }
                }
            }
            for (auto entry : to_remove) {
                devices_.erase(entry);
                retval = true;
            }
            for (auto& entry : token->devices) {
                LOG_TRACE("Processing device ID " << entry.dev_id);
                auto it = devices_.find(entry.dev_id);
                if (it == devices_.end()) {
                    int retries = 3;
                    do {
                        try {
                            auto new_device = make_shared<AlsaDevice>(entry);
                            devices_.insert(make_pair(entry.dev_id, move(new_device)));
                            LOG_TRACE("Device ID " << entry.dev_id << " added");
                            retval = true;
                        } catch (exception& e) {
                            if (--retries > 0) {
                                const int wait_time = 100 + (rand() * 500 / RAND_MAX);
                                std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
                                continue;
                            } else {
                                LOG_WARN(e.what());
                            }
                        }
                    } while (false);
                }
            }
            LOG_TRACE("exit, #devices = " << devices_.size());
            return retval;
        }

        std::vector<daudio::AudioDevice> devices() override
        {
            LOG_TRACE("");
            lock_guard<mutex> lock{m_};
            std::vector<daudio::AudioDevice> devs;
            for (auto it : devices_)
                devs.emplace_back(it.second->externalDevice());
            return devs;
        }

        std::shared_ptr<Device> getDevice(device_id_t id) override
        {
            LOG_TRACE("");
            lock_guard<mutex> lock{m_};
            auto it = std::find_if(devices_.begin(), devices_.end(), [&](auto& entry) {
                return entry.second->id() == id;
            });
            if (it == devices_.end())
                return nullptr;
            return it->second;
        }
    };
}  // namespace

namespace dminternal
{
    template <>
    unique_ptr<dminternal::DeviceType> DeviceType::create<daudio::AudioDeviceType::alsa>(
        std::function<void()> callback)
    {
        return make_unique<AlsaDeviceType>(callback);
    }
}  // namespace dminternal
