#include "dminternal/device.h"

#include <AVFoundation/AVFoundation.h>
#include <AudioUnit/AUComponent.h>
#include <AudioUnit/AudioUnit.h>
#include <Security/SecBase.h>

#include <set>
#include <cmath>

#include <log/log.hpp>

#include <daudio/waveform.h>
#include <dminternal/defer.h>
#include <dminternal/mathutil.h>
#include <dminternal/active.h>

using namespace std;

typedef string AudioDeviceID;
const string kAudioDeviceUnknown = "unknown";

// Element (Bus) 1 is Audio Unit Input, think of 1 as the letter I for Input.
// The Output Scope of Element 1 allows you to set the Client Format - this is the format of the data you will receive when you pull on the input.
// The Input Scope of Element 1 is the Virtual Input Format or Hardware Format - format conversion will occur from HW format to client format as required.
const AudioUnitElement kAudioUnitInputElement = 1;
// Element (Bus) 0 is Audio Unit Output, think of 0 as the letter O for Output.
// The Input Scope of Element 0 allows you to set the Client Format - this is the format of the data you supply to the output unit for rendering.
// The Output Scope of Element 0 is the Virtual Output Format or Hardware Format - format conversion will happen from client format to HW format as required.
const AudioUnitElement kAudioUnitOutputElement = 0;

namespace {
    struct IosAudioDeviceType;
}

//==============================================================================
@interface AudioSessionNotification: NSObject
{
@private
    IosAudioDeviceType* audioDeviceHolder;
};

- (id) init: (IosAudioDeviceType*) audioDeviceHolder;

@end
//==============================================================================

namespace
{
    static void checkRecordPermission()
    {
        auto recordPermission = [[AVAudioSession sharedInstance] recordPermission];
        if (recordPermission != AVAudioSessionRecordPermissionGranted)
        {
            throw runtime_error("record permission not granted");
        }
    }

    static void checkStatus(OSStatus err)
    {
        if (err == noErr)
            return;

        NSError* error = [NSError errorWithDomain: NSOSStatusErrorDomain code: err userInfo: nil];
        NSString* errorMessage = [error localizedDescription];
        throw runtime_error("Error: " + string([errorMessage UTF8String]));
    }

    static void checkError(NSError* error)
    {
        if (!error)
            return;

        NSString* errorMessage = [error localizedDescription];
        throw runtime_error("Error: " + string([errorMessage UTF8String]));
    }

    static void setAudioSessionCategory(AVAudioSessionCategory category = AVAudioSessionCategoryPlayAndRecord)
    {
        NSError* error;
        NSUInteger options = 0;

        if (category == AVAudioSessionCategoryPlayAndRecord)
        {
            // Enable AVAudioSessionCategoryOptionAllowBluetooth, AVAudioSessionCategoryOptionAllowBluetoothA2DP and
            // AVAudioSessionCategoryOptionAllowAirPlay when needed
            options = AVAudioSessionCategoryOptionDefaultToSpeaker;
        }
        else if (category == AVAudioSessionCategoryRecord)
        {
            options = AVAudioSessionCategoryOptionAllowBluetooth;
        }


        [[AVAudioSession sharedInstance] setCategory: category
                                         withOptions: options
                                               error: &error];

        checkError(error);

        LOG_DEBUG("Set audio session category: " << category);
    }

    static void setAudioSessionActive(bool active)
    {
        NSError* error;
        [[AVAudioSession sharedInstance] setActive: active error: &error];

        checkError(error);

        LOG_DEBUG("Set audio session active: " << active);
    }

    static NSArray* getAllInputs()
    {
        auto session = [AVAudioSession sharedInstance];
        auto inputs = [session.currentRoute.inputs arrayByAddingObjectsFromArray: session.availableInputs];

        auto uniqueIds = [NSMutableSet set];
        auto filteredInputs = [NSMutableArray array];
        for (AVAudioSessionPortDescription* input in inputs) {
            if (![uniqueIds containsObject: input.UID]) {
                [uniqueIds addObject: input.UID];
                [filteredInputs addObject: input];
            }
        }

        return filteredInputs;
    }

    static NSArray* getAllOutpus()
    {
        auto session = [AVAudioSession sharedInstance];
        return session.currentRoute.outputs;
    }

    static NSArray* getCurrentRouteInputsAndOutpus()
    {
        auto session = [AVAudioSession sharedInstance];
        auto inputs = session.currentRoute.inputs;
        auto outputs = session.currentRoute.outputs;

        return [inputs arrayByAddingObjectsFromArray: outputs];
    }

    void findSubstrings(const std::string& word, std::set<std::string>& substrings)
    {
        int l = (int)word.length();
        for (int start = 0; start < l; start++) {
            for (int length = 1; length < l - start + 1; length++) {
                substrings.insert(word.substr(start, length));
            }
        }
    }

    std::string lcs(const std::string& first, const std::string& second)
    {
        std::set<std::string> firstSubstrings, secondSubstrings;
        findSubstrings(first, firstSubstrings);
        findSubstrings(second, secondSubstrings);
        std::set<std::string> common;
        std::set_intersection(firstSubstrings.begin(),
                              firstSubstrings.end(),
                              secondSubstrings.begin(),
                              secondSubstrings.end(),
                              std::inserter(common, common.begin()));
        std::vector<std::string> commonSubs(common.begin(), common.end());
        std::sort(
            commonSubs.begin(), commonSubs.end(), [](const std::string& s1, const std::string& s2) {
                return s1.length() > s2.length();
            });
        if (commonSubs.empty())
            return std::string{};

        std::string result = *(commonSubs.begin());
        // Check that it is a whole word, for both inputs
        for (auto& s : {first, second}) {
            auto idx = s.find(result);
            if (idx > 0 && s.at(idx - 1) != ' ')
                return string{};
            if (idx + result.length() >= s.length())
                continue;

            auto c = s.at(idx + result.length() - 1);
            if (c != ' ')
                return string{};
        }
        return result;
    }

    // trim from start (in place)
    static inline void ltrim(std::string& s)
    {
        s.erase(s.begin(),
                std::find_if(s.begin(), s.end(), [](int ch) { return !std::isspace(ch); }));
    }

    // trim from end (in place)
    static inline void rtrim(std::string& s)
    {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) { return !std::isspace(ch); }).base(),
                s.end());
    }

    // trim from both ends (in place)
    static inline void trim(std::string& s)
    {
        ltrim(s);
        rtrim(s);
    }

    // trim from both ends (copying)
    static inline std::string trim_copy(std::string s)
    {
        trim(s);
        return s;
    }

    string remove(string s, const string& to_remove)
    {
        auto idx = s.find(to_remove);
        if (idx != string::npos) {
            s.replace(idx, to_remove.length(), string{});
        }
        return s;
    }

    struct AudioDevice {
        AudioDeviceID id_;
        AudioUnit audio_unit_;

        vector<float> sample_rates_;
        float default_sample_rate_;
        float current_sample_rate_;

        vector<int> buffer_sizes_;
        int default_buffer_size_;
        int input_buffer_size;
        int current_buffer_size_;

        vector<string> channels_;
        daudio::ActiveChannels active_channels_;
        unsigned actual_ca_channels_{0};

        bool interleaved_{false};

        static OSStatus internalProcess(void* inRefCon,
                                        AudioUnitRenderActionFlags* ioActionFlags,
                                        const AudioTimeStamp* inTimeStamp,
                                        UInt32 inBusNumber,
                                        UInt32 inNumberFrames,
                                        AudioBufferList* ioData)
        {
            return reinterpret_cast<AudioDevice*>(inRefCon)->process(
                ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, ioData);
        }

        AudioDevice(AudioDeviceID id, vector<string> channels) : id_(id), channels_(channels)
        {
            actual_ca_channels_ = (unsigned int)channels_.size();
        }

        ~AudioDevice()
        {
        }

        void setupAudioUnit(bool input)
        {
            LOG_DEBUG("Setup AudioUnit for input: " << input);

            OSStatus err = noErr;

            AudioComponent comp;
            AudioComponentDescription desc;

            desc.componentType = kAudioUnitType_Output;
            desc.componentSubType = kAudioUnitSubType_RemoteIO;
            desc.componentManufacturer = kAudioUnitManufacturer_Apple;
            desc.componentFlags = 0;
            desc.componentFlagsMask = 0;

            comp = AudioComponentFindNext(NULL, &desc);
            if (comp == NULL)
                throw runtime_error("AU find next failed");

            err = AudioComponentInstanceNew(comp, &audio_unit_);
            checkStatus(err);

            // Enable/disable input
            UInt32 enableIO = input ? 1 : 0;
            err = AudioUnitSetProperty(audio_unit_,
                                       kAudioOutputUnitProperty_EnableIO,
                                       kAudioUnitScope_Input,
                                       kAudioUnitInputElement,
                                       &enableIO,
                                       sizeof(enableIO));
            checkStatus(err);

            // Disable/enable output
            enableIO = input ? 0 : 1;
            err = AudioUnitSetProperty(audio_unit_,
                                       kAudioOutputUnitProperty_EnableIO,
                                       kAudioUnitScope_Output,
                                       kAudioUnitOutputElement,
                                       &enableIO,
                                       sizeof(enableIO));
            checkStatus(err);

            // Set up callback
            AURenderCallbackStruct cb;
            cb.inputProc = internalProcess;
            cb.inputProcRefCon = this;
            if (input) {
                err = AudioUnitSetProperty(audio_unit_,
                                           kAudioOutputUnitProperty_SetInputCallback,
                                           kAudioUnitScope_Global,
                                           0,
                                           &cb,
                                           sizeof(cb));
                checkStatus(err);
            } else {
                err = AudioUnitSetProperty(audio_unit_,
                                           kAudioUnitProperty_SetRenderCallback,
                                           kAudioUnitScope_Global,
                                           0,
                                           &cb,
                                           sizeof(cb));
                checkStatus(err);
            }
        }

        void disposeAudioUnit()
        {
            LOG_DEBUG("Dispose AudioUnit");

            AudioComponentInstanceDispose(audio_unit_);
            audio_unit_ = nullptr;
        }

        AudioStreamBasicDescription getHardwareStreamFormat(bool input)
        {
            OSStatus err = noErr;
            AudioStreamBasicDescription format;

            UInt32 size = sizeof(format);
            if (input) {
                err = AudioUnitGetProperty(audio_unit_,
                                           kAudioUnitProperty_StreamFormat,
                                           kAudioUnitScope_Input,
                                           kAudioUnitInputElement,
                                           &format,
                                           &size);
                checkStatus(err);
            } else {
                err = AudioUnitGetProperty(audio_unit_,
                                           kAudioUnitProperty_StreamFormat,
                                           kAudioUnitScope_Output,
                                           kAudioUnitOutputElement,
                                           &format,
                                           &size);
                checkStatus(err);
            }

            LOG_DEBUG("Read Hardware AudioStreamBasicDescription:");
            LOG_DEBUG("Sample Rate: " << format.mSampleRate);
            LOG_DEBUG("Format ID: " << format.mFormatID);
            LOG_DEBUG("Format Flags: " << format.mFormatFlags);
            LOG_DEBUG("Bytes per Packet: " << format.mBytesPerPacket);
            LOG_DEBUG("Frames per Packet: " << format.mFramesPerPacket);
            LOG_DEBUG("Bytes per Frame: " << format.mBytesPerFrame);
            LOG_DEBUG("Channels per Frame: " << format.mChannelsPerFrame);
            LOG_DEBUG("Bits per Channel: " << format.mBitsPerChannel);

//            validateStreamFormat(format);

            UInt32 maxBufferSizeFrames;
            size = sizeof(UInt32);
            err = AudioUnitGetProperty(audio_unit_,
                                       kAudioUnitProperty_MaximumFramesPerSlice,
                                       kAudioUnitScope_Global,
                                       0,
                                       &maxBufferSizeFrames,
                                       &size);
            checkStatus(err);

            LOG_DEBUG("Maximum frames per slice: " << maxBufferSizeFrames);

            return format;
        }

        void validateStreamFormat(AudioStreamBasicDescription format)
        {
            if (format.mFormatID != kAudioFormatLinearPCM)
            {
                throw runtime_error("Format is not lpcm");
            }

            if (!(format.mFormatFlags & kAudioFormatFlagIsFloat) || !(format.mFormatFlags & kAudioFormatFlagIsPacked))
            {
                throw runtime_error("Format is not packed float");
            }
        }

        void setClientStreamFormat(AudioStreamBasicDescription hardwareFormat, float fs, bool input)
        {
            auto clientFormat = hardwareFormat;

            clientFormat.mSampleRate = fs;
            clientFormat.mFormatID = kAudioFormatLinearPCM;
            clientFormat.mFormatFlags = kAudioFormatFlagIsFloat |
                kAudioFormatFlagIsNonInterleaved |
                kAudioFormatFlagsNativeEndian |
                kLinearPCMFormatFlagIsPacked;
            clientFormat.mChannelsPerFrame = actual_ca_channels_;
            clientFormat.mBitsPerChannel = 8 * sizeof(float);
            clientFormat.mBytesPerFrame = clientFormat.mBytesPerPacket = sizeof (float);

            LOG_DEBUG("Write Client AudioStreamBasicDescription:");
            LOG_DEBUG("Sample Rate: " << clientFormat.mSampleRate);
            LOG_DEBUG("Format ID: " << clientFormat.mFormatID);
            LOG_DEBUG("Format Flags: " << clientFormat.mFormatFlags);
            LOG_DEBUG("Bytes per Packet: " << clientFormat.mBytesPerPacket);
            LOG_DEBUG("Frames per Packet: " << clientFormat.mFramesPerPacket);
            LOG_DEBUG("Bytes per Frame: " << clientFormat.mBytesPerFrame);
            LOG_DEBUG("Channels per Frame: " << clientFormat.mChannelsPerFrame);
            LOG_DEBUG("Bits per Channel: " << clientFormat.mBitsPerChannel);

            OSStatus err;
            if (input) {
                err = AudioUnitSetProperty(audio_unit_,
                                           kAudioUnitProperty_StreamFormat,
                                           kAudioUnitScope_Output,
                                           kAudioUnitInputElement,
                                           &clientFormat,
                                           sizeof(clientFormat));
                checkStatus(err);
            } else {
                err = AudioUnitSetProperty(audio_unit_,
                                           kAudioUnitProperty_StreamFormat,
                                           kAudioUnitScope_Input,
                                           kAudioUnitOutputElement,
                                           &clientFormat,
                                           sizeof(clientFormat));
                checkStatus(err);
            }
        }

        void setHardwareSampleRateAndBufferSize(float fs, int buffer_size)
        {
            // Make sure this code is called when AVAudioSession is NOT active
            // https://developer.apple.com/library/archive/qa/qa1631/_index.html

            // Do not use results from following functions, they can be different when AVAudioSession is ACTIVE
            setHardwareSampleRate(fs);
            setHardwareBufferSize(fs, buffer_size);
        }

        vector<double> possibleSampleRates()
        {
            return {8000.0f,
                    11025.0f,
                    16000.0f,
                    22050.0f,
                    32000.0f,
                    44100.0f,
                    48000.0f,
                    64000.0f,
                    88200.0f,
                    96000.0f,
                    128000.0f,
                    176400.0f,
                    192000.0f};
        }

        vector<int> possibleBufferSizes()
        {
            const auto min_size = 128;
            const auto max_size = 4096;

            vector<int> retval;
            auto n = dminternal::next_power_of_2(unsigned(min_size));
            while (n <= unsigned(max_size)) {
                retval.push_back(n);
                n += dminternal::next_power_of_2(n / 3);
            }
            return retval;
        }

        double setHardwareSampleRate(double rate)
        {
            NSError* error;
            auto session = [AVAudioSession sharedInstance];
            [session setPreferredSampleRate: rate error: &error];

            if (error)
            {
                NSString* errorMessage = [error localizedDescription];
                LOG_DEBUG("Set hardware sample rate error: " << string([errorMessage UTF8String]));
            }

            return session.sampleRate;
        }

        double getHardwareSampleRate()
        {
            return [AVAudioSession sharedInstance].sampleRate;
        }

        void updateAvailableHardwareSampleRates()
        {
            NSError* error;
            auto session = [AVAudioSession sharedInstance];
            auto defaultSampleRate = session.sampleRate;

            std::set<double> sampleRates;
            for (auto & sampleRate : possibleSampleRates())
            {
                const double supportedSampleRate = setHardwareSampleRate(sampleRate);
                sampleRates.insert(supportedSampleRate);
            }

            for (auto& sampleRate : sampleRates) {
                sample_rates_.push_back(sampleRate);
            }

            default_sample_rate_ = defaultSampleRate;

            [session setPreferredSampleRate: defaultSampleRate error: &error];

            LOG_DEBUG("Default sample rate: " << default_sample_rate_);
        }

        int setHardwareBufferSize(const double currentSampleRate, const int bufferSize)
        {
            NSError* error;
            NSTimeInterval bufferDuration = currentSampleRate > 0 ? (NSTimeInterval) ((bufferSize + 1) / currentSampleRate) : 0.0;

            auto session = [AVAudioSession sharedInstance];
            [session setPreferredIOBufferDuration: bufferDuration error: &error];

            if (error)
            {
                NSString* errorMessage = [error localizedDescription];
                LOG_DEBUG("Set hardware buffer size error: " << string([errorMessage UTF8String]));
            }


            return round(currentSampleRate * [AVAudioSession sharedInstance].IOBufferDuration);
        }

        int getHardwareBufferSize()
        {
            auto session = [AVAudioSession sharedInstance];
            return round(session.sampleRate * session.IOBufferDuration);
        }

        void updateAvailableHardwareBufferSizes()
        {
            NSError* error;
            auto session = [AVAudioSession sharedInstance];
            auto defaultBufferDuration = session.IOBufferDuration;

            std::set<double> bufferSizes;
            for (auto & bufferSize : possibleBufferSizes())
            {
                const double supportedBufferSize = setHardwareBufferSize(session.sampleRate, bufferSize);
                bufferSizes.insert(supportedBufferSize);
            }

            for (auto& bufferSize : bufferSizes) {
                buffer_sizes_.push_back(bufferSize);
            }

            default_buffer_size_ = (int)round(session.sampleRate * defaultBufferDuration);
            if (find(buffer_sizes_.begin(), buffer_sizes_.end(), default_buffer_size_) == buffer_sizes_.end())
            {
                auto it = std::upper_bound(buffer_sizes_.begin(), buffer_sizes_.end(), default_buffer_size_);
                if (it == buffer_sizes_.end()) {
                    default_buffer_size_ = buffer_sizes_.back();
                }
                else
                {
                    default_buffer_size_ = buffer_sizes_.begin() == it ? *it : *(--it);
                }
            }

            [session setPreferredIOBufferDuration: defaultBufferDuration error: &error];

            LOG_DEBUG("Default buffer size: " << default_buffer_size_);
        }

        void getHardwareSampleRateAndBufferSize(float fs, int buffer_size)
        {
            // Make sure this code is called when AVAudioSession is active
            // https://developer.apple.com/library/archive/qa/qa1631/_index.html

            // These are the real values from hardware device
            auto currentHardwareSampleRate = getHardwareSampleRate();
            auto currentHardwareBufferSize = getHardwareBufferSize();

            current_sample_rate_ = fs;
            input_buffer_size = buffer_size;
            // This is a hardware buffer size and we assume that client buffer size is the same,
            // but we can get a different number of frames in AURenderCallback (internalProcess)
            current_buffer_size_ = currentHardwareBufferSize;
            // Here we are calculating probable client buffer size, but this is only for logging purpose
            auto probable_buffer_size = (int)round(currentHardwareBufferSize * (fs / currentHardwareSampleRate));

            LOG_DEBUG("Current hardware sample rate (ACTIVE): " << currentHardwareSampleRate);
            LOG_DEBUG("Current hardware buffer size (ACTIVE): " << currentHardwareBufferSize);
            LOG_DEBUG("Current client sample rate: " << current_sample_rate_);
            LOG_DEBUG("Current client buffer size: " << current_buffer_size_);
            LOG_DEBUG("Probable client buffer size: " << probable_buffer_size);
        }

        vector<string> channels() const
        {
            return channels_;
        }

        void openDevice(float fs, int buffer_size, daudio::ActiveChannels channels, bool input)
        {
            LOG_DEBUG("Open device:");
            LOG_DEBUG("fs = " << fs);
            LOG_DEBUG("buffer size = " << buffer_size);
            LOG_DEBUG("channels = " << channels.channels.size());
            LOG_DEBUG("input = " << input);

            if (none_of(sample_rates_.begin(), sample_rates_.end(), [fs](auto v) { return v == fs; }))
                throw runtime_error("sample rate not supported");
            if (none_of(buffer_sizes_.begin(), buffer_sizes_.end(), [buffer_size](auto v) { return v == buffer_size; }))
                throw runtime_error("buffer size not supported");

            if (channels.channels.size() != actual_ca_channels_)
                throw runtime_error("wrong number of entries in channels, should be " +
                                    to_string(actual_ca_channels_));

            active_channels_ = channels;

            setupAudioUnit(input);
            auto hardwareFormat = getHardwareStreamFormat(input);
            setClientStreamFormat(hardwareFormat, fs, input);
            setHardwareSampleRateAndBufferSize(fs, buffer_size);
        }

        void initializeAudioUnit()
        {
            LOG_DEBUG("Initialize AudioUnit");

            auto err = AudioUnitInitialize(audio_unit_);
            checkStatus(err);
        }

        void start()
        {
            LOG_DEBUG("Start AudioUnit");

            auto err = AudioOutputUnitStart(audio_unit_);
            checkStatus(err);
        }

        void uninitializeAudioUnit()
        {
            LOG_DEBUG("Uninitialize AudioUnit");

            auto err = AudioUnitUninitialize(audio_unit_);
            checkStatus(err);
        }

        void stop()
        {
            LOG_DEBUG("Stop AudioUnit");

            auto err = AudioOutputUnitStop(audio_unit_);
            checkStatus(err);
        }

        bool isRunning() const
        {
            UInt32 running = 0;
            UInt32 size = sizeof(running);
            if (audio_unit_) {
                auto err = AudioUnitGetProperty(audio_unit_,
                                                kAudioOutputUnitProperty_IsRunning,
                                                kAudioUnitScope_Global,
                                                0,
                                                &running,
                                                &size);
                checkStatus(err);
                return running;
            }
            return false;
        }

        void closeDevice()
        {
            if (isRunning()) {
                stop();
                active_channels_ = daudio::ActiveChannels{};
                uninitializeAudioUnit();
                disposeAudioUnit();
                setAudioSessionActive(false);
            }
        }

        virtual OSStatus process(AudioUnitRenderActionFlags*,
                                 const AudioTimeStamp*,
                                 UInt32,
                                 UInt32,
                                 AudioBufferList*) = 0;
        virtual void restart() = 0;

        virtual bool hasVolumeControl() const { return false; }
        virtual float getVolume() const { return 0.f; }
        virtual void setVolume(float) {}
        virtual bool getMuted() const { return false; }
        virtual void setMuted(bool) {}
    };

    struct AudioInputDevice : AudioDevice {
        daudio::Waveforms buffers_;
        daudio::InputBufferView input_view_;
        function<void()> callback_;
        AudioBufferList* buffer_list_{nullptr};

        AudioInputDevice(AudioDeviceID id, vector<string> channels) : AudioDevice(id, channels) { prepare(); }

        ~AudioInputDevice() { close(); }

        void prepare()
        {
            setAudioSessionCategory();
            setAudioSessionActive(true);

            setPrefferedInput();

            updateAvailableHardwareSampleRates();
            updateAvailableHardwareBufferSizes();

            resetPrefferedInput();

            setAudioSessionActive(false);
        }

        void open(float fs, int buffer_size, daudio::ActiveChannels channels)
        {
            checkRecordPermission();

            setAudioSessionCategory();
            openDevice(fs, buffer_size, channels, true);
            setAudioSessionActive(true);
            setPrefferedInput();
            initializeAudioUnit();
            getHardwareSampleRateAndBufferSize(fs, buffer_size);
            setupBuffers();
        }

        void setupBuffers()
        {
            buffers_.resize(actual_ca_channels_, current_buffer_size_);
            input_view_.channel.clear();
            for (unsigned i = 0; i < actual_ca_channels_; ++i) {
                if (active_channels_.channels[i]) {
                    input_view_.channel.emplace_back(buffers_.waveforms[i].data(),
                                                     buffers_.waveforms[i].data() + current_buffer_size_);
                }
            }

            if (interleaved_) {
                const auto size =
                    offsetof(AudioBufferList, mBuffers[0]) + (sizeof(AudioBuffer) * 1);
                buffer_list_ = (AudioBufferList*)malloc(size);
                buffer_list_->mNumberBuffers = 1;
                buffer_list_->mBuffers[0].mNumberChannels = actual_ca_channels_;
                buffer_list_->mBuffers[0].mDataByteSize =
                    actual_ca_channels_ * current_buffer_size_ * sizeof(float);
                buffer_list_->mBuffers[0].mData = malloc(buffer_list_->mBuffers[0].mDataByteSize);
            } else {
                const auto size = offsetof(AudioBufferList, mBuffers[0]) +
                                  (sizeof(AudioBuffer) * actual_ca_channels_);
                buffer_list_ = (AudioBufferList*)malloc(size);
                buffer_list_->mNumberBuffers = actual_ca_channels_;
                for (unsigned i = 0; i < actual_ca_channels_; ++i) {
                    buffer_list_->mBuffers[i].mNumberChannels = 1;
                    buffer_list_->mBuffers[i].mData = buffers_.waveforms[i].data();
                    buffer_list_->mBuffers[i].mDataByteSize = current_buffer_size_ * sizeof(float);
                }
            }
        }

        void setPrefferedInput()
        {
            LOG_DEBUG("Set preffered input for: " << id_);

            auto inputs = getAllInputs();

            for (AVAudioSessionPortDescription* port in inputs) {
                auto portId = string([port.UID UTF8String]);
                if (portId == id_)
                {
                    LOG_DEBUG("Setting preffered input using AVAudioSession: " << id_);

                    NSError* error;
                    [[AVAudioSession sharedInstance] setPreferredInput: port error: &error];
                    checkError(error);
                    break;
                }
            }
        }

        void resetPrefferedInput()
        {
            NSError* error;
            [[AVAudioSession sharedInstance] setPreferredInput: nil error: &error];
            checkError(error);
        }

        void close()
        {
            closeDevice();
            if (buffer_list_ != nullptr) {
                if (interleaved_)
                    free(buffer_list_->mBuffers[0].mData);
                free(buffer_list_);
                buffer_list_ = nullptr;
            }
        }

        void restart() override
        {
            auto sampleRate = current_sample_rate_;
            auto bufferSize = current_buffer_size_;
            auto channels = active_channels_;

            close();
            open(sampleRate, bufferSize, channels);
        }

        void prepare(function<void()> callback) { callback_ = callback; }

        OSStatus process(AudioUnitRenderActionFlags* ioActionFlags,
                         const AudioTimeStamp* inTimeStamp,
                         UInt32 busNumber,
                         UInt32 numOfFrames,
                         AudioBufferList*) override
        {
            LOG_DEBUG("Input process:");
            LOG_DEBUG("busNumber = " << busNumber);
            LOG_DEBUG("numOfFrames = " << numOfFrames);

            if (numOfFrames != (UInt32)current_buffer_size_)
            {
                LOG_DEBUG("Buffer size changed:");
                LOG_DEBUG("old = " << current_buffer_size_);
                LOG_DEBUG("current = " << numOfFrames);

                current_buffer_size_ = numOfFrames;
                setupBuffers();
            }

            auto err = AudioUnitRender(
                audio_unit_, ioActionFlags, inTimeStamp, busNumber, numOfFrames, buffer_list_);

            if (interleaved_) {
                unsigned out_ch = 0;
                for (unsigned ch = 0; ch < actual_ca_channels_; ++ch) {
                    if (!active_channels_.channels[ch])
                        continue;

                    const float* src =
                        static_cast<const float*>(buffer_list_->mBuffers[0].mData) + ch;
                    float* dst = buffers_.waveforms[out_ch++].data();
                    for (unsigned i = 0; i < numOfFrames; ++i, src += actual_ca_channels_, dst++)
                        *dst = *src;
                }
            }
            callback_();
            return err;
        };

        bool hasVolumeControl() const override {
            auto session = [AVAudioSession sharedInstance];
            return [session isInputGainSettable];
        }

        float getVolume() const override {
            auto session = [AVAudioSession sharedInstance];
            return [session inputGain];
        }

        void setVolume(float volume) override {
            auto session = [AVAudioSession sharedInstance];
            if ([session isInputGainSettable])
            {
                NSError* error;
                [session setInputGain: volume error: &error];
                checkError(error);
            }
        }
    };

    struct AudioOutputDevice : AudioDevice {
        daudio::OutputBufferView output_view_;
        daudio::Waveforms buffers_;
        function<void()> callback_;

        AudioOutputDevice(AudioDeviceID id, vector<string> channels) : AudioDevice(id, channels) { prepare(); }
        ~AudioOutputDevice() { close(); }

        void prepare()
        {
            LOG_DEBUG("Prepare output AudioDevice");

            setAudioSessionCategory();
            setAudioSessionActive(true);

            updateAvailableHardwareSampleRates();
            updateAvailableHardwareBufferSizes();

            setAudioSessionActive(false);
        }

        void open(float fs, int buffer_size, daudio::ActiveChannels channels)
        {
            setAudioSessionCategory();
            openDevice(fs, buffer_size, channels, false);
            setAudioSessionActive(true);
            initializeAudioUnit();
            getHardwareSampleRateAndBufferSize(fs, buffer_size);
            setupBuffers();
        }

        void setupBuffers()
        {
            if (interleaved_) {
                buffers_.resize((int)active_channels_, current_buffer_size_);
                output_view_.channel.clear();
                for (unsigned i = 0; i < active_channels_; ++i)
                    output_view_.channel.emplace_back(buffers_.waveforms[i].data(),
                                                      buffers_.waveforms[i].data() + current_buffer_size_);
            } else {
                output_view_.channel.clear();
                for (unsigned i = 0; i < active_channels_; ++i)
                    output_view_.channel.emplace_back(nullptr, nullptr);
            }
        }

        void close() { closeDevice(); }

        void restart() override
        {
            auto sampleRate = current_sample_rate_;
            auto bufferSize = current_buffer_size_;
            auto channels = active_channels_;

            close();
            open(sampleRate, bufferSize, channels);
        }

        void prepare(function<void()> callback) { callback_ = callback; }

        OSStatus process(AudioUnitRenderActionFlags*,
                         const AudioTimeStamp*,
                         UInt32 busNumber,
                         UInt32 numOfFrames,
                         AudioBufferList* bufList) override
        {
            LOG_DEBUG("Output process:");
            LOG_DEBUG("busNumber = " << busNumber);
            LOG_DEBUG("numOfFrames = " << numOfFrames);

            if (numOfFrames != (UInt32)current_buffer_size_)
            {
                LOG_DEBUG("Buffer size changed:");
                LOG_DEBUG("old = " << current_buffer_size_);
                LOG_DEBUG("current = " << numOfFrames);

                current_buffer_size_ = numOfFrames;
                setupBuffers();
            }

            if (!interleaved_) {
                for (unsigned i = 0; i < bufList->mNumberBuffers; ++i) {
                    char* ptr = static_cast<char*>(bufList->mBuffers[i].mData);
                    output_view_.channel[i] = daudio::Range<float>(
                        (float*)ptr, (float*)(ptr + bufList->mBuffers[i].mDataByteSize));
                }
            }

            callback_();

            if (interleaved_) {
                const unsigned long no_of_channels = active_channels_;
                for (unsigned ch = 0; ch < no_of_channels; ++ch) {
                    const float* src = buffers_.waveforms[ch].data();
                    float* dst = static_cast<float*>(bufList->mBuffers[0].mData) + ch;
                    for (unsigned i = 0; i < numOfFrames; ++i, src++, dst += no_of_channels)
                        *dst = *src;
                }
            }
            return noErr;
        };

        float getVolume() const override
        {
            auto audioSession = [AVAudioSession sharedInstance];
            float volume = audioSession.outputVolume;

            return volume;
        }
    };

    struct IosAudioDeviceInfo {
        daudio::AudioDevice dev;
        AudioDeviceID input_id;
        AudioDeviceID output_id;
        vector<std::string> channels;
    };

    struct IosAudioDevice : dminternal::Device {
        unique_ptr<AudioInputDevice> capture_;
        unique_ptr<AudioOutputDevice> render_;

        vector<float> sample_rates_;
        float default_sample_rate_{0.f};
        float current_sample_rate_{0.f};
        vector<int> buffer_sizes_;
        int default_buffer_size_{0};
        int current_buffer_size_{0};

        atomic<bool> opened_{false};

        IosAudioDevice(IosAudioDeviceInfo dev) : dminternal::Device(dev.dev)
        {
            if (dev.input_id != kAudioDeviceUnknown) {
                capture_ = make_unique<AudioInputDevice>(dev.input_id, dev.channels);
            }
            if (dev.output_id != kAudioDeviceUnknown) {
                render_ = make_unique<AudioOutputDevice>(dev.output_id, dev.channels);
            }

            if (!capture_ && !render_)
                throw runtime_error("No input or output");

            init();
        }
        ~IosAudioDevice() { close(); }

        void init()
        {
            // Check sample rates
            if (capture_ && render_) {
                std::set_intersection(capture_->sample_rates_.begin(),
                                      capture_->sample_rates_.end(),
                                      render_->sample_rates_.begin(),
                                      render_->sample_rates_.end(),
                                      back_inserter(sample_rates_));
                if (sample_rates_.empty())
                    throw runtime_error("capture&render does not share any sample rates");

                if (capture_->default_sample_rate_ != render_->default_sample_rate_) {
                    default_sample_rate_ = sample_rates_[0];
                    for (auto rate : sample_rates_) {
                        if (rate > 48000.0f)
                            break;
                        default_sample_rate_ = rate;
                    }
                } else {
                    default_sample_rate_ = capture_->default_sample_rate_;
                }

                std::set_intersection(capture_->buffer_sizes_.begin(),
                                      capture_->buffer_sizes_.end(),
                                      render_->buffer_sizes_.begin(),
                                      render_->buffer_sizes_.end(),
                                      back_inserter(buffer_sizes_));
                if (buffer_sizes_.empty())
                    throw runtime_error("capture&render does not share any buffer sizes");

            } else {
                auto d = capture_ ? static_cast<AudioDevice*>(capture_.get())
                                  : static_cast<AudioDevice*>(render_.get());
                sample_rates_ = d->sample_rates_;
                buffer_sizes_ = d->buffer_sizes_;
                default_sample_rate_ = d->default_sample_rate_;
                default_buffer_size_ = d->default_buffer_size_;
            }
        }

        float defaultSampleRate() const override { return default_sample_rate_; }

        int defaultBuffersize() const override { return default_buffer_size_; }

        vector<float> supportedSamplerates() const override { return sample_rates_; }

        vector<int> supportedBuffersizes() const override { return buffer_sizes_; }

        vector<string> inputChannelNames() const override
        {
            if (capture_) {
                return capture_->channels();
            }

            return {};
        }

        vector<string> outputChannelNames() const override
        {
            if (render_) {
                return render_->channels();
            }
            return {};
        }

        void open(float fs,
                  int bufferSize,
                  daudio::ActiveChannels inputMask,
                  daudio::ActiveChannels outputMask) override
        {
            if (inputMask == 0 && outputMask == 0)
            {
                throw runtime_error("no input or output mask");
            }

            if (capture_ && inputMask > 0)
            {
                capture_->open(fs, bufferSize, inputMask);
            }
            if (render_ && outputMask > 0)
            {
                render_->open(fs, bufferSize, outputMask);
            }

            if (capture_) {
                if (!render_ || outputMask == 0) {
                    capture_->prepare([this, output_view = daudio::OutputBufferView()]() mutable {
                        callCallbacks(capture_->input_view_, output_view);
                    });
                }
            }

            if (render_) {
                render_->prepare([this,
                                  dummy_view = daudio::InputBufferView(),
                                  useCaptureView = (capture_ && (inputMask > 0))]() mutable {
                    auto& input_view = useCaptureView ? capture_->input_view_ : dummy_view;
                    callCallbacks(input_view, render_->output_view_);
                });
            }

            if (capture_ && inputMask > 0)
                capture_->start();
            if (render_ && outputMask > 0)
                render_->start();

            opened_ = true;
        }

        void close() override
        {
            if (opened_.exchange(false)) {
                if (capture_)
                    capture_->close();
                if (render_)
                    render_->close();

                current_sample_rate_ = 0.f;
                current_buffer_size_ = 0;
            }
        }

        bool isOpened() const override { return opened_; }

        string getAudioDeviceID()
        {
            auto d = capture_ ? static_cast<AudioDevice*>(capture_.get())
                              : static_cast<AudioDevice*>(render_.get());

            return d->id_;
        }

        float currentSampleRate() const override {
            auto d = capture_ ? static_cast<AudioDevice*>(capture_.get())
                              : static_cast<AudioDevice*>(render_.get());

            return d->current_sample_rate_;
        }

        int currentBuffersize() const override {
            auto d = capture_ ? static_cast<AudioDevice*>(capture_.get())
                              : static_cast<AudioDevice*>(render_.get());

            return d->current_buffer_size_;
        }

        const daudio::ActiveChannels& activeInputChannels() const override
        {
            static daudio::ActiveChannels none{};
            return capture_ ? capture_->active_channels_ : none;
        }

        const daudio::ActiveChannels& activeOutputChannels() const override
        {
            static daudio::ActiveChannels none{};
            return render_ ? render_->active_channels_ : none;
        }

        bool hasInputGainControl() const override
        {
            return capture_ ? capture_->hasVolumeControl() : false;
        }

        float getInputGain() const override
        {
            if (!capture_)
                throw runtime_error("no capture endpoint");
            return capture_->getVolume();
        }

        void setInputGain(float gain) override
        {
            if (!capture_)
                throw runtime_error("no capture endpoint");
            capture_->setVolume(gain);
        }

        bool getInputMuted() const override
        {
            if (!capture_)
                throw runtime_error("no capture endpoint");
            return capture_->getMuted();
        }

        void setInputMuted(bool mute) override
        {
            if (!capture_)
                throw runtime_error("no capture endpoint");
            capture_->setMuted(mute);
        }

        bool hasOutputVolumeControl() const override
        {
            return render_ ? render_->hasVolumeControl() : false;
        }

        float getOutputVolume() const override
        {
            if (!render_)
                throw runtime_error("no render endpoint");
            return render_->getVolume();
        }

        void setOutputVolume(float volume) override
        {
            if (!render_)
                throw runtime_error("no render endpoint");
            render_->setVolume(volume);
        }

        bool getOutputMuted() const override
        {
            if (!render_)
                throw runtime_error("no render endpoint");
            return render_->getMuted();
        }

        void setOutputMuted(bool mute) override
        {
            if (!render_)
                throw runtime_error("no render endpoint");
            render_->setMuted(mute);
        }

        void handleInterruptionBegin()
        {
            if (capture_)
            {
                capture_->stop();
            }

            if (render_)
            {
                render_->stop();
            }
        }

        void handleInterruptionEnd(bool)
        {
            if (capture_)
            {
                capture_->start();
            }

            if (render_)
            {
                render_->start();
            }
        }

        void handleMediaServicesReset()
        {
            if (capture_)
            {
                capture_->restart();
            }

            if (render_)
            {
                render_->restart();
            }
        }

        void handleMediaServicesLost()
        {
            if (capture_)
            {
                capture_->stop();
            }

            if (render_)
            {
                render_->stop();
            }
        }

        void handleDeviceRemoved()
        {
            // Device is not part of the current route
            close();
            sessionTerminated(make_exception_ptr(runtime_error("device removed")));
        }
    };

    struct IosAudioDeviceType : dminternal::DeviceType {
        mutex m_;
        map<pair<AudioDeviceID, AudioDeviceID>, shared_ptr<IosAudioDevice>> devices_;
        id audio_session_notification_;

        struct ScanToken {
            vector<IosAudioDeviceInfo> devs;
        };

        function<void()> callback_;

        IosAudioDeviceType(function<void()> callback) : callback_(callback)
        {
            audio_session_notification_ = [[AudioSessionNotification alloc] init: this];
            scanDevices();
        }

        ~IosAudioDeviceType()
        {
            devices_.clear();
        }

        vector<string> deviceChannels(AVAudioSessionPortDescription* port, unsigned long channelsCount)
        {
            vector<string> channels;
            for (AVAudioSessionChannelDescription* channel in port.channels)
            {
                auto channelName = string([channel.channelName UTF8String]);
                channels.push_back(channelName);
            }

            if (channels.size() != channelsCount)
            {
                for (unsigned long i = channels.size();i < channelsCount;++i)
                {
                    auto channelName = "Channel_" + std::to_string(i);
                    channels.push_back(channelName);
                }
            }

            return channels;
        }

        void scanDevices()
        {
            LOG_DEBUG("Scan devices");

            setAudioSessionCategory();

            auto token = make_unique<ScanToken>();

            auto inputs = getAllInputs();
            for (AVAudioSessionPortDescription* port in inputs) {
                auto portId = string([port.UID UTF8String]);
                auto portName = string([port.portName UTF8String]);
                auto portChannelsCount = max((int)[port.channels count], 1);

                IosAudioDeviceInfo entry;
                entry.output_id = kAudioDeviceUnknown;
                entry.input_id = portId;
                entry.channels = deviceChannels(port, portChannelsCount);
                entry.dev = daudio::AudioDevice(trim_copy(portName),
                                                daudio::AudioDeviceType::coreaudio,
                                                portChannelsCount,
                                                0,
                                                token->devs.size() == 0);

                token->devs.push_back(entry);

                LOG_DEBUG("Input device: " << portId << ", " << portName);
                LOG_DEBUG("Channels: ");
                for (auto & ch : entry.channels)
                {
                    LOG_DEBUG("- " << ch);
                }
            }

            auto outputs = getAllOutpus();
            for (AVAudioSessionPortDescription* port in outputs) {
                auto portId = string([port.UID UTF8String]);
                auto portName = string([port.portName UTF8String]);
                auto portChannelsCount = max((int)[port.channels count], 2);

                IosAudioDeviceInfo entry;
                entry.output_id = portId;
                entry.input_id = kAudioDeviceUnknown;
                entry.channels = deviceChannels(port, portChannelsCount);
                entry.dev = daudio::AudioDevice(trim_copy(portName),
                                                daudio::AudioDeviceType::coreaudio,
                                                0,
                                                portChannelsCount,
                                                true);
                token->devs.push_back(entry);

                LOG_DEBUG("Output device: " << portId << ", " << portName);
                LOG_DEBUG("Channels: ");
                for (auto & ch : entry.channels)
                {
                    LOG_DEBUG("- " << ch);
                }
            }

            if (token && updateDevices(std::move(token))) {
                callback_();
            }
        }

        bool updateDevices(unique_ptr<ScanToken> token)
        {
            bool retval = false;
            lock_guard<mutex> lock{m_};

            set<pair<AudioDeviceID, AudioDeviceID>> to_remove;
            for (auto& entry : devices_)
            {
                auto it = find_if(token->devs.begin(), token->devs.end(), [&](auto& d) {
                    return d.input_id == entry.first.first && d.output_id == entry.first.second;
                });
                if (it == token->devs.end())
                    to_remove.insert(entry.first);
                else
                {
                    auto dev = entry.second.get();
                    // Update existing device if default status has changed
                    if (dev->externalDevice().is_default != (*it).dev.is_default) {
                        dev->setDefault((*it).dev.is_default);
                        retval = true;
                    }
                }
            }
            for (auto& key : to_remove) {
                devices_.erase(key);
                retval = true;
            }

            for (auto& entry : token->devs) {
                auto key = make_pair(entry.input_id, entry.output_id);
                auto it = devices_.find(key);
                if (it == devices_.end()) {
                    try {
                        auto new_device = make_shared<IosAudioDevice>(entry);
                        assert(none_of(devices_.begin(), devices_.end(), [id = new_device->externalDevice().id](auto& it) { return it.second->externalDevice().id == id; }));
                        devices_.insert(make_pair(key, std::move(new_device)));
                        retval = true;
                    } catch(exception& e) {
                        LOG_DEBUG(e.what());
                    }
                }
            }

            return retval;
        }

        vector<daudio::AudioDevice> devices() override
        {
            lock_guard<mutex> lock{m_};
            std::vector<daudio::AudioDevice> devs;
            for (auto it : devices_)
                devs.emplace_back(it.second->externalDevice());
            return devs;
        }

        shared_ptr<dminternal::Device> getDevice(device_id_t id) override
        {
            lock_guard<mutex> lock{m_};
            auto it = find_if(devices_.begin(), devices_.end(), [&](auto& entry) {
                return entry.second->id() == id;
            });
            if (it == devices_.end())
                return nullptr;
            return it->second;
        }

        void handleInterruptionBegin()
        {
            LOG_DEBUG("Handle interruption begin");

            lock_guard<mutex> lock{m_};
            for (auto it : devices_)
            {
                auto device = it.second;
                if (device->isOpened())
                {
                    device->handleInterruptionBegin();
                }
            }
        }

        void handleInterruptionEnd(bool shouldResume)
        {
            LOG_DEBUG("Handle interruption end, shouldResume = " << shouldResume);

            lock_guard<mutex> lock{m_};
            for (auto it : devices_)
            {
                auto device = it.second;
                if (device->isOpened())
                {
                    device->handleInterruptionEnd(shouldResume);
                }
            }
        }

        void handleMediaServicesReset()
        {
            LOG_DEBUG("Handle media services reset");

            lock_guard<mutex> lock{m_};
            for (auto it : devices_)
            {
                auto device = it.second;
                if (device->isOpened())
                {
                    device->handleMediaServicesReset();
                }
            }
        }

        void handleMediaServicesLost()
        {
            LOG_DEBUG("Handle media services lost");

            lock_guard<mutex> lock{m_};
            for (auto it : devices_)
            {
                auto device = it.second;
                if (device->isOpened())
                {
                    device->handleMediaServicesLost();
                }
            }
        }

        void handleRouteChange(AVAudioSessionRouteChangeReason reason)
        {
            LOG_DEBUG("Handle route change: " << reason);

            switch (reason)
            {
                case AVAudioSessionRouteChangeReasonUnknown:
                case AVAudioSessionRouteChangeReasonCategoryChange:
                case AVAudioSessionRouteChangeReasonRouteConfigurationChange:
                    break;
                case AVAudioSessionRouteChangeReasonWakeFromSleep:
                case AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory:
                case AVAudioSessionRouteChangeReasonOverride:
                case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
                case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
                {
                    if (isDeviceOpened())
                    {
                        verifyActiveDevices();
                    }

                    if (!isDeviceOpened())
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            scanDevices();
                        });
                    }
                }
                break;
            }
        }

        bool isDeviceOpened()
        {
            lock_guard<mutex> lock{m_};
            auto it = find_if(devices_.begin(), devices_.end(), [&](auto& d) {
                auto device = d.second;
                return device->isOpened();
            });

            return it != devices_.end();
        }

        void verifyActiveDevices()
        {
            lock_guard<mutex> lock{m_};

            auto routeIO = getCurrentRouteInputsAndOutpus();
            
            vector<string> portIds = {};
            for (AVAudioSessionPortDescription* port in routeIO)
            {
                auto portId = string([port.UID UTF8String]);
                portIds.push_back(portId);
            }

            for (auto it : devices_)
            {
                auto device = it.second;
                if (device->isOpened())
                {
                    auto deviceId = device->getAudioDeviceID();
                    if (find(portIds.begin(), portIds.end(), deviceId) == portIds.end())
                    {
                        LOG_DEBUG("Handle device removed: " << deviceId);
                        // Current device not found in the current route
                        device->handleDeviceRemoved();
                    }
                }
            }
        }
    };
}  // namespace

namespace dminternal
{
    template <>
    unique_ptr<dminternal::DeviceType> DeviceType::create<daudio::AudioDeviceType::coreaudio>(
        function<void()> callback)
    {
        return make_unique<IosAudioDeviceType>(callback);
    }
}  // namespace dminternal

//==============================================================================
@implementation AudioSessionNotification

- (id) init: (IosAudioDeviceType*) holder
{
    self = [super init];

    if (self)
    {
        audioDeviceHolder = holder;

        auto session = [AVAudioSession sharedInstance];
        auto centre = [NSNotificationCenter defaultCenter];

        [centre addObserver: self
                   selector: @selector (handleInterruption:)
                       name: AVAudioSessionInterruptionNotification
                     object: session];

        [centre addObserver: self
                   selector: @selector (handleMediaServicesLost)
                       name: AVAudioSessionMediaServicesWereLostNotification
                     object: session];

        [centre addObserver: self
                   selector: @selector (handleMediaServicesReset)
                       name: AVAudioSessionMediaServicesWereResetNotification
                     object: session];

        [centre addObserver: self
                   selector: @selector (handleRouteChange:)
                       name: AVAudioSessionRouteChangeNotification
                     object: session];
    }

    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void) handleInterruption: (NSNotification*) notification
{
    NSNumber* typeValue = notification.userInfo[AVAudioSessionInterruptionTypeKey];

    if (typeValue) {
        AVAudioSessionInterruptionType type = (AVAudioSessionInterruptionType)typeValue.unsignedIntegerValue;
        switch (type)
        {
            case AVAudioSessionInterruptionTypeBegan:
                audioDeviceHolder->handleInterruptionBegin();
                break;

            case AVAudioSessionInterruptionTypeEnded:
                NSNumber* interruptionOptionsValue = notification.userInfo[AVAudioSessionInterruptionOptionKey];
                if (interruptionOptionsValue)
                {
                    AVAudioSessionInterruptionOptions interruptionOptions = (AVAudioSessionInterruptionOptions)typeValue.unsignedIntegerValue;
                    audioDeviceHolder->handleInterruptionEnd(interruptionOptions == AVAudioSessionInterruptionOptionShouldResume);
                }
                else
                {
                    audioDeviceHolder->handleInterruptionEnd(false);
                }
                break;
        }

    }
}

- (void) handleMediaServicesReset
{
    audioDeviceHolder->handleMediaServicesReset();
}

- (void) handleMediaServicesLost
{
    audioDeviceHolder->handleMediaServicesLost();
}

- (void) handleRouteChange: (NSNotification*) notification
{
    NSNumber* reasonValue = notification.userInfo[AVAudioSessionRouteChangeReasonKey];
    if (reasonValue)
    {
        AVAudioSessionRouteChangeReason reason = (AVAudioSessionRouteChangeReason)reasonValue.unsignedIntegerValue;
        audioDeviceHolder->handleRouteChange(reason);
    }
}

@end
//==============================================================================
