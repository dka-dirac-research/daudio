#include "dminternal/device.h"

using namespace std;

#if defined(CMAKE_SYSTEM_NAME__Android)

#include "datamodel/Waveform.h"

#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include <SLES/OpenSLES_AndroidConfiguration.h>

#include <assert.h>
#include <dlfcn.h>

#include <algorithm>
#include <atomic>

namespace
{
    template <typename Type>
    inline Type limit(const Type lo, const Type hi, const Type value)
    {
        assert(lo <= hi);
        return (value < lo) ? lo : ((value > hi) ? hi : value);
    }

    template <typename Container>
    inline bool contains(Container const& c, typename Container::const_reference v)
    {
        return find(c.begin(), c.end(), v) != c.end();
    }

    struct DynamicLibrary {
        void* handle;
        DynamicLibrary(std::string name) : handle(dlopen(name.c_str(), RTLD_NOW)) {}
        bool valid() const { return handle != nullptr; }

        template <typename Type = void*>
        Type symbol(std::string name)
        {
            return (Type)symbol_raw(name);
        }

    private:
        void* symbol_raw(std::string name) { return dlsym(handle, name.c_str()); }
    };

    struct libOpenSLES : DynamicLibrary {
        libOpenSLES() : DynamicLibrary("libOpenSLES.so") {}
    };

    //==============================================================================
    static inline bool checkRetVal(const SLresult result)
    {
        assert(result == SL_RESULT_SUCCESS);
        return result == SL_RESULT_SUCCESS;
    }

    struct Player;
    struct Recorder;

    struct Engine {
        libOpenSLES lib;
        SLObjectItf engineObject{nullptr};
        SLEngineItf engineInterface{nullptr};
        SLObjectItf outputMixObject{nullptr};

        SLInterfaceID* SL_IID_AUDIOIODEVICECAPABILITIES{nullptr};
        SLInterfaceID* SL_IID_ANDROIDSIMPLEBUFFERQUEUE{nullptr};
        SLInterfaceID* SL_IID_PLAY{nullptr};
        SLInterfaceID* SL_IID_RECORD{nullptr};
        SLInterfaceID* SL_IID_ANDROIDCONFIGURATION{nullptr};
        SLInterfaceID* SL_IID_DEVICEVOLUME{nullptr};

        Engine()
        {
            typedef SLresult (*CreateEngineFunc)(SLObjectItf*,
                                                 SLuint32,
                                                 const SLEngineOption*,
                                                 SLuint32,
                                                 const SLInterfaceID*,
                                                 const SLboolean*);

            if (auto createEngine = lib.symbol<CreateEngineFunc>("slCreateEngine")) {
                checkRetVal(createEngine(&engineObject, 0, nullptr, 0, nullptr, nullptr));

                SLInterfaceID* SL_IID_ENGINE = lib.symbol<SLInterfaceID*>("SL_IID_ENGINE");
                SL_IID_AUDIOIODEVICECAPABILITIES =
                    lib.symbol<SLInterfaceID*>("SL_IID_AUDIOIODEVICECAPABILITIES");
                SL_IID_ANDROIDSIMPLEBUFFERQUEUE =
                    lib.symbol<SLInterfaceID*>("SL_IID_ANDROIDSIMPLEBUFFERQUEUE");
                SL_IID_PLAY   = lib.symbol<SLInterfaceID*>("SL_IID_PLAY");
                SL_IID_RECORD = lib.symbol<SLInterfaceID*>("SL_IID_RECORD");
                SL_IID_ANDROIDCONFIGURATION =
                    lib.symbol<SLInterfaceID*>("SL_IID_ANDROIDCONFIGURATION");
                SL_IID_DEVICEVOLUME = lib.symbol<SLInterfaceID*>("SL_IID_DEVICEVOLUME");

                checkRetVal((*engineObject)->Realize(engineObject, SL_BOOLEAN_FALSE));
                checkRetVal(
                    (*engineObject)->GetInterface(engineObject, *SL_IID_ENGINE, &engineInterface));

                checkRetVal(
                    (*engineInterface)
                        ->CreateOutputMix(engineInterface, &outputMixObject, 0, nullptr, nullptr));
                checkRetVal((*outputMixObject)->Realize(outputMixObject, SL_BOOLEAN_FALSE));
            }
        }
        ~Engine()
        {
            if (outputMixObject != nullptr)
                (*outputMixObject)->Destroy(outputMixObject);
            if (engineObject != nullptr)
                (*engineObject)->Destroy(engineObject);
        }

        template <typename ObjectType, typename... Args>
        unique_ptr<ObjectType> create(Args... args)
        {
            auto object = make_unique<ObjectType>(*this, args...);
            if (!object->openedOk())
                return nullptr;
            return move(object);
        }
    };

    //==================================================================================================
    struct BufferList {
        BufferList(const int numChannels_, const int numBuffers_, const int numSamples_)
            : numChannels(numChannels_)
            , numBuffers(numBuffers_)
            , numSamples(numSamples_)
            , bufferSpace(numChannels_ * numSamples * numBuffers, 0)
            , nextBlock(0)
        {
        }

        bool canGetNextBuffer() const noexcept { return (numBlocksOut < numBuffers); }

        int16_t* getNextBuffer() noexcept
        {
            if (numBlocksOut == numBuffers)
                return nullptr;

            if (++nextBlock == numBuffers)
                nextBlock = 0;

            return &bufferSpace.front() + nextBlock * numChannels * numSamples;
        }

        void bufferReturned() noexcept { --numBlocksOut; }
        void bufferSent() noexcept
        {
            ++numBlocksOut;
            assert(numBlocksOut <= numBuffers);
        }

        int getBufferSizeBytes() const noexcept
        {
            return numChannels * numSamples * sizeof(int16_t);
        }

        const int numChannels, numBuffers, numSamples;

    private:
        vector<int16_t> bufferSpace;
        int nextBlock;
        atomic<int> numBlocksOut{0};
    };

    typedef function<void(void)> callback_type;

    //==================================================================================================
    struct Player {
        Player(Engine& engine,
               const datamodel::Waveforms* _buffer,
               int sampleRate,
               int playerNumBuffers)
            : buffer(_buffer)
            , bufferList(buffer->waveforms.size(), playerNumBuffers, buffer->waveformSize())
        {
            if (bufferList.numChannels > 0) {
                SLDataFormat_PCM pcmFormat = {
                    SL_DATAFORMAT_PCM,
                    (SLuint32)bufferList.numChannels,
                    (SLuint32)(sampleRate * 1000),
                    SL_PCMSAMPLEFORMAT_FIXED_16,
                    SL_PCMSAMPLEFORMAT_FIXED_16,
                    (bufferList.numChannels == 1)
                        ? SL_SPEAKER_FRONT_CENTER
                        : (SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT),
                    SL_BYTEORDER_LITTLEENDIAN};

                SLDataLocator_AndroidSimpleBufferQueue bufferQueue = {
                    SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE,
                    static_cast<SLuint32>(bufferList.numBuffers)};
                SLDataSource audioSrc = {&bufferQueue, &pcmFormat};

                SLDataLocator_OutputMix outputMix = {SL_DATALOCATOR_OUTPUTMIX,
                                                     engine.outputMixObject};
                SLDataSink audioSink = {&outputMix, nullptr};

                // (SL_IID_BUFFERQUEUE is not guaranteed to remain future-proof, so use
                // SL_IID_ANDROIDSIMPLEBUFFERQUEUE)
                const SLInterfaceID interfaceIDs[] = {*engine.SL_IID_ANDROIDSIMPLEBUFFERQUEUE};
                const SLboolean flags[]            = {SL_BOOLEAN_TRUE};

                checkRetVal((*engine.engineInterface)
                                ->CreateAudioPlayer(engine.engineInterface,
                                                    &playerObject,
                                                    &audioSrc,
                                                    &audioSink,
                                                    1,
                                                    interfaceIDs,
                                                    flags));

                checkRetVal((*playerObject)->Realize(playerObject, SL_BOOLEAN_FALSE));
                checkRetVal(
                    (*playerObject)->GetInterface(playerObject, *engine.SL_IID_PLAY, &playerPlay));
                checkRetVal((*playerObject)
                                ->GetInterface(playerObject,
                                               *engine.SL_IID_ANDROIDSIMPLEBUFFERQUEUE,
                                               &playerBufferQueue));
                checkRetVal((*playerBufferQueue)
                                ->RegisterCallback(playerBufferQueue, staticCallback, this));
            }
        }

        ~Player()
        {
            if (playerPlay != nullptr)
                stop();

            if (playerBufferQueue != nullptr)
                checkRetVal((*playerBufferQueue)->Clear(playerBufferQueue));

            if (playerObject != nullptr)
                (*playerObject)->Destroy(playerObject);
        }

        bool openedOk() const noexcept { return playerBufferQueue != nullptr; }

        void setProcessCallback(callback_type fn)
        {
            lock_guard<mutex> lock{cbMutex};
            cbFunction = fn;
        }

        void start()
        {
            assert(openedOk());
            checkRetVal((*playerPlay)->SetPlayState(playerPlay, SL_PLAYSTATE_PLAYING));
        }

        void stop()
        {
            assert(openedOk());
            checkRetVal((*playerPlay)->SetPlayState(playerPlay, SL_PLAYSTATE_STOPPED));
        }

        bool canWriteBuffer() const { return bufferList.canGetNextBuffer(); }

        bool writeBuffer() noexcept
        {
            if (int16_t* const destBuffer = bufferList.getNextBuffer()) {
                for (int i = 0; i < bufferList.numChannels; ++i) {
                    int16_t* p       = destBuffer + i;
                    const float* src = buffer->waveforms[i].data();
                    for (int n = 0; n < bufferList.numSamples; ++n, p += bufferList.numChannels)
                        *p = (int16_t)limit(-32768.0f, 32767.0f, 32768.0f * (*src++));
                }

                enqueueBuffer(destBuffer);
                return true;
            }
            return false;
        }

    private:
        const datamodel::Waveforms* buffer;
        SLObjectItf playerObject{nullptr};
        SLPlayItf playerPlay{nullptr};
        SLAndroidSimpleBufferQueueItf playerBufferQueue{nullptr};

        BufferList bufferList;
        callback_type cbFunction;
        mutex cbMutex;

        void enqueueBuffer(const int16_t* buffer) noexcept
        {
            checkRetVal((*playerBufferQueue)
                            ->Enqueue(playerBufferQueue, buffer, bufferList.getBufferSizeBytes()));
            bufferList.bufferSent();
        }

        void callback()
        {
            bufferList.bufferReturned();
            unique_lock<mutex> lock{cbMutex, try_to_lock};
            if (lock.owns_lock() && cbFunction)
                cbFunction();
        }

        static void staticCallback(SLAndroidSimpleBufferQueueItf queue, void* context) noexcept
        {
            auto pThis = static_cast<Player*>(context);
            assert(queue == pThis->playerBufferQueue);
            (void)queue;
            pThis->callback();
        }
    };

    //==================================================================================================
    struct Recorder {
        Recorder(Engine& engine,
                 datamodel::Waveforms* _buffer,
                 int sampleRate,
                 const int numRecBuffers,
                 const bool enableProcessing)
            : buffer(_buffer)
            , bufferList(buffer->waveforms.size(), numRecBuffers, buffer->waveformSize())
        {
            if (bufferList.numChannels > 0) {
                SLDataFormat_PCM pcmFormat = {
                    SL_DATAFORMAT_PCM,
                    (SLuint32)bufferList.numChannels,
                    (SLuint32)(sampleRate * 1000),  // (sample rate units are millihertz)
                    SL_PCMSAMPLEFORMAT_FIXED_16,
                    SL_PCMSAMPLEFORMAT_FIXED_16,
                    (bufferList.numChannels == 1)
                        ? SL_SPEAKER_FRONT_CENTER
                        : (SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT),
                    SL_BYTEORDER_LITTLEENDIAN};

                SLDataLocator_IODevice ioDevice = {SL_DATALOCATOR_IODEVICE,
                                                   SL_IODEVICE_AUDIOINPUT,
                                                   SL_DEFAULTDEVICEID_AUDIOINPUT,
                                                   nullptr};
                SLDataSource audioSrc = {&ioDevice, nullptr};

                SLDataLocator_AndroidSimpleBufferQueue bufferQueue = {
                    SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE,
                    static_cast<SLuint32>(bufferList.numBuffers)};
                SLDataSink audioSink = {&bufferQueue, &pcmFormat};

                const SLInterfaceID interfaceIDs[] = {*engine.SL_IID_ANDROIDSIMPLEBUFFERQUEUE,
                                                      *engine.SL_IID_ANDROIDCONFIGURATION,
                                                      *engine.SL_IID_DEVICEVOLUME};
                const SLboolean flags[] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_FALSE};

                if (checkRetVal((*engine.engineInterface)
                                    ->CreateAudioRecorder(engine.engineInterface,
                                                          &recorderObject,
                                                          &audioSrc,
                                                          &audioSink,
                                                          sizeof(flags) / sizeof(flags[0]),
                                                          interfaceIDs,
                                                          flags))) {
                    if (checkRetVal((*recorderObject)
                                        ->GetInterface(recorderObject,
                                                       *engine.SL_IID_ANDROIDCONFIGURATION,
                                                       &configObject))) {
                        SLuint32 mode = enableProcessing
                                            ? SL_ANDROID_RECORDING_PRESET_GENERIC
                                            : SL_ANDROID_RECORDING_PRESET_VOICE_RECOGNITION;
                        checkRetVal((*configObject)
                                        ->SetConfiguration(configObject,
                                                           SL_ANDROID_KEY_RECORDING_PRESET,
                                                           &mode,
                                                           sizeof(mode)));
                    }

                    if (checkRetVal((*recorderObject)->Realize(recorderObject, SL_BOOLEAN_FALSE))) {
                        checkRetVal((*recorderObject)
                                        ->GetInterface(recorderObject,
                                                       *engine.SL_IID_RECORD,
                                                       &recorderRecord));
                        checkRetVal((*recorderObject)
                                        ->GetInterface(recorderObject,
                                                       *engine.SL_IID_ANDROIDSIMPLEBUFFERQUEUE,
                                                       &recorderBufferQueue));
                        if ((*recorderObject)
                                ->GetInterface(recorderObject,
                                               *engine.SL_IID_DEVICEVOLUME,
                                               &volumeItf) == SL_RESULT_SUCCESS) {
                            SLint32 volume;
                            checkRetVal((*volumeItf)->GetVolume(volumeItf, 0, &volume));
                            checkRetVal((*volumeItf)->SetVolume(volumeItf, 0, 0));
                        }

                        checkRetVal(
                            (*recorderBufferQueue)
                                ->RegisterCallback(recorderBufferQueue, staticCallback, this));
                        checkRetVal((*recorderRecord)
                                        ->SetRecordState(recorderRecord, SL_RECORDSTATE_STOPPED));
                    }
                } else {
                    assert(false);
                }
            }
        }

        ~Recorder()
        {
            if (recorderRecord != nullptr)
                stop();

            if (recorderBufferQueue != nullptr)
                checkRetVal((*recorderBufferQueue)->Clear(recorderBufferQueue));

            if (recorderObject != nullptr)
                (*recorderObject)->Destroy(recorderObject);
        }

        bool openedOk() const noexcept { return recorderBufferQueue != nullptr; }

        void setProcessCallback(callback_type fn)
        {
            lock_guard<mutex> lock{cbMutex};
            cbFunction = fn;
        }

        void start()
        {
            assert(openedOk());
            checkRetVal(
                (*recorderRecord)->SetRecordState(recorderRecord, SL_RECORDSTATE_RECORDING));
        }

        void stop()
        {
            assert(openedOk());
            checkRetVal((*recorderRecord)->SetRecordState(recorderRecord, SL_RECORDSTATE_STOPPED));
        }

        bool readNextBlock()
        {
            if (const int16_t* const srcBuffer = bufferList.getNextBuffer()) {
                for (int i = 0; i < bufferList.numChannels; ++i) {
                    const auto k     = 1.0f / 32768;
                    const int16_t* p = srcBuffer + i;
                    float* dst       = buffer->waveforms[i].data();
                    for (int n = 0; n < bufferList.numSamples; ++n, p += bufferList.numChannels)
                        *dst++ = (*p) * k;
                }

                enqueueBuffer(srcBuffer);
                return true;
            }

            return false;
        }

    private:
        datamodel::Waveforms* buffer;
        SLObjectItf recorderObject{nullptr};
        SLRecordItf recorderRecord{nullptr};
        SLAndroidSimpleBufferQueueItf recorderBufferQueue{nullptr};
        SLAndroidConfigurationItf configObject{nullptr};
        SLDeviceVolumeItf volumeItf{nullptr};

        BufferList bufferList;
        callback_type cbFunction;
        mutex cbMutex;

        void enqueueBuffer(const int16_t* buffer) noexcept
        {
            checkRetVal(
                (*recorderBufferQueue)
                    ->Enqueue(recorderBufferQueue, buffer, bufferList.getBufferSizeBytes()));
            bufferList.bufferSent();
        }

        void callback()
        {
            bufferList.bufferReturned();
            unique_lock<mutex> lock{cbMutex, try_to_lock};
            if (lock.owns_lock() && cbFunction)
                cbFunction();
        }

        static void staticCallback(SLAndroidSimpleBufferQueueItf queue, void* context) noexcept
        {
            auto pThis = static_cast<Recorder*>(context);
            assert(queue == pThis->recorderBufferQueue);
            (void)queue;
            pThis->callback();
        }
    };

    struct OpenSLESAudioDevice : dminternal::AudioDevice {
        static constexpr auto kDefaultBufferSize = 512;
        static constexpr auto kNumBuffersInQueue = 4;

        const string deviceName;

        Engine engine;
        unique_ptr<Player> player;
        unique_ptr<Recorder> recorder;

        atomic<bool> opened;

        datamodel::Waveforms inBuffers;
        datamodel::Waveforms outBuffers;
        datamodel::InputBufferView inputBufferView;
        datamodel::OutputBufferView outputBufferView;

        datamodel::ActiveChannels activeInChans;
        datamodel::ActiveChannels activeOutChans;

        int currentBufSize{0};
        float currentRate{0.f};

        vector<float> supportedSampleRates;

        OpenSLESAudioDevice(string name) : deviceName(name)
        {
            SLAudioIODeviceCapabilitiesItf devCapsItf;
            if ((*engine.engineObject)
                    ->GetInterface(engine.engineObject,
                                   *engine.SL_IID_AUDIOIODEVICECAPABILITIES,
                                   &devCapsItf) == SL_RESULT_SUCCESS) {
                SLAudioOutputDescriptor outputDescriptor;
                if ((*devCapsItf)->QueryAudioOutputCapabilities(devCapsItf, 0, &outputDescriptor) ==
                    SL_RESULT_SUCCESS) {
                    const float rates[] = {44100.0f, 48000.0f, 96000.0f};
                    for (auto rate : rates) {
                        if (rate < outputDescriptor.minSampleRate * 0.001f ||
                            rate > outputDescriptor.maxSampleRate * 0.001f)
                            continue;
                        supportedSampleRates.push_back(rate);
                    }
                }
            }

            if (!contains(supportedSampleRates, 48000.0f))
                supportedSampleRates.push_back(48000.0f);

            sort(supportedSampleRates.begin(), supportedSampleRates.end());
            supportedSampleRates.erase(
                unique(supportedSampleRates.begin(), supportedSampleRates.end()),
                supportedSampleRates.end());
        }

        ~OpenSLESAudioDevice() { close(); }

        virtual datamodel::AudioDeviceType type() const override
        {
            return datamodel::AudioDeviceType::opensles;
        }
        virtual dminternal::IOType iotype() const override { return dminternal::IOType::inout; }
        virtual string name() const override { return deviceName; }
        virtual int inputChannels() const override { return 1; }
        virtual int outputChannels() const override { return 2; }
        virtual vector<string> inputChannelNames() const override { return {"Audio Input"}; }
        virtual vector<string> outputChannelNames() const override { return {"Left", "Right"}; }
        virtual vector<int> bufferSizes() const override
        {
            return vector<int>{defaultBufferSize()};
        }
        virtual int defaultBufferSize() const override { return kDefaultBufferSize; }
        virtual vector<float> sampleRates() const override { return supportedSampleRates; }

        virtual void open(float fs,
                          int bufferSize,
                          datamodel::ActiveChannels inputMask,
                          datamodel::ActiveChannels outputMask) override
        {
            if (!contains(sampleRates(), fs))
                throw runtime_error("sample rate not supported");

            if (!contains(bufferSizes(), bufferSize))
                throw runtime_error("buffer size not supported");

            int numInChans  = count(inputMask.channels.begin(), inputMask.channels.end(), true);
            int numOutChans = count(outputMask.channels.begin(), outputMask.channels.end(), true);

            inBuffers.resize(numInChans, bufferSize);
            inBuffers.fs = fs;
            outBuffers.resize(numOutChans, bufferSize);
            outBuffers.fs = fs;

            recorder = engine.create<Recorder>(&inBuffers, int(fs), kNumBuffersInQueue, false);
            if (numInChans > 0 && !recorder)
                throw runtime_error("failed to create recorder");
            player = engine.create<Player>(&outBuffers, int(fs), kNumBuffersInQueue);
            if (numOutChans > 0 && !player)
                throw runtime_error("failed to create player");

            for (auto& channel : inBuffers.waveforms)
                inputBufferView.channel.emplace_back(channel.data(),
                                                     channel.data() + channel.size());
            for (auto& channel : outBuffers.waveforms)
                outputBufferView.channel.emplace_back(channel.data(),
                                                      channel.data() + channel.size());

            auto processBuffers = [&] {
                while (1) {
                    if (recorder && !recorder->readNextBlock() && !player)
                        break;

                    if (player && !player->canWriteBuffer())
                        break;

                    callAllCallbacks(inputBufferView, outputBufferView);

                    if (player)
                        player->writeBuffer();
                }
            };

            if (player == nullptr)
                recorder->setProcessCallback(processBuffers);
            else
                player->setProcessCallback(processBuffers);

            // Preload buffers
            processBuffers();

            if (recorder != nullptr)
                recorder->start();
            if (player != nullptr)
                player->start();

            activeInChans  = inputMask;
            activeOutChans = outputMask;
            currentRate    = fs;
            currentBufSize = bufferSize;
            opened         = true;
        }

        virtual void close() override
        {
            if (opened) {
                // Make sure we don't get spurious callbacks during destruction
                if (player)
                    player->setProcessCallback(callback_type{});
                if (recorder)
                    recorder->setProcessCallback(callback_type{});

                player   = nullptr;
                recorder = nullptr;

                opened         = false;
                currentRate    = 0.f;
                currentBufSize = 0;
                inBuffers.waveforms.clear();
                outBuffers.waveforms.clear();
                inputBufferView.channel.clear();
                outputBufferView.channel.clear();
                activeInChans  = datamodel::ActiveChannels{};
                activeOutChans = datamodel::ActiveChannels{};
            }
        }

        virtual bool isOpened() const override { return opened; }
        virtual int currentBufferSize() const override { return currentBufSize; }
        virtual float currentSampleRate() const override { return currentRate; }
        virtual const datamodel::ActiveChannels& activeInputChannels() const override
        {
            return activeInChans;
        }
        virtual const datamodel::ActiveChannels& activeOutputChannels() const override
        {
            return activeOutChans;
        }
    };

    struct OpenSLESAudioDeviceType : dminternal::AudioDeviceType {
        const string openSLTypeName = "Android OpenSL";

        virtual bool scanDevices() override { return true; }

        virtual vector<string> devices(dminternal::IOType) const override
        {
            return {openSLTypeName};
        }

        virtual int defaultIndex() const override { return 0; }

        virtual int indexOfDevice(const string& name) const override
        {
            return name == openSLTypeName ? 0 : -1;
        }

        virtual unique_ptr<dminternal::AudioDevice> createDevice(const string& name) override
        {
            if (name != openSLTypeName)
                return nullptr;
            return make_unique<OpenSLESAudioDevice>(openSLTypeName);
        }
    };
}

namespace dminternal
{
    template <>
    unique_ptr<AudioDeviceType> AudioDeviceType::create<datamodel::AudioDeviceType::opensles>()
    {
        if (!libOpenSLES().valid())
            return nullptr;

        return make_unique<OpenSLESAudioDeviceType>();
    }
}

#else

template <>
unique_ptr<dminternal::AudioDeviceType>
dminternal::AudioDeviceType::create<datamodel::AudioDeviceType::opensles>()
{
    return nullptr;
}

#endif