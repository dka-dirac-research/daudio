#include <daudio/waveform.h>
#include <dminternal/active.h>
#include <dminternal/device.h>
#include <dminternal/platform.h>
#include <dminternal/sampleformat.h>

#include <daudio/exceptions.h>

#include <algorithm>
#include <codecvt>
#include <deque>
#include <future>
#include <iostream>
#include <iterator>
#include <limits>
#include <map>
#include <mutex>
#include <queue>
#include <set>
using namespace std;

#include <windows.h>

#include <mmdeviceapi.h>

#include <Audioclient.h>
#include <Audiopolicy.h>
#include <Shlwapi.h>
#include <assert.h>
#include <avrt.h>
#include <dminternal/comutils.h>
#include <dminternal/mathutil.h>
#include <dminternal/threadutil.h>
#include <endpointvolume.h>

#include <functiondiscoverykeys_devpkey.h>
using dminternal::ComSmartPtr;

#include <dminternal/defer.h>
#include <dminternal/utils.h>

#include <log/log.hpp>

static const CLSID CLSID_MMDeviceEnumerator       = __uuidof(MMDeviceEnumerator);
static const IID IID_IMMDeviceEnumerator          = __uuidof(IMMDeviceEnumerator);
static const IID IID_IMMEndpoint                  = __uuidof(IMMEndpoint);
static const IID IID_IAudioClient                 = __uuidof(IAudioClient);
static const IID IID_IAudioClient2                = __uuidof(IAudioClient2);
static const IID IID_IAudioClient3                = __uuidof(IAudioClient3);
static const IID IID_IAudioEndpointVolume         = __uuidof(IAudioEndpointVolume);
static const IID IID_IAudioEndpointVolumeCallback = __uuidof(IAudioEndpointVolumeCallback);
static const IID IID_IAudioCaptureClient          = __uuidof(IAudioCaptureClient);
static const IID IID_IAudioRenderClient           = __uuidof(IAudioRenderClient);

// {FCEF82DE-AC57-4624-9D87-E79077A0C3F2}
static const GUID kVolumeChangeContext = {0xfcef82de,
                                          0xac57,
                                          0x4624,
                                          {0x9d, 0x87, 0xe7, 0x90, 0x77, 0xa0, 0xc3, 0xf2}};

#define RUNTIME_ERROR(x) std::runtime_error("wasapi: "##x)

namespace
{
    static daudio::ActiveChannels none{};

    // convert wstring to UTF-8 string
    static string wstring_to_utf8(const wstring& str)
    {
        wstring_convert<codecvt_utf8<wchar_t>> myconv;
        return myconv.to_bytes(str);
    }

    template <typename Type>
    Type limit(const Type lo, const Type hi, const Type value)
    {
        assert(lo <= hi);
        return (value < lo) ? lo : ((value > hi) ? hi : value);
    }

    int refTimeToSamples(const REFERENCE_TIME& t, const double sampleRate) noexcept
    {
        return (int)std::round(sampleRate * ((double)t) * 0.0000001);
    }

    REFERENCE_TIME samplesToRefTime(const int numSamples, const double sampleRate) noexcept
    {
        return (REFERENCE_TIME)((numSamples * 10000.0 * 1000.0 / sampleRate) + 0.5);
    }

    void copyWavFormat(WAVEFORMATEXTENSIBLE& dest, const WAVEFORMATEX* const src) noexcept
    {
        memcpy(&dest,
               src,
               src->wFormatTag == WAVE_FORMAT_EXTENSIBLE ? sizeof(WAVEFORMATEXTENSIBLE)
                                                         : sizeof(WAVEFORMATEX));
    }

    bool tryFormat(const dminternal::SampleFormat sampleFormat,
                   IAudioClient* clientToUse,
                   double sampleRate,
                   unsigned channels,
                   DWORD newMixFormatChannelMask,
                   WAVEFORMATEXTENSIBLE& format,
                   bool exclusive_mode)
    {
        memset(&format, 0, sizeof(format));

        if (channels <= 2 && sampleFormat.bitDepth <= 16) {
            format.Format.wFormatTag = WAVE_FORMAT_PCM;
        } else {
            format.Format.wFormatTag = WAVE_FORMAT_EXTENSIBLE;
            format.Format.cbSize     = sizeof(WAVEFORMATEXTENSIBLE) - sizeof(WAVEFORMATEX);
        }

        format.Format.nSamplesPerSec       = (DWORD)sampleRate;
        format.Format.nChannels            = (WORD)channels;
        format.Format.wBitsPerSample       = (WORD)(8 * sampleFormat.byteStride);
        format.Samples.wValidBitsPerSample = (WORD)(sampleFormat.bitDepth);
        format.Format.nBlockAlign =
            (WORD)(format.Format.nChannels * format.Format.wBitsPerSample / 8);
        format.Format.nAvgBytesPerSec =
            (DWORD)(format.Format.nSamplesPerSec * format.Format.nBlockAlign);
        format.SubFormat =
            sampleFormat.formatIsFloat ? KSDATAFORMAT_SUBTYPE_IEEE_FLOAT : KSDATAFORMAT_SUBTYPE_PCM;
        format.dwChannelMask = newMixFormatChannelMask;

        WAVEFORMATEXTENSIBLE* nearestFormat = nullptr;

        HRESULT hr = clientToUse->IsFormatSupported(
            exclusive_mode ? AUDCLNT_SHAREMODE_EXCLUSIVE : AUDCLNT_SHAREMODE_SHARED,
            (WAVEFORMATEX*)&format,
            exclusive_mode ? nullptr : (WAVEFORMATEX**)&nearestFormat);

        if (hr == S_FALSE && format.Format.nSamplesPerSec == nearestFormat->Format.nSamplesPerSec) {
            copyWavFormat(format, (WAVEFORMATEX*)nearestFormat);
            hr = S_OK;
        }

        CoTaskMemFree(nearestFormat);
        return SUCCEEDED(hr);
    }

    bool findSupportedFormat(IAudioClient* clientToUse,
                             double sampleRate,
                             unsigned channels,
                             DWORD newMixFormatChannelMask,
                             WAVEFORMATEXTENSIBLE& format,
                             bool exclusive_mode)
    {
        using dminternal::SampleFormat;
        for (auto sf : {
                 SampleFormat::Type::kFloat32LSB,
                 SampleFormat::Type::kInt32LSB,
                 SampleFormat::Type::kInt32LSB24,
                 SampleFormat::Type::kInt24LSB,
                 SampleFormat::Type::kInt32LSB20,
                 SampleFormat::Type::kInt20LSB,
                 SampleFormat::Type::kInt16LSB,
             })
            if (tryFormat(sf,
                          clientToUse,
                          sampleRate,
                          channels,
                          newMixFormatChannelMask,
                          format,
                          exclusive_mode))
                return true;
        return false;
    }

    ComSmartPtr<IMMDeviceEnumerator> getEnumerator()
    {
        ComSmartPtr<IMMDeviceEnumerator> enumerator;
        auto hr = CoCreateInstance(CLSID_MMDeviceEnumerator,
                                   NULL,
                                   CLSCTX_ALL,
                                   IID_IMMDeviceEnumerator,
                                   (void**)&enumerator);
        if (FAILED(hr))
            throw RUNTIME_ERROR("failed getting enumerator");
        return enumerator;
    }

    //==============================================================================
    struct WasapiDeviceBase {
        WasapiDeviceBase(const ComSmartPtr<IMMDevice>& d, const bool exclusiveMode)
            : device_(d)
            , sample_rate_(0)
            , default_sample_rate_(0)
            , num_channels_(0)
            , actual_num_channels_(0)
            , min_buffer_size_(0)
            , default_buffer_size_(0)
            , buffer_size_(0)
            , max_buffer_size_(0)
            , latency_samples_(0)
            , use_exclusive_mode_(exclusiveMode)
            , bytes_per_sample_(0)
            , bytes_per_frame_(0)
        {
            client_event_ = CreateEventW(nullptr, false, false, nullptr);

            ComSmartPtr<IAudioClient> tempClient(createClient());

            REFERENCE_TIME defaultPeriod, minPeriod;
            if (!SUCCEEDED(tempClient->GetDevicePeriod(&defaultPeriod, &minPeriod)))
                throw RUNTIME_ERROR("failed getting device period");

            WAVEFORMATEX* mixFormat = nullptr;
            if (!SUCCEEDED(tempClient->GetMixFormat(&mixFormat)))
                throw RUNTIME_ERROR("failed getting mix format");
            defer { CoTaskMemFree(mixFormat); };

            WAVEFORMATEXTENSIBLE format;
            copyWavFormat(format, mixFormat);

            actual_num_channels_ = num_channels_ = format.Format.nChannels;
            default_sample_rate_                 = static_cast<float>(format.Format.nSamplesPerSec);
            min_buffer_size_         = refTimeToSamples(minPeriod, default_sample_rate_);
            default_buffer_size_     = refTimeToSamples(defaultPeriod, default_sample_rate_);
            mix_format_channel_mask_ = format.dwChannelMask;

            std::set<float> rates;
            rates.insert(default_sample_rate_);

            if (use_exclusive_mode_ && findSupportedFormat(tempClient,
                                                           default_sample_rate_,
                                                           num_channels_,
                                                           format.dwChannelMask,
                                                           format,
                                                           true)) {
                // Got a format that is supported by the device so we can ask what sample rates are
                // supported (in whatever format)
                for (auto rate : dminternal::Device::possibleSampleRates()) {
                    if (rates.find(rate) != rates.end())
                        continue;

                    format.Format.nSamplesPerSec = (DWORD)rate;
                    format.Format.nAvgBytesPerSec =
                        (DWORD)(format.Format.nSamplesPerSec * format.Format.nChannels *
                                format.Format.wBitsPerSample / 8);

                    if (FAILED(tempClient->IsFormatSupported(
                            AUDCLNT_SHAREMODE_EXCLUSIVE, (WAVEFORMATEX*)&format, 0)))
                        break;

                    rates.insert(rate);
                }
            }
            for (auto rate : rates)
                sample_rates.push_back(rate);

            // Endpoint volume
            device_->Activate(IID_IAudioEndpointVolume, CLSCTX_ALL, NULL, (void**)&volume_);
        }

        ~WasapiDeviceBase()
        {
            unregisterEndpointVolumeCallback();
            device_ = nullptr;
            CloseHandle(client_event_);
        }

        bool isOk() const noexcept { return default_buffer_size_ > 0 && default_sample_rate_ > 0; }

        bool hasEndpointVolumeControl() const { return !!volume_; }

        float getVolume() const
        {
            if (!volume_)
                throw RUNTIME_ERROR("no volume control");
            float value;
            volume_->GetMasterVolumeLevelScalar(&value);
            return value;
        }

        void setVolume(float value)
        {
            if (!volume_)
                throw RUNTIME_ERROR("no volume control");
            volume_->SetMasterVolumeLevelScalar(value, &kVolumeChangeContext);
        }

        bool getMuted() const
        {
            if (!volume_)
                throw RUNTIME_ERROR("no volume control");
            BOOL value;
            volume_->GetMute(&value);
            return value;
        }

        void setMuted(bool mute)
        {
            if (!volume_)
                throw RUNTIME_ERROR("no volume control");
            volume_->SetMute(mute, &kVolumeChangeContext);
        }

        void openClient(const float newSampleRate,
                        const int bufferSizeSamples,
                        const daudio::ActiveChannels& newChannels)
        {
            sample_rate_     = newSampleRate;
            active_channels_ = newChannels;
            num_channels_    = int(active_channels_);

            if (num_channels_ == 0)
                throw logic_error("wasapi: no channels given to open");

            try {
                client_ = createClient();
                if (!client_)
                    throw RUNTIME_ERROR("Failed to create client");

                if (tryInitialisingWithBufferSize(bufferSizeSamples)) {
                    channel_maps_.clear();
                    for (int i = 0; i < active_channels_.channels.size(); ++i)
                        if (active_channels_.channels[i])
                            channel_maps_.push_back(i);

                    REFERENCE_TIME latency;
                    if (SUCCEEDED(client_->GetStreamLatency(&latency)))
                        latency_samples_ = refTimeToSamples(latency, sample_rate_);

                    buffer_size_ = bufferSizeSamples;

                    if (FAILED(client_->GetBufferSize(&max_buffer_size_)))
                        throw RUNTIME_ERROR("failed getting buffer size");

                    createSessionEventCallback();

                    if (FAILED(client_->SetEventHandle(client_event_)))
                        throw RUNTIME_ERROR("failed setting event handle");
                } else
                    throw RUNTIME_ERROR("failed to init with buffer size " +
                                        to_string(bufferSizeSamples));
            } catch (...) {
                closeClient();
                throw;
            }
        }

        void closeClient()
        {
            if (client_ != nullptr)
                client_->Stop();

            deleteSessionEventCallback();
            client_ = nullptr;
            ResetEvent(client_event_);
        }

        bool isOpen() const { return client_ != nullptr; }

        void setDeviceError(std::exception& e) { device_error_ = std::make_exception_ptr(e); }

        void checkError()
        {
            if (device_error_)
                rethrow_exception(device_error_);
        }

        void registerEndpointVolumeCallback(dminternal::Device::notification_callback_type fn)
        {
            if (volume_ && fn) {
                endpointVolumeCallback = new EndpointVolumeCallback(fn);
                volume_->RegisterControlChangeNotify(endpointVolumeCallback);
            }
        }

        void unregisterEndpointVolumeCallback()
        {
            if (volume_ && endpointVolumeCallback != nullptr) {
                volume_->UnregisterControlChangeNotify(endpointVolumeCallback);
                endpointVolumeCallback = nullptr;
            }
            volume_ = nullptr;
        }

        //==============================================================================
        ComSmartPtr<IMMDevice> device_;
        ComSmartPtr<IAudioClient> client_;
        ComSmartPtr<IAudioEndpointVolume> volume_;
        float sample_rate_;
        float default_sample_rate_;
        int num_channels_;
        int actual_num_channels_;
        int min_buffer_size_;
        int default_buffer_size_;
        int buffer_size_;
        UINT32 max_buffer_size_;
        int latency_samples_;
        DWORD mix_format_channel_mask_;
        const bool use_exclusive_mode_;
        vector<float> sample_rates;
        HANDLE client_event_;
        daudio::ActiveChannels active_channels_;
        vector<int> channel_maps_;
        int bytes_per_sample_;
        int bytes_per_frame_;
        std::exception_ptr device_error_;
        unique_ptr<dminternal::SampleFormat> format_;

    private:
        //==============================================================================
        class SessionEventCallback : public IAudioSessionEvents
        {
        public:
            SessionEventCallback(WasapiDeviceBase& d) : owner(d) {}

            HRESULT STDMETHODCALLTYPE OnDisplayNameChanged(LPCWSTR, LPCGUID) override
            {
                return S_OK;
            }
            HRESULT STDMETHODCALLTYPE OnIconPathChanged(LPCWSTR, LPCGUID) override { return S_OK; }
            HRESULT STDMETHODCALLTYPE OnSimpleVolumeChanged(float, BOOL, LPCGUID) override
            {
                return S_OK;
            }
            HRESULT STDMETHODCALLTYPE OnChannelVolumeChanged(DWORD, float*, DWORD, LPCGUID) override
            {
                return S_OK;
            }
            HRESULT STDMETHODCALLTYPE OnGroupingParamChanged(LPCGUID, LPCGUID) override
            {
                return S_OK;
            }
            HRESULT STDMETHODCALLTYPE OnStateChanged(AudioSessionState) override { return S_OK; }

            HRESULT STDMETHODCALLTYPE
            OnSessionDisconnected(AudioSessionDisconnectReason reason) override
            {
                switch (reason) {
                case DisconnectReasonDeviceRemoval:
                    owner.setDeviceError(RUNTIME_ERROR("device removed"));
                    break;
                case DisconnectReasonFormatChanged:
                    owner.setDeviceError(RUNTIME_ERROR("format changed"));
                    break;
                case DisconnectReasonExclusiveModeOverride:
                    owner.setDeviceError(RUNTIME_ERROR("exclusive mode override"));
                    break;
                default:
                    owner.setDeviceError(RUNTIME_ERROR("session terminated"));
                    break;
                }
                return S_OK;
            }

            HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
            {
                if (IID_IUnknown == riid) {
                    AddRef();
                    *ppvObject = (IUnknown*)this;
                } else if (__uuidof(IAudioSessionEvents) == riid) {
                    AddRef();
                    *ppvObject = (IAudioSessionEvents*)this;
                } else {
                    *ppvObject = NULL;
                    return E_NOINTERFACE;
                }
                return S_OK;
            }

            ULONG STDMETHODCALLTYPE AddRef(void) { return InterlockedIncrement(&cntr_); }
            ULONG STDMETHODCALLTYPE Release(void) { return InterlockedDecrement(&cntr_); }

        private:
            ULONG cntr_{0};
            WasapiDeviceBase& owner;
        };

        class EndpointVolumeCallback : public IAudioEndpointVolumeCallback
        {
        public:
            EndpointVolumeCallback(dminternal::Device::notification_callback_type fn)
                : callback_(fn)
            {
            }

            virtual HRESULT STDMETHODCALLTYPE OnNotify(PAUDIO_VOLUME_NOTIFICATION_DATA pNotify)
            {
                const bool internal_change =
                    IsEqualGUID(pNotify->guidEventContext, kVolumeChangeContext);
                callback_(pNotify->fMasterVolume, pNotify->bMuted, internal_change);
                return S_OK;
            }

            HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
            {
                if (IID_IUnknown == riid) {
                    AddRef();
                    *ppvObject = (IUnknown*)this;
                } else if (__uuidof(IAudioEndpointVolumeCallback) == riid) {
                    AddRef();
                    *ppvObject = (IAudioEndpointVolumeCallback*)this;
                } else {
                    *ppvObject = NULL;
                    return E_NOINTERFACE;
                }
                return S_OK;
            }

            ULONG STDMETHODCALLTYPE AddRef(void) { return InterlockedIncrement(&cntr_); }
            ULONG STDMETHODCALLTYPE Release(void) { return InterlockedDecrement(&cntr_); }

        private:
            ULONG cntr_{0};
            dminternal::Device::notification_callback_type callback_;
        };

        ComSmartPtr<IAudioSessionControl> audioSessionControl;
        ComSmartPtr<SessionEventCallback> sessionEventCallback;
        ComSmartPtr<EndpointVolumeCallback> endpointVolumeCallback;

        void createSessionEventCallback()
        {
            deleteSessionEventCallback();
            client_->GetService(__uuidof(IAudioSessionControl), (void**)&audioSessionControl);

            if (audioSessionControl != nullptr) {
                sessionEventCallback = new SessionEventCallback(*this);
                audioSessionControl->RegisterAudioSessionNotification(sessionEventCallback);
                sessionEventCallback->Release();  // (required because ComBaseClassHelper objects
                                                  // are constructed with a ref count of 1)
            }
        }

        void deleteSessionEventCallback()
        {
            if (audioSessionControl != nullptr && sessionEventCallback != nullptr)
                audioSessionControl->UnregisterAudioSessionNotification(sessionEventCallback);

            audioSessionControl  = nullptr;
            sessionEventCallback = nullptr;
        }

        //==============================================================================
        ComSmartPtr<IAudioClient> createClient()
        {
            ComSmartPtr<IAudioClient> newClient;
            if (device_ != nullptr) {
                auto hr = device_->Activate(
                    IID_IAudioClient, CLSCTX_INPROC_SERVER, nullptr, (void**)&newClient);

                if (FAILED(hr))
                    throw RUNTIME_ERROR("failed activating audio client");
            }
            return newClient;
        }

        void updateFormat(bool is_float_)
        {
            using dminternal::SampleFormat;
            if (is_float_)
                format_ = make_unique<SampleFormat>(SampleFormat::Type::kFloat32LSB);
            else {
                switch (bytes_per_sample_) {
                case 2:
                    format_ = make_unique<SampleFormat>(SampleFormat::Type::kInt16LSB);
                    break;
                case 3:
                    format_ = make_unique<SampleFormat>(SampleFormat::Type::kInt24LSB);
                    break;
                case 4:
                    format_ = make_unique<SampleFormat>(SampleFormat::Type::kInt32LSB);
                    break;
                default:
                    break;
                }
            }
        }

        bool tryInitialisingWithBufferSize(const int bufferSizeSamples)
        {
            WAVEFORMATEXTENSIBLE format;

            if (findSupportedFormat(client_,
                                    sample_rate_,
                                    actual_num_channels_,
                                    mix_format_channel_mask_,
                                    format,
                                    use_exclusive_mode_)) {
                REFERENCE_TIME defaultPeriod = 0, minPeriod = 0;

                if (FAILED(client_->GetDevicePeriod(&defaultPeriod, &minPeriod)))
                    throw RUNTIME_ERROR("failed getting device period");

                if (use_exclusive_mode_ && bufferSizeSamples > 0)
                    defaultPeriod = (std::max)(minPeriod,
                                               samplesToRefTime(bufferSizeSamples,
                                                                format.Format.nSamplesPerSec));

                for (;;) {
                    GUID session;
                    HRESULT hr =
                        client_->Initialize(use_exclusive_mode_ ? AUDCLNT_SHAREMODE_EXCLUSIVE
                                                                : AUDCLNT_SHAREMODE_SHARED,
                                            AUDCLNT_STREAMFLAGS_EVENTCALLBACK,
                                            defaultPeriod,
                                            use_exclusive_mode_ ? defaultPeriod : 0,
                                            (WAVEFORMATEX*)&format,
                                            &session);
                    if (E_ACCESSDENIED == hr) {
                        throw daudio::AccessDenied();
                    }

                    if (SUCCEEDED(hr)) {
                        actual_num_channels_ = format.Format.nChannels;
                        const bool isFloat   = format.Format.wFormatTag == WAVE_FORMAT_EXTENSIBLE &&
                                             format.SubFormat == KSDATAFORMAT_SUBTYPE_IEEE_FLOAT;
                        bytes_per_sample_ = format.Format.wBitsPerSample / 8;
                        bytes_per_frame_  = format.Format.nBlockAlign;

                        updateFormat(isFloat);
                        return true;
                    }

                    // Handle the "alignment dance" :
                    // http://msdn.microsoft.com/en-us/library/windows/desktop/dd370875(v=vs.85).aspx
                    // (see Remarks)
                    if (hr != AUDCLNT_E_BUFFER_SIZE_NOT_ALIGNED)
                        break;

                    UINT32 numFrames = 0;
                    if (FAILED(client_->GetBufferSize(&numFrames)))
                        break;

                    // Recreate client
                    client_ = nullptr;
                    client_ = createClient();

                    defaultPeriod = samplesToRefTime(numFrames, format.Format.nSamplesPerSec);
                }
            }

            return false;
        }
    };

    struct WasapiInputDevice : WasapiDeviceBase {
        ComSmartPtr<IAudioCaptureClient> capture_client_;
        vector<uint8_t> reservoir_;
        size_t reservoir_size_;
        size_t reservoir_mask_;
        volatile size_t read_ptr_;
        volatile size_t write_ptr_;
        daudio::Waveforms waveforms_;
        daudio::InputBufferView view_;

        WasapiInputDevice(const ComSmartPtr<IMMDevice>& d, bool exclusive_mode)
            : WasapiDeviceBase(d, exclusive_mode)
        {
        }

        void open(float fs, int buffer_size, daudio::ActiveChannels channels)
        {
            openClient(fs, buffer_size, channels);

            if (FAILED(client_->GetService(IID_IAudioCaptureClient, (void**)&capture_client_)))
                throw RUNTIME_ERROR("failed getting capture service");

            reservoir_size_ = max_buffer_size_ + buffer_size;
            reservoir_mask_ = dminternal::next_power_of_2(reservoir_size_) - 1;
            reservoir_.resize((reservoir_mask_ + 1) * bytes_per_frame_);
            read_ptr_  = 0;
            write_ptr_ = 0;
            waveforms_.resize(actual_num_channels_, buffer_size_);
            view_.channel.clear();
            auto num_channels_min =
                (std::min)(active_channels_.channels.size(), size_t(actual_num_channels_));
            for (size_t ch = 0; ch < num_channels_min; ++ch) {
                if (active_channels_.channels[ch]) {
                    view_.channel.emplace_back(
                        waveforms_.waveforms[ch].data(),
                        waveforms_.waveforms[ch].data() + waveforms_.waveforms[ch].size());
                }
            }
        }

        void close()
        {
            capture_client_ = nullptr;
            view_.channel.clear();
            closeClient();
        }

        void start()
        {
            if (FAILED(client_->Start()))
                throw RUNTIME_ERROR("failed to start capture");
            purgeInputBuffers();
        }

        void purgeInputBuffers()
        {
            uint8_t* inputData;
            UINT32 numSamplesAvailable;
            DWORD flags;
            while (capture_client_->GetBuffer(
                       &inputData, &numSamplesAvailable, &flags, nullptr, nullptr) !=
                   AUDCLNT_S_BUFFER_EMPTY)
                capture_client_->ReleaseBuffer(numSamplesAvailable);
        }

        inline size_t numSamplesInReservoir() const { return write_ptr_ - read_ptr_; }

        void handleBuffer()
        {
            checkError();

            while (true) {
                uint8_t* inputData = nullptr;
                DWORD flags;
                UINT32 numFramesAvailable;
                HRESULT hr = capture_client_->GetBuffer(
                    &inputData, &numFramesAvailable, &flags, nullptr, nullptr);
                if (FAILED(hr)) {
                    throw RUNTIME_ERROR("failed getting capture buffer");
                }
                if (AUDCLNT_S_BUFFER_EMPTY == hr) {
                    break;
                }

                size_t framesLeft = size_t(numFramesAvailable);
                while (framesLeft > 0) {
                    auto localWrite   = write_ptr_ & reservoir_mask_;
                    auto framesNow    = (std::min)(framesLeft, reservoir_mask_ + 1 - localWrite);
                    auto reservoirPtr = reservoir_.data() + localWrite * bytes_per_frame_;
                    auto framesNowInBytes = framesNow * bytes_per_frame_;

                    if ((flags & AUDCLNT_BUFFERFLAGS_SILENT) != 0) {
                        std::fill_n(reservoirPtr, framesNowInBytes, 0);
                    } else {
                        std::copy_n(inputData, framesNowInBytes, reservoirPtr);
                    }

                    write_ptr_ += framesNow;
                    inputData += framesNowInBytes;
                    framesLeft -= framesNow;
                }

                if (numSamplesInReservoir() > reservoir_size_) {
                    read_ptr_ = write_ptr_ - reservoir_size_;
                }

                capture_client_->ReleaseBuffer(numFramesAvailable);
            }
        }

        void copyBuffersFromReservoir()
        {
            size_t framesToCopy = buffer_size_;
            int offset          = (std::max)(0, buffer_size_ - (int)numSamplesInReservoir());
            if (offset > 0) {
                framesToCopy -= offset;
                read_ptr_ -= offset / 2;
            }

            auto dst = (float** const)alloca(actual_num_channels_ * sizeof(float*));
            while (framesToCopy) {
                auto localRead = read_ptr_ & reservoir_mask_;
                int framesNow  = (int)(std::min)({framesToCopy,
                                                 numSamplesInReservoir(),
                                                 reservoir_mask_ + 1 - localRead});
                if (framesNow <= 0)
                    break;

                auto reservoirPtr = reservoir_.data() + localRead * bytes_per_frame_;

                for (int i = 0; i < actual_num_channels_; ++i) {
                    dst[i] = waveforms_.waveforms[i].data() + offset;
                }

                format_->convertToFloat(reservoirPtr, dst, actual_num_channels_, framesNow);

                framesToCopy -= framesNow;
                offset += framesNow;
                read_ptr_ += framesNow;
            }
        }
    };

    struct WasapiOutputDevice : WasapiDeviceBase {
        ComSmartPtr<IAudioRenderClient> render_client_;
        vector<uint8_t> reservoir_;
        daudio::Waveforms waveforms_;
        daudio::OutputBufferView view_;

        WasapiOutputDevice(const ComSmartPtr<IMMDevice>& d, bool exclusive_mode)
            : WasapiDeviceBase(d, exclusive_mode)
        {
        }

        void open(float fs, int buffer_size, daudio::ActiveChannels channels)
        {
            openClient(fs, buffer_size, channels);
            if (FAILED(client_->GetService(IID_IAudioRenderClient, (void**)&render_client_)))
                throw RUNTIME_ERROR("failed getting render service");

            waveforms_.resize(actual_num_channels_, buffer_size_);
            view_.channel.clear();
            auto num_channels_min =
                (std::min)(active_channels_.channels.size(), size_t(actual_num_channels_));
            for (size_t ch = 0; ch < num_channels_min; ++ch) {
                if (active_channels_.channels[ch]) {
                    view_.channel.emplace_back(
                        waveforms_.waveforms[ch].data(),
                        waveforms_.waveforms[ch].data() + waveforms_.waveforms[ch].size());
                }
            }
        }

        void close()
        {
            render_client_ = nullptr;
            view_.channel.clear();
            closeClient();
        }

        void start()
        {
            auto samplesToDo = availableToCopy();
            uint8_t* outputData;

            if (samplesToDo > 0 && SUCCEEDED(render_client_->GetBuffer(samplesToDo, &outputData)))
                render_client_->ReleaseBuffer(samplesToDo, AUDCLNT_BUFFERFLAGS_SILENT);

            if (FAILED(client_->Start()))
                throw RUNTIME_ERROR("failed to start rendering");
        }

        UINT32 availableToCopy()
        {
            if (!use_exclusive_mode_) {
                UINT32 padding         = 0;
                UINT32 max_buffer_size = 0;
                if (SUCCEEDED(client_->GetBufferSize(&max_buffer_size))) {
                    if (SUCCEEDED(client_->GetCurrentPadding(&padding)))
                        return max_buffer_size - (int)padding;
                }
            }

            return buffer_size_;
        }

        void handleBuffer()
        {
            checkError();

            UINT32 bufSize  = UINT32(buffer_size_);
            size_t offset = 0;

            auto src = (const float**)alloca(actual_num_channels_ * sizeof(float*));

            while (bufSize > 0) {
                auto numToCopy = min(availableToCopy(), bufSize);

                for (int i = 0; i < actual_num_channels_; ++i)
                    src[i] = waveforms_.waveforms[i].data() + offset;

                uint8_t* outputData = nullptr;
                if (SUCCEEDED(render_client_->GetBuffer(numToCopy, &outputData))) {
                    format_->convertFromFloat(src, outputData, actual_num_channels_, numToCopy);
                    render_client_->ReleaseBuffer(numToCopy, 0);
                } else
                    throw RUNTIME_ERROR("failed getting buffer from render client");

                bufSize -= numToCopy;
                offset += numToCopy;

                if(bufSize > 0) {
                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                }
            }
        }
    };

    struct WasapiDeviceInfo {
        daudio::AudioDevice dev;
        wstring input_id;
        wstring output_id;
    };

    template <typename Type>
    inline bool ok(const unique_ptr<Type>& p)
    {
        return !!p && p->isOpen();
    }

    struct WasapiDevice : dminternal::Device {
        const bool exclusive_mode_;

        std::unique_ptr<WasapiInputDevice> capture_;
        std::unique_ptr<WasapiOutputDevice> render_;

        vector<float> sample_rates_;
        float current_sample_rate_{0};
        float default_sample_rate_{0};
        vector<int> buffer_sizes_;
        int current_buffer_size_{0};
        int min_buffer_size_{0};
        int default_buffer_size_{0};

        std::atomic<bool> opened_{false};

        unique_ptr<dminternal::AudioThread> audio_thread_;

        WasapiDevice(WasapiDeviceInfo info, bool exclusive_mode)
            : Device{info.dev}, exclusive_mode_(exclusive_mode)
        {
            auto enumerator = getEnumerator();
            if (!info.input_id.empty()) {
                ComSmartPtr<IMMDevice> dev;
                auto hr = enumerator->GetDevice(info.input_id.c_str(), &dev);
                if (FAILED(hr))
                    throw RUNTIME_ERROR("failed getting input device");
                capture_ = make_unique<WasapiInputDevice>(dev, exclusive_mode_);
                capture_->registerEndpointVolumeCallback(
                    [this](float value, bool muted, bool internal_change) {
                        inputGainChanged(value, muted, internal_change);
                    });
            }
            if (!info.output_id.empty()) {
                ComSmartPtr<IMMDevice> dev;
                auto hr = enumerator->GetDevice(info.output_id.c_str(), &dev);
                if (FAILED(hr))
                    throw RUNTIME_ERROR("failed getting output device");
                render_ = make_unique<WasapiOutputDevice>(dev, exclusive_mode_);
                render_->registerEndpointVolumeCallback(
                    [this](float value, bool muted, bool internal_change) {
                        outputVolumeChanged(value, muted, internal_change);
                    });
            }

            init();
        }

        ~WasapiDevice()
        {
            //
            WasapiDevice::close();
        }

        void init()
        {
            // Check sample rates
            if (capture_ && render_) {
                std::set_intersection(capture_->sample_rates.begin(),
                                      capture_->sample_rates.end(),
                                      render_->sample_rates.begin(),
                                      render_->sample_rates.end(),
                                      back_inserter(sample_rates_));
                if (sample_rates_.empty())
                    throw RUNTIME_ERROR("capture&render does not share any sample rates");

                min_buffer_size_ =
                    (std::max)(capture_->min_buffer_size_, render_->min_buffer_size_);
                default_buffer_size_ =
                    (std::max)(capture_->default_buffer_size_, render_->default_buffer_size_);
                if (capture_->default_sample_rate_ != render_->default_sample_rate_) {
                    default_sample_rate_ = sample_rates_[0];
                } else {
                    default_sample_rate_ = capture_->default_sample_rate_;
                }
            } else {
                auto d               = capture_ ? static_cast<WasapiDeviceBase*>(capture_.get())
                                                : static_cast<WasapiDeviceBase*>(render_.get());
                sample_rates_        = d->sample_rates;
                min_buffer_size_     = d->min_buffer_size_;
                default_buffer_size_ = d->default_buffer_size_;
                default_sample_rate_ = d->default_sample_rate_;
            }

            buffer_sizes_.push_back(min_buffer_size_);
            for (int n = 64; n <= 2048; n <<= 1)
                if (n >= min_buffer_size_)
                    buffer_sizes_.push_back(n);
            for (int n = default_buffer_size_; n <= 2048; n <<= 1)
                buffer_sizes_.push_back(n);
            std::sort(buffer_sizes_.begin(), buffer_sizes_.end());
            buffer_sizes_.erase(std::unique(buffer_sizes_.begin(), buffer_sizes_.end()),
                                buffer_sizes_.end());
        }

        float defaultSampleRate() const override { return default_sample_rate_; }

        int defaultBuffersize() const override { return default_buffer_size_; }

        std::vector<float> supportedSamplerates() const override { return sample_rates_; }

        std::vector<int> supportedBuffersizes() const override { return buffer_sizes_; }

        vector<string> getChannelNames(bool input) const
        {
            vector<string> names;
            auto count = input ? device.inputchannels : device.outputchannels;
            for (int i = 0; i < count; ++i)
                names.push_back("Channel " + to_string(i + 1));
            return names;
        }

        std::vector<std::string> inputChannelNames() const override
        {
            return move(getChannelNames(true));
        }

        std::vector<std::string> outputChannelNames() const override
        {
            return move(getChannelNames(false));
        }

        void open(float fs,
                  int buffer_size,
                  daudio::ActiveChannels input_mask,
                  daudio::ActiveChannels output_mask) override
        {
            close();

            if (capture_ && input_mask > 0)
                capture_->open(fs, buffer_size, input_mask);
            if (render_ && output_mask > 0)
                render_->open(fs, buffer_size, output_mask);

            if (ok(render_) && ok(capture_) && render_->buffer_size_ != capture_->buffer_size_)
                throw RUNTIME_ERROR("render/capture buffer size mismatch");

            current_sample_rate_ = fs;
            current_buffer_size_ = ok(render_) ? render_->buffer_size_ : capture_->buffer_size_;

            audio_thread_ = make_unique<dminternal::AudioThread>(
                bind(&WasapiDevice::processThread, this, placeholders::_1, placeholders::_2));

            if (ok(capture_))
                capture_->start();
            if (ok(render_))
                render_->start();
            opened_ = true;
        }

        void close() override
        {
            audio_thread_ = nullptr;
            if (opened_.exchange(false)) {
                if (capture_)
                    capture_->close();
                if (render_)
                    render_->close();

                current_sample_rate_ = 0.f;
                current_buffer_size_ = 0;
            }
        }

        bool isOpened() const override { return opened_; }

        float currentSampleRate() const override { return current_sample_rate_; }

        int currentBuffersize() const override { return current_buffer_size_; }

        const daudio::ActiveChannels& activeInputChannels() const override
        {
            return ok(capture_) ? capture_->active_channels_ : none;
        }

        const daudio::ActiveChannels& activeOutputChannels() const override
        {
            return ok(render_) ? render_->active_channels_ : none;
        }

        void processThread(atomic<bool>& thread_started, atomic<bool>& stop_thread)
        {
            daudio::InputBufferView& inputBufferView =
                ok(capture_) ? capture_->view_ : daudio::InputBufferView{};
            daudio::OutputBufferView outputBufferView =
                ok(render_) ? render_->view_ : daudio::OutputBufferView{};

            // Dummy event if lacking input or output
            HANDLE hDummy = NULL;
            enum { CAPTURE, RENDER, CNT };
            HANDLE waitEvents[CNT] = {
                ok(capture_) ? capture_->client_event_
                             : hDummy = CreateEventW(nullptr, false, false, nullptr),
                ok(render_) ? render_->client_event_
                            : hDummy = CreateEventW(nullptr, false, false, nullptr)};

            // Set timeout to 5 times a buffer period
            const auto timeoutMs =
                (std::max)(DWORD(250),
                           5 * DWORD(1000 * current_buffer_size_ / current_sample_rate_));

            try {
                thread_started = true;
                while (!stop_thread) {
                    auto res = WaitForMultipleObjects(CNT, waitEvents, FALSE, timeoutMs);
                    switch (res) {
                    case WAIT_OBJECT_0 + CAPTURE:
                        capture_->handleBuffer();
                        capture_->copyBuffersFromReservoir();
                        // Fall-through if no rendering
                        if (ok(render_))
                            break;

                    case WAIT_OBJECT_0 + RENDER:
                        callCallbacks(inputBufferView, outputBufferView);

                        if (ok(render_))
                            render_->handleBuffer();
                        break;

                    case WAIT_TIMEOUT:
                    default:
                        if (ok(capture_))
                            capture_->checkError();
                        if (ok(render_))
                            render_->checkError();
                        throw RUNTIME_ERROR("audio thread timeout");
                    }
                }
            } catch (...) {
                sessionTerminated(current_exception());
            }

            if (hDummy != NULL)
                CloseHandle(hDummy);
        }

        virtual bool hasInputGainControl() const override
        {
            return capture_ ? capture_->hasEndpointVolumeControl() : false;
        }

        virtual float getInputGain() const override
        {
            if (!capture_)
                throw RUNTIME_ERROR("no capture endpoint");
            return capture_->getVolume();
        }

        virtual void setInputGain(float gain) override
        {
            if (!capture_)
                throw RUNTIME_ERROR("no capture endpoint");
            capture_->setVolume(gain);
        }

        virtual bool getInputMuted() const override
        {
            if (!capture_)
                throw RUNTIME_ERROR("no capture endpoint");
            return capture_->getMuted();
        }

        virtual void setInputMuted(bool mute) override
        {
            if (!capture_)
                throw RUNTIME_ERROR("no capture endpoint");
            capture_->setMuted(mute);
        }
        virtual bool hasOutputVolumeControl() const override
        {
            return render_ ? render_->hasEndpointVolumeControl() : false;
        }

        virtual float getOutputVolume() const override
        {
            if (!render_)
                throw RUNTIME_ERROR("no render endpoint");
            return render_->getVolume();
        }

        virtual void setOutputVolume(float volume) override
        {
            if (!render_)
                throw RUNTIME_ERROR("no render endpoint");
            render_->setVolume(volume);
        }

        virtual bool getOutputMuted() const override
        {
            if (!render_)
                throw RUNTIME_ERROR("no render endpoint");
            return render_->getMuted();
        }

        virtual void setOutputMuted(bool mute) override
        {
            if (!render_)
                throw RUNTIME_ERROR("no render endpoint");
            render_->setMuted(mute);
        }
    };

    struct NotificationClient : IMMNotificationClient {
        ULONG cntr_;
        ComSmartPtr<IMMDeviceEnumerator> enumerator_;
        function<void(LPCWSTR)> callback_;

        NotificationClient(function<void(LPCWSTR)> callback) : cntr_(0), callback_(callback)
        {
            enumerator_ = getEnumerator();
            if (enumerator_) {
                auto hr = enumerator_->RegisterEndpointNotificationCallback(this);
                if (FAILED(hr)) {
                    enumerator_ = nullptr;
                } else {
                    // Do initial callback
                    do_callback(NULL);
                }
            }
        }
        ~NotificationClient() {}

        void do_callback(LPCWSTR dev)
        {  //
            callback_(dev);
        }

        HRESULT STDMETHODCALLTYPE OnDeviceStateChanged(LPCWSTR pwstrDeviceId,
                                                       DWORD dwNewState) override
        {
            do_callback(pwstrDeviceId);
            return S_OK;
        }

        HRESULT STDMETHODCALLTYPE OnDeviceAdded(LPCWSTR pwstrDeviceId) override
        {
            do_callback(pwstrDeviceId);
            return S_OK;
        }

        HRESULT STDMETHODCALLTYPE OnDeviceRemoved(LPCWSTR pwstrDeviceId) override
        {
            do_callback(pwstrDeviceId);
            return S_OK;
        }

        HRESULT STDMETHODCALLTYPE OnDefaultDeviceChanged(EDataFlow,
                                                         ERole,
                                                         LPCWSTR pwstrDeviceId) override
        {
            do_callback(pwstrDeviceId);
            return S_OK;
        }

        HRESULT STDMETHODCALLTYPE OnPropertyValueChanged(LPCWSTR, const PROPERTYKEY) override
        {
            return S_OK;
        }

        HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
        {
            if (IID_IUnknown == riid) {
                AddRef();
                *ppvObject = (IUnknown*)this;
            } else if (__uuidof(IMMNotificationClient) == riid) {
                AddRef();
                *ppvObject = (IMMNotificationClient*)this;
            } else {
                *ppvObject = NULL;
                return E_NOINTERFACE;
            }
            return S_OK;
        }

        ULONG STDMETHODCALLTYPE AddRef(void) { return InterlockedIncrement(&cntr_); }
        ULONG STDMETHODCALLTYPE Release(void) { return InterlockedDecrement(&cntr_); }
    };

    struct WasapiDeviceType : dminternal::DeviceType {
        struct ScanToken {
            vector<WasapiDeviceInfo> devices;
        };
        using device_map = map<wstring, std::shared_ptr<WasapiDevice>>;
        device_map devices_;
        mutex mutable dev_mutex_;

        dminternal::Active active;

        unique_ptr<NotificationClient> callback_handler_;

        const bool exclusive_mode_;

        WasapiDeviceType(function<void()> callback, bool exclusive_mode)
            : exclusive_mode_(exclusive_mode)
        {
            if (callback) {
                callback_handler_ = make_unique<NotificationClient>([this, callback](LPCWSTR id) {
                    wstring ws(id != NULL ? id : L"");
                    active.send([this, callback, ws]() {
                        volatile dminternal::PlatformInitializer p_init;
                        auto token = scanDevices(ws);
                        if (token && updateDevices(move(token)))
                            callback();
                    });
                });
                active.wait();
            }
        }
        ~WasapiDeviceType() {}

        unique_ptr<ScanToken> scanDevices(const wstring& id)
        {
            LOG_TRACE("");
            auto enumerator = getEnumerator();

            ComSmartPtr<IMMDeviceCollection> collection;
            auto hr =
                enumerator->EnumAudioEndpoints(EDataFlow::eAll, DEVICE_STATE_ACTIVE, &collection);
            if (FAILED(hr))
                return nullptr;

            auto get_default_id = [&](EDataFlow flow) {
                ComSmartPtr<IMMDevice> dev;
                if (SUCCEEDED(
                        enumerator->GetDefaultAudioEndpoint(flow, ERole::eMultimedia, &dev))) {
                    LPWSTR id;
                    if (SUCCEEDED(dev->GetId(&id))) {
                        defer { CoTaskMemFree(id); };
                        return wstring(id);
                    }
                }
                return wstring{};
            };

            const wstring default_input  = get_default_id(EDataFlow::eCapture);
            const wstring default_output = get_default_id(EDataFlow::eRender);

            UINT noODevices = 0;
            hr              = collection->GetCount(&noODevices);
            if (FAILED(hr) || noODevices == 0)
                return nullptr;

            struct EndpointInfo {
                wstring device_id;
                bool is_capture;
                bool is_default;
                string endpoint_name;
                string interface_name;
                unsigned channels;
            };

            vector<EndpointInfo> infos;
            for (UINT i = 0; i < noODevices; ++i) {
                ComSmartPtr<IMMDevice> device;
                hr = collection->Item(i, &device);
                if (FAILED(hr))
                    continue;

                LPWSTR pwszID = NULL;
                hr            = device->GetId(&pwszID);
                if (FAILED(hr))
                    continue;
                defer { CoTaskMemFree(pwszID); };

                ComSmartPtr<IMMEndpoint> ep;
                hr = device->QueryInterface(IID_IMMEndpoint, (void**)&ep);
                if (FAILED(hr))
                    continue;

                EDataFlow flow;
                hr = ep->GetDataFlow(&flow);
                if (FAILED(hr))
                    continue;

                ComSmartPtr<IPropertyStore> properties;
                hr = device->OpenPropertyStore(STGM_READ, &properties);
                if (FAILED(hr))
                    continue;

                auto get_property_string = [&](REFPROPERTYKEY key) -> string {
                    string retval;
                    PROPVARIANT varName;
                    PropVariantInit(&varName);
                    auto hr = properties->GetValue(key, &varName);
                    if (SUCCEEDED(hr)) {
                        retval = wstring_to_utf8(varName.pwszVal);
                        PropVariantClear(&varName);
                    }
                    return retval;
                };

                ComSmartPtr<IAudioClient> client;
                hr = device->Activate(IID_IAudioClient, CLSCTX_ALL, NULL, (void**)&client);
                if (FAILED(hr))
                    continue;

                WAVEFORMATEX* mix_format = NULL;
                hr                       = client->GetMixFormat(&mix_format);
                if (FAILED(hr))
                    continue;
                WAVEFORMATEXTENSIBLE format;
                memcpy(&format,
                       mix_format,
                       mix_format->wFormatTag == WAVE_FORMAT_EXTENSIBLE
                           ? sizeof(WAVEFORMATEXTENSIBLE)
                           : sizeof(WAVEFORMATEX));
                CoTaskMemFree(mix_format);

                auto mixFormatChannelMask = format.dwChannelMask;
                const auto mix_channels   = format.Format.nChannels;

                EndpointInfo epi;
                epi.device_id  = pwszID;
                epi.is_capture = (flow == EDataFlow::eCapture);
                epi.is_default = epi.is_capture ? (epi.device_id == default_input)
                                                : (epi.device_id == default_output);
                epi.interface_name =
                    utils::trim(get_property_string(PKEY_DeviceInterface_FriendlyName));
                epi.endpoint_name = utils::trim(get_property_string(PKEY_Device_DeviceDesc));
                epi.channels      = mix_channels;
                if (exclusive_mode_) {
                    if (id.empty()) {
                        for (unsigned channels = mix_channels; channels <= 8; channels += 2) {
                            if (findSupportedFormat(client,
                                                    format.Format.nSamplesPerSec,
                                                    channels,
                                                    KSAUDIO_SPEAKER_DIRECTOUT,
                                                    format,
                                                    true)) {
                                epi.channels = channels;
                            }
                        }
                    }
                }
                infos.push_back(epi);
            }

            auto token = make_unique<ScanToken>();
            // First all inputs
            for (auto& endpoint : infos) {
                if (!endpoint.is_capture)
                    continue;

                string name = endpoint.endpoint_name;
                name += " (" + endpoint.interface_name + ")";
                token->devices.push_back({daudio::AudioDevice(name,
                                                              daudio::AudioDeviceType::wasapi,
                                                              endpoint.channels,
                                                              0,
                                                              endpoint.is_default),
                                          endpoint.device_id,
                                          L""});
            }
            // Next all outputs
            for (auto& endpoint : infos) {
                if (endpoint.is_capture)
                    continue;

                string name = endpoint.endpoint_name;
                name += " (" + endpoint.interface_name + ")";
                token->devices.push_back({daudio::AudioDevice(name,
                                                              daudio::AudioDeviceType::wasapi,
                                                              0,
                                                              endpoint.channels,
                                                              endpoint.is_default),
                                          L"",
                                          endpoint.device_id});
            }

            // Then possible combos
            for (auto& output : infos) {
                if (output.is_capture)
                    continue;
                for (auto& input : infos) {
                    if (!input.is_capture)
                        continue;

                    if (input.is_default && output.is_default) {
#if !DEVICEMANAGER_ENABLE_COMBO_DEVICES
                        if (output.interface_name != input.interface_name)
                            continue;
#endif
                        string name = input.endpoint_name;
                        if (output.endpoint_name != input.endpoint_name)
                            name += " + " + output.endpoint_name;

                        if (output.interface_name != input.interface_name)
                            name += " (" + input.interface_name + ":" + output.interface_name + ")";
                        else
                            name += " (" + input.interface_name + ")";

                        token->devices.push_back(
                            {daudio::AudioDevice(name,
                                                 daudio::AudioDeviceType::wasapi,
                                                 input.channels,
                                                 output.channels,
                                                 true),
                             input.device_id,
                             output.device_id});
                    } else {
                        if (output.interface_name != input.interface_name)
                            continue;

                        string name = input.endpoint_name;
                        if (output.endpoint_name != input.endpoint_name)
                            name += " + " + output.endpoint_name;
                        name += " (" + input.interface_name + ")";

                        token->devices.push_back(
                            {daudio::AudioDevice(name,
                                                 daudio::AudioDeviceType::wasapi,
                                                 input.channels,
                                                 output.channels,
                                                 false),
                             input.device_id,
                             output.device_id});
                    }
                }
            }
            return token;
        }

        bool updateDevices(unique_ptr<ScanToken> newDevs)
        {
            LOG_TRACE("");
            bool retval = false;
            lock_guard<mutex> lock{dev_mutex_};
            // Check what to remove
            set<wstring> to_remove;
            for (auto& entry : devices_) {
                auto it =
                    std::find_if(newDevs->devices.begin(), newDevs->devices.end(), [&](auto& d) {
                        return (d.input_id + d.output_id) == entry.first;
                    });
                if (it == newDevs->devices.end()) {
                    to_remove.insert(entry.first);
                } else {
                    // For existing devices, check if default device state has changed
                    WasapiDevice* dev = entry.second.get();
                    if (dev->externalDevice().is_default != (*it).dev.is_default) {
                        dev->setDefault((*it).dev.is_default);
                        retval = true;
                    }
                }
            }
            // Then remove
            for (auto& entry : to_remove) {
                devices_.erase(entry);
                retval = true;
            }
            // Check what to add
            for (auto& entry : newDevs->devices) {
                auto key = entry.input_id + entry.output_id;
                auto it  = devices_.find(key);
                if (it == devices_.end()) {
                    try {
                        // This *might* fail for in/out devices not sharing a single
                        // sample rate.
                        auto new_device = shared_ptr<WasapiDevice>(
                            new WasapiDevice(entry, exclusive_mode_),
                            [this](WasapiDevice* ptr) { active.send([ptr] { delete ptr; }); });
                        assert(none_of(devices_.begin(), devices_.end(), [&](auto& it) {
                            return it.second->externalDevice().id ==
                                   new_device->externalDevice().id;
                        }));
                        devices_.insert(make_pair(key, move(new_device)));
                        retval = true;
                    } catch (exception&) {
                        // Don't mind this
                    }
                }
            }
            return retval;
        }

        std::vector<daudio::AudioDevice> devices() override
        {
            lock_guard<mutex> lock{dev_mutex_};
            std::vector<daudio::AudioDevice> devs;
            for (auto it : devices_)
                devs.emplace_back(it.second->externalDevice());
            return devs;
        }

        std::shared_ptr<dminternal::Device> getDevice(device_id_t id) override
        {
            lock_guard<mutex> lock{dev_mutex_};
            auto it = std::find_if(devices_.begin(), devices_.end(), [&](auto& entry) {
                return entry.second->id() == id;
            });
            if (it == devices_.end())
                return nullptr;
            return it->second;
        }
    };
}  // namespace

namespace dminternal
{
    template <>
    unique_ptr<dminternal::DeviceType> DeviceType::create<daudio::AudioDeviceType::wasapi>(
        function<void()> callback)
    {
        bool exclusive = (getenv("DAUDIO_WASAPI_NON_EXCLUSIVE") == nullptr ||
                          (_stricmp(getenv("DAUDIO_WASAPI_NON_EXCLUSIVE"), "OFF") == 0));
        return make_unique<WasapiDeviceType>(callback, exclusive);
    }
}  // namespace dminternal
