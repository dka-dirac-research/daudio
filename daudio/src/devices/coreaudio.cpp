
#include "dminternal/device.h"

#include <CoreAudio/CoreAudio.h>
//#include <CoreAudio/CoreAudioTypes.h>
#include <AudioUnit/AUComponent.h>
#include <AudioUnit/AudioUnit.h>
#include <Security/SecBase.h>

#include <set>
using namespace std;

#include <log/log.hpp>

#include <daudio/waveform.h>
#include <dminternal/defer.h>
#include <dminternal/mathutil.h>

#include <dminternal/active.h>

namespace
{
    void findSubstrings(const std::string& word, std::set<std::string>& substrings)
    {
        int l = word.length();
        for (int start = 0; start < l; start++) {
            for (int length = 1; length < l - start + 1; length++) {
                substrings.insert(word.substr(start, length));
            }
        }
    }

    std::string lcs(const std::string& first, const std::string& second)
    {
        std::set<std::string> firstSubstrings, secondSubstrings;
        findSubstrings(first, firstSubstrings);
        findSubstrings(second, secondSubstrings);
        std::set<std::string> common;
        std::set_intersection(firstSubstrings.begin(),
                              firstSubstrings.end(),
                              secondSubstrings.begin(),
                              secondSubstrings.end(),
                              std::inserter(common, common.begin()));
        std::vector<std::string> commonSubs(common.begin(), common.end());
        std::sort(
            commonSubs.begin(), commonSubs.end(), [](const std::string& s1, const std::string& s2) {
                return s1.length() > s2.length();
            });
        if (commonSubs.empty())
            return std::string{};

        std::string result = *(commonSubs.begin());
        // Check that it is a whole word, for both inputs
        for (auto& s : {first, second}) {
            auto idx = s.find(result);
            if (idx > 0 && s.at(idx - 1) != ' ')
                return string{};
            if (idx + result.length() >= s.length())
                continue;

            auto c = s.at(idx + result.length() - 1);
            if (c != ' ')
                return string{};
        }
        return result;
    }

    // trim from start (in place)
    static inline void ltrim(std::string& s)
    {
        s.erase(s.begin(),
                std::find_if(s.begin(), s.end(), [](int ch) { return !std::isspace(ch); }));
    }

    // trim from end (in place)
    static inline void rtrim(std::string& s)
    {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) { return !std::isspace(ch); }).base(),
                s.end());
    }

    // trim from both ends (in place)
    static inline void trim(std::string& s)
    {
        ltrim(s);
        rtrim(s);
    }

    // trim from both ends (copying)
    static inline std::string trim_copy(std::string s)
    {
        trim(s);
        return s;
    }

    string remove(string s, const string& to_remove)
    {
        auto idx = s.find(to_remove);
        if (idx != string::npos) {
            s.replace(idx, to_remove.length(), string{});
        }
        return s;
    }

    static bool checkErr(OSStatus err)
    {
        if (err == noErr)
            return true;

        NSError* error = [NSError errorWithDomain:NSOSStatusErrorDomain code:err userInfo:nil];
        NSString* errorMessage = [error localizedDescription];
        throw runtime_error("Error: " + string([errorMessage UTF8String]));
    }

    AudioDeviceID getDefaultDevice(bool input)
    {
        AudioDeviceID deviceID = kAudioDeviceUnknown;
        UInt32 size            = sizeof(deviceID);

        AudioObjectPropertyAddress pa{.mSelector = input
                                                       ? kAudioHardwarePropertyDefaultInputDevice
                                                       : kAudioHardwarePropertyDefaultOutputDevice,
                                      .mScope   = kAudioObjectPropertyScopeWildcard,
                                      .mElement = kAudioObjectPropertyElementMaster};

        AudioObjectGetPropertyData(kAudioObjectSystemObject, &pa, 0, nullptr, &size, &deviceID);
        return deviceID;
    }

    static OSStatus getBufferFrameSizeRange(AudioUnit inAUHAL,
                                            UInt32* outMinimum,
                                            UInt32* outMaximum)
    {
        AudioValueRange theRange = {0, 0};
        UInt32 theDataSize       = sizeof(AudioValueRange);
        OSStatus theError        = AudioUnitGetProperty(inAUHAL,
                                                 kAudioDevicePropertyBufferFrameSizeRange,
                                                 kAudioUnitScope_Global,
                                                 0,
                                                 &theRange,
                                                 &theDataSize);

        if (theError == 0) {
            *outMinimum = theRange.mMinimum;
            *outMaximum = theRange.mMaximum;
        }
        return theError;
    }

    static vector<float> getSampleRatesFromDevice(AudioDeviceID id)
    {
        AudioObjectPropertyAddress pa{.mSelector = kAudioDevicePropertyAvailableNominalSampleRates,
                                      .mScope    = kAudioObjectPropertyScopeWildcard,
                                      .mElement  = kAudioObjectPropertyElementMaster};

        UInt32 size = 0;
        vector<float> rates;
        if (checkErr(AudioObjectGetPropertyDataSize(id, &pa, 0, nullptr, &size))) {
            vector<AudioValueRange> ranges(size);

            if (checkErr(AudioObjectGetPropertyData(id, &pa, 0, nullptr, &size, ranges.data()))) {
                for (auto rate : dminternal::Device::possibleSampleRates()) {
                    for (int j = size / (int)sizeof(AudioValueRange); --j >= 0;) {
                        if (rate >= ranges[j].mMinimum - 2 && rate <= ranges[j].mMaximum + 2) {
                            rates.push_back(rate);
                            break;
                        }
                    }
                }
            }
        }

        if (rates.empty())
            throw runtime_error("no sample rates for device " + to_string(id));
        return rates;
    }

    float getNominalSampleRateFromDevice(AudioDeviceID id)
    {
        AudioObjectPropertyAddress pa{.mSelector = kAudioDevicePropertyNominalSampleRate,
                                      .mScope    = kAudioObjectPropertyScopeWildcard,
                                      .mElement  = kAudioObjectPropertyElementMaster};
        Float64 sr;
        UInt32 size = sizeof(sr);
        checkErr(AudioObjectGetPropertyData(id, &pa, 0, nullptr, &size, &sr));
        return (float)sr;
    }

    struct AudioSessionNotifier {
        const AudioDeviceID id_;
        function<void()> fn_;
        AudioSessionNotifier(AudioDeviceID id, function<void()> fn) : id_(id), fn_(fn)
        {
            // Add listener for detecting when a device is removed
            const AudioObjectPropertyAddress alive_address{
                .mSelector = kAudioDevicePropertyDeviceIsAlive,
                .mScope    = kAudioObjectPropertyScopeGlobal,
                .mElement  = kAudioObjectPropertyElementMaster};
            auto err =
                AudioObjectAddPropertyListener(id_, &alive_address, deviceIsAliveCallback, this);
            checkErr(err);
        }
        ~AudioSessionNotifier()
        {
            // Add listener for detecting when a device is removed
            const AudioObjectPropertyAddress alive_address{
                .mSelector = kAudioDevicePropertyDeviceIsAlive,
                .mScope    = kAudioObjectPropertyScopeGlobal,
                .mElement  = kAudioObjectPropertyElementMaster};
            AudioObjectRemovePropertyListener(id_, &alive_address, deviceIsAliveCallback, this);
        }

        static OSStatus deviceIsAliveCallback(AudioObjectID,
                                              UInt32,
                                              const AudioObjectPropertyAddress*,
                                              void* inClientData)
        {
            auto fn = reinterpret_cast<AudioSessionNotifier*>(inClientData)->fn_;
            if (fn)
                fn();
            return noErr;
        }
    };

    struct AudioVolumeCallback {
        const bool input_;
        AudioDeviceID id_;
        dminternal::Device::notification_callback_type callback_;

        static OSStatus _listenerProcess(AudioObjectID inObjectID,
                                         UInt32 inNumberAddresses,
                                         const AudioObjectPropertyAddress* inAddresses,
                                         void* __nullable inClientData)
        {
            return reinterpret_cast<AudioVolumeCallback*>(inClientData)
                ->listenerProcess(inObjectID, inNumberAddresses, inAddresses);
        }

        OSStatus listenerProcess(AudioObjectID /*inObjectID*/,
                                 UInt32 inNumberAddresses,
                                 const AudioObjectPropertyAddress* inAddresses)
        {
            for (UInt32 i = 0; i < inNumberAddresses; ++i) {
                switch (inAddresses[i].mSelector) {
                case kAudioDevicePropertyVolumeScalar:
                case kAudioDevicePropertyMute:
                    break;
                }
            }
            return noErr;
        }

        AudioVolumeCallback(bool input,
                            AudioDeviceID id,
                            dminternal::Device::notification_callback_type fn)
            : input_(input), id_(id), callback_(fn)
        {
            AudioObjectPropertyAddress pa{
                .mSelector = kAudioDevicePropertyVolumeScalar,
                .mScope = input ? kAudioDevicePropertyScopeInput : kAudioDevicePropertyScopeOutput,
                .mElement = kAudioObjectPropertyElementMaster};
            if (AudioObjectAddPropertyListener(
                    id_, &pa, &AudioVolumeCallback::_listenerProcess, this) != noErr) {
                pa.mElement = 1;
                if (AudioObjectAddPropertyListener(
                        id_, &pa, &AudioVolumeCallback::_listenerProcess, this) != noErr) {
                    int foo = 1;
                }
            }
        }
        ~AudioVolumeCallback()
        {
            AudioObjectPropertyAddress pa{
                .mSelector = kAudioDevicePropertyVolumeScalar,
                .mScope = input_ ? kAudioDevicePropertyScopeInput : kAudioDevicePropertyScopeOutput,
                .mElement = kAudioObjectPropertyElementMaster};
            if (AudioObjectRemovePropertyListener(
                    id_, &pa, &AudioVolumeCallback::_listenerProcess, this) != noErr) {
                pa.mElement = 1;
                if (AudioObjectRemovePropertyListener(
                        id_, &pa, &AudioVolumeCallback::_listenerProcess, this) != noErr) {
                    int foo = 1;
                }
            }
        }
    };

    struct AudioDevice {
        AudioDeviceID id_;
        AudioUnit audio_unit_;
        AudioStreamBasicDescription format_;
        const bool is_default_{false};

        vector<float> sample_rates_;
        float default_sample_rate_;
        vector<int> buffer_sizes_;
        daudio::ActiveChannels active_channels_;
        unsigned actual_ca_channels_{0};

        shared_ptr<AudioSessionNotifier> removed_notifier_;
        shared_ptr<AudioVolumeCallback> volume_callback_;

        bool interleaved_{false};

        static OSStatus internalProcess(void* inRefCon,
                                        AudioUnitRenderActionFlags* ioActionFlags,
                                        const AudioTimeStamp* inTimeStamp,
                                        UInt32 inBusNumber,
                                        UInt32 inNumberFrames,
                                        AudioBufferList* ioData)
        {
            return reinterpret_cast<AudioDevice*>(inRefCon)->process(
                ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, ioData);
        }

        void setupAudioUnit(bool input)
        {
            OSStatus err = noErr;

            AudioComponent comp;
            AudioComponentDescription desc;

            // There are several different types of Audio Units.
            // Some audio units serve as Outputs, Mixers, or DSP
            // units. See AUComponent.h for listing
            desc.componentType = kAudioUnitType_Output;

            // Every Component has a subType, which will give a clearer picture
            // of what this components function will be.

            desc.componentSubType = kAudioUnitSubType_HALOutput;

            // all Audio Units in AUComponent.h must use
            //"kAudioUnitManufacturer_Apple" as the Manufacturer
            desc.componentManufacturer = kAudioUnitManufacturer_Apple;
            desc.componentFlags        = 0;
            desc.componentFlagsMask    = 0;

            // Finds a component that meets the desc spec's
            comp = AudioComponentFindNext(NULL, &desc);
            if (comp == NULL)
                throw runtime_error("AU find next failed");

            // gains access to the services provided by the component
            err = AudioComponentInstanceNew(comp, &audio_unit_);
            checkErr(err);

            // AUHAL needs to be initialized before anything is done to it
            err = AudioUnitInitialize(audio_unit_);
            checkErr(err);

            // Enable/disable input on the AUHAL
            UInt32 enableIO = input ? 1 : 0;
            err             = AudioUnitSetProperty(audio_unit_,
                                       kAudioOutputUnitProperty_EnableIO,
                                       kAudioUnitScope_Input,
                                       1,  // input element
                                       &enableIO,
                                       sizeof(enableIO));
            checkErr(err);

            // disable/enable Output on the AUHAL
            enableIO = input ? 0 : 1;
            err      = AudioUnitSetProperty(audio_unit_,
                                       kAudioOutputUnitProperty_EnableIO,
                                       kAudioUnitScope_Output,
                                       0,  // output element
                                       &enableIO,
                                       sizeof(enableIO));
            checkErr(err);

            err = AudioUnitSetProperty(audio_unit_,
                                       kAudioOutputUnitProperty_CurrentDevice,
                                       kAudioUnitScope_Global,
                                       0,
                                       &id_,
                                       sizeof(id_));
            checkErr(err);

            AURenderCallbackStruct cb;
            cb.inputProc       = internalProcess;
            cb.inputProcRefCon = this;
            if (input) {
                err = AudioUnitSetProperty(audio_unit_,
                                           kAudioOutputUnitProperty_SetInputCallback,
                                           kAudioUnitScope_Global,
                                           0,
                                           &cb,
                                           sizeof(cb));
                checkErr(err);
            } else {
                err = AudioUnitSetProperty(audio_unit_,
                                           kAudioUnitProperty_SetRenderCallback,
                                           kAudioUnitScope_Global,
                                           0,
                                           &cb,
                                           sizeof(cb));
                checkErr(err);
            }
        }

        AudioDevice(AudioDeviceID id)
            : id_(id), is_default_(id_ == getDefaultDevice(true) || id_ == getDefaultDevice(false))
        {
        }

        ~AudioDevice()
        {
            try {
                stop();
            } catch (...) {
            }
        }

        void init(bool input)
        {
            setupAudioUnit(input);
            defer
            {
                auto err = AudioUnitUninitialize(audio_unit_);
                checkErr(err);
            };

            // Check buffer sizes
            UInt32 min_size, max_size;
            checkErr(getBufferFrameSizeRange(audio_unit_, &min_size, &max_size));
            auto n = dminternal::next_power_of_2(min_size);
            for (; n <= max_size;
                 n += n < 64 ? 16 : (n < 512 ? 32 : (n < 1024 ? 64 : (n < 2048 ? 128 : 256))))
                buffer_sizes_.push_back(n);

            // Check supported sample rates
            sample_rates_        = getSampleRatesFromDevice(id_);
            default_sample_rate_ = getNominalSampleRateFromDevice(id_);
            if (default_sample_rate_ == 0.f) {
                for (auto rate : sample_rates_) {
                    if (rate > 48000.0f)
                        break;
                    default_sample_rate_ = rate;
                }
            }

            // Get stream format
            UInt32 size = sizeof(format_);
            checkErr(AudioUnitGetProperty(audio_unit_,
                                          kAudioUnitProperty_StreamFormat,
                                          kAudioUnitScope_Input,
                                          1,
                                          &format_,
                                          &size));
            if (format_.mFormatID != kAudioFormatLinearPCM ||
                !(format_.mFormatFlags & kAudioFormatFlagIsFloat) ||
                !(format_.mFormatFlags & kAudioFormatFlagIsPacked))
                throw runtime_error("Format is not lpcm, or not packed float");

            interleaved_        = !(format_.mFormatFlags & kAudioFormatFlagIsNonInterleaved);
            actual_ca_channels_ = format_.mChannelsPerFrame;
        }

        void registerRemovedCallback(function<void()> fn)
        {
            removed_notifier_ = make_unique<AudioSessionNotifier>(id_, fn);
        }

        int channels(const bool input, vector<string>* names = nullptr) const
        {
            OSStatus err;
            UInt32 propSize;
            int chanNum = 0;

            AudioObjectPropertyAddress pa{
                .mSelector = kAudioDevicePropertyStreamConfiguration,
                .mScope = input ? kAudioDevicePropertyScopeInput : kAudioDevicePropertyScopeOutput,
                .mElement = 0  // channel
            };                 // channel

            err = AudioObjectGetPropertyDataSize(id_, &pa, 0, NULL, &propSize);
            if (err)
                return 0;

            AudioBufferList* buflist = (AudioBufferList*)malloc(propSize);
            defer { free(buflist); };
            err = AudioObjectGetPropertyData(id_, &pa, 0, NULL, &propSize, buflist);
            if (err == noErr) {
                for (UInt32 i = 0; i < buflist->mNumberBuffers; ++i) {
                    for (UInt32 j = 0; j < buflist->mBuffers[i].mNumberChannels; ++j) {
                        if (names != nullptr) {
                            pa.mSelector           = kAudioObjectPropertyElementName;
                            pa.mElement            = (AudioObjectPropertyElement)chanNum + 1;
                            NSString* nameNSString = nil;
                            UInt32 size            = sizeof(nameNSString);
                            if (AudioObjectGetPropertyData(
                                    id_, &pa, 0, nullptr, &size, &nameNSString) == noErr) {
                                defer { [nameNSString release]; };
                                string name{[nameNSString UTF8String]};
                                if (name.empty()) {
                                    name = (input ? "Input " : "Output ") + to_string(chanNum + 1);
                                }
                                (*names).push_back(name);
                            }
                        }
                        ++chanNum;
                    }
                }
            }
            return chanNum;
        }

        string name() const
        {
            char buf[256];
            UInt32 maxlen = sizeof(buf) - 1;
            AudioObjectPropertyAddress pa{
                .mSelector = kAudioDevicePropertyDeviceName,
                .mScope    = kAudioObjectPropertyScopeGlobal,
                .mElement  = 0  // channel
            };

            auto res = AudioObjectGetPropertyData(id_, &pa, 0, NULL, &maxlen, buf);
            return string{buf};
        }

        bool isDefault() const { return is_default_; }

        void openDevice(float fs, int buffer_size, daudio::ActiveChannels channels, bool is_input)
        {
            if (none_of(
                    sample_rates_.begin(), sample_rates_.end(), [fs](auto v) { return v == fs; }))
                throw runtime_error("sample rate not supported");
            if (none_of(buffer_sizes_.begin(), buffer_sizes_.end(), [buffer_size](auto v) {
                    return v == buffer_size;
                }))
                throw runtime_error("buffer size not supported");

            if (channels.channels.size() != actual_ca_channels_)
                throw runtime_error("wrong number of entries in channels, should be " +
                                    to_string(actual_ca_channels_));

            auto err = AudioUnitInitialize(audio_unit_);
            checkErr(err);

            format_.mSampleRate       = fs;
            format_.mChannelsPerFrame = actual_ca_channels_;

            if (!is_input) {
                err = AudioUnitSetProperty(audio_unit_,
                                           kAudioUnitProperty_StreamFormat,
                                           kAudioUnitScope_Input,
                                           0,
                                           &format_,
                                           sizeof(format_));
                checkErr(err);
            } else {
                err = AudioUnitSetProperty(audio_unit_,
                                           kAudioUnitProperty_StreamFormat,
                                           kAudioUnitScope_Output,
                                           1,
                                           &format_,
                                           sizeof(format_));
                checkErr(err);
            }

            UInt32 bufferSizeFrames = buffer_size;
            err                     = AudioUnitSetProperty(audio_unit_,
                                       kAudioDevicePropertyBufferFrameSize,
                                       kAudioUnitScope_Global,
                                       0,
                                       &bufferSizeFrames,
                                       sizeof(UInt32));
            checkErr(err);

            active_channels_ = channels;
        }

        void start()
        {
            auto err = AudioOutputUnitStart(audio_unit_);
            checkErr(err);
        }

        void stop()
        {
            auto err = AudioOutputUnitStop(audio_unit_);
            checkErr(err);
        }

        bool isRunning() const
        {
            UInt32 auhalRunning = 0;
            UInt32 size         = sizeof(auhalRunning);
            if (audio_unit_) {
                auto err = AudioUnitGetProperty(audio_unit_,
                                                kAudioOutputUnitProperty_IsRunning,
                                                kAudioUnitScope_Global,
                                                0,  // input element
                                                &auhalRunning,
                                                &size);
                checkErr(err);
                return auhalRunning;
            }
            return false;
        }

        void closeDevice()
        {
            if (isRunning()) {
                stop();
                active_channels_ = daudio::ActiveChannels{};
                auto err         = AudioUnitUninitialize(audio_unit_);
                checkErr(err);
            }
        }

        virtual OSStatus process(AudioUnitRenderActionFlags*,
                                 const AudioTimeStamp*,
                                 UInt32,
                                 UInt32,
                                 AudioBufferList*)
        {
            //
            return noErr;
        };

        virtual bool hasVolumeControl() const { return false; }
        virtual float getVolume() const { return 0.f; }
        virtual void setVolume(float) {}
        virtual bool getMuted() const { return false; }
        virtual void setMuted(bool) {}

        void setEndpointVolumeCallback(bool input,
                                       dminternal::Device::notification_callback_type fn)
        {
            volume_callback_ = make_unique<AudioVolumeCallback>(input, id_, fn);
        }
    };

    vector<AudioDevice> getAudioDevices()
    {
        vector<AudioDevice> devs;

        UInt32 propsize;
        AudioObjectPropertyAddress theAddress = {.mSelector = kAudioHardwarePropertyDevices,
                                                 .mScope    = kAudioObjectPropertyScopeGlobal,
                                                 .mElement  = kAudioObjectPropertyElementMaster};

        auto res = AudioObjectGetPropertyDataSize(
            kAudioObjectSystemObject, &theAddress, 0, NULL, &propsize);

        int nDevices          = propsize / sizeof(AudioDeviceID);
        AudioDeviceID* devids = new AudioDeviceID[nDevices];
        defer { delete[] devids; };
        res = AudioObjectGetPropertyData(
            kAudioObjectSystemObject, &theAddress, 0, NULL, &propsize, devids);

        for (int i = 0; i < nDevices; ++i) {
            AudioDevice dev(devids[i]);
            devs.push_back(move(dev));
        }

        return devs;
    }

    struct AudioInputDevice : AudioDevice {
        daudio::Waveforms buffers_;
        daudio::InputBufferView input_view_;
        function<void()> callback_;
        AudioBufferList* buffer_list_{nullptr};

        AudioInputDevice(AudioDeviceID id) : AudioDevice(id) { init(true); }

        void open(float fs, int buffer_size, daudio::ActiveChannels channels)
        {
            openDevice(fs, buffer_size, channels, true);
            buffers_.resize(actual_ca_channels_, buffer_size);
            input_view_.channel.clear();
            for (unsigned i = 0; i < actual_ca_channels_; ++i) {
                if (active_channels_.channels[i]) {
                    input_view_.channel.emplace_back(buffers_.waveforms[i].data(),
                                                     buffers_.waveforms[i].data() + buffer_size);
                }
            }

            if (interleaved_) {
                const auto size =
                    offsetof(AudioBufferList, mBuffers[0]) + (sizeof(AudioBuffer) * 1);
                buffer_list_                              = (AudioBufferList*)malloc(size);
                buffer_list_->mNumberBuffers              = 1;
                buffer_list_->mBuffers[0].mNumberChannels = actual_ca_channels_;
                buffer_list_->mBuffers[0].mDataByteSize =
                    actual_ca_channels_ * buffer_size * sizeof(float);
                buffer_list_->mBuffers[0].mData = malloc(buffer_list_->mBuffers[0].mDataByteSize);
            } else {
                const auto size = offsetof(AudioBufferList, mBuffers[0]) +
                                  (sizeof(AudioBuffer) * actual_ca_channels_);
                buffer_list_                 = (AudioBufferList*)malloc(size);
                buffer_list_->mNumberBuffers = actual_ca_channels_;
                for (unsigned i = 0; i < actual_ca_channels_; ++i) {
                    buffer_list_->mBuffers[i].mNumberChannels = 1;
                    buffer_list_->mBuffers[i].mData           = buffers_.waveforms[i].data();
                    buffer_list_->mBuffers[i].mDataByteSize   = buffer_size * sizeof(float);
                }
            }
        }

        void close()
        {
            closeDevice();
            if (buffer_list_ != nullptr) {
                if (interleaved_)
                    free(buffer_list_->mBuffers[0].mData);
                free(buffer_list_);
                buffer_list_ = nullptr;
            }
        }

        void prepare(function<void()> callback) { callback_ = callback; }

        OSStatus process(AudioUnitRenderActionFlags* ioActionFlags,
                         const AudioTimeStamp* inTimeStamp,
                         UInt32 busNumber,
                         UInt32 numOfFrames,
                         AudioBufferList*) override
        {
            //
            auto err = AudioUnitRender(
                audio_unit_, ioActionFlags, inTimeStamp, busNumber, numOfFrames, buffer_list_);
            if (interleaved_) {
                unsigned out_ch = 0;
                for (unsigned ch = 0; ch < actual_ca_channels_; ++ch) {
                    if (!active_channels_.channels[ch])
                        continue;

                    const float* src =
                        static_cast<const float*>(buffer_list_->mBuffers[0].mData) + ch;
                    float* dst = buffers_.waveforms[out_ch++].data();
                    for (unsigned i = 0; i < numOfFrames; ++i, src += actual_ca_channels_, dst++)
                        *dst = *src;
                }
            }
            callback_();
            return err;
        };

        bool hasVolumeControl() const override
        {
            AudioObjectPropertyAddress pa{.mSelector = kAudioDevicePropertyVolumeScalar,
                                          .mScope    = kAudioDevicePropertyScopeInput,
                                          .mElement  = kAudioObjectPropertyElementMaster};
            Boolean isSettable = false;
            if (AudioObjectIsPropertySettable(id_, &pa, &isSettable) == noErr && isSettable)
                return true;

            // Check first channel
            pa.mElement = 1;
            if (AudioObjectIsPropertySettable(id_, &pa, &isSettable) == noErr && isSettable)
                return true;

            return false;
        }

        float getVolume() const override
        {
            AudioObjectPropertyAddress pa{.mSelector = kAudioDevicePropertyVolumeScalar,
                                          .mScope    = kAudioDevicePropertyScopeInput,
                                          .mElement  = kAudioObjectPropertyElementMaster};
            Float32 volume     = 0.f;
            UInt32 size        = sizeof(volume);
            Boolean isSettable = false;
            if (AudioObjectIsPropertySettable(id_, &pa, &isSettable) == noErr && isSettable) {
                checkErr(AudioObjectGetPropertyData(id_, &pa, 0, NULL, &size, &volume));
            } else {
                // We only get the first channels volume
                pa.mElement = 1;
                checkErr(AudioObjectGetPropertyData(id_, &pa, 0, NULL, &size, &volume));
            }
            return volume;
        }

        void setVolume(float value) override
        {
            AudioObjectPropertyAddress pa{.mSelector = kAudioDevicePropertyVolumeScalar,
                                          .mScope    = kAudioDevicePropertyScopeInput,
                                          .mElement  = kAudioObjectPropertyElementMaster};
            Float32 volume     = value;
            UInt32 size        = sizeof(volume);
            Boolean isSettable = false;
            if (AudioObjectIsPropertySettable(id_, &pa, &isSettable) == noErr && isSettable) {
                checkErr(AudioObjectSetPropertyData(id_, &pa, 0, NULL, size, &volume));
            } else {
                for (UInt32 i = 1; i <= actual_ca_channels_; ++i) {
                    pa.mElement = i;
                    checkErr(AudioObjectSetPropertyData(id_, &pa, 0, NULL, size, &volume));
                }
            }
        }

        bool getMuted() const override
        {
            AudioObjectPropertyAddress pa{.mSelector = kAudioDevicePropertyMute,
                                          .mScope    = kAudioDevicePropertyScopeInput,
                                          .mElement  = kAudioObjectPropertyElementMaster};
            UInt32 muted       = 0;
            UInt32 size        = sizeof(muted);
            Boolean isSettable = false;
            if (AudioObjectIsPropertySettable(id_, &pa, &isSettable) == noErr && isSettable) {
                checkErr(AudioObjectGetPropertyData(id_, &pa, 0, NULL, &size, &muted));
            }
            return !!muted;
        }

        void setMuted(bool mute) override
        {
            AudioObjectPropertyAddress pa{.mSelector = kAudioDevicePropertyMute,
                                          .mScope    = kAudioDevicePropertyScopeInput,
                                          .mElement  = kAudioObjectPropertyElementMaster};
            UInt32 muted       = mute ? 1 : 0;
            UInt32 size        = sizeof(muted);
            Boolean isSettable = false;
            if (AudioObjectIsPropertySettable(id_, &pa, &isSettable) == noErr && isSettable) {
                checkErr(AudioObjectSetPropertyData(id_, &pa, 0, NULL, size, &muted));
            }
        }
    };

    struct AudioOutputDevice : AudioDevice {
        daudio::OutputBufferView output_view_;
        daudio::Waveforms buffers_;
        function<void()> callback_;

        AudioOutputDevice(AudioDeviceID id) : AudioDevice(id) { init(false); }

        void open(float fs, int buffer_size, daudio::ActiveChannels channels)
        {
            openDevice(fs, buffer_size, channels, false);
            if (interleaved_) {
                buffers_.resize(active_channels_, buffer_size);
                output_view_.channel.clear();
                for (unsigned i = 0; i < active_channels_; ++i)
                    output_view_.channel.emplace_back(buffers_.waveforms[i].data(),
                                                      buffers_.waveforms[i].data() + buffer_size);
            } else {
                output_view_.channel.clear();
                for (unsigned i = 0; i < active_channels_; ++i)
                    output_view_.channel.emplace_back(nullptr, nullptr);
            }
        }

        void close() { closeDevice(); }

        void prepare(function<void()> callback) { callback_ = callback; }

        OSStatus process(AudioUnitRenderActionFlags*,
                         const AudioTimeStamp*,
                         UInt32 /*busNumber*/,
                         UInt32 numOfFrames,
                         AudioBufferList* bufList) override
        {
            //
            if (!interleaved_) {
                for (unsigned i = 0; i < bufList->mNumberBuffers; ++i) {
                    char* ptr               = static_cast<char*>(bufList->mBuffers[i].mData);
                    output_view_.channel[i] = daudio::Range<float>(
                        (float*)ptr, (float*)(ptr + bufList->mBuffers[i].mDataByteSize));
                }
            }

            callback_();

            if (interleaved_) {
                const unsigned no_of_channels = active_channels_;
                for (unsigned ch = 0; ch < no_of_channels; ++ch) {
                    const float* src = buffers_.waveforms[ch].data();
                    float* dst       = static_cast<float*>(bufList->mBuffers[0].mData) + ch;
                    for (unsigned i = 0; i < numOfFrames; ++i, src++, dst += no_of_channels)
                        *dst = *src;
                }
            }
            return noErr;
        };
    };

    struct CAAudioDevice {
        daudio::AudioDevice dev;
        AudioDeviceID input_id;
        AudioDeviceID output_id;
    };

    struct CoreAudioDevice : dminternal::Device {
        unique_ptr<AudioInputDevice> capture_;
        unique_ptr<AudioOutputDevice> render_;

        vector<float> sample_rates_;
        float default_sample_rate_{0.f};
        float current_sample_rate_{0.f};
        vector<int> buffer_sizes_;
        int default_buffer_size_{0};
        int current_buffer_size_{0};

        atomic<bool> opened_{false};

        CoreAudioDevice(CAAudioDevice dev) : dminternal::Device(dev.dev)
        {
            if (dev.input_id != kAudioDeviceUnknown) {
                capture_ = make_unique<AudioInputDevice>(dev.input_id);
                capture_->setEndpointVolumeCallback(
                    true, [this](float value, bool muted, bool internal_change) {
                        inputGainChanged(value, muted, internal_change);
                    });
            }
            if (dev.output_id != kAudioDeviceUnknown) {
                render_ = make_unique<AudioOutputDevice>(dev.output_id);
                render_->setEndpointVolumeCallback(
                    false, [this](float value, bool muted, bool internal_change) {
                        outputVolumeChanged(value, muted, internal_change);
                    });
            }

            if (!capture_ && !render_)
                throw runtime_error("No input or output");

            init();
        }
        ~CoreAudioDevice() { close(); }

        void init()
        {
            // Check sample rates
            if (capture_ && render_) {
                std::set_intersection(capture_->sample_rates_.begin(),
                                      capture_->sample_rates_.end(),
                                      render_->sample_rates_.begin(),
                                      render_->sample_rates_.end(),
                                      back_inserter(sample_rates_));
                if (sample_rates_.empty())
                    throw runtime_error("capture&render does not share any sample rates");

                if (capture_->default_sample_rate_ != render_->default_sample_rate_) {
                    default_sample_rate_ = sample_rates_[0];
                    for (auto rate : sample_rates_) {
                        if (rate > 48000.0f)
                            break;
                        default_sample_rate_ = rate;
                    }
                } else {
                    default_sample_rate_ = capture_->default_sample_rate_;
                }

                std::set_intersection(capture_->buffer_sizes_.begin(),
                                      capture_->buffer_sizes_.end(),
                                      render_->buffer_sizes_.begin(),
                                      render_->buffer_sizes_.end(),
                                      back_inserter(buffer_sizes_));
                if (buffer_sizes_.empty())
                    throw runtime_error("capture&render does not share any buffer sizes");

            } else {
                auto d = capture_ ? static_cast<AudioDevice*>(capture_.get())
                                  : static_cast<AudioDevice*>(render_.get());
                sample_rates_        = d->sample_rates_;
                buffer_sizes_        = d->buffer_sizes_;
                default_sample_rate_ = d->default_sample_rate_;
            }

            default_buffer_size_ = buffer_sizes_[0];
            for (auto size : buffer_sizes_) {
                if (size > 512)
                    break;
                default_buffer_size_ = size;
            }
        }

        float defaultSampleRate() const override { return default_sample_rate_; }

        int defaultBuffersize() const override { return default_buffer_size_; }

        vector<float> supportedSamplerates() const override { return sample_rates_; }

        vector<int> supportedBuffersizes() const override { return buffer_sizes_; }

        vector<string> inputChannelNames() const override
        {
            vector<string> names;
            if (capture_) {
                capture_->channels(true, &names);
            }
            return names;
        }

        vector<string> outputChannelNames() const override
        {
            vector<string> names;
            if (render_) {
                render_->channels(false, &names);
            }
            return names;
        }

        void open(float fs,
                  int bufferSize,
                  daudio::ActiveChannels inputMask,
                  daudio::ActiveChannels outputMask) override
        {
            if (capture_ && inputMask > 0)
                capture_->open(fs, bufferSize, inputMask);
            if (render_ && outputMask > 0)
                render_->open(fs, bufferSize, outputMask);

            if (capture_) {
                capture_->registerRemovedCallback([this] {
                    close();
                    sessionTerminated(make_exception_ptr(runtime_error("device removed")));
                });

                if (!render_ || outputMask == 0) {
                    capture_->prepare([this, output_view = daudio::OutputBufferView()]() mutable {
                        callCallbacks(capture_->input_view_, output_view);
                    });
                }
            }

            if (render_) {
                render_->registerRemovedCallback([this] {
                    close();
                    sessionTerminated(make_exception_ptr(runtime_error("device removed")));
                });

                render_->prepare([this,
                                  dummy_view     = daudio::InputBufferView(),
                                  useCaptureView = (capture_ && (inputMask > 0))]() mutable {
                    auto& input_view = useCaptureView ? capture_->input_view_ : dummy_view;
                    callCallbacks(input_view, render_->output_view_);
                });
            }

            if (capture_ && inputMask > 0)
                capture_->start();
            if (render_ && outputMask > 0)
                render_->start();

            current_sample_rate_ = fs;
            current_buffer_size_ = bufferSize;
            opened_              = true;
        }

        void close() override
        {
            if (opened_.exchange(false)) {
                if (capture_)
                    capture_->close();
                if (render_)
                    render_->close();

                current_sample_rate_ = 0.f;
                current_buffer_size_ = 0;
            }
        }

        bool isOpened() const override { return !!opened_; }

        float currentSampleRate() const override { return current_sample_rate_; }

        int currentBuffersize() const override { return current_buffer_size_; }

        const daudio::ActiveChannels& activeInputChannels() const override
        {
            static daudio::ActiveChannels none{};
            return capture_ ? capture_->active_channels_ : none;
        }

        const daudio::ActiveChannels& activeOutputChannels() const override
        {
            static daudio::ActiveChannels none{};
            return render_ ? render_->active_channels_ : none;
        }

        bool hasInputGainControl() const override
        {
            return capture_ ? capture_->hasVolumeControl() : false;
        }

        float getInputGain() const override
        {
            if (!capture_)
                throw runtime_error("no capture endpoint");
            return capture_->getVolume();
        }

        void setInputGain(float gain) override
        {
            if (!capture_)
                throw runtime_error("no capture endpoint");
            capture_->setVolume(gain);
        }

        bool getInputMuted() const override
        {
            if (!capture_)
                throw runtime_error("no capture endpoint");
            return capture_->getMuted();
        }

        void setInputMuted(bool mute) override
        {
            if (!capture_)
                throw runtime_error("no capture endpoint");
            capture_->setMuted(mute);
        }

        bool hasOutputVolumeControl() const override
        {
            return render_ ? render_->hasVolumeControl() : false;
        }

        float getOutputVolume() const override
        {
            if (!render_)
                throw runtime_error("no render endpoint");
            return render_->getVolume();
        }

        void setOutputVolume(float volume) override
        {
            if (!render_)
                throw runtime_error("no render endpoint");
            render_->setVolume(volume);
        }

        bool getOutputMuted() const override
        {
            if (!render_)
                throw runtime_error("no render endpoint");
            return render_->getMuted();
        }

        void setOutputMuted(bool mute) override
        {
            if (!render_)
                throw runtime_error("no render endpoint");
            render_->setMuted(mute);
        }
    };

    struct CoreAudioDeviceType : dminternal::DeviceType {
        mutex m_;
        map<pair<AudioDeviceID, AudioDeviceID>, shared_ptr<CoreAudioDevice>> devices_;

        dminternal::Active active;

        struct ScanToken {
            vector<CAAudioDevice> devs;
        };

        function<void()> callback_;

        static OSStatus hardwareListenerProc(AudioDeviceID,
                                             UInt32,
                                             const AudioObjectPropertyAddress*,
                                             void* clientData)
        {
            static_cast<CoreAudioDeviceType*>(clientData)->updateDevices();
            return noErr;
        }

        void updateDevices()
        {
            active.send([this] {
                auto token = scanDevices();
                if (token && updateDevices(move(token)) && callback_) {
                    callback_();
                }
            });
        }

        CoreAudioDeviceType(function<void()> callback) : callback_(callback)
        {
            // Initial update
            updateDevices();
            if (callback_) {
                AudioObjectPropertyAddress pa{
                    .mScope   = kAudioObjectPropertyScopeWildcard,
                    .mElement = kAudioObjectPropertyElementWildcard,
                };
                for (auto selector : {kAudioHardwarePropertyDevices,
                                      kAudioHardwarePropertyDefaultInputDevice,
                                      kAudioHardwarePropertyDefaultOutputDevice}) {
                    pa.mSelector = selector;
                    AudioObjectAddPropertyListener(
                        kAudioObjectSystemObject, &pa, hardwareListenerProc, this);
                }
            }
        }
        ~CoreAudioDeviceType()
        {
            if (callback_) {
                AudioObjectPropertyAddress pa{
                    .mScope   = kAudioObjectPropertyScopeWildcard,
                    .mElement = kAudioObjectPropertyElementWildcard,
                };
                for (auto selector : {kAudioHardwarePropertyDevices,
                                      kAudioHardwarePropertyDefaultInputDevice,
                                      kAudioHardwarePropertyDefaultOutputDevice}) {
                    pa.mSelector = selector;
                    AudioObjectRemovePropertyListener(
                        kAudioObjectSystemObject, &pa, hardwareListenerProc, this);
                }
            }
            devices_.clear();

            std::mutex mtx_done;
            std::condition_variable cv_done;
            bool done = false;
            active.send([&] {
                std::lock_guard<std::mutex> lock(mtx_done);
                done = true;
                cv_done.notify_one();
            });

            std::unique_lock<std::mutex> lock(mtx_done);
            cv_done.wait(lock, [&] { return done; });
        }

        unique_ptr<ScanToken> scanDevices()
        {
            auto token  = make_unique<ScanToken>();
            auto caDevs = getAudioDevices();

            LOG_DEBUG(caDevs.size() << " devices discovered");

            for (auto& d : caDevs) {
                CAAudioDevice entry;
                entry.input_id  = d.channels(true) > 0 ? d.id_ : kAudioDeviceUnknown;
                entry.output_id = d.channels(false) > 0 ? d.id_ : kAudioDeviceUnknown;
                entry.dev       = daudio::AudioDevice(trim_copy(d.name()),
                                                daudio::AudioDeviceType::coreaudio,
                                                d.channels(true),
                                                d.channels(false),
                                                d.isDefault());
                token->devs.push_back(entry);
            }

#if DEVICEMANAGER_ENABLE_COMBO_DEVICES
            // Then create a virtual synchronous device for pure input + pure output devices (that
            // share part of name)
            for (auto& in_dev : caDevs) {
                if (in_dev.channels(false) > 0)
                    continue;

                for (auto& out_dev : caDevs) {
                    if (out_dev.channels(true) > 0)
                        continue;

                    auto in_name     = trim_copy(in_dev.name());
                    auto out_name    = trim_copy(out_dev.name());
                    auto common_name = trim_copy(lcs(in_name, out_name));
                    if (common_name.empty())
                        continue;

                    in_name             = trim_copy(remove(in_name, common_name));
                    out_name            = trim_copy(remove(out_name, common_name));
                    in_name             = in_name.empty() ? "Input" : in_name;
                    out_name            = out_name.empty() ? "Output" : out_name;
                    auto composite_name = in_name + " + " + out_name + " (" + common_name + ")";

                    CAAudioDevice entry;
                    entry.dev       = daudio::AudioDevice(composite_name,
                                                    daudio::AudioDeviceType::coreaudio,
                                                    in_dev.channels(true),
                                                    out_dev.channels(false),
                                                    in_dev.isDefault() && out_dev.isDefault());
                    entry.input_id  = in_dev.id_;
                    entry.output_id = out_dev.id_;
                    token->devs.push_back(entry);
                }
            }
#endif
            return token;
        }

        bool updateDevices(unique_ptr<ScanToken> token)
        {
            LOG_DEBUG("Updating device list");
            bool retval = false;
            lock_guard<mutex> lock{m_};
            set<pair<AudioDeviceID, AudioDeviceID>> to_remove;
            for (auto& entry : devices_) {
                auto it = find_if(token->devs.begin(), token->devs.end(), [&](auto& d) {
                    return d.input_id == entry.first.first && d.output_id == entry.first.second;
                });
                if (it == token->devs.end())
                    to_remove.insert(entry.first);
                else {
                    auto dev = entry.second;
                    // Update existing device if default status has changed
                    if (dev->externalDevice().is_default != (*it).dev.is_default) {
                        dev->setDefault((*it).dev.is_default);
                        retval = true;
                    }
                }
            }
            for (auto& key : to_remove) {
                devices_.erase(key);
                retval = true;
            }

            for (auto& entry : token->devs) {
                auto key = make_pair(entry.input_id, entry.output_id);
                auto it  = devices_.find(key);
                if (it == devices_.end()) {
                    try {
                        LOG_DEBUG("Adding device '" << entry.dev.name << "'");
                        auto new_device = shared_ptr<CoreAudioDevice>(
                            new CoreAudioDevice(entry),
                            [this](CoreAudioDevice* ptr) { active.send([ptr] { delete ptr; }); });
                        assert(none_of(devices_.begin(),
                                       devices_.end(),
                                       [id = new_device->externalDevice().id](auto& it) {
                                           return it.second->externalDevice().id == id;
                                       }));
                        devices_.insert(make_pair(key, move(new_device)));
                        retval = true;
                    } catch (exception& e) {
                        LOG_DEBUG(e.what());
                    }
                }
            }
            return retval;
        }

        vector<daudio::AudioDevice> devices() override
        {
            lock_guard<mutex> lock{m_};
            std::vector<daudio::AudioDevice> devs;
            for (auto it : devices_)
                devs.emplace_back(it.second->externalDevice());
            return devs;
        }

        shared_ptr<dminternal::Device> getDevice(device_id_t id) override
        {
            lock_guard<mutex> lock{m_};
            auto it = find_if(devices_.begin(), devices_.end(), [&](auto& entry) {
                return entry.second->id() == id;
            });
            if (it == devices_.end())
                return nullptr;
            return it->second;
        }
    };
}  // namespace

namespace dminternal
{
    template <>
    unique_ptr<dminternal::DeviceType> DeviceType::create<daudio::AudioDeviceType::coreaudio>(
        function<void()> callback)
    {
        return make_unique<CoreAudioDeviceType>(callback);
    }
}  // namespace dminternal
