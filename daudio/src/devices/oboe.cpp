#include "dminternal/device.h"
#include "dminternal/internal_jni.h"

#include "daudio/waveform.h"

#include <jni.h>

#include <oboe/Oboe.h>

#include <assert.h>
#include <dlfcn.h>

#include <algorithm>
#include <atomic>
#include <cmath>
#include <thread>

#include <log/log_android_backend.h>
#include <log/log.hpp>

template <typename Type>
static std::string getOboeString(const Type& value)
{
    return std::string(oboe::convertToText(value));
}

typedef std::function<void(void)> callback_type;

namespace
{
    std::unique_ptr<dirac::log::Logger> Logger = dirac::log::createAndroidLogHandler();

    struct oboeDeviceInfo {
        daudio::AudioDevice dev;
        std::vector<int> sampleRates;
        oboe::InputPreset inputPreset;
        int input_id;
        int output_id;
    };

    struct deviceInfo {
        std::string name;
        int id;
        int numChannels;
        std::vector<int> sampleRates;
        oboe::InputPreset inputPreset;
    };

    std::vector<deviceInfo> inputDevices;
    std::vector<deviceInfo> outputDevices;
    std::pair<deviceInfo, deviceInfo> defaultInputOutputDevice;

    JavaVM* androidJNIJavaVM = nullptr;

    JNIEnv* getEnv()
    {
        if (androidJNIJavaVM == nullptr) {
            throw std::runtime_error("JavaVM has not been registered.");
        }
        JNIEnv* env;
        androidJNIJavaVM->AttachCurrentThread(&env, nullptr);

        return env;
    }

    template <typename JavaType>
    class LocalRef
    {
    public:
        explicit inline LocalRef() noexcept : obj(nullptr) {}
        explicit inline LocalRef(JavaType o) noexcept : obj(o) {}
        inline LocalRef(const LocalRef& other) noexcept : obj(retain(other.obj)) {}
        inline LocalRef(LocalRef&& other) noexcept : obj(nullptr) { std::swap(obj, other.obj); }
        ~LocalRef() { clear(); }

        void clear()
        {
            if (obj != nullptr) {
                getEnv()->DeleteLocalRef(obj);
                obj = nullptr;
            }
        }

        LocalRef& operator=(const LocalRef& other)
        {
            JavaType newObj = retain(other.obj);
            clear();
            obj = newObj;
            return *this;
        }

        LocalRef& operator=(LocalRef&& other)
        {
            clear();
            std::swap(other.obj, obj);
            return *this;
        }

        inline operator JavaType() const noexcept { return obj; }
        inline JavaType get() const noexcept { return obj; }

    private:
        JavaType obj;

        static JavaType retain(JavaType obj)
        {
            return obj == nullptr ? nullptr : static_cast<JavaType>(getEnv()->NewLocalRef(obj));
        }
    };

    extern "C" jint JNIEXPORT JNI_OnLoad(JavaVM* vm, void*)
    {
        if (androidJNIJavaVM != nullptr) {
            throw std::runtime_error("JNI_OnLoad was called two times.");
        }

        androidJNIJavaVM = vm;
        JNIEnv* env;

        if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
            return JNI_ERR;
        }

        return JNI_VERSION_1_6;
    }

    static jobject getApplicationContext(JNIEnv* env)
    {
        const auto activityThreadCls =
            LocalRef<jclass>(env->FindClass("android/app/ActivityThread"));
        const jmethodID currentActivityThread = env->GetStaticMethodID(
            activityThreadCls, "currentActivityThread", "()Landroid/app/ActivityThread;");
        const auto activityThreadObj = LocalRef<jobject>(
            env->CallStaticObjectMethod(activityThreadCls, currentActivityThread));

        const jmethodID getApplication =
            env->GetMethodID(activityThreadCls, "getApplication", "()Landroid/app/Application;");
        jobject appContext = env->CallObjectMethod(activityThreadObj, getApplication);
        return appContext;
    }

    static std::string deviceTypeToString(int type)
    {
        switch (type) {
        case 0:
            return {};
        case 1:
            return "built-in earphone speaker";
        case 2:
            return "built-in speaker";
        case 3:
            return "wired headset";
        case 4:
            return "wired headphones";
        case 5:
            return "line analog";
        case 6:
            return "line digital";
        case 7:
            return "Bluetooth device typically used for telephony";
        case 8:
            return "Bluetooth device supporting the A2DP profile";
        case 9:
            return "HDMI";
        case 10:
            return "HDMI audio return channel";
        case 11:
            return "USB device";
        case 12:
            return "USB accessory";
        case 13:
            return "DOCK";
        case 14:
            return "FM";
        case 15:
            return "built-in microphone";
        case 16:
            return "FM tuner";
        case 17:
            return "TV tuner";
        case 18:
            return "telephony";
        case 19:
            return "auxiliary line-level connectors";
        case 20:
            return "IP";
        case 21:
            return "BUS";
        case 22:
            return "USB headset";
        case 23:
            return "hearing aid";
        case 24:
            return "built-in speaker safe";
        case 25:
            return "";
        default:
            return "";  // type not supported yet, needs to be added!
        }
    }

    static std::string jstring2string(JNIEnv* env, jstring jStr)
    {
        if (!jStr)
            return "";

        const auto stringClass = LocalRef<jclass>(env->GetObjectClass(jStr));
        const jmethodID getBytes =
            env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
        const auto stringJbytes = LocalRef<jbyteArray>(static_cast<jbyteArray>(
            env->CallObjectMethod(jStr, getBytes, env->NewStringUTF("UTF-8"))));

        const size_t length = static_cast<size_t>(env->GetArrayLength(stringJbytes));
        jbyte* pBytes       = env->GetByteArrayElements(stringJbytes, nullptr);

        const std::string ret = std::string(reinterpret_cast<char*>(pBytes), length);
        env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

        return ret;
    }

    static std::vector<int> jintarray2stdvector(JNIEnv* env, jintArray arr)
    {
        const jsize size = env->GetArrayLength(arr);
        std::vector<int> out(static_cast<unsigned>(size));
        env->GetIntArrayRegion(arr, jsize{0}, size, out.data());
        return out;
    }

    static LocalRef<jobject> getAudioManager()
    {
        const auto env                = getEnv();
        const jclass context          = env->FindClass("android/content/Context");
        const auto applicationContext = LocalRef<jobject>(getApplicationContext(env));
        const jmethodID getSystemServiceID =
            env->GetMethodID(context, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");

        const jfieldID audioServiceField =
            env->GetStaticFieldID(context, "AUDIO_SERVICE", "Ljava/lang/String;");
        const jobject jstr = env->GetStaticObjectField(context, audioServiceField);

        return LocalRef<jobject>(
            env->CallObjectMethod(applicationContext, getSystemServiceID, jstr));
    }

    static std::string getAudioManagerProperty(const std::string& property)
    {
        const auto env               = getEnv();
        const auto audioManagerClass = env->FindClass("android/media/AudioManager");
        const auto audioManager      = getAudioManager();

        if (audioManager != nullptr) {
            std::vector<unsigned short> c(property.length());
            std::copy(property.begin(), property.end(), c.data());
            LocalRef<jstring> jProperty(
                env->NewString(c.data(), static_cast<int>(property.length())));
            static jmethodID getPropertyMethod = env->GetMethodID(
                audioManagerClass, "getProperty", "(Ljava/lang/String;)Ljava/lang/String;");
            if (getPropertyMethod != nullptr) {
                auto retval = LocalRef<jstring>(static_cast<jstring>(
                    env->CallObjectMethod(audioManager.get(), getPropertyMethod, jProperty.get())));
                return jstring2string(env, retval);
            }
        }
        return "-1";
    }

    static int getNativeSampleRate()
    {
        return std::stoi(getAudioManagerProperty("android.media.property.OUTPUT_SAMPLE_RATE"));
    }

    static std::vector<int> getDefaultSampleRates()
    {
        std::vector<int> sampleRates = {
            8000, 11025, 12000, 16000, 22050, 24000, 32000, 44100, 48000};
        const auto nativeSampleRate = getNativeSampleRate();

        const auto it = std::find(sampleRates.begin(), sampleRates.end(), nativeSampleRate);
        if (it == sampleRates.end()) {
            sampleRates.push_back(nativeSampleRate);
        }
        return sampleRates;
    }

    static void addDevice(const jobject device, JNIEnv* env)
    {
        const auto deviceClass =
            LocalRef<jclass>(static_cast<jclass>(env->FindClass("android/media/AudioDeviceInfo")));

        const jmethodID getProductNameMethod =
            env->GetMethodID(deviceClass, "getProductName", "()Ljava/lang/CharSequence;");

        const jmethodID getTypeMethod = env->GetMethodID(deviceClass, "getType", "()I");
        const jmethodID getIdMethod   = env->GetMethodID(deviceClass, "getId", "()I");
        const jmethodID getSampleRatesMethod =
            env->GetMethodID(deviceClass, "getSampleRates", "()[I");
        const jmethodID getChannelCountsMethod =
            env->GetMethodID(deviceClass, "getChannelCounts", "()[I");
        const jmethodID isSourceMethod = env->GetMethodID(deviceClass, "isSource", "()Z");

        const auto deviceTypeString = deviceTypeToString(env->CallIntMethod(device, getTypeMethod));

        if (deviceTypeString.empty())  // unknown device
            return;

        const auto name =
            jstring2string(
                env, static_cast<jstring>(env->CallObjectMethod(device, getProductNameMethod))) +
            " " + deviceTypeString;
        const auto id = env->CallIntMethod(device, getIdMethod);

        const auto jSampleRates = LocalRef<jintArray>(
            static_cast<jintArray>(env->CallObjectMethod(device, getSampleRatesMethod)));

        //  Note: an empty array indicates that the device supports arbitrary rates.
        auto sampleRates = jintarray2stdvector(env, jSampleRates);
        if (sampleRates.empty()) {
            sampleRates = getDefaultSampleRates();
        }

        const auto isInput = env->CallBooleanMethod(device, isSourceMethod);

        const auto jChannelCounts = LocalRef<jintArray>(
            static_cast<jintArray>(env->CallObjectMethod(device, getChannelCountsMethod)));
        const auto channelCounts = jintarray2stdvector(env, jChannelCounts);

        //  Note: an empty array indicates that the device supports arbitrary channel counts.
        int numChannels = channelCounts.empty() ? -1 : channelCounts.back();
        if (numChannels == -1) {
            numChannels = isInput ? 1 : 2;
        } else if (isInput) {
            // Even if the microphone support multiple input channels we choose only use one input.
            // Eg.
            numChannels = 1;
        }

        auto& devices = isInput ? inputDevices : outputDevices;

        // Remove mutiples
        auto it = std::find_if(
            devices.begin(), devices.end(), [&](auto& entry) { return entry.name == name; });
        if (it == devices.end())
            devices.push_back({name, id, numChannels, sampleRates});
    };

    void checkInputOutputDevices()
    {
        inputDevices.clear();
        outputDevices.clear();

        const auto defaultSampleRates = getDefaultSampleRates();

        defaultInputOutputDevice.first  = {"System Default (Input)",
                                          oboe::kUnspecified,
                                          1,
                                          defaultSampleRates,
                                          oboe::InputPreset::Unprocessed};
        defaultInputOutputDevice.second = {"System Default (Output)",
                                           oboe::kUnspecified,
                                           2,
                                           defaultSampleRates,
                                           oboe::InputPreset::Unprocessed};

        const auto env                = getEnv();
        const auto applicationContext = LocalRef<jobject>(getApplicationContext(env));

        const auto audioManagerClass      = env->FindClass("android/media/AudioManager");
        static jmethodID getDevicesMethod = env->GetMethodID(
            audioManagerClass, "getDevices", "(I)[Landroid/media/AudioDeviceInfo;");

        static constexpr int allDevices = 3;
        const auto audioManager         = getAudioManager();
        const auto devices              = LocalRef<jobjectArray>(static_cast<jobjectArray>(
            env->CallObjectMethod(audioManager, getDevicesMethod, allDevices)));

        const int numDevices = env->GetArrayLength(devices);

        for (int i = 0; i < numDevices; ++i) {
            const auto device =
                LocalRef<jobject>(static_cast<jobject>(env->GetObjectArrayElement(devices, i)));
            addDevice(device, env);
        }

        // Add default Camcorder
        inputDevices.push_back(
            {"Camcorder Microphone", 0, 1, getDefaultSampleRates(), oboe::InputPreset::Camcorder});
    }

    static int getNativeBufferSizeHint()
    {
        // This property is a hint of a native buffer size but it does not guarantee the size used.
        const auto deviceBufferSize =
            std::stoi(getAudioManagerProperty("android.media.property.OUTPUT_FRAMES_PER_BUFFER"));

        if (deviceBufferSize == 0)
            return 192;

        return deviceBufferSize;
    }

    template <typename Type>
    inline Type limit(const Type lo, const Type hi, const Type value)
    {
        assert(lo <= hi);
        return (value < lo) ? lo : ((value > hi) ? hi : value);
    }

    //==================================================================================================
    template <class SampleType>
    struct BufferList {
        BufferList(const int numChannels_, const int numBuffers_, const int numSamples_)
            : numChannels(numChannels_)
            , numBuffers(numBuffers_)
            , numSamples(numSamples_)
            , bufferSpace(numChannels_ * numSamples * numBuffers, 0)
            , nextBlock(0)
        {
        }

        bool canGetNextBuffer() const noexcept { return (numBlocksOut < numBuffers); }

        SampleType* getNextBuffer() noexcept
        {
            if (numBlocksOut == numBuffers)
                return nullptr;

            if (++nextBlock == numBuffers)
                nextBlock = 0;

            return &bufferSpace.front() + nextBlock * numChannels * numSamples;
        }

        void bufferReturned() noexcept { --numBlocksOut; }
        void bufferSent() noexcept
        {
            ++numBlocksOut;
            assert(numBlocksOut <= numBuffers);
        }

        int getBufferSizeBytes() const noexcept
        {
            return numChannels * numSamples * sizeof(SampleType);
        }

        const int numChannels, numBuffers, numSamples;

    private:
        std::vector<SampleType> bufferSpace;
        int nextBlock;
        std::atomic<int> numBlocksOut{0};
    };

    template <typename OboeDataFormat>
    struct OboeAudioIODeviceBufferHelpers {
    };

    template <>
    struct OboeAudioIODeviceBufferHelpers<int16_t> {
        static oboe::AudioFormat oboeAudioFormat() { return oboe::AudioFormat::I16; }

        static constexpr int bitDepth() { return 16; }

        static void readBuffer(daudio::Waveforms* destbuffer,
                               const int16_t* srcBuffer,
                               const int numFrames,
                               const int numChannels)
        {
            for (unsigned i = 0; i < static_cast<unsigned>(numChannels); ++i) {
                const auto k     = 1.0f / 32768;
                const int16_t* p = srcBuffer + i;
                float* dst       = destbuffer->waveforms[i].data();
                for (int n = 0; n < numFrames; ++n, p += numChannels)
                    *dst++ = (*p) * k;
            }
        }

        static void writeBuffer(const daudio::Waveforms* outputBuffer,
                                int16_t* destBuffer,
                                const int numFrames,
                                const int numChannels)
        {
            for (unsigned i = 0; i < static_cast<unsigned>(numChannels); ++i) {
                const float* src = outputBuffer->waveforms[i].data();
                int16_t* p       = destBuffer + i;
                for (int n = 0; n < numFrames; ++n, p += numChannels)
                    *p = static_cast<int16_t>(limit(-32768.0f, 32767.0f, 32768.0f * (*src++)));
            }
        }
    };

    template <>
    struct OboeAudioIODeviceBufferHelpers<float> {
        static oboe::AudioFormat oboeAudioFormat() { return oboe::AudioFormat::Float; }

        static constexpr int bitDepth() { return 32; }

        static void readBuffer(daudio::Waveforms* destbuffer,
                               const float* srcBuffer,
                               const int numFrames,
                               const int numChannels)
        {
            for (unsigned i = 0; i < static_cast<unsigned>(numChannels); ++i) {
                const float* p = srcBuffer + i;
                float* dst     = destbuffer->waveforms[i].data();
                for (int n = 0; n < numFrames; ++n, p += numChannels)
                    *dst++ = (*p);
            }
        }

        static void writeBuffer(daudio::Waveforms* outputBuffer,
                                float* destBuffer,
                                const int numFrames,
                                const int numChannels)
        {
            for (unsigned i = 0; i < static_cast<unsigned>(numChannels); ++i) {
                const float* src = outputBuffer->waveforms[i].data();
                float* p         = destBuffer + i;
                for (int n = 0; n < numFrames; ++n, p += numChannels)
                    *p = *src++;
            }
        }
    };

    class OboeStream
    {
    public:
        OboeStream(int deviceId,
                   oboe::Direction direction,
                   oboe::SharingMode sharingMode,
                   int channelCount,
                   oboe::AudioFormat format,
                   int32_t sampleRateIn,
                   int32_t bufferSize,
                   oboe::InputPreset inputPreset,
                   oboe::AudioStreamCallback* callbackIn = nullptr)
        {
            open(deviceId,
                 direction,
                 sharingMode,
                 channelCount,
                 format,
                 sampleRateIn,
                 bufferSize,
                 inputPreset,
                 callbackIn);
        }

        ~OboeStream()
        {
            close();
            delete stream;
        }

        bool openedOk() const noexcept { return openResult == oboe::Result::OK; }

        void start()
        {
            if (openedOk() && stream != nullptr) {
                const auto expectedState   = oboe::StreamState::Starting;
                auto nextState             = oboe::StreamState::Started;
                const int64_t timeoutNanos = 1000 * oboe::kNanosPerMillisecond;

                auto startResult = stream->requestStart();
                LOG_DEBUG("Requesed Oboe stream start with result: " + getOboeString(startResult))

                startResult = stream->waitForStateChange(expectedState, &nextState, timeoutNanos);

                LOG_DEBUG(
                    "Starting Oboe stream with result: " + getOboeString(startResult) +
                    "\nUses AAudio = " + std::to_string(static_cast<int>(stream->usesAAudio())) +
                    "\nDirection = " + getOboeString(stream->getDirection()) +
                    "\nSharingMode = " + getOboeString(stream->getSharingMode()) +
                    "\nChannelCount = " + std::to_string(stream->getChannelCount()) +
                    "\nFormat = " + getOboeString(stream->getFormat()) +
                    "\nSampleRate = " + std::to_string(stream->getSampleRate()) +
                    "\nBufferSizeInFrames = " + std::to_string(stream->getBufferSizeInFrames()) +
                    "\nBufferCapacityInFrames = " +
                    std::to_string(stream->getBufferCapacityInFrames()) +
                    "\nFramesPerBurst = " + std::to_string(stream->getFramesPerBurst()) +
                    "\nFramesPerCallback = " + std::to_string(stream->getFramesPerCallback()) +
                    "\nBytesPerFrame = " + std::to_string(stream->getBytesPerFrame()) +
                    "\nBytesPerSample = " + std::to_string(stream->getBytesPerSample()) +
                    "\nPerformanceMode = " + getOboeString(stream->getPerformanceMode()) +
                    "\ngetDeviceId = " + std::to_string(stream->getDeviceId()))
            }
        }

        oboe::AudioStream* getNativeStream() const { return stream; }

        int getXRunCount() const
        {
            if (stream != nullptr) {
                auto count = stream->getXRunCount();

                if (count)
                    return count.value();

                LOG_ERROR("Failed to get Xrun count: " + getOboeString(count.error()))
            }

            return 0;
        }

    private:
        void open(int deviceId,
                  oboe::Direction direction,
                  oboe::SharingMode sharingMode,
                  int channelCount,
                  oboe::AudioFormat format,
                  int32_t newSampleRate,
                  int32_t newBufferSize,
                  oboe::InputPreset inputPreset,
                  oboe::AudioStreamCallback* newCallback = nullptr)
        {
            oboe::DefaultStreamValues::FramesPerBurst = getNativeBufferSizeHint();

            LOG_DEBUG("FramesPerBusst: " +
                      std::to_string(oboe::DefaultStreamValues::FramesPerBurst));

            oboe::AudioStreamBuilder builder;

            if (deviceId != -1)
                builder.setDeviceId(deviceId);

            // Note: letting OS to choose the buffer capacity & frames per callback.
            builder.setDirection(direction);
            builder.setSharingMode(sharingMode);
            builder.setChannelCount(channelCount);
            builder.setFormat(format);
            builder.setSampleRate(newSampleRate);
            builder.setPerformanceMode(oboe::PerformanceMode::LowLatency);
            builder.setCallback(newCallback);
            builder.setFramesPerDataCallback(newBufferSize);
            if (direction == oboe::Direction::Input)
                builder.setInputPreset(inputPreset);

            LOG_DEBUG(
                "Preparing Oboe stream with params: "
                "\nAAudio supported = " +
                std::to_string(int(builder.isAAudioSupported())) + "\nAPI = " +
                getOboeString(builder.getAudioApi()) + "\nDeviceId = " + std::to_string(deviceId) +
                "\nDirection = " + getOboeString(direction) + "\nSharingMode = " +
                getOboeString(sharingMode) + "\nChannelCount = " + std::to_string(channelCount) +
                "\nFormat = " + getOboeString(format) +
                "\nSampleRate = " + std::to_string(newSampleRate) +
                "\nPerformanceMode = " + getOboeString(oboe::PerformanceMode::LowLatency))

            openResult = builder.openStream(&stream);
            LOG_DEBUG(
                "Building Oboe stream with result: " + getOboeString(openResult) +
                "\nStream state = " + (stream != nullptr ? getOboeString(stream->getState()) : "?"))

            if (stream != nullptr && newBufferSize != 0) {
                LOG_DEBUG("Setting the bufferSizeInFrames to " + std::to_string(newBufferSize))
                stream->setBufferSizeInFrames(newBufferSize);
            }

            LOG_DEBUG(
                "Stream details: \nUses AAudio = " +
                (stream != nullptr ? std::to_string(static_cast<int>(stream->usesAAudio())) : "?") +
                "\nDeviceId = " +
                (stream != nullptr ? std::to_string(stream->getDeviceId()) : "?") +
                "\nDirection = " +
                (stream != nullptr ? getOboeString(stream->getDirection()) : "?") +
                "\nSharingMode = " +
                (stream != nullptr ? getOboeString(stream->getSharingMode()) : "?") +
                "\nChannelCount = " +
                (stream != nullptr ? std::to_string(stream->getChannelCount()) : "?") +
                "\nFormat = " + (stream != nullptr ? getOboeString(stream->getFormat()) : "?") +
                "\nSampleRate = " +
                (stream != nullptr ? std::to_string(stream->getSampleRate()) : "?") +
                "\nBufferSizeInFrames = " +
                (stream != nullptr ? std::to_string(stream->getBufferSizeInFrames()) : "?") +
                "\nBufferCapacityInFrames = " +
                (stream != nullptr ? std::to_string(stream->getBufferCapacityInFrames()) : "?") +
                "\nFramesPerBurst = " +
                (stream != nullptr ? std::to_string(stream->getFramesPerBurst()) : "?") +
                "\nFramesPerCallback = " +
                (stream != nullptr ? std::to_string(stream->getFramesPerCallback()) : "?") +
                "\nBytesPerFrame = " +
                (stream != nullptr ? std::to_string(stream->getBytesPerFrame()) : "?") +
                "\nBytesPerSample = " +
                (stream != nullptr ? std::to_string(stream->getBytesPerSample()) : "?") +
                "\nPerformanceMode = " +
                (stream != nullptr ? getOboeString(stream->getPerformanceMode()) : "?"))
        }

        void close()
        {
            if (stream != nullptr) {
                auto expectedState   = oboe::StreamState::Stopping;
                auto nextState       = oboe::StreamState::Stopped;
                int64_t timeoutNanos = 10000 * oboe::kNanosPerMillisecond;

                auto stopResult = stream->requestStop();

                stopResult = stream->waitForStateChange(expectedState, &nextState, timeoutNanos);

                LOG_DEBUG("Requesed Oboe stream stopped with result: " + getOboeString(stopResult))

                expectedState = oboe::StreamState::Closing;
                nextState     = oboe::StreamState::Closed;

                auto closeResult = stream->close();

                stopResult = stream->waitForStateChange(expectedState, &nextState, timeoutNanos);

                LOG_DEBUG("Requesed Oboe stream closed with result: " + getOboeString(closeResult))
            }
        }

        oboe::AudioStream* stream = nullptr;
        oboe::Result openResult;
    };

    //==============================================================================
    class OboeSessionBase : protected oboe::AudioStreamCallback
    {
    public:
        static OboeSessionBase* create(daudio::Waveforms* _input_buffer,
                                       daudio::Waveforms* _output_buffer,
                                       int inputDeviceId,
                                       int outputDeviceId,
                                       int numInputChannels,
                                       int numOutputChannels,
                                       int sampleRate,
                                       oboe::InputPreset inputPresetToUse,
                                       int bufferSize);

        virtual void start()                              = 0;
        virtual void stop()                               = 0;
        virtual void setProcessCallback(callback_type fn) = 0;

        bool openedOk() const noexcept
        {
            return (outputStream != nullptr && outputStream->openedOk()) ||
                   (inputStream != nullptr && inputStream->openedOk());
        }

    protected:
        OboeSessionBase(int inputDeviceIdToUse,
                        int outputDeviceIdToUse,
                        int numInputChannelsToUse,
                        int numOutputChannelsToUse,
                        int sampleRateToUse,
                        int bufferSizeToUse,
                        oboe::AudioFormat streamFormatToUse,
                        oboe::InputPreset inputPresetToUse,
                        int bitDepthToUse)
            : inputDeviceId(inputDeviceIdToUse)
            , outputDeviceId(outputDeviceIdToUse)
            , numInputChannels(numInputChannelsToUse)
            , numOutputChannels(numOutputChannelsToUse)
            , sampleRate(sampleRateToUse)
            , bufferSize(bufferSizeToUse)
            , streamFormat(streamFormatToUse)
            , inputPreset(inputPresetToUse)
            , bitDepth(bitDepthToUse)
        {
            outputStream.reset(new OboeStream(outputDeviceId,
                                              oboe::Direction::Output,
                                              oboe::SharingMode::Exclusive,
                                              numOutputChannels,
                                              streamFormatToUse,
                                              sampleRateToUse,
                                              bufferSizeToUse,
                                              inputPreset,
                                              this));
            if (numInputChannels > 0) {
                inputStream.reset(new OboeStream(inputDeviceId,
                                                 oboe::Direction::Input,
                                                 oboe::SharingMode::Exclusive,
                                                 numInputChannels,
                                                 streamFormatToUse,
                                                 sampleRateToUse,
                                                 bufferSizeToUse,
                                                 inputPreset,
                                                 nullptr));

                if (inputStream->openedOk() && outputStream->openedOk()) {
                    // Input & output sample rates should match!
                    assert(inputStream->getNativeStream()->getSampleRate() ==
                           outputStream->getNativeStream()->getSampleRate());
                }

                checkStreamSetup(inputStream.get(), numInputChannels, sampleRate, streamFormat);
            }

            checkStreamSetup(outputStream.get(), numOutputChannels, sampleRate, streamFormat);
        }

        // Not strictly required as these should not change, but recommended by Google anyway
        void checkStreamSetup(OboeStream* stream,
                              int numChannels,
                              int expectedSampleRate,
                              oboe::AudioFormat format)
        {
            if (auto* nativeStream = stream != nullptr ? stream->getNativeStream() : nullptr) {
                assert(numChannels == 0 || numChannels == nativeStream->getChannelCount());
                assert(expectedSampleRate == 0 ||
                       expectedSampleRate == nativeStream->getSampleRate());
                assert(format == nativeStream->getFormat());
            }
        }

        const int inputDeviceId, outputDeviceId;
        const int numInputChannels, numOutputChannels;
        const int sampleRate;
        const int bufferSize;
        const oboe::AudioFormat streamFormat;
        const oboe::InputPreset inputPreset;
        const int bitDepth;

        std::unique_ptr<OboeStream> inputStream, outputStream;
    };

    //==============================================================================
    template <typename SampleType>
    class OboeSessionImpl : public OboeSessionBase
    {
    public:
        OboeSessionImpl(daudio::Waveforms* _input_buffer,
                        daudio::Waveforms* _output_buffer,
                        int inputDeviceIdIn,
                        int outputDeviceIdIn,
                        int numInputChannelsToUse,
                        int numOutputChannelsToUse,
                        int sampleRateToUse,
                        oboe::InputPreset inputPresetToUse,
                        int bufferSizeToUse)
            : OboeSessionBase(inputDeviceIdIn,
                              outputDeviceIdIn,
                              numInputChannelsToUse,
                              numOutputChannelsToUse,
                              sampleRateToUse,
                              bufferSizeToUse,
                              OboeAudioIODeviceBufferHelpers<SampleType>::oboeAudioFormat(),
                              inputPresetToUse,
                              OboeAudioIODeviceBufferHelpers<SampleType>::bitDepth())
            , inputBuffer(_input_buffer)
            , outputBuffer(_output_buffer)
            , tempBuffer(std::vector<SampleType>(
                  static_cast<unsigned>(bufferSizeToUse * numInputChannelsToUse)))
        {
        }

        void setProcessCallback(callback_type fn) override
        {
            std::lock_guard<std::mutex> lock{cbMutex};
            cbFunction = fn;
        }

        void callback()
        {
            std::unique_lock<std::mutex> lock{cbMutex, std::try_to_lock};
            if (lock.owns_lock() && cbFunction)
                cbFunction();
        }

        void start() override
        {
            const std::lock_guard<std::mutex> lock(audioCallbackGuard);

            if (inputStream != nullptr)
                inputStream->start();

            outputStream->start();
        }

        void stop() override
        {
            const std::lock_guard<std::mutex> lock(audioCallbackGuard);

            inputStream  = nullptr;
            outputStream = nullptr;
        }

    private:
        oboe::DataCallbackResult onAudioReady(oboe::AudioStream* stream,
                                              void* audioData,
                                              int32_t numFrames) override
        {
            // daudio callbacks
            callback();

            if (stream == nullptr)
                return oboe::DataCallbackResult::Stop;

            if (outputStream != nullptr) {
                OboeAudioIODeviceBufferHelpers<SampleType>::writeBuffer(
                    outputBuffer,
                    static_cast<SampleType*>(audioData),
                    numFrames,
                    numOutputChannels);
            } else {
                OboeAudioIODeviceBufferHelpers<SampleType>::readBuffer(
                    inputBuffer, static_cast<SampleType*>(audioData), numFrames, numInputChannels);
            }

            if (outputStream != nullptr && inputStream != nullptr) {
                auto* nativeInputStream = inputStream->getNativeStream();

                if (!nativeInputStream) {
                    LOG_ERROR("Input stream is NULL")
                    return oboe::DataCallbackResult::Continue;
                } else if (nativeInputStream->getFormat() != oboe::AudioFormat::I16 &&
                           nativeInputStream->getFormat() != oboe::AudioFormat::Float) {
                    LOG_ERROR("Unsupported input stream audio format: " +
                              getOboeString(nativeInputStream->getFormat()))
                    return oboe::DataCallbackResult::Continue;
                }

                auto result =
                    inputStream->getNativeStream()->read(tempBuffer.data(), numFrames, 2000);

                if (result.value() != bufferSize) {
                    LOG_WARN("inputStream supplied " + std::to_string(result.value()) +
                             "samples. " + std::to_string(bufferSize) +
                             "samples requested. Buffer is dumped.")
                } else if (result) {
                    OboeAudioIODeviceBufferHelpers<SampleType>::readBuffer(
                        inputBuffer, tempBuffer.data(), numFrames, numInputChannels);
                } else {
                    LOG_ERROR("Failed to read from input stream: " + getOboeString(result.error()))
                    return oboe::DataCallbackResult::Stop;
                }
            }
            return oboe::DataCallbackResult::Continue;
        }

        void printStreamDebugInfo(oboe::AudioStream* stream)
        {
            if (stream == nullptr)
                return;

            LOG_DEBUG(
                "\nUses AAudio = " +
                (stream != nullptr ? std::to_string(static_cast<int>(stream->usesAAudio())) : "?") +
                "\nDirection = " +
                (stream != nullptr ? getOboeString(stream->getDirection()) : "?") +
                "\nSharingMode = " +
                (stream != nullptr ? getOboeString(stream->getSharingMode()) : "?") +
                "\nChannelCount = " +
                (stream != nullptr ? std::to_string(stream->getChannelCount()) : "?") +
                "\nFormat = " + (stream != nullptr ? getOboeString(stream->getFormat()) : "?") +
                "\nSampleRate = " +
                (stream != nullptr ? std::to_string(stream->getSampleRate()) : "?") +
                "\nBufferSizeInFrames = " +
                (stream != nullptr ? std::to_string(stream->getBufferSizeInFrames()) : "?") +
                "\nBufferCapacityInFrames = " +
                (stream != nullptr ? std::to_string(stream->getBufferCapacityInFrames()) : "?") +
                "\nFramesPerBurst = " +
                (stream != nullptr ? std::to_string(stream->getFramesPerBurst()) : "?") +
                "\nFramesPerCallback = " +
                (stream != nullptr ? std::to_string(stream->getFramesPerCallback()) : "?") +
                "\nBytesPerFrame = " +
                (stream != nullptr ? std::to_string(stream->getBytesPerFrame()) : "?") +
                "\nBytesPerSample = " +
                (stream != nullptr ? std::to_string(stream->getBytesPerSample()) : "?") +
                "\nPerformanceMode = " +
                (stream != nullptr ? getOboeString(stream->getPerformanceMode()) : "?") +
                "\ngetDeviceId = " +
                (stream != nullptr ? std::to_string(stream->getDeviceId()) : "?"))
        }

        void onErrorBeforeClose(oboe::AudioStream* stream, oboe::Result error) override
        {
            LOG_ERROR("Oboe stream onErrorBeforeClose(): " + getOboeString(error))
            printStreamDebugInfo(stream);
        }

        void onErrorAfterClose(oboe::AudioStream* stream, oboe::Result error) override
        {
            LOG_ERROR("Oboe stream onErrorAfterClose(): " + getOboeString(error))

            if (error == oboe::Result::ErrorDisconnected) {
                const std::lock_guard<std::mutex> lock_restart(audioCallbackGuard);

                // Close, recreate, and start the stream, not much use in current one.
                // Use default device id, to let the OS pick the best ID (since our was
                // disconnected).

                outputStream = nullptr;
                outputStream.reset(new OboeStream(oboe::kUnspecified,
                                                  oboe::Direction::Output,
                                                  oboe::SharingMode::Exclusive,
                                                  numOutputChannels,
                                                  streamFormat,
                                                  sampleRate,
                                                  bufferSize,
                                                  inputPreset,
                                                  this));

                outputStream->start();
            }
        }

        std::mutex audioCallbackGuard, cbMutex;
        callback_type cbFunction;

        daudio::Waveforms* inputBuffer;
        daudio::Waveforms* outputBuffer;
        std::vector<SampleType> tempBuffer;
    };

    template <typename Container, typename ValueType>
    inline bool contains(const Container& c, ValueType value)
    {
        return any_of(c.begin(), c.end(), [&](const auto& e) { return e == value; });
    }

    int getAndroidSDKVersion()
    {
        auto* env = getEnv();

        auto buildVersion = env->FindClass("android/os/Build$VERSION");
        assert(buildVersion != nullptr);

        auto sdkVersionField = env->GetStaticFieldID(buildVersion, "SDK_INT", "I");
        assert(sdkVersionField != nullptr);

        return env->GetStaticIntField(buildVersion, sdkVersionField);
    }

    std::unique_ptr<OboeSessionBase> session{nullptr};

    OboeSessionBase* OboeSessionBase::create(daudio::Waveforms* _input_buffer,
                                             daudio::Waveforms* _output_buffer,
                                             int inputDeviceId,
                                             int outputDeviceId,
                                             int numInputChannels,
                                             int numOutputChannels,
                                             int sampleRate,
                                             oboe::InputPreset inputPreset,
                                             int bufferSize)
    {
        std::unique_ptr<OboeSessionBase> session;

        auto sdkVersion = getAndroidSDKVersion();
        // SDK versions 21 and higher should natively support floating point...
        if (sdkVersion >= 21) {
            session.reset(new OboeSessionImpl<float>(_input_buffer,
                                                     _output_buffer,
                                                     inputDeviceId,
                                                     outputDeviceId,
                                                     numInputChannels,
                                                     numOutputChannels,
                                                     sampleRate,
                                                     inputPreset,
                                                     bufferSize));

            // ...however, some devices lie so re-try without floating point
            if (session != nullptr && (!session->openedOk()))
                session.reset();
        }

        if (session == nullptr) {
            session.reset(new OboeSessionImpl<int16_t>(_input_buffer,
                                                       _output_buffer,
                                                       inputDeviceId,
                                                       outputDeviceId,
                                                       numInputChannels,
                                                       numOutputChannels,
                                                       sampleRate,
                                                       inputPreset,
                                                       bufferSize));

            if (session != nullptr && (!session->openedOk()))
                session.reset();
        }

        return session.release();
    }

    static unsigned getNativeBufferSize(const oboeDeviceInfo& info)
    {
        struct DummyCallback : public oboe::AudioStreamCallback {
            oboe::DataCallbackResult onAudioReady(oboe::AudioStream*, void*, int32_t) override
            {
                return oboe::DataCallbackResult::Stop;
            }
        };

        DummyCallback cb;

        OboeStream tempStream(
            info.output_id,
            oboe::Direction::Output,
            oboe::SharingMode::Exclusive,
            2,
            getAndroidSDKVersion() >= 21 ? oboe::AudioFormat::Float : oboe::AudioFormat::I16,
            getNativeSampleRate(),
            getNativeBufferSizeHint(),
            oboe::InputPreset::Unprocessed,
            &cb);

        if (auto* nativeStream = tempStream.getNativeStream()) {
            return static_cast<unsigned>(nativeStream->getFramesPerBurst());
        }
        return static_cast<unsigned>(getNativeBufferSizeHint());
    }

    static unsigned getMaximumBuffersToEnqueue(const unsigned nativeBufferSize,
                                               const double maximumSampleRate)
    {
        static constexpr int maxBufferSizeMs = 25;

        const auto maxBufferFrames =
            static_cast<unsigned>(std::ceil(maxBufferSizeMs * maximumSampleRate / 1000.0));
        const auto maxNumBuffers = static_cast<unsigned>(
            std::ceil(static_cast<float>(maxBufferFrames) / static_cast<float>(nativeBufferSize)));

        return maxNumBuffers;
    }

    static std::vector<int> getAvaibleBufferSizes(const oboeDeviceInfo& info)
    {
        const auto maxNumBuffers = getMaximumBuffersToEnqueue(
            getNativeBufferSize(info),
            *std::max_element(info.sampleRates.begin(), info.sampleRates.end()));

        const unsigned nativeBufferSize = getNativeBufferSize(info);

        std::vector<int> bufferSizes;

        for (unsigned i = 1; i <= maxNumBuffers; ++i) {
            bufferSizes.push_back(i * nativeBufferSize);
        }

        return bufferSizes;
    }

    struct OboeAudioDevice : dminternal::Device {
        const std::string deviceName;

        const int inputDevId, outputDevId;

        const oboe::InputPreset inputPreset = oboe::InputPreset::Unprocessed;

        std::atomic<bool> opened;

        daudio::Waveforms inBuffers;
        daudio::Waveforms outBuffers;
        daudio::InputBufferView inputBufferView;
        daudio::OutputBufferView outputBufferView;

        daudio::ActiveChannels activeInChans;
        daudio::ActiveChannels activeOutChans;

        int currentBufSize{0};
        float currentRate{0.f};

        std::vector<float> supportedSampleRates_;
        std::vector<int> supportedBufferSizes_;

        int numInputs  = 0;
        int numOutputs = 0;

        OboeAudioDevice(oboeDeviceInfo info)
            : Device{info.dev}
            , inputDevId(info.input_id)
            , outputDevId(info.output_id)
            , inputPreset(info.inputPreset)
        {
            for (const auto& fs : info.sampleRates) {
                supportedSampleRates_.push_back(static_cast<float>(fs));
            }

            supportedBufferSizes_ = getAvaibleBufferSizes(info);

            numInputs  = info.dev.inputchannels;
            numOutputs = info.dev.outputchannels;
        }

        ~OboeAudioDevice() override { close(); }

        virtual std::vector<std::string> inputChannelNames() const override
        {
            std::vector<std::string> inputChannelNames;

            for (int i = 0; i < numInputs; ++i) {
                inputChannelNames.push_back(std::string("Input: " + std::to_string(i)));
            }

            return inputChannelNames;
        }
        virtual std::vector<std::string> outputChannelNames() const override
        {
            std::vector<std::string> outputChannelNames;

            for (int i = 0; i < numOutputs; ++i) {
                outputChannelNames.push_back(std::string("Output: " + std::to_string(i)));
            }

            return outputChannelNames;
        }
        virtual std::vector<int> supportedBuffersizes() const override
        {
            return supportedBufferSizes_;
        }
        virtual int defaultBuffersize() const override { return supportedBufferSizes_[0]; }
        virtual std::vector<float> supportedSamplerates() const override
        {
            return supportedSampleRates_;
        }
        virtual float defaultSampleRate() const override
        {
            float retval = supportedSampleRates_.front();
            for (const auto fs : supportedSampleRates_) {
                if (fs > 48000.0f)
                    break;
                retval = fs;
            }
            return retval;
        }

        virtual void open(float fs,
                          int bufferSize,
                          daudio::ActiveChannels inputMask,
                          daudio::ActiveChannels outputMask) override
        {
            if (!contains(supportedSamplerates(), fs))
                throw std::runtime_error("sample rate not supported");

            if (!contains(supportedBuffersizes(), bufferSize))
                throw std::runtime_error("buffer size not supported");

            auto numInChans  = count(inputMask.channels.begin(), inputMask.channels.end(), true);
            auto numOutChans = count(outputMask.channels.begin(), outputMask.channels.end(), true);

            inBuffers.resize(static_cast<int>(numInChans), bufferSize);
            inBuffers.fs = fs;
            outBuffers.resize(static_cast<int>(numOutChans), bufferSize);
            outBuffers.fs = fs;

            for (auto& channel : inBuffers.waveforms)
                inputBufferView.channel.emplace_back(channel.data(),
                                                     channel.data() + channel.size());
            for (auto& channel : outBuffers.waveforms)
                outputBufferView.channel.emplace_back(channel.data(),
                                                      channel.data() + channel.size());

            session.reset(OboeSessionBase::create(&inBuffers,
                                                  &outBuffers,
                                                  inputDevId,
                                                  outputDevId,
                                                  static_cast<int>(numInChans),
                                                  static_cast<int>(numOutChans),
                                                  static_cast<int>(fs),
                                                  inputPreset,
                                                  bufferSize));

            auto processBuffers = [&] { callCallbacks(inputBufferView, outputBufferView); };
            if (session)
                session->setProcessCallback(processBuffers);

            processBuffers();

            if (session) {
                session->start();
            }

            activeInChans  = inputMask;
            activeOutChans = outputMask;
            currentRate    = fs;
            currentBufSize = bufferSize;
            opened         = true;
        }

        virtual void close() override
        {

            if (opened) {

                if (session) {
                    session->stop();
                    session->setProcessCallback(callback_type{});
                    session.reset(nullptr);
                }

                opened         = false;
                currentRate    = 0.f;
                currentBufSize = 0;
                inBuffers.waveforms.clear();
                outBuffers.waveforms.clear();
                inputBufferView.channel.clear();
                outputBufferView.channel.clear();
                activeInChans  = daudio::ActiveChannels{};
                activeOutChans = daudio::ActiveChannels{};
            }
        }

        virtual bool isOpened() const override { return opened; }
        virtual int currentBuffersize() const override { return currentBufSize; }
        virtual float currentSampleRate() const override { return currentRate; }
        virtual const daudio::ActiveChannels& activeInputChannels() const override
        {
            return activeInChans;
        }
        virtual const daudio::ActiveChannels& activeOutputChannels() const override
        {
            return activeOutChans;
        }
    };

    struct OboeAudioDeviceType : dminternal::DeviceType {
        std::mutex mutable dev_mutex_;

        std::vector<std::shared_ptr<OboeAudioDevice>> devices_;

        OboeAudioDeviceType()
        {
            std::lock_guard<std::mutex> lock{dev_mutex_};
            checkInputOutputDevices();

            const oboeDeviceInfo defaultDevice = {{"System Default",
                                                   daudio::AudioDeviceType::oboe,
                                                   defaultInputOutputDevice.first.numChannels,
                                                   defaultInputOutputDevice.second.numChannels,
                                                   true},
                                                  defaultInputOutputDevice.first.sampleRates,
                                                  defaultInputOutputDevice.first.inputPreset,
                                                  defaultInputOutputDevice.first.id,
                                                  defaultInputOutputDevice.second.id};

            devices_.push_back(std::make_shared<OboeAudioDevice>(defaultDevice));

            for (const auto& device : inputDevices) {
                oboeDeviceInfo dev;
                auto it = std::find_if(outputDevices.begin(),
                                       outputDevices.end(),
                                       [&](auto& entry) { return entry.name == device.name; });
                if (it == outputDevices.end()) {
                    dev = {
                        {device.name, daudio::AudioDeviceType::oboe, device.numChannels, 0, false},
                        device.sampleRates,
                        device.inputPreset,
                        device.id,
                        -1};
                } else {
                    dev = {{device.name,
                            daudio::AudioDeviceType::oboe,
                            device.numChannels,
                            it->numChannels,
                            false},
                           device.sampleRates,
                           dev.inputPreset,
                           device.id,
                           it->id};
                }

                devices_.push_back(std::make_shared<OboeAudioDevice>(dev));
            }

            for (const auto& device : outputDevices) {
                auto it = std::find_if(inputDevices.begin(), inputDevices.end(), [&](auto& entry) {
                    return entry.name == device.name;
                });
                if (it == inputDevices.end()) {
                    const oboeDeviceInfo dev = {
                        {device.name, daudio::AudioDeviceType::oboe, 0, device.numChannels, false},
                        device.sampleRates,
                        device.inputPreset,
                        -1,
                        device.id};
                    devices_.push_back(std::make_shared<OboeAudioDevice>(dev));
                }
            }
        }

        virtual std::vector<daudio::AudioDevice> devices() override
        {
            std::lock_guard<std::mutex> lock{dev_mutex_};
            std::vector<daudio::AudioDevice> retval;
            for (const auto& dev : devices_) {
                retval.push_back(dev->externalDevice());
            }
            return retval;
        }

        virtual std::shared_ptr<dminternal::Device> getDevice(device_id_t id) override
        {
            std::lock_guard<std::mutex> lock{dev_mutex_};
            auto it = std::find_if(
                devices_.begin(), devices_.end(), [&](auto& entry) { return entry->id() == id; });
            if (it == devices_.end())
                return nullptr;
            return *it;
        }
    };
}  // namespace

namespace dminternal
{
    void register_jni_vm(JavaVM* vm)
    {
        if (androidJNIJavaVM != nullptr) {
            throw std::runtime_error(
                "JNI_OnLoad has already been called or register_jni_vm is called twice.");
        }
        androidJNIJavaVM = vm;
    }

    template <>
    std::unique_ptr<dminternal::DeviceType> DeviceType::create<daudio::AudioDeviceType::oboe>(
        std::function<void()>)
    {
        return std::make_unique<OboeAudioDeviceType>();
    }
}  // namespace dminternal
