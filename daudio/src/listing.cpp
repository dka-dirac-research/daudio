#include <algorithm>
#include <iterator>

#include "daudio/listing.h"
#include "dminternal/device.h"
#include "dminternal/dmholder.h"

std::vector<daudio::AudioDevice> daudio::allDevices() { return dminternal::allDevices(); }

std::vector<daudio::AudioDevice> daudio::inputDevices()
{
    auto devs = dminternal::allDevices();
    devs.erase(
        remove_if(devs.begin(), devs.end(), [](const auto& d) { return d.outputchannels > 0; }),
        devs.end());

    return devs;
}

std::vector<daudio::AudioDevice> daudio::outputDevices()
{
    auto devs = dminternal::allDevices();
    devs.erase(
        remove_if(devs.begin(), devs.end(), [](const auto& d) { return d.inputchannels > 0; }),
        devs.end());

    return devs;
}

std::vector<daudio::AudioDevice> daudio::synchronousDevices()
{
    auto devs = dminternal::allDevices();
    devs.erase(
        remove_if(devs.begin(),
                  devs.end(),
                  [](const auto& d) { return d.outputchannels <= 0 || d.inputchannels <= 0; }),
        devs.end());

    return devs;
}

std::unique_ptr<daudio::CallbackToken> daudio::registerDevicesChangedCallback(
    std::function<void()> callback)
{
    return dminternal::registerDevicesChangedCallback(callback);
}

void daudio::setTypeToList(daudio::AudioDeviceType type_to_list)
{
    dminternal::setTypeToList(type_to_list);
}

void daudio::setListAllTypes() { dminternal::setListAllTypes(); }

void daudio::setLoopBackEnabled(bool should_be_enabled)
{
    dminternal::setLoopBackEnabled(should_be_enabled);
}

void daudio::release() { dminternal::release(); }
