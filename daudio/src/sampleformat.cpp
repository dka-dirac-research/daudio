#include <dminternal/sampleformat.h>
#include <string.h>

#include <algorithm>
#include <cmath>
#include <stdexcept>

template <typename Type>
Type limit(const Type lo, const Type hi, const Type value)
{
    assert(lo <= hi);
    return (value < lo) ? lo : ((value > hi) ? hi : value);
}

dminternal::SampleFormat::SampleFormat(Type sample_type)
    : bitDepth(24), byteStride(4), formatIsFloat(false), littleEndian(true)
{
    switch (sample_type) {
    case Type::kInt16MSB:
        byteStride   = 2;
        littleEndian = false;
        bitDepth     = 16;
        break;
    case Type::kInt24MSB:
        byteStride   = 3;
        littleEndian = false;
        break;
    case Type::kInt32MSB:
        bitDepth     = 32;
        littleEndian = false;
        break;
    case Type::kFloat32MSB:
        bitDepth      = 32;
        littleEndian  = false;
        formatIsFloat = true;
        break;
    case Type::kFloat64MSB:
        bitDepth     = 64;
        byteStride   = 8;
        littleEndian = false;
        break;
    case Type::kInt32MSB16:
        bitDepth     = 16;
        littleEndian = false;
        break;
    case Type::kInt32MSB18:
        littleEndian = false;
        break;
    case Type::kInt32MSB20:
        littleEndian = false;
        break;
    case Type::kInt32MSB24:
        littleEndian = false;
        break;
    case Type::kInt16LSB:
        byteStride = 2;
        bitDepth   = 16;
        break;
    case Type::kInt24LSB:
        byteStride = 3;
        break;
    case Type::kInt20LSB:  // WASAPI special
        bitDepth   = 20;
        byteStride = 3;
        break;
    case Type::kInt32LSB:
        bitDepth = 32;
        break;
    case Type::kFloat32LSB:
        bitDepth      = 32;
        formatIsFloat = true;
        break;
    case Type::kFloat64LSB:
        bitDepth   = 64;
        byteStride = 8;
        break;

    case Type::kInt32LSB16:
        bitDepth = 16;
        break;
    case Type::kInt32LSB18:
        bitDepth = 18;
        break;
    case Type::kInt32LSB20:
        bitDepth = 20;
        break;
    case Type::kInt32LSB24:
        break;

    case Type::kDSDInt8LSB1:
    case Type::kDSDInt8MSB1:
    case Type::kDSDInt8NER8:
        throw std::runtime_error("unhandled format");

    default:
        throw std::runtime_error("invalid format");
    }
}

void dminternal::SampleFormat::convertToFloat(const void* const src,
                                              float* const dst,
                                              const int samps) const noexcept
{
    if (formatIsFloat) {
        memcpy(dst, src, samps * sizeof(float));
    } else {
        switch (bitDepth) {
        case 16:
            convertInt16ToFloat(
                static_cast<const char*>(src), dst, byteStride, samps, littleEndian);
            break;
        case 24:
            convertInt24ToFloat(
                static_cast<const char*>(src), dst, byteStride, samps, littleEndian);
            break;
        case 32:
            convertInt32ToFloat(
                static_cast<const char*>(src), dst, byteStride, samps, littleEndian);
            break;
        default:
            assert(false);
            break;
        }
    }
}

void dminternal::SampleFormat::convertToFloat(const void* const src,
                                              float** const dsts,
                                              const int channels,
                                              const int samps) const noexcept
{
    for (int i = 0; i < channels; ++i) {
        float* dst = dsts[i];
        if (formatIsFloat) {
            const float* src_float = reinterpret_cast<const float*>(src) + i;
            for (int n = 0; n < samps; ++n, src_float += channels)
                dst[n] = *src_float;
        } else {
            int realByteStride = byteStride * channels;
            int offset         = byteStride * i;
            auto real_src      = static_cast<const char*>(src) + offset;
            switch (bitDepth) {
            case 16:
                convertInt16ToFloat(real_src, dst, realByteStride, samps, littleEndian);
                break;
            case 24:
                convertInt24ToFloat(real_src, dst, realByteStride, samps, littleEndian);
                break;
            case 32:
                convertInt32ToFloat(real_src, dst, realByteStride, samps, littleEndian);
                break;
            default:
                assert(false);
                break;
            }
        }
    }
}

void dminternal::SampleFormat::convertToFloatNonInterleaved(const void** const src,
                                                            float** const dst,
                                                            const int channels,
                                                            const int samps) const noexcept
{
    for (int i = 0; i < channels; ++i) {
        convertToFloat(src[i], dst[i], samps);
    }
}

void dminternal::SampleFormat::convertFromFloat(const float* const src,
                                                void* const dst,
                                                const int samps) const noexcept
{
    if (formatIsFloat) {
        memcpy(dst, src, samps * sizeof(float));
    } else {
        switch (bitDepth) {
        case 16:
            convertFloatToInt16(src, static_cast<char*>(dst), byteStride, samps, littleEndian);
            break;
        case 24:
            convertFloatToInt24(src, static_cast<char*>(dst), byteStride, samps, littleEndian);
            break;
        case 32:
            convertFloatToInt32(src, static_cast<char*>(dst), byteStride, samps, littleEndian);
            break;
        default:
            assert(false);
            break;
        }
    }
}

void dminternal::SampleFormat::convertFromFloat(const float** const srcs,
                                                void* const dst,
                                                const int channels,
                                                const int samps) const noexcept
{
    for (int i = 0; i < channels; ++i) {
        const float* src = srcs[i];
        if (formatIsFloat) {
            float* dst_float = reinterpret_cast<float*>(dst) + i;
            for (int n = 0; n < samps; ++n, dst_float += channels)
                *dst_float = src[n];
        } else {
            int realByteStride = byteStride * channels;
            int offset         = byteStride * i;
            auto real_dst      = static_cast<char*>(dst) + offset;
            switch (bitDepth) {
            case 16:
                convertFloatToInt16(src, real_dst, realByteStride, samps, littleEndian);
                break;
            case 24:
                convertFloatToInt24(src, real_dst, realByteStride, samps, littleEndian);
                break;
            case 32:
                convertFloatToInt32(src, real_dst, realByteStride, samps, littleEndian);
                break;
            default:
                assert(false);
                break;
            }
        }
    }
}

void dminternal::SampleFormat::convertFromFloatNonInterleaved(const float** const src,
                                                              void** const dst,
                                                              const int channels,
                                                              const int samps) const noexcept
{
    for (int i = 0; i < channels; ++i) {
        convertFromFloat(src[i], dst[i], samps);
    }
}

void dminternal::SampleFormat::clear(void* dst, const int numSamps) noexcept
{
    if (dst != nullptr)
        memset(dst, 0, numSamps * byteStride);
}

void dminternal::SampleFormat::convertInt16ToFloat(const char* __restrict src,
                                                   float* __restrict dest,
                                                   const int srcStrideBytes,
                                                   int numSamples,
                                                   const bool littleEndian) noexcept
{
    const double g = 1.0 / 32768.0;

    if (littleEndian) {
        while (--numSamples >= 0) {
            *dest++ = (float)(g * (*(short*)src));
            src += srcStrideBytes;
        }
    } else {
        while (--numSamples >= 0) {
            *dest++ = (float)(g * (short)swap((*(uint16_t*)src)));
            src += srcStrideBytes;
        }
    }
}

void dminternal::SampleFormat::convertFloatToInt16(const float* __restrict src,
                                                   char* __restrict dest,
                                                   const int dstStrideBytes,
                                                   int numSamples,
                                                   const bool littleEndian) noexcept
{
    const double maxVal = (double)0x7fff;

    if (littleEndian) {
        while (--numSamples >= 0) {
            *(uint16_t*)dest = (short)limit(-maxVal, maxVal, maxVal * *src++);
            dest += dstStrideBytes;
        }
    } else {
        while (--numSamples >= 0) {
            *(uint16_t*)dest = swap((uint16_t)(short)limit(-maxVal, maxVal, maxVal * *src++));
            dest += dstStrideBytes;
        }
    }
}

void dminternal::SampleFormat::convertInt24ToFloat(const char* __restrict src,
                                                   float* __restrict dest,
                                                   const int srcStrideBytes,
                                                   int numSamples,
                                                   const bool littleEndian) noexcept
{
    const double g = 1.0 / 0x7fffff;

    if (littleEndian) {
        while (--numSamples >= 0) {
            *dest++ = (float)(g * littleEndian24Bit(src));
            src += srcStrideBytes;
        }
    } else {
        while (--numSamples >= 0) {
            *dest++ = (float)(g * bigEndian24Bit(src));
            src += srcStrideBytes;
        }
    }
}

void dminternal::SampleFormat::convertFloatToInt24(const float* __restrict src,
                                                   char* __restrict dest,
                                                   const int dstStrideBytes,
                                                   int numSamples,
                                                   const bool littleEndian) noexcept
{
    // Convert to int32 first, then use upper three bytes
    const double maxVal = (double)0x7fffffff;

    if (littleEndian) {
        while (--numSamples >= 0) {
            littleEndian24BitToChars((int32_t)round(limit(-maxVal, maxVal, maxVal * *src++)), dest);
            dest += dstStrideBytes;
        }
    } else {
        while (--numSamples >= 0) {
            bigEndian24BitToChars((int32_t)round(limit(-maxVal, maxVal, maxVal * *src++)), dest);
            dest += dstStrideBytes;
        }
    }
}

void dminternal::SampleFormat::convertInt32ToFloat(const char* __restrict src,
                                                   float* __restrict dest,
                                                   const int srcStrideBytes,
                                                   int numSamples,
                                                   const bool littleEndian) noexcept
{
    const double g = 1.0 / 0x7fffffff;

    if (littleEndian) {
        while (--numSamples >= 0) {
            *dest++ = (float)(g * (int)littleEndianInt(src));
            src += srcStrideBytes;
        }
    } else {
        while (--numSamples >= 0) {
            *dest++ = (float)(g * (int)bigEndianInt(src));
            src += srcStrideBytes;
        }
    }
}

void dminternal::SampleFormat::convertFloatToInt32(const float* __restrict src,
                                                   char* __restrict dest,
                                                   const int dstStrideBytes,
                                                   int numSamples,
                                                   const bool littleEndian) noexcept
{
    const double maxVal = (double)0x7fffffff;

    if (littleEndian) {
        while (--numSamples >= 0) {
            *(int32_t*)dest = (int32_t)round(limit(-maxVal, maxVal, maxVal * *src++));
            dest += dstStrideBytes;
        }
    } else {
        while (--numSamples >= 0) {
            *(int32_t*)dest = swap((int32_t)round(limit(-maxVal, maxVal, maxVal * *src++)));
            dest += dstStrideBytes;
        }
    }
}
