#pragma once

#include <functional>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <utility>
#include <vector>

#include <daudio/active_channels.h>
#include <daudio/audio_device.h>
#include <daudio/callbacks.h>
#include <daudio/session.h>

namespace dminternal
{  // Forward declarations
    struct Device;
    struct LoopbackManager;
    struct LocalCallbackToken;
    struct LocalSession;
}  // namespace dminternal

namespace dminternal
{
    using Token = daudio::CallbackToken;

    std::vector<daudio::AudioDevice> allDevices();

    std::pair<std::shared_ptr<dminternal::Device>, std::shared_lock<std::shared_timed_mutex>>
    internalDevice(const daudio::AudioSession& session);

    //     std::pair<dminternal::Device*, std::shared_lock<std::shared_timed_mutex>> internalDevice(
    //         const daudio::AudioDevice& device);
    //
    std::pair<std::shared_ptr<dminternal::Device>, std::shared_lock<std::shared_timed_mutex>>
    internalDevice(device_id_t device_id);

    //     std::pair<dminternal::Device*, std::shared_lock<std::shared_timed_mutex>> internalDevice(
    //         const std::string& name);
    //
    //     std::pair<int, std::unique_lock<std::shared_timed_mutex>> idForInsert();
    //     void registerDevice(std::unique_ptr<dminternal::Device>,
    //                         std::unique_lock<std::shared_timed_mutex>);
    daudio::AudioDevice registerLoopbackDevice(int inputs, int outputs, float fs, int buffer_size);

    std::unique_ptr<daudio::CallbackToken> registerDevicesChangedCallback(
        std::function<void()> callback);

    void setTypeToList(daudio::AudioDeviceType type_to_list);

    void setListAllTypes();

    void setLoopBackEnabled(bool should_be_enabled);

    void release();
}  // namespace dminternal
