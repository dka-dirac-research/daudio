#pragma once

#include <cstdint>

#include <assert.h>

namespace dminternal
{
    struct SampleFormat {
        // Using ASIO types
        enum struct Type {
            kInt16MSB   = 0,
            kInt24MSB   = 1,  // used for 20 bits as well
            kInt32MSB   = 2,
            kFloat32MSB = 3,  // IEEE 754 32 bit float
            kFloat64MSB = 4,  // IEEE 754 64 bit double float

            // these are used for 32 bit data buffer, with different alignment of the data inside
            // 32 bit PCI bus systems can be more easily used with these
            kInt32MSB16 = 8,   // 32 bit data with 16 bit alignment
            kInt32MSB18 = 9,   // 32 bit data with 18 bit alignment
            kInt32MSB20 = 10,  // 32 bit data with 20 bit alignment
            kInt32MSB24 = 11,  // 32 bit data with 24 bit alignment

            kInt16LSB   = 16,
            kInt24LSB   = 17,  // used for 20 bits as well
            kInt32LSB   = 18,
            kFloat32LSB = 19,  // IEEE 754 32 bit float, as found on Intel x86 architecture
            kFloat64LSB = 20,  // IEEE 754 64 bit double float, as found on Intel x86 architecture

            kInt20LSB = 21,  // Special for 20 bit with 24 bit alignment (used with WASAPI)

            // these are used for 32 bit data buffer, with different alignment of the data inside
            // 32 bit PCI bus systems can more easily used with these
            kInt32LSB16 = 24,  // 32 bit data with 18 bit alignment
            kInt32LSB18 = 25,  // 32 bit data with 18 bit alignment
            kInt32LSB20 = 26,  // 32 bit data with 20 bit alignment
            kInt32LSB24 = 27,  // 32 bit data with 24 bit alignment

            //	ASIO DSD format.
            kDSDInt8LSB1 =
                32,  // DSD 1 bit data, 8 samples per byte. First sample in Least significant bit.
            kDSDInt8MSB1 =
                33,  // DSD 1 bit data, 8 samples per byte. First sample in Most significant bit.
            kDSDInt8NER8 = 40,  // DSD 8 bit data, 1 sample per byte. No Endianness required.

            kLastEntry
        };

        SampleFormat() noexcept {}
        SampleFormat(Type sample_type);

        void convertToFloat(const void* const src, float* const dst, const int samps) const
            noexcept;

        void convertToFloat(const void* const src,
                            float** const dst,
                            const int channels,
                            const int samps) const noexcept;

        void convertToFloatNonInterleaved(const void** const src,
                                          float** const dst,
                                          const int channels,
                                          const int samps) const noexcept;

        void convertFromFloat(const float* const src, void* const dst, const int samps) const
            noexcept;

        void convertFromFloat(const float** const src,
                              void* const dst,
                              const int channels,
                              const int samps) const noexcept;

        void convertFromFloatNonInterleaved(const float** const src,
                                            void** const dst,
                                            const int channels,
                                            const int samps) const noexcept;

        void clear(void* dst, const int numSamps) noexcept;

        int bitDepth;
        int byteStride;
        bool formatIsFloat;
        bool littleEndian;

    private:
        template <class ValueType>
        static inline ValueType swap(ValueType n)
        {
            if (sizeof(ValueType) == 2)
                return static_cast<ValueType>((n << 8) | (n >> 8));
            else if (sizeof(ValueType) == 4)
                return (n << 24) | (n >> 24) | ((n & 0xff00) << 8) | ((n & 0xff0000) >> 8);
        }

        static void convertInt16ToFloat(const char* src,
                                        float* dest,
                                        const int srcStrideBytes,
                                        int numSamples,
                                        const bool littleEndian) noexcept;

        static void convertFloatToInt16(const float* src,
                                        char* dest,
                                        const int dstStrideBytes,
                                        int numSamples,
                                        const bool littleEndian) noexcept;

        static inline int littleEndian24Bit(const void* const bytes)
        {
            return (((int)static_cast<const int8_t*>(bytes)[2]) << 16) |
                   (((int)static_cast<const uint8_t*>(bytes)[1]) << 8) |
                   ((int)static_cast<const uint8_t*>(bytes)[0]);
        }

        static inline int bigEndian24Bit(const void* const bytes)
        {
            return (((int)static_cast<const int8_t*>(bytes)[0]) << 16) |
                   (((int)static_cast<const uint8_t*>(bytes)[1]) << 8) |
                   ((int)static_cast<const uint8_t*>(bytes)[2]);
        }

        static inline void littleEndian24BitToChars(const int value, void* const destBytes)
        {
            static_cast<uint8_t*>(destBytes)[0] = (uint8_t)(value >> 8);
            static_cast<uint8_t*>(destBytes)[1] = (uint8_t)(value >> 16);
            static_cast<uint8_t*>(destBytes)[2] = (uint8_t)(value >> 24);
        }

        static inline void bigEndian24BitToChars(const int value, void* const destBytes)
        {
            static_cast<uint8_t*>(destBytes)[0] = (uint8_t)(value >> 24);
            static_cast<uint8_t*>(destBytes)[1] = (uint8_t)(value >> 16);
            static_cast<uint8_t*>(destBytes)[2] = (uint8_t)(value >> 8);
        }

        static inline uint32_t littleEndianInt(const void* const bytes)
        {
            return *static_cast<const uint32_t*>(bytes);
        }

        static inline uint32_t bigEndianInt(const void* const bytes)
        {
            return swap(*static_cast<const uint32_t*>(bytes));
        }

        static void convertInt24ToFloat(const char* src,
                                        float* dest,
                                        const int srcStrideBytes,
                                        int numSamples,
                                        const bool littleEndian) noexcept;

        static void convertFloatToInt24(const float* src,
                                        char* dest,
                                        const int dstStrideBytes,
                                        int numSamples,
                                        const bool littleEndian) noexcept;

        static void convertInt32ToFloat(const char* src,
                                        float* dest,
                                        const int srcStrideBytes,
                                        int numSamples,
                                        const bool littleEndian) noexcept;

        static void convertFloatToInt32(const float* src,
                                        char* dest,
                                        const int dstStrideBytes,
                                        int numSamples,
                                        const bool littleEndian) noexcept;
    };
}  // namespace dminternal
