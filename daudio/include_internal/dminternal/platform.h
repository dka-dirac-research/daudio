#pragma once

#include <cstdint>

namespace dminternal
{
    struct PlatformInitializer {
        PlatformInitializer();
        ~PlatformInitializer();
        uint64_t ps_init;
    };
}
