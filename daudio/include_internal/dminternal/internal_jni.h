#pragma once
#include <jni.h>

namespace dminternal
{
    void register_jni_vm(JavaVM* vm);
}
