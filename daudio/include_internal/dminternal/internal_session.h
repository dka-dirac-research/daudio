#pragma once

#include <dminternal/device.h>

#include <condition_variable>
#include <exception>
#include <memory>
#include <mutex>

namespace dminternal
{
    struct AudioSession : public daudio::AudioSession {
        explicit AudioSession(std::shared_ptr<dminternal::Device> device);
        ~AudioSession();

        void waitForTermination(int millisecs = -1);
        device_id_t getDeviceId() const;

    private:
        std::weak_ptr<Device> device_;
        std::mutex m_;
        std::exception_ptr error_;
        std::condition_variable cv_;
        std::unique_ptr<daudio::CallbackToken> token_;
    };
}  // namespace dminternal
