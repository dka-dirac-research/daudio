#pragma once

#include <memory>
#include <string>

#if defined(_MSC_VER)
#define USE_CXX17 _HAS_CXX17
#else
#if __cplusplus == 201703L
#define USE_CXX17 1
#endif
#endif

namespace utils
{
    std::string ltrim(const std::string& str, const std::string& chars = "\t\n\v\f\r ");
    std::string rtrim(const std::string& str, const std::string& chars = "\t\n\v\f\r ");
    std::string trim(const std::string& str, const std::string& chars = "\t\n\v\f\r ");

// weak_from_this() requires C++17
#if USE_CXX17
    template <class T>
    using enable_shared_from_this = std::enable_shared_from_this<T>;
#else
    template <class T>
    class enable_shared_from_this : public std::enable_shared_from_this<T>
    {
    public:
        typedef std::enable_shared_from_this<T> this_type;
        inline std::weak_ptr<T> weak_from_this()
        {
            return std::weak_ptr<T>(this_type::shared_from_this());
        }
    };
#endif
}  // namespace utils
