#pragma once

#include <condition_variable>
#include <functional>
#include <mutex>
#include <queue>
#include <thread>

namespace dminternal
{
    using Message      = std::function<void()>;
    using MessageQueue = std::queue<Message>;

    struct Active {
        Active();
        ~Active();

        void send(Message m);
        void wait();

    private:
        bool done = false;

        std::mutex mq_lock;
        std::condition_variable mq_cv;
        MessageQueue mq;

        std::thread thd;
    };
}  // namespace dminternal
