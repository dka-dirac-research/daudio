#pragma once

#include <atomic>
#include <chrono>
#include <map>
#include <mutex>
#include <thread>

#include <daudio/audio_device.h>
#include <daudio/callbacks.h>
#include <daudio/session.h>
#include <daudio/waveform.h>

#include "device.h"

namespace dminternal
{
    struct Loopback : Device {
        Loopback(int index, int num_inputs, int num_outputs, float fs, int buffer_size);

        Loopback(const Loopback&) = delete;
        Loopback(Loopback&&)      = default;
        Loopback& operator=(Loopback&) = delete;
        Loopback& operator=(Loopback&&) = default;
        ~Loopback();

        float defaultSampleRate() const override;
        int defaultBuffersize() const override;
        std::vector<float> supportedSamplerates() const override;
        std::vector<int> supportedBuffersizes() const override;
        std::vector<std::string> inputChannelNames() const override;
        std::vector<std::string> outputChannelNames() const override;

        void open(float fs,
                  int bufferSize,
                  daudio::ActiveChannels inputMask,
                  daudio::ActiveChannels outputMask) override;
        void close() override;
        bool isOpened() const override;

        float currentSampleRate() const override;
        int currentBuffersize() const override;
        const daudio::ActiveChannels& activeInputChannels() const override;
        const daudio::ActiveChannels& activeOutputChannels() const override;

        void setTransferFunc(daudio::AudioCallback) override;

    private:
        void process();

        const int buffer_size;
        const float fs;
        const std::chrono::nanoseconds period;
        daudio::Waveforms in;
        daudio::Waveforms out;
        daudio::AudioCallback h;

        daudio::ActiveChannels activeInChannels;
        daudio::ActiveChannels activeOutChannels;

        std::atomic<bool> stop_work_thread{false};
        std::thread work_thread;

        std::atomic<bool> opened{false};
        std::mutex lock;
    };

    daudio::AudioDevice createNewLoopbackDevice(DeviceType&,
                                                int inputs,
                                                int outputs,
                                                float fs,
                                                int buffer_size);
}  // namespace dminternal
