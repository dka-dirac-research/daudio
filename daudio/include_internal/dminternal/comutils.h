#pragma once

namespace dminternal
{
    template <class ComClass>
    struct ComSmartPtr {
        ComSmartPtr() throw() : p_(nullptr) {}
        ComSmartPtr(ComClass* const p) : p_(p)
        {
            if (p_)
                p_->AddRef();
        }
        ComSmartPtr(const ComSmartPtr<ComClass>& other) : p_(other.p_)
        {
            if (p_)
                p_->AddRef();
        }
        ~ComSmartPtr() { release(); }

        operator ComClass*() const throw() { return p_; }
        ComClass& operator*() const throw() { return *p_; }
        ComClass* operator->() const throw() { return p_; }

        ComSmartPtr& operator=(ComClass* const p)
        {
            if (p != nullptr)
                p->AddRef();
            release();
            p_ = p;
            return *this;
        }

        ComSmartPtr& operator=(const ComSmartPtr<ComClass>& p) { return operator=(p.p_); }

        ComClass** operator&() throw()
        {
            release();
            return &p_;
        }

    private:
        ComClass* p_;

        void release()
        {
            if (p_ != nullptr) {
                p_->Release();
                p_ = nullptr;
            }
        }
    };
}