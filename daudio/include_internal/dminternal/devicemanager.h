#pragma once

#include <atomic>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <daudio/audio_device.h>
#include <daudio/callbacks.h>

#include <memory>
#include <set>

#include "dminternal/platform.h"
#include "dminternal/threadutil.h"
#include "dminternal/utils.h"

namespace dminternal
{  // Forward declarations.
    struct Device;
    struct DeviceType;
}  // namespace dminternal

namespace dminternal
{
    struct DeviceManager : utils::enable_shared_from_this<DeviceManager> {
        DeviceManager();
        DeviceManager(const DeviceManager&) = delete;
        DeviceManager(DeviceManager&&)      = delete;
        DeviceManager& operator=(DeviceManager&) = delete;
        DeviceManager& operator=(DeviceManager&&) = delete;

        ~DeviceManager();

        void init();
        void uninitialize();
        std::vector<daudio::AudioDevice> allDevices();

        std::shared_ptr<Device> find(device_id_t id);

        daudio::AudioDevice registerLoopbackDevice(int inputs,
                                                   int outputs,
                                                   float fs,
                                                   int buffer_size);

        std::unique_ptr<daudio::CallbackToken> registerDevicesChangedCallback(
            std::function<void()> callback);

        /** As a default, the device manager will look for and list all types of
         *  devices it can find. It is also possible to restrict it to one specific type of devices.
         *  This sets the type of device that the manager should look for */
        void setTypeToList(daudio::AudioDeviceType type_to_list);
        /** Sets the device manager to list all different types of devices */
        void setListAllTypes();

        void setLoopBackEnabled(bool should_be_enabled);

    private:
        bool initialized_ = false;
        std::map<daudio::AudioDeviceType, std::unique_ptr<dminternal::DeviceType>> device_types_;

        bool list_all_types_                  = true;
        bool loopback_enabled_                = false;
        daudio::AudioDeviceType type_to_list_ = daudio::AudioDeviceType::unknown;

        std::map<size_t, std::function<void()>> change_callbacks_;
        std::mutex callback_lock_;

        std::unique_ptr<dminternal::TimerThread> change_thread_;
        std::atomic<int> pending_updates_;
    };
}  // namespace dminternal
