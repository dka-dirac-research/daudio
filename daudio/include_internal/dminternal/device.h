#pragma once

#include <daudio/active_channels.h>
#include <daudio/audio_device.h>
#include <daudio/callbacks.h>
#include <daudio/session.h>
#include <dminternal/utils.h>

#include <atomic>
#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <stdexcept>
#include <string>

namespace dminternal
{
    struct Device;

    struct DeviceType {
        virtual ~DeviceType() = default;

        virtual std::vector<daudio::AudioDevice> devices()     = 0;
        virtual std::shared_ptr<Device> getDevice(device_id_t) = 0;

        template <daudio::AudioDeviceType>
        static std::unique_ptr<DeviceType> create(std::function<void()>);
    };

    struct Device : utils::enable_shared_from_this<Device> {
        Device(const daudio::AudioDevice&);
        virtual ~Device() = default;

        virtual float defaultSampleRate() const                     = 0;
        virtual int defaultBuffersize() const                       = 0;
        virtual std::vector<float> supportedSamplerates() const     = 0;
        virtual std::vector<int> supportedBuffersizes() const       = 0;
        virtual std::vector<std::string> inputChannelNames() const  = 0;
        virtual std::vector<std::string> outputChannelNames() const = 0;

        virtual void open(float fs,
                          int bufferSize,
                          daudio::ActiveChannels inputMask,
                          daudio::ActiveChannels outputMask) = 0;
        virtual void close()                                 = 0;
        virtual bool isOpened() const                        = 0;

        virtual bool hasInputGainControl() const;
        virtual float getInputGain() const;
        virtual void setInputGain(float gain);
        virtual bool getInputMuted() const;
        virtual void setInputMuted(bool mute);

        virtual bool hasOutputVolumeControl() const;
        virtual float getOutputVolume() const;
        virtual void setOutputVolume(float volume);
        virtual bool getOutputMuted() const;
        virtual void setOutputMuted(bool mute);

        /* The following functions are guaranteed to be valid once AudioDevice::open has returned
         * (without exceptions thrown)
         */
        virtual float currentSampleRate() const                            = 0;
        virtual int currentBuffersize() const                              = 0;
        virtual const daudio::ActiveChannels& activeInputChannels() const  = 0;
        virtual const daudio::ActiveChannels& activeOutputChannels() const = 0;

        /* Following functions are base implementations for all device types */
        std::unique_ptr<daudio::CallbackToken> addCallback(daudio::AudioCallback callback);
        std::unique_ptr<daudio::CallbackToken> addReadOnlyCallback(
            daudio::AudioCallbackRO callback);
        std::unique_ptr<daudio::AudioSession> addSession();
        std::unique_ptr<daudio::AudioSession> addSession(float);
        std::unique_ptr<daudio::AudioSession> addSession(float, int);
        std::unique_ptr<daudio::AudioSession> addSession(float,
                                                         int,
                                                         daudio::ActiveChannels,
                                                         daudio::ActiveChannels);
        void removeSession();
        bool hasOpenSessions();
        const std::map<std::string, std::string>& getKeyValueMap() const;

        using notification_callback_type =
            std::function<void(float level, bool muted, bool internal_change)>;

        std::unique_ptr<daudio::CallbackToken> addSessionTerminatedCallback(
            std::function<void(std::exception_ptr)>);
        std::unique_ptr<daudio::CallbackToken> addInputGainChangeCallback(
            notification_callback_type);
        std::unique_ptr<daudio::CallbackToken> addOutputVolumeChangeCallback(
            notification_callback_type);
        virtual void setTransferFunc(daudio::AudioCallback);

        const daudio::AudioDevice& externalDevice() const { return device; };
        device_id_t id() const;

        void setDefault(bool is_default) { device.is_default = is_default; }

        static std::vector<float> possibleSampleRates();
        static std::vector<int> possibleBufferSizes(int min_size, int max_size);

    protected:
        void callCallbacks(daudio::InputBufferView&, daudio::OutputBufferView&);

        void inputGainChanged(float value, bool muted, bool internal_change);

        void outputVolumeChanged(float value, bool muted, bool internal_change);

        void sessionTerminated(std::exception_ptr);

        mutable daudio::AudioDevice device;

        std::map<std::string, std::string> keyValueMap;

    private:
        std::map<size_t, daudio::AudioCallback> callbacks;
        std::map<size_t, daudio::AudioCallbackRO> readonly_callbacks;
        std::map<size_t, std::function<void(std::exception_ptr)>> terminated_callbacks;
        std::map<size_t, notification_callback_type> input_gain_callbacks;
        std::map<size_t, notification_callback_type> output_volume_callbacks;
        std::atomic<int> num_sessions;
        std::mutex add_lock;

        friend struct CallbackToken;
    };
}  // namespace dminternal
