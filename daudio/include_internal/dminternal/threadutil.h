#pragma once

#include <atomic>
#include <chrono>
#include <functional>
#include <thread>

namespace dminternal
{
    struct TimerThread {
        TimerThread(std::function<void()> fn_to_run,
                    int interval_ms,
                    int time_to_first_callback = -1)
        {
            std::thread t([this, interval_ms, fn_to_run, time_to_first_callback] {
                const auto period = std::chrono::milliseconds(interval_ms);
                auto t_next =
                    std::chrono::system_clock::now() +
                    (time_to_first_callback >= 0 ? std::chrono::milliseconds(time_to_first_callback)
                                                 : period);
                while (!stop_) {
                    auto t_now = std::chrono::system_clock::now();
                    if (t_now >= t_next) {
                        t_next = t_now + period;
                        fn_to_run();
                    }
                    std::this_thread::sleep_for(std::chrono::milliseconds(50));
                }
            });
            thread_.swap(t);
        }
        ~TimerThread()
        {
            if (thread_.joinable()) {
                stop();
                thread_.join();
            }
        }
        void stop() { stop_ = true; }

    private:
        std::thread thread_;
        std::atomic<bool> stop_{false};
    };

    struct AudioThread {
        using thread_function_type = std::function<void(std::atomic<bool>&, std::atomic<bool>&)>;
        thread_function_type fn_;

        std::atomic<bool> stop_thread_{false};
        std::atomic<bool> thread_started_{false};
        std::thread thread_;
        AudioThread(thread_function_type fn) : fn_(fn)
        {
            std::thread t([&] {
                setSchedulerHighPrio();
                fn_(thread_started_, stop_thread_);
            });
            thread_.swap(t);
            while (!thread_started_)
                std::this_thread::yield();
        }
        ~AudioThread()
        {
            if (thread_.joinable()) {
                stop_thread_ = true;
                thread_.join();
            }
        }

        static void setSchedulerHighPrio();
    };
}  // namespace dminternal
