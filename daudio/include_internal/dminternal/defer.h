#pragma once

#ifdef defer
#error defer already defined ?
#else
namespace dminternal
{
    template <class F>
    struct deferrer {
        F f;
        ~deferrer() noexcept(false) { f(); }
    };
    struct defer_dummy {
    };
    template <class F>
    deferrer<F> operator*(defer_dummy, F f)
    {
        return {f};
    }
}  // namespace dminternal

#define DFRCAT_(LINE) _defer##LINE
#define DFRCAT(LINE) DFRCAT_(LINE)
#define defer auto DFRCAT(__LINE__) = dminternal::defer_dummy{}* [&]() -> void
#endif
