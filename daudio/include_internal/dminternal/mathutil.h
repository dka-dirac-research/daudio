#pragma once

namespace dminternal
{
    template <typename ValueType>
    ValueType next_power_of_2(ValueType v)
    {
        static_assert(std::is_integral<ValueType>::value, "non-integer type");
        static_assert(std::is_unsigned<ValueType>::value, "not unsigned type");
        v--;
        for (unsigned b_shift = 1; b_shift < sizeof(ValueType) * 8; b_shift <<= 1)
            v |= v >> b_shift;
        v++;
        return v;
    }
}
