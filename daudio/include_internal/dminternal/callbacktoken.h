#pragma once

#include <daudio/callbacks.h>
#include <functional>

namespace dminternal
{
    template <class CallbackContainer, class CallbackType>
    std::size_t add_callback(CallbackContainer& container, CallbackType callback)
    {
        std::size_t cb_id = container.empty() ? 1 : container.rbegin()->first + 1;
        container[cb_id]  = callback;
        return cb_id;
    }

    struct CallbackToken : daudio::CallbackToken {
        using function_type = std::function<void(void)>;
        function_type fn_;
        CallbackToken(function_type fn);
        ~CallbackToken();
    };
}  // namespace dminternal
