#pragma once

#include <vector>

#include <daudio/audio_buffer.h>

namespace daudio
{
    struct Waveform {
        std::vector<float> x;
        float fs;
    };

    struct Waveforms {
        std::vector<std::vector<float>> waveforms;
        float fs;

        Waveforms() = default;
        Waveforms(std::vector<std::vector<float>> waveforms, float fs)
            : waveforms(waveforms), fs{fs}
        {
        }

        Waveforms(int channelCount, int bufferSize, float fs)
            : waveforms(channelCount, std::vector<float>(bufferSize, 0.f)), fs{fs}
        {
        }

        void resize(int channelCount, int bufferSize)
        {
            waveforms.resize(channelCount);
            for (auto& w : waveforms)
                w.resize(bufferSize);
        }

        std::size_t waveformSize() const
        {
            if (!waveforms.size())
                return 0u;

            return waveforms[0].size();
        }

        InputBufferView view(int start_offset = 0)
        {
            InputBufferView view;

            const auto size = int(waveformSize());

            if (start_offset > size)
                start_offset = size;

            if (start_offset < 0)
                start_offset = 0;

            for (const auto& w : waveforms)
                view.channel.emplace_back(w.data() + start_offset, &w.back() + 1);

            return view;
        }
    };
}  // namespace daudio
