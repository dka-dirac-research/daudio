#pragma once

#include <daudio/audio_buffer.h>
#include <functional>

namespace daudio
{
    struct CallbackToken {
        virtual ~CallbackToken()            = default;
        CallbackToken(const CallbackToken&) = delete;
        CallbackToken& operator=(const CallbackToken&) = delete;

    protected:
        CallbackToken() = default;
    };

    using AudioCallback =
        std::function<void(const daudio::InputBufferView&, daudio::OutputBufferView&)>;

    using AudioCallbackRO = std::function<void(const daudio::InputBufferView&)>;
}  // namespace daudio
