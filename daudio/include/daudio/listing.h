#pragma once

#include <daudio/audio_device.h>
#include <daudio/callbacks.h>

#include <functional>
#include <memory>
#include <vector>

namespace daudio
{
    /**
     * List all available input devices.
     *
     * It is important to note that the available devices may change
     * during execution in case a user plugs in or unplugs a device.
     *
     * @return a vector of input audio devices available at the moment.
     */
    std::vector<AudioDevice> inputDevices();

    /**
     * List all available output devices.
     *
     * It is important to note that the available devices may change
     * during execution in case a user plugs in or unplugs a device.
     *
     * @return a vector of output audio devices available at the moment.
     */
    std::vector<AudioDevice> outputDevices();

    /**
     * List all available devices that (could have) synchronous input/output.
     *
     * It is important to note that the available devices may change
     * during execution in case a user plugs in or unplugs a device.
     *
     * @return a vector of audio devices with both inputs and outputs.
     */
    std::vector<AudioDevice> synchronousDevices();

    /**
     * List all available audio devices (i.e. input + output +
     * synchronous devices).
     *
     * It is important to note that the available devices may change
     * during execution in case a user plugs in or unplugs a device.
     *
     * @return a vector of audio devices available at the moment.
     */
    std::vector<AudioDevice> allDevices();

    /**
     * Register a callback that is notified when device list changes.
     *
     * @param callback  the callback
     * @return a callback token that will automatically unregister
     *         the callback when destroyed.
     */
    std::unique_ptr<CallbackToken> registerDevicesChangedCallback(std::function<void()> callback);

    /** As a default, the device manager will look for and list all types of
     *  devices it can find. It is also possible to restrict it to one specific type of devices.
     *  This sets the type of device that the manager should look for.
     *
     *   @param type_to_list The type of devices that should be listed by the device manager */
    void setTypeToList(AudioDeviceType type_to_list);

    /** Sets the device manager to list all different types of devices */
    void setListAllTypes();

    /** Sets loopback devices enabled or disabled. Per default, loopback devices will not show
     *  up in the listing */
    void setLoopBackEnabled(bool should_be_enabled);

    /**
     * Explicit release for all devicemanager state.
     *
     * This function will unconditionally clear all internal datastructures
     * for the devicemanager, including all open sessions and all loopbacks.
     *
     * Calling this function is normally NOT NEEDED, but can be necessary as
     * a workaround for programs that clobber threads without letting them
     * clean up and call destructors.
     *
     * Calling this function while a callback is registered is NOT
     * recommended.
     */
    void release();
}  // namespace daudio
