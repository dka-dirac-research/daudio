#pragma once

#include <memory>

#include <daudio/active_channels.h>
#include <daudio/audio_device.h>
#include <daudio/callbacks.h>

namespace daudio
{
    /** AudioSession token
     */
    struct AudioSession {
        virtual ~AudioSession()           = default;
        AudioSession(const AudioSession&) = delete;
        AudioSession& operator=(const AudioSession&) = delete;

    protected:
        AudioSession() = default;
    };

    /**
     * Attempt to open an audio device.
     *
     * Add a new session to an already open audio device, or open up an audio device using the
     * preferred samplerate, buffer size and channels.
     *
     * @param device    an audio device with a possibly ongoing session.
     *
     * @return a session object if successful, otherwise an exception will be thrown.
     */
    std::unique_ptr<AudioSession> open(const AudioDevice& device);

    /**
     * Attempt to open a session for an audio device with specified sample rate, preferred
     * buffer size and channels.
     *
     * If a session using incompatible settings is already underway, then this
     * function will fail and throw an exception.
     *
     * @param device  the audio device to open.
     * @param fs      the desired samplerate to use.
     *
     * @return a session object if successful, otherwise an exception will be thrown.
     */
    std::unique_ptr<AudioSession> open(const AudioDevice& device, float fs);

    /**
     * Attempt to open a session for an audio device with specified sample rate and
     * buffer size and preferred channels.
     *
     * If a session using incompatible settings is already underway, then this
     * function will fail and throw an exception.
     *
     * @param device     the audio device to open.
     * @param fs         the desired samplerate to use.
     * @param bufferSize the desired buffer size to use.
     *
     * @return a session object if successful, otherwise an exception will be thrown.
     */
    std::unique_ptr<AudioSession> open(const AudioDevice& device, float fs, int bufferSize);

    /**
     * Attempt to open a session for an audio device.
     *
     * If a session using incompatible settings is already underway, then this
     * function will fail and throw an exception.
     *
     * @param device     the audio device to open.
     * @param fs         the desired samplerate to use.
     * @param bufferSize the desired buffer size to use.
     * @param inputMask  the input channels to use.
     * @param outputMask the output channels to use.
     *
     * @return a session object if successful, otherwise an exception will be thrown.
     */
    std::unique_ptr<AudioSession> open(const AudioDevice& device,
                                       float fs,
                                       int bufferSize,
                                       ActiveChannels inputMask,
                                       ActiveChannels outputMask);

    /**
     * Adds a callback to an existing audio session.
     *
     * When the session owning this callback goes out of scope it will
     * be removed automatically.
     *
     * @param session  the session to add a callback to.
     * @param callback the function to call for each block of data.
     * @return a callback token, that will keep the callback alive for as long as it is in scope.
     */
    std::unique_ptr<CallbackToken> registerCallback(AudioSession& session, AudioCallback callback);
    /**
     * Adds a read-only callback to an existing audio session. This will only be able to listen
     * on the audio stream, not modify its content
     *
     * When the session owning this callback goes out of scope it will
     * be removed automatically.
     *
     * @param session  the session to add a callback to.
     * @param readonly_callback the function to call for each block of data.
     * @return a callback token, that will keep the callback alive for as long as it is in scope.
     */
    std::unique_ptr<CallbackToken> registerCallback(AudioSession& session,
                                                    AudioCallbackRO readonly_callback);

    /**
     * Wait for session termination.
     *
     * Waits until session is terminated (by f.i. unplugging the device used by the session, or any
     * other error) upon which an exception will be thrown.
     *
     * @param session    the session to wait upon
     * @param timeout_ms time in milliseconds to wait, if < 0 time is indefinite
     */
    void waitForTermination(AudioSession& session, const int timeout_ms = -1);

    /**
     * Returns masks for a device with all channels active.
     *
     * @param device the device to create channel masks for.
     * @return a pair <inputmask, outputmask> that masks no channels.
     */
    std::pair<ActiveChannels, ActiveChannels> allChannels(const AudioDevice& device);
}  // namespace daudio
