#pragma once

#include <functional>
#include <memory>

#include <daudio/audio_device.h>
#include <daudio/callbacks.h>

namespace daudio
{
    using notification_callback_type =
        std::function<void(float level, bool muted, bool internal_change)>;
    /**
     * hasInputGainControl - Checks whether device has any input gain control.
     *
     * @param device    The device
     * @returns bool
     */
    bool hasInputGainControl(const AudioDevice& device);

    /**
     * getInputGain - Get input gain for device
     *
     * @param device    The device
     * @returns float gain ranged [0.0, 1.0]
     */
    float getInputGain(const AudioDevice& device);

    /**
     * setInputGain - Set input gain value for device
     *
     * @param device    The device
     * @param value     Value ranged [0.0, 1.0]
     * @returns void
     */
    void setInputGain(const AudioDevice& device, float value);

    /**
     * getInputMuted - Get input muted state for device
     *
     * @param device    The device
     * @returns bool
     */
    bool getInputMuted(const AudioDevice& device);

    /**
     * setInputMuted - Set input muted state for device
     *
     * @param device    The device
     * @param mute      Mute state
     * @returns void
     */
    void setInputMuted(const AudioDevice& device, bool mute);

    /**
     * registerInputGainChangeCallback - Register a function to retrieve
     * changes in input gain, should it be controlled from elsewhere.
     *
     * @param device    The device
     * @param callback  Callback function
     * @returns std::unique_ptr<CallbackToken>
     */
    std::unique_ptr<CallbackToken> registerInputGainChangeCallback(
        const AudioDevice& device,
        notification_callback_type callback);

    /**
     * hasOutputVolumeControl - Checks whether device has any output volume control.
     *
     * @param device    The device
     * @returns bool
     */
    bool hasOutputVolumeControl(const AudioDevice& device);

    /**
     * getOutputVolume - Get output volume for device
     *
     * @param device    The device
     * @returns float volume ranged [0.0, 1.0]
     */
    float getOutputVolume(const AudioDevice& device);

    /**
     * setOutputVolume - Set output volume vale for device
     *
     * @param device    The device
     * @param value     Value ranged [0.0, 1.0]
     * @returns void
     */
    void setOutputVolume(const AudioDevice& device, float value);

    /**
     * getOutputMuted - Get output muted state
     *
     * @param device    The device
     * @returns bool
     */
    bool getOutputMuted(const AudioDevice& device);

    /**
     * setOutputMuted - Set output muted state
     *
     * @param device    The device
     * @param mute      Mute state
     * @returns void
     */
    void setOutputMuted(const AudioDevice& device, bool mute);

    /**
     * registerOutputVolumeChangeCallback - Register a function to retrieve
     * changes in output volume, should it be controlled from elsewhere.
     *
     * @param device    The device
     * @param callback  Callback function
     * @returns std::unique_ptr<CallbackToken>
     */
    std::unique_ptr<CallbackToken> registerOutputVolumeChangeCallback(
        const AudioDevice& device,
        notification_callback_type callback);
}  // namespace daudio
