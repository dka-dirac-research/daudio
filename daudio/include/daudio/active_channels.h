#pragma once

#include <algorithm>
#include <initializer_list>
#include <vector>

namespace daudio
{
    struct ActiveChannels {
        ActiveChannels(size_t n = 0) : channels(n, true) {}
        ActiveChannels(std::initializer_list<bool> ch, int marked_ch)
            : channels{ch}, markedChannel{marked_ch}
        {
        }
        std::vector<bool> channels;
        int markedChannel{-1};
        operator size_t() const { return std::count(channels.begin(), channels.end(), true); }
    };
}  // namespace daudio
