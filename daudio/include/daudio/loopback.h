#pragma once

#include <daudio/audio_device.h>
#include <daudio/callbacks.h>

namespace daudio
{
    /**
     * Register a new loopback device.
     *
     * Adds a new custom loopback in addition to the default ones.
     *
     * @param inputCount the number of inputs in the loopback.
     * @param outputCount the number of outputs in the loopback.
     * @param fs the loopback samplerate.
     * @param bufferSize the buffer size the loopback should use.
     */
    daudio::AudioDevice registerLoopback(int inputCount,
                                         int outputCount,
                                         float fs       = 48000.f,
                                         int bufferSize = 512);

    /**
     * Set the transfer function of a loopback device.
     *
     * The intended purpose of this functionality is mainly diagnostics
     * and testing.
     *
     * It is an error to supply a transfer function with mismatched dimensions.
     *
     * @param device the loopback device to set a transfer function for.
     * @param h the new transfer function, nullptr implies a standard loopback.
     */
    void setTransferFunc(const daudio::AudioDevice& device, daudio::AudioCallback h = nullptr);
}  // namespace daudio
