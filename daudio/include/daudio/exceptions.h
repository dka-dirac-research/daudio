#pragma once

#include <stdexcept>

namespace daudio
{
    // Thrown when access is denied opening a device
    struct AccessDenied {
    };
}  // namespace daudio
