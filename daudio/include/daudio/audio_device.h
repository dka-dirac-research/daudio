#pragma once

#include <string>
#include <vector>

using device_id_t = size_t;

namespace daudio
{
    enum struct AudioDeviceType { alsa, asio, coreaudio, opensles, oboe, wasapi, loopback, unknown };

    struct AudioDevice {
        std::string name     = "";
        AudioDeviceType type = AudioDeviceType::unknown;
        int inputchannels    = 0;
        int outputchannels   = 0;
        bool is_default      = false;
        device_id_t id       = 0;

        AudioDevice() = default;
        AudioDevice(const std::string&,
                    AudioDeviceType,
                    int inputchannels,
                    int outputchannels,
                    bool is_default);

        std::string typeName() const;
        bool operator==(const AudioDevice&) const;
        bool operator!=(const AudioDevice&) const;
        operator bool() const { return (*this) != none; }

        static const AudioDevice none;
        static AudioDeviceType typeFromName(std::string name);
        static std::string nameFromType(AudioDeviceType type);
        static AudioDeviceType defaultForPlatform();
    };

    using AudioDevices = std::vector<AudioDevice>;
}  // namespace daudio
