#pragma once

#include <vector>

namespace daudio
{
    template <typename T>
    struct Range {
        Range(T* begin, T* end) : b{begin}, e{end} {};
        T* begin() { return b; };
        T* end() { return e; };
        const T* begin() const { return b; };
        const T* end() const { return e; };
        const T& operator[](int index) const { return *(b + index); }
        T& operator[](int index) { return *(b + index); }

    private:
        T* b;
        T* e;
    };

    template <typename T>
    struct AudioBufferView {
        bool is_silent{true};
        std::vector<Range<T>> channel;
        std::size_t bufferSize() const
        {
            if (!channel.size())
                return 0u;

            return channel[0].end() - channel[0].begin();
        }
        const T** get_pointers(void* p, unsigned offset) const
        {
            return const_cast<AudioBufferView<T>*>(this)->get_pointers(p, offset);
        }
        T** get_pointers(void* p, unsigned offset)
        {
            T** ptrs = static_cast<T**>(p);
            for (unsigned i = 0; i < channel.size(); ++i)
                ptrs[i] = channel[i].begin() + offset;
            return ptrs;
        }
    };

    using InputBufferView  = daudio::AudioBufferView<const float>;
    using OutputBufferView = daudio::AudioBufferView<float>;
}  // namespace daudio

#define view_alloca(view, offset) \
    view.get_pointers(alloca(view.channel.size() * sizeof(float*)), offset)
