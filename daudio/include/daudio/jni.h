#pragma once
#include <jni.h>

namespace daudio
{
    /**
     * Register JavaVM* for daudio on android. Required to get information about the available
     * devices on the unit.
     *
     * @param vm The JavaVM for the Android process.
     *
     */
    void register_jni_vm(JavaVM* vm);
}  // namespace daudio
