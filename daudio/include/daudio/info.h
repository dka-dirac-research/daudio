#pragma once

#include <daudio/active_channels.h>
#include <daudio/audio_device.h>
#include <daudio/session.h>

#include <map>
#include <string>
#include <vector>

namespace daudio
{
    /**
     * Retrieve the current samplerate for a session.
     *
     * @param session the session to query for a samplerate.
     *
     * @return the session samplerate in Hz.
     */
    float currentSamplerate(const AudioSession& session);

    /**
     * Retrieve the current buffersize for a session.
     *
     * @param session the session to query for a buffersize.
     *
     * @return the buffersize in frames.
     */
    int currentBuffersize(const AudioSession& session);

    /**
     * Retrieve the active input channels for a session.
     *
     * @param session the session to query for input channels.
     *
     * @returns ActiveChannels
     */
    ActiveChannels currentInputChannels(const AudioSession& session);

    /**
     * Retrieve the active output channels for a session.
     *
     * @param session the session to query for output channels.
     *
     * @returns ActiveChannels
     */
    ActiveChannels currentOutputChannels(const AudioSession& session);

    /**
     * List samplerates supported by a device.
     *
     * This function can not be called on a loopback device.
     *
     * @param device the device to find the samplerate for.
     * @return a vector of the supported samplerates in Hz.
     */
    std::vector<float> supportedSamplerates(const AudioDevice& device);

    /**
     * Retrieve the default samplerate for a device.
     *
     * @param device The device to find the default sample rate for.
     *
     * @return the sample rate in hertz.
     */
    float defaultSamplerate(const AudioDevice& device);

    /**
     * List buffer sizes (in frames) supported by a device.
     *
     * This function can not be called on a loopback device.
     *
     * @param device The device to find the buffer size for.
     * @return a vector of the supported buffer sizes.
     */
    std::vector<int> supportedBufferSizes(const AudioDevice& device);

    /**
     * Retrieve the default buffersize for a device.
     *
     * @param device The device to find the default buffer size for.
     *
     * @return the buffersize in frames.
     */
    int defaultBuffersize(const AudioDevice& device);

    /**
     * Retrieve the underlying device for a session.
     *
     * If this function is called with a loopback session as argument it will
     * return an AudioDevice, but this device is just a description, it can not
     * be used as an input to any of the devicemanager functions that take an
     * AudioDevice as argument.
     *
     * @param session the session to find the underlying device for.
     * @return the audio device behind a session.
     */
    AudioDevice deviceForSession(const AudioSession& session);

    /**
     * List the input channel names for a device.
     *
     * This function can not be called on a loopback device.
     *
     * @param device the device to get the input channel names for.
     * @return the input channel names for device.
     */
    std::vector<std::string> inputChannelNames(const AudioDevice& device);

    /**
     * List the output channel names for a device.
     *
     * This function can not be called on a loopback device.
     *
     * @param device the device to get the output channel names for.
     * @return the output channel names for device.
     */
    std::vector<std::string> outputChannelNames(const AudioDevice& device);

    /**
     * Checks whether there is an open session for a device.
     *
     * This function can not be called on a loopback device.
     *
     * @param device the device to check for a session.
     * @return true if there is an open session, otherwise false.
     */
    bool isOpen(const AudioDevice& device);

    /**
     * Get key/value pair map. Might contain platform specific values
     *
     * @param device the device to get key/value map from.
     * @returns key/value map
     */
    std::map<std::string, std::string> getKeyValuePairs(const AudioDevice& device);
}  // namespace daudio
